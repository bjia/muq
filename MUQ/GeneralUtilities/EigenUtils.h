//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * ArmadilloUtils.h
 *
 *  Created on: Jan 19, 2011
 *      Author: prconrad
 */

#ifndef EIGENUTILS_H_
#define EIGENUTILS_H_

#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include <Eigen/Core>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/string.hpp>



/**
 * This directory includes some helpful utilities for the eigen toolkit,
 * especially regarding predicates for testing.
 */

namespace Eigen{
 
  ///Define some unsigned types
  typedef Matrix<unsigned int, Dynamic, Dynamic> MatrixXu;
  typedef Matrix<unsigned int, Dynamic, 1> VectorXu;
  typedef Matrix<unsigned int, 1, Dynamic> RowVectorXu;
  
}


namespace muq{
  namespace GeneralUtilities{
    
/**
 * Find non zero entries in a column vector
 **/
Eigen::VectorXu FindNonZeroInVector(Eigen::VectorXd const& input);

template<typename derived>
void PrintMatrixToFile(Eigen::MatrixBase<derived> const& input, std::string const& filename)
{
 
  std::ofstream outFile (filename.c_str());
  
  assert(outFile.good());
  
  outFile.precision(15);
  outFile << input;
  outFile.close();
  
}

template<typename derivedA, typename derivedB>
bool MatrixEqual(Eigen::MatrixBase<derivedA> const& a, Eigen::MatrixBase<derivedB> const& b)
{
 //check rows and cols equal
  if(a.cols() != b.cols()|| a.rows() != b.rows())
  {
    return false;
  }

  //return true if they're all the same
  return (a.array() == b.array()).all();
}
  
template<typename derivedA, typename derivedB>
bool MatrixApproxEqual(Eigen::MatrixBase<derivedA> const& a, Eigen::MatrixBase<derivedB> const& b, double const tol)
{
  if(a.cols() != b.cols()|| a.rows() != b.rows()) //check the length first
  {
    return false;
  }

  //return true if all the differences are less than the tol
	
  return ((a-b).array().abs() < tol).all();
}

    ///Apply a function to the columns of the input matrix. Assumes the input dimensionality has been checked.
    Eigen::MatrixXd ApplyVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn, unsigned const outputDim, Eigen::MatrixXd const& inputs);
    
    ///Call an external executable, passing files through /tmp/.
    Eigen::MatrixXd ApplyExecutableFunction(std::string const& executable, unsigned const outputDim, Eigen::MatrixXd const& inputs);

  }
}


///This block makes Mat, Row, and Col types serializable using the built-in binary format.
namespace boost {
namespace serialization {


  // Copy for MatrixXd  *************************************
template<class Archive>
void save(Archive & ar, Eigen::MatrixXd const& mat, const unsigned int version);

template<class Archive>
void load(Archive & ar, Eigen::MatrixXd& mat, const unsigned int version);

template<class Archive>
 void serialize(
		Archive & ar,
		Eigen::MatrixXd& mat,
		const unsigned int file_version
);

// Copy for MatrixXd  *************************************
template<class Archive>
void save(Archive & ar, Eigen::MatrixXu const& mat, const unsigned int version);

template<class Archive>
void load(Archive & ar, Eigen::MatrixXu& mat, const unsigned int version);

template<class Archive>
 void serialize(
		Archive & ar,
		Eigen::MatrixXu& mat,
		const unsigned int file_version
);

// Copy for RowVectorXu  *************************************
template<class Archive>
void save(Archive & ar, Eigen::RowVectorXu const& vec, const unsigned int version);

template<class Archive>
void load(Archive & ar, Eigen::RowVectorXu& vec, const unsigned int version);

template<class Archive>
 void serialize(
		Archive & ar,
		Eigen::RowVectorXu& vec,
		const unsigned int file_version
);

// Copy for VectorXd  *************************************
template<class Archive>
void save(Archive & ar, Eigen::VectorXd const& vec, const unsigned int version)
{

  unsigned int rows = vec.rows();
  ar & rows;
  for(unsigned int i=0; i<vec.rows(); ++i)
  {
    ar & vec(i);
  }
}

template<class Archive>
void load(Archive & ar, Eigen::VectorXd& vec, const unsigned int version)
{
  unsigned int numrows=0;
  ar & numrows;
    
  vec.resize(numrows); // resize the derived object
  for(unsigned int i=0; i<vec.rows(); ++i)
  {
      ar & vec(i);
  }
}

template<class Archive>
 void  serialize(
		Archive & ar,
		Eigen::VectorXd& vec,
		const unsigned int file_version
)
{
	split_free(ar, vec, file_version);
}

} // namespace serialization
} // namespace boost

BOOST_CLASS_EXPORT_KEY(Eigen::MatrixXd)
BOOST_CLASS_EXPORT_KEY(Eigen::MatrixXu)
BOOST_CLASS_EXPORT_KEY(Eigen::RowVectorXu)
BOOST_CLASS_EXPORT_KEY(Eigen::VectorXd)

#endif /* EIGENUTILS_H_ */
