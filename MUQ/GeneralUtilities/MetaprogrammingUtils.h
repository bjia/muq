//
//  MetaprogrammingUtils.h
//  
//
//  Created by Matthew Parno on 7/16/12.
//  Copyright (c) 2012 MIT. All rights reserved.
//

#ifndef _MetaprogrammingUtils_h
#define _MetaprogrammingUtils_h


/** \brief Structure useful in template metaprogramming that checks for equivalent types.
 
 In order to check the equality of two template parameters, two classes are needed.  This class holds the case where the template parameters are different and the class SameType<T,T> holds the case where the template parameters are the same.  The following code snippet is an example of how to use this comparison in a template function.  The function adds two inputs of different types and returns the result in the form of the first type.  If the second type is different than the first, a copy constructor is used to convert it.
 \code 
 template<typename T1, typename T2>
 T1 Add(T1 In1, T2 In2){
    if(SameType<T1,T2>::result){ // check if the template parameters are the same
        return In1+In2;
    }else{
        return In1+T1(In2);
    }
 }
  \endcode
 */
template<typename X, typename Y>
struct SameType
{
    enum { result = 0 };
};
template<typename T>
struct SameType<T, T>
{
    enum { result = 1 };
};



#endif
