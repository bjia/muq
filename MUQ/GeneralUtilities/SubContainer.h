//MIT UQ Library
//Copyright (C) 2012 Matt Parno
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef _SUBCONTAINER_H_
#define _SUBCONTAINER_H_

// standar library includes
#include <vector>
#include <iostream>
#include <math.h>
#include <cstdlib>



namespace muq {
    namespace GeneralUtilities{
        
        /** Define a templated utility class that is useful to accessing subsets of 
         STL containers.  The idea is to construct an instance of this class
         for an instance of an STL container to make it easy to access parts of the
         original container and easily swap the ordering of the elements.  Originally 
         this class was designed for use in my DRAM code.
         */
        template <class T>
        class SubContainer{
            
        public:
            // ///////////////////
            // constructors
            // ///////////////////
            
            /** Default constructors sets everything to null and 0 */
            SubContainer(){
                inc = 0;
            };
            
            /** Create a subcontainer covering an entire container */
            SubContainer(std::vector<T> &containerIN){
                set(containerIN);
            };
            
            /** Create a subcontainer from a begin iterator, end iterator, and increment */
            SubContainer(typename std::vector<T>::iterator &startIterIN, typename std::vector<T>::iterator &endIterIN, int Increment=1){
                set(startIterIN, endIterIN, Increment);
            };
            
            /** Set this subcontainer from an entire container */
            void set(std::vector<T> &containerIN){
                startIter = containerIN.begin();
                endIter = containerIN.end();
                inc = 1;
            };
            
            /** Set this subcontainer from start and end iterators */
            void set(typename std::vector<T>::iterator &startIterIN, typename std::vector<T>::iterator &endIterIN, int Increment=1){
                startIter = startIterIN;
                endIter=endIterIN;
                inc=Increment;
            };
            
            /** Set this subcontainer from another container */
            void set(SubContainer<T> &rhs){
                startIter=rhs.startIter;
                endIter=rhs.endIter;
                inc=rhs.inc;
            };
            
            /** Swap direction */
            void flip(){
                typename std::vector<T>::iterator temp;
                temp = endIter;
                
                int offset = 1;
                if(inc<0)
                    offset = -1;
                
                endIter = startIter-offset;
                
                startIter = temp-offset;
                
                inc = -1*inc;
                
            };
            
            /** get subcontainer from this subcontainer */
            SubContainer<T> sub(int start, int end, int incIN=1){
                
                if(end<start){
                    std::cerr << "In SubContainer<T> sub(int start, int end, int incIN), the 'end' index must be great than the 'start' index.\n\t To flip order as well, change increment to '-1'\n\n";
                    exit(1);
                }
                
                // use the absolute value of incIN
                bool flip = false;
                if(incIN<0){
                    flip = true;
                    incIN = -incIN;
                }
                
                
                if(incIN!=1){
                    std::cerr << "In SubContainer<T> sub(int start, int end, int incIN), currently only -1 and 1 are valid values for incIN\n";
                    exit(1);
                }
                
                
                SubContainer temp;
                temp.startIter=startIter+start*inc;
                temp.endIter=startIter+end*inc;
                temp.inc = incIN*inc;
                
                if(flip)
                    temp.flip();
                
                return temp;
            };
            
            /** acces a particular element of this subcontainer */
            T access(int ind){
                return *(startIter+ind*inc);
            };
            
            /** overlaod the = operator */
            SubContainer<T> & operator=(SubContainer<T> &rhs){
                set(rhs);
                return *this;
            };
            
            /** overload the index operator */
            T operator[](int ind){
                return access(ind);
            };
            
            
            /** print contents -- assume type T has << operator */
            void print() const{
                for(typename std::vector<T>::iterator ind = startIter; ind != endIter; ind+=inc)
                    std::cout << *ind << " ";
                std::cout << std::endl;
            };
            
            T start() const{
                return *startIter;
            };
            
            T end() const{
                return *(endIter-inc);
            };
            
            int size() const{
                return fabs(endIter-startIter);
            };
            
            
        private:
            typename std::vector<T>::iterator startIter;
            typename std::vector<T>::iterator endIter;
            int inc;
            
        };
        
        
        // other utility functions
        
        // make a new SubContainer by flipping another
        template <class T>
        SubContainer<T> flip(const SubContainer<T> &input){
            
            SubContainer<T> temp = input;	
            temp.flip();
            return temp;
        };
        
        
    }
}
// make a new SubContainer by taking part of another





#endif
