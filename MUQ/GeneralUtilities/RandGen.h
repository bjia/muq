//
//  RandGen.h
//  
//
//  Created by Matthew Parno on 1/30/12.
//  Copyright (c) 2012 MIT. All rights reserved.
//

#ifndef _RandGen_h
#define _RandGen_h


#include <random>

namespace muq {
    namespace GeneralUtilities{
        
        
        /** The RandGen generator class is a wrapper around the std::random number generation library.  The point of this class
         is to provide a super easy to use interface  that can easily be mimiced when Boost is not available.
         */
        class RandomGenerator{
            
        public:
            
            /** Default constructor sets up Generator type */
            RandomGenerator(){Uniform_RNG =  std::uniform_real_distribution<>(0,1); 
                Gauss_RNG = std::normal_distribution<>(0,1);};
            
            /** Get a standard Gaussian distribution sample. */
            double GetNormal(){return Gauss_RNG(BaseGenerator);};
            
            /** Get a uniformly distributed real sample in [0,1). */
            double GetUniform(){return Uniform_RNG(BaseGenerator);};
            
            /** Get a random integer, distributed uniformly, between the bounds. */
            int GetUniformInt(int lb, int ub){return round(Uniform_RNG(BaseGenerator)*(ub-lb)+lb);};
            
            /** Set the seed for the random number generator.*/
            void SetSeed(int seedval){
                BaseGenerator.seed(seedval);
                
                Uniform_RNG.reset();
                Gauss_RNG.reset();
            }
            
        private:
            
            
            /** Uniform distribution. */
            std::uniform_real_distribution<> Uniform_RNG;
            
            
            /** Gaussian Distribution. */
            std::normal_distribution<> Gauss_RNG;
            
            /** Use a Mersenne twister generator. */
            std::mt19937 BaseGenerator;
            
            
        };
        
    }// namespace GeneralUtilties
}//namespace muq

#endif
