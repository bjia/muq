//
//  ParameterList.h
//  
//
//  Created by Matthew Parno on 2/1/12.
//  Copyright (c) 2012 MIT. All rights reserved.
//

#ifndef _ParameterList_h
#define _ParameterList_h

#include<string>

#include <boost/property_tree/ptree.hpp>

namespace muq{
  namespace GeneralUtilities{
    
    /** This class provides an easy way to keep track of parameters that dictate which algorithms to use
     or to keep track of different options within an algorithm.  We could just use a property_tree directly but using this wrapper class gives the possibility of using the Trilinos Teuchos ParameterList class in the future. 
     */
    class ParameterList {
        
    public:
        
        /// Construct an empty list
        ParameterList(){};
        
        /// Construct a list from and xml file
        ParameterList(const std::string &XmlFile);
        
        /// Construct list from another list
        ParameterList(const ParameterList &input);
        
        /// set parameter list from xml input
        void SetFromFile(const std::string &XmlFile);
        
        /// write parameter list to an xml file
        void WriteToFile(const std::string &XmlFile);
        
        /// specializations of the get template
        double Get(std::string name, double DefVal);
        int Get(std::string name, int DefVal);
        std::string Get(std::string name, std::string DefVal);
        
        // specializations of the set template
        void Set(std::string name, const double &value);
        void Set(std::string name, const int &value);
        void Set(std::string name, const std::string &value);
        
    private:
        
        /** keep a boost property_tree to hold all the information. */
        boost::property_tree::ptree pt;
        
        
    };// parameter list
    
  }
}// namespace muq

#endif // _ParameterList_h
