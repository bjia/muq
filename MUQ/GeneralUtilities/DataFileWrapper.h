//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#ifndef DATAFILEWRAPPER_H
#define DATAFILEWRAPPER_H

#include <map>
#include <set>
#include <string>


#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>

#include <Eigen/Core>


#include "MUQ/GeneralUtilities/CachedFunctionWrapper.h"
#include "MUQ/GeneralUtilities/PointWrapper.h"


namespace muq{
  namespace GeneralUtilities{
    
    /**
     * This class is a type of function wrapper designed to take a file
     * with a database of function evaluations in it. It will return those points
     * when queried, or throws an error if the values are not present.
     * 
     * Separately counts which evaluations have actually been used.
     * **/
    class DataFileWrapper: public CachedFunctionWrapper
    {
    public:
  
      static boost::shared_ptr<DataFileWrapper> WrapFile(std::string filename, unsigned int inputDim, unsigned int outputDim);
      
      virtual ~DataFileWrapper();
      
      ///Override that saves which ones are used, then calls the parent method
      virtual Eigen::MatrixXd Evaluate(Eigen::MatrixXd const& x) override;
      
      ///Returns the number of elements in the cache.
      virtual unsigned int GetNumOfEvals() const override;
      
      ///Clear the cache of points that were used, not the cache of results
      virtual void ClearCache();
      
    private:
      //default constructor for serialization
      DataFileWrapper();
      
      ///Standard constructor, takes the data from the file and uses it.
      DataFileWrapper(std::string filename, unsigned int inputDim, unsigned int outputDim);
      
      
      ///Asserts an error as you should never leave the cache
      virtual void ExtendCache(Eigen::MatrixXd const& newPoints) override;
      
      ///A map from quadrature points to function evaluations, used to avoid ever calling fn twice on identical inputs
      std::set<muq::GeneralUtilities::PointWrapper, muq::GeneralUtilities::PointComp> pointsUsedCache;
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
    };
    
  }
}

#endif // DATAFILEWRAPPER_H
