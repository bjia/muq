//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * FunctionWrapper.h
 *
 *  Created on: Jan 31, 2011
 *      Author: prconrad
 */

#ifndef FUNCTIONWRAPPER_H_
#define FUNCTIONWRAPPER_H_

#include <string>
#include <functional>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/export.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <Eigen/Core>

namespace muq{
  namespace GeneralUtilities{
    
    /**
     * A class for wrapping functions that take R^n and map to R^m. Created so
     * we could subclass it with a cached variant. Uses boost::function to store the
     * function pointer.
     *
     * This is the simplest possible implementation that simply calls through to the function.
     */
    class FunctionWrapper {
    public:
      typedef boost::shared_ptr<FunctionWrapper> Ptr;
            

      ///Factory method. Create a wrapper around a function that maps a single vector to a vector
      static Ptr WrapVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> vectorFn, unsigned const inputDim, unsigned const outputDim);
      
      ///Factory method. Wrap a function that maps a batch of vectors to a batch of vectors
      static Ptr WrapBatchVectorFunction(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchVectorFn, unsigned const inputDim, unsigned const outputDim);
      
      ///Factory method. Wrap a function around an external exectuable
      /**
       * The executable is called as "executableName inputPath outputPath", where the dataFile is a path pointing
       * to a file that has the column vectors to evaluate. The resulting column vectors should be written to outputPath.
       * Writes these files to /tmp/ .
       */
      static Ptr WrapExecutableFunction(std::string const& executable, unsigned const inputDim, unsigned const outputDim);

      
      virtual ~FunctionWrapper();
      
      ///Evaluate a batch of column vectors. Works just fine with a VectorXd too.
      /**
       * The base implementation just repeatedly calls EvaluateOne,
       * but is here so that subclasses can handle batches more efficiently,
       * for example, by distribuing them to multiple cores/machines.
       **/
      virtual Eigen::MatrixXd Evaluate(Eigen::MatrixXd const& x);
      
      
      
      ///records the input dimension of this function
      unsigned int inputDim;
      ///records the output dimension of this function
      unsigned int outputDim;
      
      ///Returns the number of calls to the underlying function.
      /**
       * The base implementation is non-cached, so unique calls are counted twice.
       */
      virtual unsigned int GetNumOfEvals() const;
      
      virtual void ResetEvalsCount();
            
      ///Set the function using a vector function, as after loading
      void SetVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn);
      
      ///Set the function directly with a batch function, as after loading
      void SetBatchVectorFunction(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> fn);
      
      ///Set the function to call an executable, as after loading
      void SetExecutableFunction(std::string const& executable);
      
    protected:
      
       /**
       * A private constructor, takes a fn that can evaluate a batch of vectors at once.
       * 
       * We hide this for a tricky reason. We want it to work whether the user has a batch or 
       * vector function. We can't just overload the constructor because the Eigen types
       * are not distinguishable. So, we decided to make all cases use a factory method
       * so the naming would be consistent.
       */
      FunctionWrapper(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchFn, 
		      unsigned int inputDim, unsigned int outputDim);
      
      ///A more basic private constructor, does not take a function as input. Be careful, since this doesn't set a function
      FunctionWrapper(unsigned int inputDim, unsigned int outputDim);
      
      ///The function handle.
      std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchFn;
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      ///private default constructor for serialization
      FunctionWrapper();
     
      
    private:
      unsigned int numEvals;
      

      
      
    };
    

  }
}
BOOST_CLASS_EXPORT_KEY(muq::GeneralUtilities::FunctionWrapper)

#endif /* FUNCTIONWRAPPER_H_ */
