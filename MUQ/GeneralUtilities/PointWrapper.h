//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 *
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */

#ifndef QUADRATUREPOINTWRAPPER_H_
#define QUADRATUREPOINTWRAPPER_H_

#include <assert.h>

#include <boost/serialization/access.hpp>

#include <Eigen/Core>

namespace muq{
  namespace GeneralUtilities{

/**
 * A simple class for wrapping a point. Generally not for user code,
 * specifically created so we can create a std::map that uses vectors as
 * the key.
 */
class PointWrapper {
public:
	PointWrapper();
	PointWrapper(Eigen::VectorXd const& point);
	virtual ~PointWrapper();

	Eigen::VectorXd point;

	///Return if two multiindices are equal, used for std::map
	bool operator==(const PointWrapper& b) const;

	bool operator<(const PointWrapper& b) const;

	static const double tolerance;
	
	 protected:
  friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
};

/**
 * A comparison structure used to initialize std::map to hold multi-indices.
 */
struct PointComp {
	bool operator()(const PointWrapper& lhs, const PointWrapper& rhs) const
	{
		return lhs<rhs;
	}
	
	 friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
};

  } //namespace GeneralUtilities
} //namespace muq
#endif /* QUADRATUREPOINTWRAPPER_H_ */
