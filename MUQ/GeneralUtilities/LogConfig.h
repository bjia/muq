//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * LogConfig.h
 *
 *  Created on: Mar 28, 2011
 *      Author: prconrad
 */

#ifndef LOGCONFIG_H_
#define LOGCONFIG_H_

#include <glog/logging.h>

namespace muq{
  namespace GeneralUtilities{
    
    /**
     * This file provides an easy wrapper around the Google logging framework. The code here
     * is designed around boost.log, but as that library is not actually supported,
     * it is not suitable for distribution. So, without altering the code elsewhere, 
     * we've changed the definitions here to call the google library instead.
     *
     * The primary interface from before is the macro: BOOST_LOG_POLY(channel, severity), for example:
     * BOOST_LOG_POLY("pce",debug) << "your message" << printable object;
     * 
     * The primary google call is  LOG(INFO) << message. All the other calls are transformed into 
     * these with a #def.
     * 
     * Call executables with flags to determine how much to log and whether to show 
     * it in stderr. For example, --logtostderr=1 See the google docs.
     * http://google-glog.googlecode.com/svn/trunk/doc/glog.html
     */
    
    
    ///This enum defines the levels of the debug messages. Legacy.
    enum severity_level
    {
      fntrace, 		//Very detailed tracing in and out of functions
      debug,			//A standard set of useful debug messages
      performance,	//Maybe useful, a limited set that prints more detailed values to tell how the code is doing
      //E.g. error indicators as an algorithm iterates.
      normal,			//Basic reporting on what the code is doing. Shouldn't be a ton of this.
      warning,		//Something unexpected has happened.
      error,			//Something bad has happened.
      critical		//Something very bad has happened. Probably going to crash or exit.
    };
    
    
    /**
     * Legacy macro to rewrite logging.
     */
    #define BOOST_LOG_POLY(chan,lvl) \
    LOG(INFO)
    
    /**
     * Boilerplate code, goes in main functions to initialize the logging code. Will exit if
     * logging initialization is unsuccessful.
     */
    void InitializeLogging(int argc, char **argv);
    
  }
}

#endif /* LOGCONFIG_H_ */
