//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef PARALLELFUNCTIONWRAPPER_H
#define PARALLELFUNCTIONWRAPPER_H

#include <utility>


#include <boost/serialization/access.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/CachedFunctionWrapper.h"

///A relatively simple MPI implementation of the FunctionWrapper interface, which distributes function evaluations.
/**
 * If the function of interest is particularly expensive, then we may need to distribute the runs 
 * onto a cluster. This will have the rank 0 machine act as the server and all the others 
 * as workers. Once the object is constructed by each process, with the function provided,
 * call InitializeWorkers(). This function will trap all the workers into their
 * primary loop.
 * 
 * NOTE: this implementation will not work with more than one ParallelFunctionWrapper 
 * per EXECUTABLE. One call to InitializeWorkers will take over all the workers forever.
 * Be careful.
 **/
namespace muq
{
  namespace GeneralUtilities
  {
    
    class ParallelFunctionWrapper: public CachedFunctionWrapper
    {
    public: 
      typedef boost::shared_ptr<ParallelFunctionWrapper> Ptr;


      ///Factory method. Create a wrapper around a function that maps a single vector to a vector
      static Ptr WrapVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> vectorFn, unsigned const inputDim, unsigned const outputDim);
      
      ///Factory method. Wrap a function that maps a batch of vectors to a batch of vectors
      static Ptr WrapBatchVectorFunction(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchVectorFn, unsigned const inputDim, unsigned const outputDim);
      
      ///Factory method. Wrap a function around an external exectuable
      /**
       * The executable is called as "executableName inputPath outputPath", where the dataFile is a path pointing
       * to a file that has the column vectors to evaluate. The resulting column vectors should be written to outputPath.
       * Writes these files to /tmp/ .
       */
      static Ptr WrapExecutableFunction(std::string const& executable, unsigned const inputDim, unsigned const outputDim);
      
      virtual ~ParallelFunctionWrapper() = default;
            
      ///Start the workers. They spend their entire life in this function.
      void InitializeWorkers();
      
      ///Tell the workers to quit.
      void CloseWorkers();
      
    private:
      
      ///Calls the internal method to actually compute new points using the workers
      virtual void ExtendCache(Eigen::MatrixXd const& newPoints) override;

      ParallelFunctionWrapper(boost::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> fn, 
			      unsigned int inputDim, unsigned int outputDim);	    

	    
      //default constructor for serialization
      ParallelFunctionWrapper() = default;
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
      ///Make the implementation of extend a friend so it can add cache values
      friend  void ExtendCacheImpl(Eigen::MatrixXd const& x, ParallelFunctionWrapper &wrapper);

    };
    
  }
}



#endif // PARALLELFUNCTIONWRAPPER_H
