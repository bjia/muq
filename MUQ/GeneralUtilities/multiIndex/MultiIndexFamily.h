//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * MultiIndexFamily.h
 *
 *  Created on: Jan 12, 2011
 *      Author: prconrad
 */

#ifndef MULTIINDEXFAMILY_H_
#define MULTIINDEXFAMILY_H_


#include <map>
#include <assert.h>

#include <boost/bimap.hpp>
#include <boost/bimap/unordered_set_of.hpp>
#include <boost/functional/hash.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexWrapper.h"

namespace muq{
  namespace GeneralUtilities{
    
    ///An abstract class to organize groups of multi-indices.
    /**
     * We represent a single multi-index as an urowvec, for example, i=[1 0 2], a three dimensional
     * multi-index. Typically, the task with multi-indices is to loop over them, or associate values
     * or objects with each multi-index, so it is helpful to have a stable ordering to rely on.
     * This object defines a canonical ordering of the multi-indices, and provides a way to map
     * from the number of the multi-index to the multi-index itself.
     *
     * With the default initialization routine, the family is defined by the zero element
     * provided by GetFirstIndex() and the generator DeriveNextMultiIndex(). The TotalOrder and FullTensor
     * families use this interface. Adaptive multi-indices start almost empty and are created on the fly.
     *
     * Note that all the user code for non-subclasses is const.
     *
     * Always starts with the zero element provided by GetFirstIndex(), which subclasses may overwrite.
     *
     * IMPORTANT: code that uses this should not assume any properties about the order, or that running
     * the code twice gives the same answer - merely that one instance of a MultiIndexFamily (or subclasses),
     * once created and initialized, will provide some arbitrary stable ordering, and that iterating through
     * indicies [0,GetLength()-1)] will get all of them. Also, that looking up the index of a particular
     * multi-index twice, with the same family, will produce the same index.
     *
     * In the future it might make sense to create an iterator or some more advanced syntax for
     * cycling through the family.
     */
    class MultiIndexFamily {
    public:
      typedef boost::shared_ptr<MultiIndexFamily> Ptr;
      
      virtual ~MultiIndexFamily();
      
      ///Return the number of multi-indices
      unsigned int GetNumberOfIndices() const;
      
      ///Return the length of each multi-index, i.e. the number of dimensions
      unsigned int GetMultiIndexLength() const;
      
      ///Return the i-th multi-index.
      Eigen::RowVectorXu IndexToMulti(unsigned int const i) const;
      
      ///Take a multi-index and look up its index.
      unsigned int MultiToIndex(Eigen::RowVectorXu const& multiIndexToFind) const;
      
      ///Return whether the input multiIndex is in this family.
      bool IsMultiInFamily(Eigen::RowVectorXu const& multiIndexToFind) const;
      
      ///Return whether an index is in this family.
      bool IsIndexInFamily(unsigned int const indexToFind) const;
      
      ///Return an imat with all the multiIndices as irowvecs
      /**
       * WARNING: This code is mostly present so that classes containing a MultiIndexFamily
       * can get an imat to print. DO NOT use this as a replacement for the above functions!
       * (you could, but it's terrible design, and your copy will go out of date if anything is added)
       */
      Eigen::MatrixXu GetAllMultiIndices() const;
      
      ///Return an imat with all the multiIndices as irowvecs
      /**
       * WARNING: This code is mostly present so that classes containing a MultiIndexFamily
       * can get an imat to print. DO NOT use this as a replacement for the above functions!
       * (you could, but it's terrible design, and your copy will go out of date if anything is added)
       */
      Eigen::VectorXu GetAllIndices() const;
      
      
      //*************************************************
      //Functions for querying where in the family indices are - leading or buried
      //*************************************************
      /**
       * Return true if all the forward neighbors of the index are already active, hence,
       * it is buried under them.
       */
      bool IsIndexBuried(unsigned int index) const;
      
      ///Return a 0/1 vector with 1s on the non-buried indices and 0s on the buried indices.
      /**
       * The idea here is that if you have a colvec of values associated with the indices, you
       * could select them out by performing termwise multiplication (as .* in matlab)
       */
      Eigen::VectorXd SelectNonBuriedIndices() const;
      
      ///Return the first element of the index set, usually zeros. Subclasses can replace as needed.
      virtual Eigen::RowVectorXu GetFirstIndex();
      
    private:
      
      ///Give access to serialization
      friend class boost::serialization::access;
      
      ///Declare serialization code
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
      
      
      /**
       * Compute the number of indices in the set. The reference implementation repetitively
       * calls DeriveNextMultiIndex until it runs out - somewhat inefficient, but always possible.
       * Subclasses may redefine a more efficient method.
       */
      virtual unsigned int DeriveNumberOfIndices();
      
      /**
       * An abstract function that defines which multi-indices are in the set when using the default
       * initialization routine. Defines a canonical ordering of the multi-indices.
       * Must not recreate any multi-indices accidentally.
       *
       * @param multiIndex multiIndex
       * @return an irowvec giving the next multi-index in the order, or an empty vector if there are no more to derive.
       */
      virtual Eigen::RowVectorXu DeriveNextMultiIndex(const Eigen::RowVectorXu& multiIndex)=0;
      
      ///A structure used to name a bimap field.
      struct index{};
      ///A structure used to name a bimap field.
      struct multiIndex{};
      
      ///The detailed type of the bimap that defines the multiIndexFamily.
      typedef boost::bimaps::bimap
      < boost::bimaps::unordered_set_of < boost::bimaps::tagged<unsigned int, index>,
      boost::hash<unsigned int> >,
      boost::bimaps::unordered_set_of < boost::bimaps::tagged< muq::GeneralUtilities::MultiIndexWrapper, multiIndex>,
      boost::hash<muq::GeneralUtilities::MultiIndexWrapper>, muq::GeneralUtilities::MultiIndexWrapperEq > > MultiIndexBimap;
      
      
      typedef MultiIndexBimap::value_type IndexMultiPair;
      
      /**
       * This bimap provides the look-up between indices and multi-indices, and is the
       * core important piece of storage for the class. Note that it is private,
       * hiding details of this data structure even from sub-classes.
       */
      MultiIndexBimap multiIndexCache;
      
    protected:
      ///The constructor, creates a multi-index family with length number of terms in the index.
      /**
       * Users may not call this constructor, hence it is protected. It merely sets the length.
       * Subclasses provide named constructors that will actually initialize it.
       */
      MultiIndexFamily(const unsigned int length);
      
      ///A default constructor for serialization.
      MultiIndexFamily();
      
      ///Stores the number of elements in a single multi-index.
      unsigned int lengthOfMultiIndex;
      
      ///Fill in the cache of multi-indicies.
      /**
       * The default constructor uses GetFirstIndex() and DeriveNextMultiIndex()
       * to define the multiIndexFamily.
       */
      virtual void init();
      
      /**
       * Add the input multiIndex, returning its index. If already in the set, return the existing index.
       * Ignores any considerations about validity of this multi-index, so use with caution!
       */
      unsigned int InsertMultiIndex(Eigen::RowVectorXu const& newMultiIndex);
      
      /**
       * This contains, as rows, all the ways of incrementing the multi-index by one, which
       * is cached because it is used a lot. A multiIndex +/- an element from the neighborCache produces a neighbor.
       * And, doing this with all the elements of the cache produces all the possible neighbors.
       */
      Eigen::MatrixXu neighborCache;
      
    };
    
  }
}


#endif /* MULTIINDEXFAMILY_H_ */
