//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * FullTensorMultiIndexFamily.h
 *
 *  Created on: Jan 12, 2011
 *      Author: prconrad
 */

#ifndef TOTALORDERMULTIINDEXFAMILY_H_
#define TOTALORDERMULTIINDEXFAMILY_H_


#include <boost/shared_ptr.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"

namespace muq{
  namespace GeneralUtilities{
    
    ///An implementation of MultiIndexFamily providing a total-order simplex of indices.
    /**
     * Subclasses MultiIndexFamily to create a simplex of indicies with upper and lower cutoffs.
     * Specifically, contains all multiIndices such that minOrder <= sum multiIndex <= maxOrder.
     *
     * For example, a two dimension family with minOrder=1 and maxOrder=2 includes, in
     * some order,
     * [0 1]
     * [0 2]
     * [1 0]
     * [1 1]
     * [2 0]
     *
     * Provides a named constructor Create() that return smart pointers to fully initialized
     * families.
     */
    class TotalOrderMultiIndexFamily: public MultiIndexFamily {
    public:
      ///A convenience typedef for smart pointers to this class.
      typedef boost::shared_ptr<TotalOrderMultiIndexFamily> Ptr;
      
      ///Creates and initializes a multi-index family with given min and max order. Min order defaults to 0.
      static Ptr Create(unsigned int const length, unsigned int const maxOrder, unsigned int const minOrder=0,
			unsigned int const baseValue = 0);
      
      virtual ~TotalOrderMultiIndexFamily();
      
    private:
      ///A private constructor
      TotalOrderMultiIndexFamily(unsigned int const length);
      
      ///Private default constructor for serialization
      TotalOrderMultiIndexFamily();
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      ///Implementation of code to derive the next multi-index.
      virtual Eigen::RowVectorXu DeriveNextMultiIndex(const Eigen::RowVectorXu& multiIndex);
      
      ///Overrides the generation of the first multi-index to account for min indices.
     Eigen::RowVectorXu GetFirstIndex();
      
      ///The max order of the multi-indices
      unsigned int maxOrder;
      
      ///The min order of multi-indices
      unsigned int minOrder;
      
      ///The base value of the multi-indices
      unsigned int baseValue;
    };
    
  }
}
#endif /* TOTALORDERMULTIINDEXFAMILY_H_ */
