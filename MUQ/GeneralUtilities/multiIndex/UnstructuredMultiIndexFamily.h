//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#ifndef UNSTRUCTUREDMULTIINDEXFAMILY_H
#define UNSTRUCTUREDMULTIINDEXFAMILY_H

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"


#include <boost/serialization/access.hpp>
#include <boost/serialization/export.hpp>
#include <Eigen/Core>

namespace muq{
  namespace GeneralUtilities{
    
/**
 * @brief An unstructured multi-index set.
 * 
 * A set that allows the user to directly add any elements they wish, without
 * structural requirements.
 **/
class UnstructuredMultiIndexFamily: public MultiIndexFamily
{
public:
    typedef boost::shared_ptr<UnstructuredMultiIndexFamily> Ptr;    
    
    virtual ~UnstructuredMultiIndexFamily();
    
    /**
     * @brief Public constructor...
     * 
     * Returns an unstructured multi-index set with exactly one element, with every dimension
     * equal to the baseElement. 
     *
     * @param length The number of dims
     * @param baseElement The first element, always included in the set. Defaults to 0.
     * @return :Ptr
     **/
    
    static Ptr Create(unsigned int const length, unsigned int const baseElement=0);
    
    /**
     * @brief Add the input multi-index into the set.
     *
     * @param multiIndex ...
     * @return The index of the multi index.
     **/
    unsigned int AddMultiIndex(Eigen::RowVectorXu const& multiIndex);
    
    static UnstructuredMultiIndexFamily::Ptr CloneExisting(MultiIndexFamily::Ptr existing);
    
private:
    UnstructuredMultiIndexFamily();
    
    ///The private constructor.
    UnstructuredMultiIndexFamily(unsigned int const length, unsigned int const baseElement);
    
    
    friend class boost::serialization::access;
    
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version);
    
    ///Return the first element of the index set, equal to the provided baseElement
    virtual Eigen::RowVectorXu GetFirstIndex();
    
    ///The real initialization method, creates an initial group of multi-indices..
    virtual void init();
    
    ///Required by the class, but should never get called! Meaningless here.
    virtual unsigned int DeriveNumberOfIndices();
    
    ///Required by the class, but should never get called! Meaningless here.
    virtual Eigen::RowVectorXu DeriveNextMultiIndex(const Eigen::RowVectorXu& multiIndex);
    
    ///Set via constructor, the first element will be [baseElement ... baseElement]
    unsigned int baseElement;
};


  }
}

BOOST_CLASS_EXPORT_KEY (muq::GeneralUtilities::UnstructuredMultiIndexFamily)
#endif // UNSTRUCTUREDMULTIINDEXFAMILY_H
