//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#ifndef SIMPLEXLIMITEDMULTIINDEXFAMILY_H
#define SIMPLEXLIMITEDMULTIINDEXFAMILY_H

#include <boost/shared_ptr.hpp>
#include <boost/serialization/set.hpp>

#include "MUQ/GeneralUtilities/multiIndex/AdaptiveMultiIndexFamily.h"

namespace muq{
  namespace GeneralUtilities{
    
    /**
     * Just like adaptive multi-index families, except that it will not say that
     * terms beyond a particular order are admissible.
     * **/
    class SimplexLimitedMultiIndexFamily : public AdaptiveMultiIndexFamily
    {
    public:
      typedef boost::shared_ptr<SimplexLimitedMultiIndexFamily> Ptr;
      
      static Ptr Create(unsigned int const length, unsigned int const simplexLimit, unsigned int const baseElement=0,
			unsigned int const initSimplexLevels = 1);
      
      
      virtual ~SimplexLimitedMultiIndexFamily();
      
      
      //*********************************************************
      //Testing properties of an index/multiIndex
      //*********************************************************
      
      ///Determines whether the input multiIndex is currently admissible.
      virtual bool IsMultiIndexAdmissible(const Eigen::RowVectorXu& multiIndex) const;
      
      
    private:
      ///The private constructor.
      SimplexLimitedMultiIndexFamily( unsigned int const simplexLimit, unsigned int const length, unsigned int const baseElement);
      ///Private default constructor does no work, for serialization,
      SimplexLimitedMultiIndexFamily();
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
      unsigned int simplexLimit;
      
    };
    
  }
}

#endif // SIMPLEXLIMITEDMULTIINDEXFAMILY_H_H
