//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * FullTensorMultiIndexFamily.h
 *
 *  Created on: Jan 12, 2011
 *      Author: prconrad
 */

#ifndef FULLTENSORMULTIINDEXFAMILY_H_
#define FULLTENSORMULTIINDEXFAMILY_H_

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"

namespace muq{
  namespace GeneralUtilities{
    
    ///An implementation of MultiIndexFamily providing full tensor multi-indices.
    /**
     * Subclasses MultiIndexFamily to create a mutli-index family that contains the entire
     * set of tensor products up to a given order. Depending on the initialization, the limits
     * may be uniform across dimensions or may vary per dimensions. The set includes the full
     * tensor product of dimensionwise elements, where each dimension ranges [0, order-1].
     *
     * For example, a two dimension family with dimension-varying order [1 2] includes, in
     * some order,
     * [0 0]
     * [0 1]
     * [0 2]
     * [1 0]
     * [1 1]
     * [1 2]
     *
     * Provides two constructors Create that return smart pointers to fully initialized
     * families.
     */
    class FullTensorMultiIndexFamily: public MultiIndexFamily {
    public:
      ///A convenience typedef for smart pointers to this class.
      typedef boost::shared_ptr<FullTensorMultiIndexFamily> Ptr;
      
      /**
       * Initialization with uniform limits.
       * @param length number of elements in the multi-index
       * @param order each element will range [0, order-1]
       * @return
       */
      static Ptr Create(unsigned int const length, unsigned int const order);
      
      /**
       * Initialization with non-uniform limits.
       * @param length number of elements in the multi-index
       * @param order Element i will range [0, order(i)-1]
       * @return
       */
      static Ptr Create(unsigned int const length, const Eigen::RowVectorXu &order);
      
      virtual ~FullTensorMultiIndexFamily();
      
    private:
      ///A hidden constructor
      FullTensorMultiIndexFamily(unsigned int const length);
      
      ///Hidden default constructor for serialization.
      FullTensorMultiIndexFamily();
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
      
      ///The max order of the multi-indices
      Eigen::RowVectorXu order;
      
      ///Implementation of next multi index
      virtual Eigen::RowVectorXu DeriveNextMultiIndex(Eigen::RowVectorXu const& multiIndex);
    };
  }
}
#endif /* FULLTENSORMULTIINDEXFAMILY_H_ */
