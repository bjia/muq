//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * MultiIndexWrapper.h
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */

#ifndef MULTIINDEXWRAPPER_H_
#define MULTIINDEXWRAPPER_H_

#include <assert.h>

#include <Eigen/Core>


#include <boost/serialization/access.hpp>

#include "MUQ/GeneralUtilities/EigenUtils.h"

namespace muq{
  namespace GeneralUtilities {
/**
 * A simple typedef for wrapping a multi-index. Recall that a multi-index
 * in our code is an arma::urowvec. This code makes it possible to use them in data
 * structures like maps.
 */
typedef Eigen::RowVectorXu MultiIndexWrapper;

struct MultiIndexWrapperEq{
    bool operator()(const MultiIndexWrapper& lhs, const MultiIndexWrapper& rhs) const;
};

struct MultiIndexWrapperLess{
    bool operator()(const MultiIndexWrapper& lhs, const MultiIndexWrapper& rhs) const;
};
  }
}

namespace Eigen{
    ///Define a hash function
    std::size_t hash_value(const Eigen::RowVectorXu& b);
}







#endif /* MULTIINDEXWRAPPER_H_ */
