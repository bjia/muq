//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * AdaptiveMultiIndexFamily.h
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */

#ifndef ADAPTIVEMULTIINDEXFAMILY_H_
#define ADAPTIVEMULTIINDEXFAMILY_H_

#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/serialization/set.hpp>

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexWrapper.h"

namespace muq{
  namespace GeneralUtilities{
    
    ///An implementation of MultiIndexFamily providing an adaptive, admissible multi-index set.
    /**
     * This class provides code for creating, managing, and growing an adaptive mulit-index set.
     * An admissible set is one where all the backward neighbors of every element are present,
     * unless those neighbors are not valid multi-indices (e.g. are negative). A backwards neighbor
     * has one dimension whose value is one less.
     *
     * For example, consider the simple 2D set:
     * [0 0], [1 0]
     * The multi-index [0 1] is admissible: it has two backwards neighbors, [-1 1] and [0 0].
     * The former is not allowed and the latter is in the set, so we could add it.
     *
     * In contrast, [1 1] is not admissible yet, because it's backward neighbor [0 1] is
     * not in the set.
     *
     * There are three categories of multi-indices:
     * 1) A multi-index is "Active" if it is in the multi-index set.
     * 2) A multi-index is "Admissible" if all its backward-neighbors are in the set.
     * 3) Other multi-indices, which are not in the set nor admissible.
     *
     * The constructor allows you to set the base element, and potentially populate
     * the initial set with some indices.
     *
     * There are several ways of growing a set, catering to different needs.
     * 1) The simplest is to find a multi-index that is already admissible and add it to
     * the set, as in MakeMultiIndexActive()
     * 2) Next is expanding a multi-index, which attempts to make its forward-neighbors
     * active. [1 1] has forward neighbors [2 1], [1 2], but not all children are necessarily
     * admissible at all times. As in ExpandIndex().
     * 3) Forcibly expanding/adding, which recursively adds all the backward-neighbors
     * needed until the desired multi-indices are added and the set is admissible. This might
     * add many new multi-indices. Ass in ForciblyExpandIndex() or ForciblyActivateMultiIndex().
     *
     * There are also methods to determine if an index or multi-index is admissible or expandable,
     * in addition to the usual ones to check for inclusion in the set (activeness)
     *
     * A note on implementation. The class tracks the multiIndices that are currently admissible
     * but not active. It also uses a cache to make it easier to compute the fwd/bwd neighbors.
     */
    class AdaptiveMultiIndexFamily: public MultiIndexFamily {
    public:
      
      typedef boost::shared_ptr<AdaptiveMultiIndexFamily> Ptr;
      
      ///User constructor method
      /**
       * This constructor creates an adaptive multi-index family. The user needs to specify how many
       * terms are in the index, and some parameters for what the initial set should look like. The
       * root multiIndex is all the same element, the input baseElement. The family is initialized
       * with an isotropic simplex with the input number of levels. A single index is simplex 1.
       *
       * E.g. a 2D 2-level simplex with baseElement=0 has multiIndices: [0,0], [0,1], [1,0].
       *
       * @param length The number of elements in each multi-index
       * @param baseElement The lowest number used in the index. Defaults to 0, so first index is
       * [0 0 ...]. If 1, [1,1, ...]
       * @param initSimplexLevels Sets the depth of the isotropic initial simplex. Defaults to 1,
       * which is just a single multiIndex.
       * @return
       */
      static Ptr Create(unsigned int const length, unsigned int const baseElement=0,
			unsigned int const initSimplexLevels = 1);
      
      
      virtual ~AdaptiveMultiIndexFamily();
      
      //*********************************************************
      //Add to the multi-index set
      //*********************************************************
      
      /**
       * Make the multi-index active. Assumes that the multiIndex input is already
       * admissible, as checked by an assertion.
       */
      void MakeMultiIndexActive(const Eigen::RowVectorXu& multiIndex);
      
      /**
       * If possible, make the neighbors of this index active, and return any that become active.
       * Do not activate a neighbor that is already part of the family
       * @param index
       * @return
       */
      Eigen::RowVectorXu ExpandIndex(unsigned int index);
      
      /**
       * Completely expands an index, whether or not it is currently expandable. In order to maintain admissability
       * of the set, it will add backward neighbors needed recursively, and return a list of all the
       * indices it adds.
       */
      Eigen::RowVectorXu ForciblyExpandIndex(unsigned int const index);
      
      /**
       * Add the given multi-index to the active set regardless of whether it's currently admissible.
       * To keep the whole set admissible, recursively add any backward neighbors necessary.
       * Returns a list of indices of any newly added elements, including itself.
       * @param multiIndex
       * @return
       */
      Eigen::RowVectorXu ForciblyActivateMultiIndex(Eigen::RowVectorXu const& multiIndex);
      
      
      //*********************************************************
      //Testing properties of an index/multiIndex
      //*********************************************************
      
      ///Determines whether the input multiIndex is currently admissible.
      virtual bool IsMultiIndexAdmissible(const Eigen::RowVectorXu& multiIndex) const;
      
      
      
      ///Return true if one of the forward neighbors of index is admissible but not active.
      virtual bool IsIndexExpandable(unsigned int index) const;
      
      
      //*********************************************************
      //Select groups of indices
      //*********************************************************
      
      
      
      ///Return a 0/1 vector with 1s on the expandable indices and 0s on the expandable indices.
      /**
       * The idea here is that if you have a colvec of values associated with the indices, you
       * could select them out by performing termwise multiplication (as .* in matlab)
       */
      Eigen::VectorXd SelectExpandable() const;
      
      ///Get a row vector of the indices of the backward neighbors of an input index
      Eigen::RowVectorXu GetActiveBackwardNeighborIndices(unsigned int const index) const;
      
      ///Return an imat whose rows are the admissible multi-indices. For debugging/testing.
      Eigen::MatrixXu GetAdmissibleMultiIndices() const;
      
    protected:
      ///The private constructor.
      AdaptiveMultiIndexFamily(unsigned int const length, unsigned int const baseElement);
      ///Private default constructor does no work, for serialization,
      AdaptiveMultiIndexFamily();
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      /**
       * If a node is made active, then this method is called to test the forward-neighbors
       * for admissibility and add them to the admissible set as appropriate.
       */
      void MakeFwdNeighborsAdmissible(const Eigen::RowVectorXu& multiIndex);
      
      ///Return the first element of the index set, equal to the provided baseElement
      virtual Eigen::RowVectorXu GetFirstIndex();
      
      ///The real initialization method, creates an initial group of multi-indices..
      virtual void init();
      
      ///The set of multiIndices that are admissible but not active.
      std::set<MultiIndexWrapper, MultiIndexWrapperLess> admissibleSet;
      
      ///Required by the class, but should never get called! Meaningless here.
      virtual unsigned int DeriveNumberOfIndices();
      
      ///Required by the class, but should never get called! Meaningless here.
      virtual Eigen::RowVectorXu DeriveNextMultiIndex(const Eigen::RowVectorXu& multiIndex);
      
      ///Set via constructor, the first element will be [baseElement ... baseElement]
      unsigned int baseElement;
    };
    
  }
}

#endif /* ADAPTIVEMULTIINDEXFAMILY_H_ */
