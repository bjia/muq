//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * ExpLimitedMultiIndexFamily.h
 *
 *  Created on: May 30, 2011
 *      Author: prconrad
 */

#ifndef EXPLIMITEDMULTIINDEXFAMILY_H_
#define EXPLIMITEDMULTIINDEXFAMILY_H_

#include <boost/serialization/access.hpp>


#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"

namespace muq{
  namespace GeneralUtilities{
    
    
    class ExpLimitedMultiIndexFamily: public MultiIndexFamily {
    public:
      virtual ~ExpLimitedMultiIndexFamily();
      
      
      ///A convenience typedef for smart pointers to this class.
      typedef boost::shared_ptr<ExpLimitedMultiIndexFamily> Ptr;
      
      ///Creates and initializes a multi-index family with given min and max order. Min order defaults to 0.
      static Ptr Create(unsigned int const length, unsigned int const maxOrder, unsigned int const baseValue = 0);
      
    private:
      ///A private constructor
      ExpLimitedMultiIndexFamily(unsigned int const length);
      
      ///Hidden default constructor for serialization.
      ExpLimitedMultiIndexFamily();
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
      ///The max order of the multi-indices
      unsigned int maxOrder;
      
      ///The base value of the multi-indices
      unsigned int baseValue;
      
      ///Implementation of code to derive the next multi-index.
      virtual Eigen::RowVectorXu DeriveNextMultiIndex(const Eigen::RowVectorXu& multiIndex);
      
      ///Overrides the generation of the first multi-index to account for min indices.
      Eigen::RowVectorXu GetFirstIndex();
      
      
    };
    
  }
}

#endif /* EXPLIMITEDMULTIINDEXFAMILY_H_ */
