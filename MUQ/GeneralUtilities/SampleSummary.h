//
//  SampleSummary.h
//  
//
//  Created by Matthew Parno on 6/4/12.
//  Copyright (c) 2012 MIT. All rights reserved.
//

#ifndef _SampleSummary_h
#define _SampleSummary_h

#include <vector>
#include <numeric>

#include <boost/shared_ptr.hpp>


namespace muq {
    namespace GeneralUtilities{
        
        class SampleSummary;
        typedef boost::shared_ptr<SampleSummary> SampleSummaryPtr;
        
        /** @class SampleSummary
         @brief Abstract interface for a method taking a vector of scalar samples and producing single summary statistic of the samples.
         @author Matthew Parno
         
         It is often necessary to take many samples of a scalar random variable and summarize all that information into a single number.  This number, called a statistic could be the sample mean, sample variance, or other function of the data.  This class provides an abstract base class for functions of this kind.  Example child classes include sample mean, sample variance, etc...
         In addition to simply evaluating the sample summary, in many situations we need to sensitivity of the summary to one of the samples (think SAA in robust optimization), this class also provides interface for that.
         */
        class SampleSummary{
            
        public:
            /** Summarize the data and provide the sensitivity (if desired).
             @param[in] samps The vector of samples to be summarized
             @return The sample summary/
             */
            virtual double Summarize(const std::vector<double> &samps) const = 0;
            
            /** Summarize the data and provide the sensitivity (if desired).
             @param[in] samps The vector of samples to be summarized
             @param[out] sens The sensitivity of the summary statistic to one value of the sample vector.
             @return The sample summary/
             */
            virtual double Summarize(const std::vector<double> &samps, std::vector<double> &sens) const = 0;
            
            
        };
        
        /** @class MeanSummary
         @brief Class to take the mean of a vector of samples.
         @author Matthew Parno
         
         This class computes the sample mean of a vector of samples and returns the results.  This is used in many robust optimization problems.
         */
        class MeanSummary : public SampleSummary{
            
            /** Summarize the data and provide the sensitivity (if desired).
             @param[in] samps The vector of samples to be summarized
             @return The sample mean/
             */
            virtual double Summarize(const std::vector<double> &samps) const{
                
                // compute the expectation
                double output=0;
                for(unsigned int i=0; i<samps.size(); ++i)
                    output += samps[i];
                output /= double(samps.size());
                return output;
                
            };
            
            /** Summarize the data and provide the sensitivity (if desired).
             @param[in] samps The vector of samples to be summarized
             @param[out] sens The sensitivity of the sample mean to one value of the sample (1/N).
             @return The sample mean/
             */
            virtual double Summarize(const std::vector<double> &samps, std::vector<double> &sens) const{
                
                // set the sensitivity
                double sensval = 1.0/samps.size();
                sens.clear();
                sens.resize(samps.size(),sensval);
                
                // compute the expectation
                double output=0;
                for(unsigned int i=0; i<samps.size(); ++i)
                    output += samps[i];
                
                output *= sensval;
                
                return output;
                
            };
            
        };
        
        /** @class VarianceSummary
         @brief Class to take the variance of a vector of samples.
         @author Matthew Parno
         
         This class computes the sample variance of a vector of samples and returns the results.  This is used in many robust optimization problems.
         */
        class VarianceSummary : public SampleSummary{
            
            /** Summarize the data and provide the sensitivity (if desired).
             @param[in] samps The vector of samples to be summarized
             @return The sample variance/
             */
            virtual double Summarize(const std::vector<double> &samps) const{
                
                unsigned int Nsamps = samps.size();
                
                // compute the expectation
                double totsum = accumulate(samps.begin(),samps.end(), 0);
                double mean = totsum/Nsamps;
                
                // compute the variance
                double var = 0;
                for(unsigned int i=0; i<Nsamps; ++i)
                    var += pow(samps[i]-mean,2);
                var /= (Nsamps-1);
                
                return var;
            };
            
            
            /** Summarize the data and provide the sensitivity (if desired).
             @param[in] samps The vector of samples to be summarized
             @param[out] sens The sensitivity of the sample mean to one value of the sample.
             @return The sample variance/
             */
            virtual double Summarize(const std::vector<double> &samps, std::vector<double> &sens) const{
                
                int Nsamps = samps.size();
                
                // compute the expectation
                double totsum = accumulate(samps.begin(),samps.end(), 0);
                double mean = totsum/Nsamps;
                
                // compute the variance
                double var = 0;
                for(int i=0; i<Nsamps; ++i)
                    var += pow(samps[i]-mean,2);
                var /= (Nsamps-1);
                
                // set the sensitivity
                sens.resize(Nsamps);
                for(int i=0; i<Nsamps;++i)
                    sens[i] = (2*samps[i]-2*mean)/(Nsamps-1) + 2*(Nsamps*mean-totsum)/(Nsamps*(Nsamps-1));
                
                
                
                return var;
            };
            
            
        };
        
    }//namespace GeneralUtilities
}// namespace muq

#endif
