//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmartPtrNull.h
 *
 *  Created on: Mar 29, 2011
 *      Author: prconrad
 */

#ifndef SMARTPTRNULL_H_
#define SMARTPTRNULL_H_

#include <boost/shared_ptr.hpp>

/*
 * This is a tricky bit of template programming. This class only does one thing:
 * overload the casting operator. When you try to cast it to boost:shared_ptr<SomeType>,
 * it picks out the SomeType, which becomes the template T. It then creates a
 * shared_ptr of that type with no initialization, and is therefore a null pointer.
 *
 * To check a pointer is initialized, just use if(aPtr).
 *
 * To avoid initialization issues I don't quite understand, (following a comment from the source)
 * don't create these directly, use the function below instead.
 *
 * Inspired by:
 * http://stackoverflow.com/questions/621220/null-pointer-with-boostshared-ptr
 */

namespace muq {
  namespace GeneralUtilities{

class SmartNullPtr {
public:
    template<typename T>
    operator boost::shared_ptr<T>() { return boost::shared_ptr<T>(); }
};

///This function returns an object that you can cast to be a null boost::shared_ptr of any type.
/**
 * Use as:
 * boost::shared_ptr<AnyType> aPtr = GetNull();
 * @return
 */
SmartNullPtr GetNull();

  }
}

#endif /* SMARTPTRNULL_H_ */
