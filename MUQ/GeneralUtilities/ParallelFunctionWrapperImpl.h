//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <boost/function.hpp>

#include <Eigen/Core>

///Implement the parallel methods away from serializable objects so we don't introduce unwanted archive types.

namespace muq{
  namespace GeneralUtilities{
    
    //fwd declare
    class ParallelFunctionWrapper;
    
    void CloseWorkersImpl();
    void InitializeWorkersImpl(boost::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> fn);
    void ExtendCacheImpl(Eigen::MatrixXd const& x, ParallelFunctionWrapper &wrapper);


  }
  
}