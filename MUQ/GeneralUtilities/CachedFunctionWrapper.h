//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * CachedFunctionWrapper.h
 *
 *  Created on: Feb 1, 2011
 *      Author: prconrad
 */

#ifndef CACHEDFUNCTIONWRAPPER_H_
#define CACHEDFUNCTIONWRAPPER_H_

#include <map>
#include <string>


#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/export.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/FunctionWrapper.h"
#include "MUQ/GeneralUtilities/PointWrapper.h"


namespace muq{
  namespace GeneralUtilities{
    /**
     * An implementation of a function wrapper that caches values to avoid re-evaluation.
     *
     * Uses the PointWrapper to determine equality of input points in R^n.
     */
    class CachedFunctionWrapper: public FunctionWrapper {
    public:
      typedef boost::shared_ptr<CachedFunctionWrapper> Ptr;
      
      
      ///Factory method. Create a wrapper around a function that maps a single vector to a vector
      static Ptr WrapVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> vectorFn, unsigned const inputDim, unsigned const outputDim);
      
      ///Factory method. Wrap a function that maps a batch of vectors to a batch of vectors
      static Ptr WrapBatchVectorFunction(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchVectorFn, unsigned const inputDim, unsigned const outputDim);
      
      ///Factory method. Wrap a function around an external exectuable
      /**
       * The executable is called as "executableName inputPath outputPath", where the dataFile is a path pointing
       * to a file that has the column vectors to evaluate. The resulting column vectors should be written to outputPath.
       * Writes these files to /tmp/ .
       */
      static Ptr WrapExecutableFunction(std::string const& executable, unsigned const inputDim, unsigned const outputDim);
      
      
      
      virtual ~CachedFunctionWrapper() = default;
      
     ///Override that works with cached values, calls the parent method to evaluate new points.
      virtual Eigen::MatrixXd Evaluate(Eigen::MatrixXd const& x) override;
   
      
      ///Returns the number of elements in the cache.
      virtual unsigned int GetNumOfEvals() const override;
      
      ///Clear the cache
      void ClearCache();
      
      ///Load the cache from a file.
      /** 
       * It is the user's responsibility to ensure that the points in the 
       * cache actually came from the function of interest, as they are 
       * blindly trusted.
       **/
      void LoadCache(std::string filename);
      
      ///Print the points stored in the cache.
      /** 
       * Prints each entry as a row, first the point, then the results.
       **/
      void SaveCache(std::string filename) const;
      
      ///Return a matrix with the column vectors for which we have values
      Eigen::MatrixXd GetCachePoints() const;
      

      
    protected:
      //default constructor for serialization
      CachedFunctionWrapper();
 
      CachedFunctionWrapper(boost::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> fn, 
			    unsigned int inputDim, unsigned int outputDim);
      
      
      ///Hacky constructor without a function
      CachedFunctionWrapper(unsigned int inputDim, unsigned int outputDim);

      bool IsEntryInCache(Eigen::VectorXd const& pointToTest) const;
	
      void AddEntryToCache(Eigen::VectorXd const& point, Eigen::VectorXd const& result);
      
      Eigen::VectorXd FetchCachedResult(Eigen::VectorXd const& point) const;
      
      virtual void ExtendCache(Eigen::MatrixXd const& newPoints);
      
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
    private: 
      ///A map from quadrature points to function evaluations, used to avoid ever calling fn twice on identical inputs
      std::map<muq::GeneralUtilities::PointWrapper, Eigen::VectorXd, 
	muq::GeneralUtilities::PointComp> fnEvalCache;
      
    };
    
  }
}

BOOST_CLASS_EXPORT_KEY(muq::GeneralUtilities::CachedFunctionWrapper)


#endif /* CACHEDFUNCTIONWRAPPER_H_ */
