//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * VariableCollection.h
 *
 *  Created on: Nov 12, 2010
 *      Author: prconrad
 */

#ifndef VARIABLECOLLECTION_H_
#define VARIABLECOLLECTION_H_

#include "MUQ/polychaos/utilities/Variable.h"

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/export.hpp>

namespace muq{
  namespace polychaos{
    
    class RecursivePolynomialFamily1D;
    class QuadratureFamily1D;
    
    /**
     * A set of variables from a subset of R^n. Used to define custom functions or
     * representations because each dimension may have different properties.
     * Provides names for the variables, and quadrature rules or polynomial families
     * for them, as appropriate. Polynomial or quadrature families should be shared
     * by all variables in the collection to leverage caching as much as possible.
     *
     * Probably needs a better way to initialize than having the user construct them.
     */
    class VariableCollection {
    public:
      typedef boost::shared_ptr<VariableCollection> Ptr;
      
      VariableCollection();
      virtual ~VariableCollection();
      
      ///Add a new variable to the back.
      /**
       * @param newVar probably called with "new Variable(...)".
       */
      void PushVariable(Variable::Ptr newVar);
      
      ///A version that will construct the variable for you, saving some syntax.
      void PushVariable(std::string name, boost::shared_ptr<RecursivePolynomialFamily1D> polyFamily,
			boost::shared_ptr<QuadratureFamily1D> quadFamily);
      
      ///Return the number of variables.
      unsigned int length();
      
      ///return a reference to the ith variable.
      Variable::Ptr GetVariable(unsigned int i);
      
    private:
      
      ///Make estimates serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      ///The list of variables
      std::vector<Variable::Ptr> variables;
    };
    
  }
}


//BOOST_CLASS_EXPORT_KEY(VariableCollection)

#endif /* VARIABLECOLLECTION_H_ */
