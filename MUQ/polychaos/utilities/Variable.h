//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * Variable.h
 *
 *  Created on: Nov 12, 2010
 *      Author: prconrad
 */

#ifndef VARIABLE_H_
#define VARIABLE_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>


namespace muq{
  namespace polychaos{
    
    class RecursivePolynomialFamily1D;
    class QuadratureFamily1D;
    
    
    ///A class for defining the properties of a dimension/variable
    /**
     * This structure holds the definition of a variable, in that it holds
     * a family of polynomials and/or quadrature, which specifies a domain
     * and probability weighting.
     *
     * One or the other pointer may be null, conveniently available through GetNull()
     * in SmartPtrNull.h
     */
    class Variable {
    public:
      typedef boost::shared_ptr<Variable> Ptr;
      
      Variable();
      
      ///Constructor that sets the name and type of the variable.
      /**
       * @param name a string, defaults to "not set"
       * @param polyFamily Pointer to the polynomial 1D family or null.
       * @param quadFamily Pointer to the quadrature 1D family or null.
       */
      Variable(std::string name, boost::shared_ptr<RecursivePolynomialFamily1D> polyFamily,
	       boost::shared_ptr<QuadratureFamily1D> quadFamily);
      
      virtual ~Variable();
      
      boost::shared_ptr<RecursivePolynomialFamily1D> polyFamily;
      boost::shared_ptr<QuadratureFamily1D> quadFamily;
      
      ///A name for the variable.
      std::string name;
      
    private:
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
    };
    
  }
}

#endif /* VARIABLE_H_ */
