//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * RecursivePolynomialFamily1D.h
 *
 *  Created on: May 31, 2011
 *      Author: prconrad
 */

#ifndef RECURSIVEPOLYNOMIALFAMILY1D_H_
#define RECURSIVEPOLYNOMIALFAMILY1D_H_

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>

///An abstract class for stable evaluation of orthogonal 1D polynomial families.
/**
 * Evaluate a polynomial family based on the three term recurrence:
 * phi_{k+1}(x) + alpha_k(x)*phi_k(x) + beta_k(x)*phi_{k-1}(x) = 0;
 *
 * Subclasses specialize for particular families of polynomials, and only provide
 * a specification of the polynomial family. Specifically, sub-classes provide
 * implementations of alpha, beta, and the base terms, phi_0(x) and phi_1(x).
 *
 * Subclasses are also expected to provide a way of determining the normalization
 * of the polynomial.
 *
 * This class implements the highly stable Clenshaw algorithm, which recursively
 * applies the recurrence to determine the values. That said, implementations
 * should independently verify accuracy for their particular families.
 *
 * Algorithm from: http://en.wikipedia.org/wiki/Clenshaw_algorithm
 */

namespace muq{
  namespace polychaos{
    
    
    class RecursivePolynomialFamily1D {
    public:
      typedef boost::shared_ptr<RecursivePolynomialFamily1D> Ptr;
      
      RecursivePolynomialFamily1D();
      virtual ~RecursivePolynomialFamily1D();
      
      ///Evaluate the order polynomial at x.
      double evaluate(unsigned int order, double x);
      
      
      ///Return the inner product of the n-th polynomial with itself<phi_k,phi_k>.
      /**
       * Subclasses must implement, hopefully a fast analytic version. Note
       * that the inner product should use whatever  measure the polynomials
       * are orthogonal with respect to.
       */
      virtual double GetNormalization(unsigned int n) = 0;
      
    private:
      ///Make class serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      ///Methods that define the polynomial.
      virtual double alpha(double x, unsigned int k)=0;
      virtual double beta(double x, unsigned int k)=0;
      virtual double phi0(double x)=0;
      virtual double phi1(double x)=0;
    };
    
  }
}

BOOST_CLASS_EXPORT_KEY(muq::polychaos::RecursivePolynomialFamily1D)

#endif /* RECURSIVEPOLYNOMIALFAMILY1D_H_ */
