//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * LegendrePolynomials1DRecursive.h
 *
 *  Created on: May 31, 2011
 *      Author: prconrad
 */

#ifndef LEGENDREPOLYNOMIALS1DRECURSIVE_H_
#define LEGENDREPOLYNOMIALS1DRECURSIVE_H_

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>

#include "MUQ/polychaos/polynomials/RecursivePolynomialFamily1D.h"

namespace muq{
  namespace polychaos{



/**
 * @brief Implements the Legendre Polynomials.
 * 
 * Subclasses the numerically stable RecursivePolynomialFamily1D with the three-term
 * recurrence necessary to generate the Legendre polynomials.
 **/
class LegendrePolynomials1DRecursive: public RecursivePolynomialFamily1D {
public:
	typedef boost::shared_ptr<LegendrePolynomials1DRecursive> Ptr;

	LegendrePolynomials1DRecursive();
	virtual ~LegendrePolynomials1DRecursive();

	///Implements normalization.
	virtual double GetNormalization(unsigned int n) override;

private:

	///Grant access to serialization
	friend class boost::serialization::access;

	///Serialization method
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

	//implement methods defining recurrence
	double alpha(double x, unsigned int k);
	double beta(double x, unsigned int k);
	double phi0(double x);
	double phi1(double x);
};



  }
}

BOOST_CLASS_EXPORT_KEY(muq::polychaos::LegendrePolynomials1DRecursive)

#endif /* LEGENDREPOLYNOMIALS1DRECURSIVE_H_ */
