//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * HermitePolynomials1DRecursive.h
 *
 *  Created on: May 31, 2011
 *      Author: prconrad
 */

#ifndef HERMITEPOLYNOMIALS1DRECURSIVE_H_
#define HERMITEPOLYNOMIALS1DRECURSIVE_H_

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>

#include "MUQ/polychaos/polynomials/RecursivePolynomialFamily1D.h"

namespace muq{
  namespace polychaos{
    
    /**
     * @brief Implements the Hermite Polynomials.
     * 
     * Subclasses the numerically stable RecursivePolynomialFamily1D with the three-term
     * recurrence necessary to generate the Hermite polynomials. Uses the physicists's
     * version of the Hermite, from http://en.wikipedia.org/wiki/Hermite_polynomials
     **/
    class HermitePolynomials1DRecursive: public RecursivePolynomialFamily1D {
    public:
      typedef boost::shared_ptr<HermitePolynomials1DRecursive> Ptr;
      
      HermitePolynomials1DRecursive();
      virtual ~HermitePolynomials1DRecursive();
      
      ///Implements normalization.
      double GetNormalization(unsigned int n);
      
    private:
      
      ///Grant access to serialization
      friend class boost::serialization::access;
      
      ///Serialization method
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      //implement methods defining recurrence
      double alpha(double x, unsigned int k);
      double beta(double x, unsigned int k);
      double phi0(double x);
      double phi1(double x);
    };
    
  }
}

#endif /* HERMITEPOLYNOMIALS1DRECURSIVE_H_ */
