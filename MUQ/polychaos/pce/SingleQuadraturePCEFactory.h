//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SingleQuadraturePCEFactory.H
 *
 *  Created on: Jun 12, 2011
 *      Author: prconrad
 */

#ifndef SINGLEQUADRATUREPCEFACTORY_H_
#define SINGLEQUADRATUREPCEFACTORY_H_

#include <boost/shared_ptr.hpp>

#include "MUQ/polychaos/pce/PolynomialChaosExpansion.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"

#include "MUQ/GeneralUtilities/SmartPtrNull.h"

///forward declarations to avoid includes
namespace muq{
  namespace GeneralUtilities{
    class FunctionWrapper;
  }
}

namespace muq{
  namespace polychaos{
    
    class VariableCollection;
    
    
    class SingleQuadraturePCEFactory {
    public:
      typedef boost::shared_ptr<SingleQuadraturePCEFactory> Ptr;
      
      ///Initialize the PCE Factory with a collection of variables.
      SingleQuadraturePCEFactory(boost::shared_ptr<VariableCollection> variables);
      
      virtual ~SingleQuadraturePCEFactory();
      
      /**
       * Construct a PCE approximating the input function and return a pointer to it.
       *
       * The quadrature is done with a single full tensor rule of order order+offset, and is defined 
       * by the quadrature rules in the variable collection. The default behavior is to include in 
       * the PCE all the terms that this quadrature rule can correctly integrate. Typically this 
       * leads to a full tensor rule. The parameter tensorOrdersIncluded gives more flexibility, 
       * allowing simulation of traditional PCE generation.
       *
       * @param fn the function to approximate
       * @param order This is the order of the full tensor quadrature rule to used.
       * @param offset The true order for quadrature used is order + offset, we do this
       * because Smolyak starts with 0th order rules, but quadrature only exists for 1st order
       * rules. Thus, we often add an offset of one. If the user calls it directly, use zero
       * offset. A vec with the same length as the number of variables.
       * @param tensorOrdersIncluded This variable allows you to accomplish traditional
       * PCE generation. If you pass in all the quadrature tensor orders included in the
       * expansion, it will create a PCE including all the terms that any of the quadrature orders
       * can integrate correctly. Omit for the full tensor PCE.
       * @return
       */
      PolynomialChaosExpansion::Ptr ConstructPCE(boost::shared_ptr<muq::GeneralUtilities::FunctionWrapper> fn,
						 Eigen::RowVectorXu const& order,
						 Eigen::RowVectorXu const& offset,
						 muq::GeneralUtilities::MultiIndexFamily::Ptr tensorOrdersIncluded = muq::GeneralUtilities::GetNull());
      
    private:
      
      ///The variables to build the PCE around.
      boost::shared_ptr<VariableCollection> variables;
    };
    
  }
}

#endif /* SINGLEQUADRATUREPCEFACTORY_H_ */
