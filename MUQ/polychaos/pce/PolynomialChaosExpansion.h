//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * PolynomialChaosExpansion.h
 *
 *  Created on: Jan 19, 2011
 *      Author: prconrad
 */

#ifndef POLYNOMIALCHAOSEXPANSION_H_
#define POLYNOMIALCHAOSEXPANSION_H_

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <boost/tuple/tuple.hpp>
#include <Eigen/Core>

#include "MUQ/polychaos/utilities/VariableCollection.h"
#include "MUQ/GeneralUtilities/multiIndex/UnstructuredMultiIndexFamily.h"


namespace muq{
  namespace polychaos{
    
    ///Forward declaration of the factory classes so we can make them friends without inclusion loops
    
    class SingleQuadraturePCEFactory;
    class SmolyakPCEFactory;
    class SmolyakTraditionalPCEFactory;
    
    
    /**
     * A particular polynomial chaos expansion for a function from R^n->R^m. This class uses
     * some subclass of MulitIndexFamily to defines which PCE terms are in the expansion.
     * For each PCE term and output, there is a coefficient. The PCE is built from 1D polynomials
     * specified in a VariableCollection, so each dimension may have independent polynomial
     * choices.
     *
     * The only user function is Evaluate(), which evaluates the PCE at a point. There is no public
     * constructor, because there's no reasonable expectation that the user knows the
     * terms to keep and the coefficients necessary to represent any given function. Instead,
     * user code calls a factory that can create a PCE that approximates their input function.
     * These factories are given access to the private data structures by being friends of this
     * class.
     */
    class PolynomialChaosExpansion {
      ///Allows polynomial chaos expansion to be printed.
      friend std::ostream& muq::polychaos::operator<<(std::ostream& output, const PolynomialChaosExpansion& pce);
      
      ///Allows classes that construct PCEs access to the internals.
      friend class SingleQuadraturePCEFactory;
      friend class SmolyakPCEFactory;
      friend class SmolyakTraditionalPCEFactory;
      
      ///Allow serialization access
      friend class boost::serialization::access;
      
    public:
      typedef boost::shared_ptr<PolynomialChaosExpansion> Ptr;
      
      virtual ~PolynomialChaosExpansion();
      
      /**
       * Evaluate the PCE at a collection of points, given by column vectors
       *
       * Can probably be much more efficient by using caching on the evaluation of
       * polynomials.
       * @param vals a matrix of the inputs, corresponding to the VariableCollection.
       * @return the value of the PCE
       */
      Eigen::MatrixXd Evaluate(Eigen::MatrixXd const& vals) const;
      
      ///Print the pce to basename+"_pce.dat" using the << operator.
      void print(std::string basename);
      
      ///Print the pce with normalized coefficients to basename+"_pce.dat" using the << operator.
      void printNormalized(std::string basename);
      
      bool IsPolynomialInExpansion(Eigen::RowVectorXu toFind);
      
      ///Return a pointer to a new PCE, a-b.
      static PolynomialChaosExpansion::Ptr SubtractExpansions(PolynomialChaosExpansion::Ptr a, PolynomialChaosExpansion::Ptr b);
      
      ///compute the variance of the current expansion
      Eigen::VectorXd ComputeVariance() const;
      
      ///Compute the L2 norm of each output.
      Eigen::VectorXd ComputeMagnitude() const;
      
      /**
       * Compute the weighted sum of polynomial expansions. Slow, because it assumes you haven't been
       * tracking which polynomials to keep, so it does that, then calls the private method. If you 
       * know already, your class should be a friend and use the private method directly, as that
       * will be faster.
       * */
      static PolynomialChaosExpansion::Ptr ComputeWeightedSum(std::vector<PolynomialChaosExpansion::Ptr> expansions, Eigen::VectorXd const& weights,
							      VariableCollection::Ptr variables, unsigned int const outputDim);
      
      unsigned int NumberOfPolynomials() const;
      
      ///Load a PCE using boost::serialization and return the result
      static Ptr LoadFromFile(std::string fileName);
      
      ///Save a PCE using boost::serialization
      static void SaveToFile(PolynomialChaosExpansion::Ptr pce, std::string fileName);
      
      ///Compute the Sobol total sensitivity index for the input dimension, for each output dimension
      Eigen::VectorXd ComputeSobolTotalSensitivityIndex(unsigned const targetDim) const;

      ///Compute all Sobol total sensitivities. Rows are outputs, each column is an input.     
     Eigen:: MatrixXd ComputeAllSobolTotalSensitivityIndices() const;

      ///Compute the main sensitivity index for the input dimension, for each output dimension
      Eigen::VectorXd ComputeMainSensitivityIndex(unsigned const targetDim) const;

      ///Compute all the main sensitivities. Rows are outputs, each column is an input.     
      Eigen::MatrixXd ComputeAllMainSensitivityIndices() const;
      
      ///Number of input dimensions
      unsigned InputDim() const;
      
      ///Number of output dimensions
      unsigned OutputDim() const;
      
      
      
    private:
      /**
       * Constructor for a PCE, takes the specification of the variables. Since it
       * can't set up the multi-index family or set coefficients, it's not useful yet.
       * Hence, the constructor is private and not callable by users.
       * @param variables
       * @return
       */
      PolynomialChaosExpansion(VariableCollection::Ptr variables);
      
      PolynomialChaosExpansion();
      
      
      ///Implements evaluation of one vector
      Eigen::VectorXd EvaluateVector(Eigen::VectorXd const& vals) const;
      
      ///The serialization operator.
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
      /**
       * Evaluates one PCE basis function, specified by the input multiIndex and
       * the 1D polynomials given in the VariableCollection, at the input point.
       * NB: Does not apply any coefficients
       *
       * @param multiIndex The multi-index specifying the polynomial basis to evaluate.
       * @param vals The point to evaluate the polynomial basis function.
       * @return The PCE basis function's value.
       */
      double EvaluateOneTerm(Eigen::RowVectorXu const& multiIndex, Eigen::VectorXd const& vals) const;
      
      ///Get the squared normalization of a PCE basis function.
      /**
       * Say p_i is the polynomial basis function specified by the multiIndex. This function
       * returns <p_i * p_i>, the inner product of the squared basis.
       * NB: This function does not change depending on the coefficients.
       */
      double GetOneTermNormalization(Eigen::RowVectorXu const& multiIndex) const;
      
      ///Return a vector with the normalization of all the terms.
      /**
       * This function returns the sqrt of the normalization, sqrt(<p*p>),
       * for each PCE basis function p.
       * NB: This function does not change depending on the coefficients.
       */
      Eigen::VectorXd GetNormalizationVec() const;
      
      /**
       * An internal function to compute the weighted sum of polynomial expansions if you already know which polynomials are included, where polynomials 
       * is a clean copy not used by other expansions. Actually does the addition.
       * */
      static PolynomialChaosExpansion::Ptr ComputeWeightedSum(std::vector<PolynomialChaosExpansion::Ptr> expansions, Eigen::VectorXd const& weights, 
							      VariableCollection::Ptr variables, unsigned int const outputDim, 
							      muq::GeneralUtilities::UnstructuredMultiIndexFamily::Ptr const& polynomials,
							      std::map<unsigned int, std::set<unsigned int> > const& basisMultiIndexCache);
      
      
      ///Holds the specification of the variables
      VariableCollection::Ptr variables;
      
      /**
       * Coefficients of the polynomials. Each row is an output dimension and each
       * column is associated with the terms multiIndexFamily.
       */
      Eigen::MatrixXd coeffs;
      
      /**
       * This mutli-index family defines the terms in the expansion.
       */
      boost::shared_ptr<muq::GeneralUtilities::MultiIndexFamily> terms;
      
    };
    
    ///Provide a way to directly print pointers to pces
    std::ostream& operator<<(std::ostream& output, const PolynomialChaosExpansion::Ptr& pce);
    
  }
}

///Define a hash function
namespace boost
{
  namespace tuples{
    std::size_t hash_value(const boost::tuples::tuple<unsigned int, unsigned int>& b);
  }
}

struct evalPointEqStruct{
  bool operator()(boost::tuple<unsigned int, unsigned int> const& a, boost::tuple<unsigned int, unsigned int> const& b) const;
  
};




#endif /* POLYNOMIALCHAOSEXPANSION_H_ */
