//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * PolynomialChaosExpansionTest.h
 *
 *  Created on: Jan 19, 2011
 *      Author: prconrad
 */

#ifndef POLYNOMIALCHAOSEXPANSIONTEST_H_
#define POLYNOMIALCHAOSEXPANSIONTEST_H_

#include <Eigen/Core>

Eigen::VectorXd pceTestConstantine1(Eigen::VectorXd const& val);
Eigen::VectorXd pceTestConstantine2(Eigen::VectorXd const& val);
Eigen::VectorXd pceTestConstantine3(Eigen::VectorXd const& val);
Eigen::VectorXd pceTestConstantine4(Eigen::VectorXd const& val);


#endif /* POLYNOMIALCHAOSEXPANSIONTEST_H_ */
