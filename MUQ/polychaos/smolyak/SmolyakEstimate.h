//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakEstimate.h
 *
 *  Created on: Jun 7, 2011
 *      Author: prconrad
 */

#ifndef SMOLYAKESTIMATE_H_
#define SMOLYAKESTIMATE_H_

#include <vector>

#include <boost/serialization/access.hpp>

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/AdaptiveMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/FullTensorMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/TotalOrderMultiIndexFamily.h"

namespace muq{
  namespace polychaos{
    
    
    /**
     * This class provides a unified structure for computing Smolyak estimates of
     * tensor products of one dimensional sequences. The common structure manages
     * different estimates, computes how they should be weighted, and controls
     * adaptivity.
     *
     * To allow Smolyak estimates of different things, it is a template class.
     * Currently, it is instantiated with arma::colvec for quadrature and
     * PolynomialChaosExpansion::Ptr for PCEs. The template must provide <<,
     * be assignable, and work as vector<T>.
     *
     * Subclasses must provide a way of computing particular index estimates, a way to
     * take the magnitude of an estimate (for use in error computation), and
     * a way of taking a weighted sum of estimates. They should also take advantage
     * of the boost::serializable framework to provide static LoadProgress and SaveProgress
     * methods.
     *
     * If desired, subclasses my override the computation of local error statistics. Also,
     * you may provide a way to carefully compute the final estimate knowing all the
     * terms included and their weights. A default implementation simply sums the
     * estimates, which could have errors because of the separation of sums in computing
     * the estimates and summing the estimates themselves.
     *
     * Subclasses may also override methods to get more control of the refining process.
     *
     * The two current implementations are for quadrature and PCE generation. Since
     * the implementation of the template is in the cpp file, you must include force
     * it to instantiate the template for any types you want to use.
     */
    template<typename T>
    class SmolyakEstimate {
    public:
      
      SmolyakEstimate(unsigned int numDimensions, unsigned int simplexLimit = 0);
      
      SmolyakEstimate(unsigned int numDimensions, muq::GeneralUtilities::MultiIndexFamily::Ptr limitingSet);
      
      virtual ~SmolyakEstimate();
      
      /**
       * An end user function. Computes a Smolyak estimate using exactly the multi-indices
       * in the input set. As needed, multi-indices are added to make the set admissible.
       */
      T StartFixedTerms(muq::GeneralUtilities::MultiIndexFamily::Ptr fixedTerms);
      
      /** 
       * Add more terms to an already initialized set, forcibly adding the ones in the set.
       **/
      T AddFixedTerms(muq::GeneralUtilities::MultiIndexFamily::Ptr fixedTerms);
      
      /**
       * An end user function. Computes the Smolyak estimate, using at minimum the input terms.
       * If there is time remaining within the limit, it refines the computation adaptively as
       * many times as it can. Will always use more than the secondsLimit time.
       * @return
       */
      T StartAdaptiveTimed(muq::GeneralUtilities::MultiIndexFamily::Ptr initialTerms, double secondsLimit);
      
      /**
       * An end user function, computes the Smolyak estimate until the global error indicator
       * has fallen below the input tolerance. Requires some domain knowledge to select this
       * magnitude in a useful way.
       */
      T StartAdaptiveToTolerance(muq::GeneralUtilities::MultiIndexFamily::Ptr initialTerms, double errorLimit);
      
      ///An overload that automatically creates a simplex of given size to start with. Input must be >0.
      T StartFixedTerms(unsigned int initialSimplexLevels);
      
      ///An overload that adds to an initialized Smolyak algorithm, adding all terms in a given simplex. Input must be >0.
      T AddFixedTerms(unsigned int simplexLevelsToInclude);
      
      ///An overload that automatically creates a simplex of given size to start with.
      T StartAdaptiveTimed(unsigned int initialSimplexLevels, double secondsLimit);
      
      ///An overload that automatically creates a simplex of given size to start with.
      T StartAdaptiveToTolerance(unsigned int initialSimplexLevels, double errorLimit);
      
      ///output the terms used and the final result
      void OutputVerbose(std::string baseName);
      
      /**
       * Continues adapting an instance that has already started, for example one that was
       * re-loaded, and adds terms adaptively for up to the time limit. As desired,
       * you may indicate that some of that limit has already elapsed.
       * 
       * Uses boost timing functions, good to microseconds.
       */
      T AdaptForTime(double secondsLimit, double timeElapsed);
      
      /**
       * Continues adapting an instance that has already started, for example one that was
       * re-loaded, and adds terms adaptively until the global error tolerance is reached.
       */
      T AdaptToTolerance(double errorLimit);
      
      double GetGlobalErrorIndicator();
      
      virtual unsigned int ComputeWorkDone() const =0;
      
      ///Return the traces of work done and the estimates along the way
      std::vector<T>* GetEstimateTrace();
      std::vector<unsigned int>* GetWorkTrace();
      
      /**
       * Find the top numberToExpand terms to expand, based on the current local error indicators.
       * Disregards any limitingSet there might be, because this is designed for the case where 
       * you specifically want to grow the limitingSet.
       * 
       * If you change the behavior of IsTermEligibleForExpansion not to be any
       * multi-index with admissible forward neighbors, this will be wrong.
       **/
      //Eigen::MatrixXu GetTopNewOrdersToAdd(unsigned int const numberToExpand);
      
    private:
      ///Make estimates serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
      /**
       * The most important pure abstract function for subclasses to override, is responsible for actually
       * computing a particular multi-index estimate of the value.
       */
      virtual T ComputeOneEstimate(Eigen::RowVectorXu const& multiIndex)=0;
      
      /**
       * A pure abstract function for subclasses to override, must sum the termEstimates with
       * multiplicative weights. Returns a pointer to the result.
       */
      virtual T WeightedSumOfEstimates(Eigen::VectorXd const& weights) const =0;
      
      ///Compute the magnitude of the input estimate, used for error estimation
      virtual double ComputeMagnitude(T estimate)=0;
      
      /**
       * If desired, the user may provide an expensive version of the weighted sum to use
       * when computing the global estimate. This function is called exactly once, and is
       * designed to allow the user to avoid numerical issues by first computing the
       * termEstimates and then summing. Instead, this code may re-derive all the terms and sum
       * all the raw components from scratch. Typically not needed since the estimates are usually
       * of similar magnitudes, so the default is just to call the standard, slightly less accurate
       * version.
       */
      virtual T PreciseWeightedSumOfEstimates(Eigen::VectorXd const& weights) const;
      
      /**
       * Computes the global error indicator. The default implementation sums the local
       * error indicator of all the terms that currently be expanded. This avoids having
       * error the adaptive algorithm may not directly attack, since that causes the adaptivity
       * to wander aimlessly while it adds the term that will allow it to attack the
       * large terms.
       * @return
       */
      double ComputeGlobalErrorIndicator();
      
      /**
       * Compute the local error indicator. The default is simply the magnitude of the
       * differential estimate of the term.
       */
      virtual double ComputeLocalErrorIndicator(unsigned int const termIndex);
      
      /**
       * Determine whether the term may be expanded. The default
       * is that any non-buried index may be forcibly expanded.
       */
      virtual bool IsTermEligibleForExpansion(unsigned int const termIndex);
      
      
      
      /**
       * Compute the estimates and update the differential error estimates and weights
       * to actually include the terms specified by the indices, which have already
       * been added to the termsIncluded multi-index family.
       * @param newTermIndices a list of the indices from termsIncluded to actually compute
       */
      void AddBatchOfTerms(Eigen::VectorXu const& newTermIndices);
      
      /**
       * An overload that will add the input multi-indices to the set.
       **/
      void AddBatchOfTerms(muq::GeneralUtilities::MultiIndexFamily::Ptr const multiIndicesToAdd);
      
      /**
       * This function assumes that the termsIncluded multi-index family may have expanded,
       * and expands the other storage to ensure that they are large enough.
       */
      void ExpandStorage();
      
      ///Compute a colvec of the combinatorial coefficients of the tensor rules.
      /**
       * Computes the coefficients for the differential integral given by the multiIndex
       * in adaptiveIndexFamily specified by the input index. Actually iterates over the
       * neighbors so it makes no assumptions on dimension or isotropy.
       *
       * @param index Corresponds to the adaptiveIndexFamily, selecting the tensor index
       * we need to update the coefficients for.
       * @return A vector with a coefficient for each tensor rule in the Smolyak construction.
       */
      void ComputeDifferentialCoeff(unsigned int const index, Eigen::VectorXd& coeff) const;
      
      /**
       * Consider the quadrature case. Although you know which multiIndex terms
       * are included in the computation, the accuracy is not known obvious unless
       * you figure out how accurate the quadrature rules used are. They may be
       * non-linear or isotropic.
       *
       * Subclasses may provide a method that computes the effective multi-indices
       * that represent the accuracy of the method. (hence must return a umat).
       *
       * The default implementation just returns the multi-indices used.
       */
      virtual Eigen::MatrixXu GetEffectiveIncludedTerms();
      
      
    protected:
      
      ///Protected default constructor
      SmolyakEstimate();
      
      
      /**
       * Refine should select new terms to add, add them to the termsIncluded adaptive set,
       * and return the indices (not multiIndices) to the ones to add. If old estimates need fixing, do that here.
       * The default implementation forcibly expands the term with the largest ComputeLocalErrorIndicator,
       * and returns any indices that got added in the process.
       * @param
       */
      virtual bool Refine(Eigen::VectorXu& newTermIndices);
      
      /**
       * Make sure every term in the input multi-index family is in the adaptive set
       * (forcibly activate them), then compute their values and update weights
       * and estimates accordingly. Assumes no terms have been computed yet; it will
       * recompute all terms that exist after adding the ones in the input.
       */
      void ComputeInitialTerms(muq::GeneralUtilities::MultiIndexFamily::Ptr const initialTerms);
      
      /**
       * Specifies the approximations terms used by this Smolyak estimate. Provides a mapping
       * between the multi-indices and scalar (unsigned int) indices, used throughout the code.
       * This is always an adaptive multi-index family, because they enforce admissibility of
       * the index set.
       */
      muq::GeneralUtilities::AdaptiveMultiIndexFamily::Ptr termsIncluded;
      
      /**
       * Maps from the scalar indices to the individual term estimates.
       */
      std::vector<T> termEstimates;
      
      /**
       * Holds the magnitudes of the differential estimates, which are typically used for error
       * estimation, although user code can ensure that these values don't do anything.
       */
      std::vector<double> differentialMagnitudes;
      
      
      /**
       * The current weighting of the terms used to create the full estimate. These values are always
       * integers because Smolyak weighting is combinatorial.
       */
      Eigen::VectorXd termWeights;
      
      ///A cache of the neighbors used to compute differential integrals.
      //FullTensorMultiIndexFamily::Ptr differentialNeighborCache;
      
      //holds the final estimate
      T finalEstimate;
      
      void RecordAdaptProgress();
      
      ///The estimates computed along the way
      std::vector<T> estimatesTrace;
      ///The work computed along the way by the refinement process
      std::vector<unsigned int> workDoneTrace;
      
    };
    
  }
}


#endif /* SMOLYAKESTIMATE_H_ */
