//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakTraditionalPCEFactory.h
 *
 *  Created on: Jun 13, 2011
 *      Author: prconrad
 */

#ifndef SMOLYAKTRADITIONALPCEFACTORY_H_
#define SMOLYAKTRADITIONALPCEFACTORY_H_

#include <boost/shared_ptr.hpp>

#include "polychaos/smolyak/SmolyakEstimate.h"
#include "polychaos/pce/PolynomialChaosExpansion.h"

class VariableCollection;
class FunctionWrapper;

/**
 * An implementation for performing the traditional way of generating PCEs. I'm not
 * commenting it because you shouldn't use it - it's only here to demonstrate why it's bad.
 *
 *  It's a lot like SmolyakPCEFactory if you're curious, except that you recompute
 *  the old index estimates to include the any new polynomial terms you add to the set.
 */
class SmolyakTraditionalPCEFactory: public SmolyakEstimate<PolynomialChaosExpansion::Ptr> {
public:
	SmolyakTraditionalPCEFactory(boost::shared_ptr<VariableCollection> inVariables,
			boost::shared_ptr<FunctionWrapper> inFn);
	virtual ~SmolyakTraditionalPCEFactory();
	
	virtual unsigned int ComputeWorkDone() const;

private:
	typedef SmolyakEstimate<PolynomialChaosExpansion::Ptr> super; //define the type super for convenience

	PolynomialChaosExpansion::Ptr ComputeOneEstimate(arma::urowvec const& multiIndex);

	/**
	 * A pure abstract function for subclasses to override, must sum the termEstimates with
	 * multiplicative weights. Returns a pointer to the result.
	 */
	PolynomialChaosExpansion::Ptr WeightedSumOfEstimates(arma::colvec const& weights) const;

	///Compute the magnitude of the input estimate, used for error estimation
	double ComputeMagnitude(PolynomialChaosExpansion::Ptr estimate);

	double ComputeLocalErrorIndicator(unsigned int const termIndex);

	///actually return the orders of the quadrature
	arma::umat GetEffectiveIncludedTerms();

	bool Refine(arma::ucolvec& newTermIndices);

	boost::shared_ptr<VariableCollection> variables;

	boost::shared_ptr<FunctionWrapper> fn;
};

#endif /* SMOLYAKTRADITIONALPCEFACTORY_H_ */
