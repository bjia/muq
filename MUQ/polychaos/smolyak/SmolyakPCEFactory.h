//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakPCEFactory.h
 *
 *  Created on: Jun 12, 2011
 *      Author: prconrad
 */

#ifndef SMOLYAKPCEFACTORY_H_
#define SMOLYAKPCEFACTORY_H_

#include <string.h>
#include <set>
#include <map>

#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>

#include "MUQ/GeneralUtilities/multiIndex/UnstructuredMultiIndexFamily.h"
#include "MUQ/polychaos/smolyak/SmolyakEstimate.h"
#include "MUQ/polychaos/pce/PolynomialChaosExpansion.h"

namespace muq{
  namespace GeneralUtilities{
    class FunctionWrapper;
  }
}

namespace muq{
  namespace polychaos{
    
    class VariableCollection;
    /**
     * An implementation of Smolyak estimates for PCEs. This version constructs the
     * sparse interpolant, summing the PCEs associated with full tensor estimates
     * of the PCE.
     *
     * Probably best used with exponential growth or nested quadrature rules. Note
     * that if you use single step growth, it may fail to notice that a function
     * is even or odd, and may stop prematurely. Avoid this by using a
     * LinearGrowthQuadratureFamily1D and set the spacing to 2.
     *
     * The VariableCollection specifies the 1D quadrature rules to use and the associated
     * orthogonal polynomial families,
     * and the FunctionWrapper specifies the function.
     */
    class SmolyakPCEFactory: public SmolyakEstimate<PolynomialChaosExpansion::Ptr> {
    public:
      typedef boost::shared_ptr<SmolyakPCEFactory> Ptr;
      
      /**
       * Initialize, providing the variables and function to approximate.
       */
      SmolyakPCEFactory(boost::shared_ptr<VariableCollection> inVariables,
			boost::shared_ptr<muq::GeneralUtilities::FunctionWrapper> inFn,
			unsigned int simplexLimit=0);
      
      SmolyakPCEFactory(boost::shared_ptr<VariableCollection> inVariables,
			boost::shared_ptr<muq::GeneralUtilities::FunctionWrapper> inFn,
			muq::GeneralUtilities::MultiIndexFamily::Ptr limitingSet);
      
      
      /**
       * @brief This computes with a fixed set of polynomials, adding quadrature as required.
       *
       * @param polys A matrix, the rows are the orders of the polynomials
       * @return A pointer to the PCE
       **/
      PolynomialChaosExpansion::Ptr ComputeFixedPolynomials(Eigen::MatrixXu polys);
      
      
      virtual ~SmolyakPCEFactory();
      
      ///Load progress from a file, the data ends up in the results directory.
      /**
       * Because saving function pointers is hard, you must provide a FunctionWrapper
       * that is the same as the one used when this was saved.
       */
      static Ptr LoadProgress(std::string baseName, boost::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn);
      
      ///Save the progress so far using boost::serialization. Writes to results/
      static void SaveProgress(SmolyakPCEFactory::Ptr pceFactory, std::string baseName);
      
      virtual unsigned int ComputeWorkDone() const;
      
      ///Asks the quadrature rules what their precise polynomial order is.
      Eigen::MatrixXu GetEffectiveIncludedTerms();
      
      
      ///Return the function we're constructing
      boost::shared_ptr<muq::GeneralUtilities::FunctionWrapper> GetFunction();
      
    private:
      
      SmolyakPCEFactory();
      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
      /**
       * Implements the method for computing a single multi-index estimate.
       *
       * It computes the coefficients of a PCE with full tensor quadrature of order
       * of the multiIndex. The PCE includes all the terms whose order is less than half
       * the total GetPrecisePolyOrder of the quadrature rule.
       */
      PolynomialChaosExpansion::Ptr ComputeOneEstimate(Eigen::RowVectorXu const& multiIndex);
      
      /**
       * Computes the weighted sum of polynomials. Grabs the terms from within the parent
       * and returns a pointer to the result.
       */
      PolynomialChaosExpansion::Ptr WeightedSumOfEstimates(Eigen::VectorXd const& weights) const;
      
      /**
       * Compute the magnitude of a PCE by summing the absolute value of the coefficients
       * times the magnitude of the polynomial. Used for error estimation.
       */
      double ComputeMagnitude(PolynomialChaosExpansion::Ptr estimate);
      
      
      
      boost::shared_ptr<VariableCollection> variables;
      
      boost::shared_ptr<muq::GeneralUtilities::FunctionWrapper> fn;
      
      muq::GeneralUtilities::UnstructuredMultiIndexFamily::Ptr polynomialBasisTermsEstimated;
      
      //For a map that goes from the index of the polynomial term to the index of the Smolyak
      //indices that create them
      std::map<unsigned int, std::set<unsigned int> > basisMultiIndexCache;
      
    };
    
  }
}



#endif /* SMOLYAKPCEFACTORY_H_ */
