//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakQuadrature.h
 *
 *  Created on: Jun 10, 2011
 *      Author: prconrad
 */

#ifndef SMOLYAKQUADRATURE_H_
#define SMOLYAKQUADRATURE_H_

#include <string.h>

#include <boost/function.hpp>
#include <boost/serialization/access.hpp>
#include <boost/shared_ptr.hpp>
#include <Eigen/Core>

#include "MUQ/polychaos/smolyak/SmolyakEstimate.h"

namespace muq{
  namespace GeneralUtilities{
    class FunctionWrapper;
  }
}

namespace muq{
  namespace polychaos{
    
    class VariableCollection;
    
    
    /**
     * Implements Smolyak estimation for quadrature of functions
     * that have inputs and outputs as colvecs.
     *
     * The VariableCollection specifies the 1D quadrature rules
     * to use, and the FunctionWrapper specifies the function.
     */
    class SmolyakQuadrature: public SmolyakEstimate<Eigen::VectorXd> {
    public:
      typedef boost::shared_ptr<SmolyakQuadrature> Ptr;
      ///default constructor ONLY for serialization
      SmolyakQuadrature();
      
      ///Initialize the function and variables.
      SmolyakQuadrature(boost::shared_ptr<VariableCollection> inVariables,
			boost::shared_ptr<muq::GeneralUtilities::FunctionWrapper> inFnWrapper);
      virtual ~SmolyakQuadrature();
      
      
      /**
       * Load previously saved progress. Because saving function pointers is hard,
       * you must provide a function wrapper with the same function as before.
       */
      static Ptr LoadProgress(std::string baseName, boost::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn);
      
      /**
       * Save the progress so far using boost::serialization. Writes to results/
       */
      static void SaveProgress(SmolyakQuadrature::Ptr quad, std::string baseName);
      
      virtual unsigned int ComputeWorkDone() const;
    private:
      
      ///Make estimates serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
            
      /**
       * Computes the full tensor quadrature of order multiIndex as defined
       * by the 1D quadrature rules in the variable collection.
       */
      Eigen::VectorXd ComputeOneEstimate(Eigen::RowVectorXu const& multiIndex);
      
      /**
       * Computes the weighted sum using simple linear algebra.
       */
      Eigen::VectorXd WeightedSumOfEstimates(Eigen::VectorXd const& weights) const;
      
      ///Computes the magnitude as the max of the abs of the estimate.
      virtual double ComputeMagnitude(Eigen::VectorXd estimate);
      
      ///Asks the quadrature rules what their precise polynomial order is.
      Eigen::MatrixXu GetEffectiveIncludedTerms();
      
      boost::shared_ptr<VariableCollection> variables;
      boost::shared_ptr<muq::GeneralUtilities::FunctionWrapper> fnWrapper;
    };

    
  }
}


#endif /* SMOLYAKQUADRATURE_H_ */
