//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * QuadratureTestFunctions.h
 *
 *  Created on: Jan 24, 2011
 *      Author: prconrad
 */

#ifndef QUADRATURETESTFUNCTIONS_H_
#define QUADRATURETESTFUNCTIONS_H_

#include <Eigen/Core>

namespace muq{
  namespace polychaos{
    
    /**
     * This file includes a collection of functions used to test the quadrature classes.
     * 
     * These are not for users, as the definitions are not actually included in the library.
     * @param vars
     * @return
     */
    
    /**
     * A 1D,  2nd order polynomial. Integrated over [-1,-1] with w(x)=1 gives
     * analytic integral: 7+1/3
     */
    Eigen::VectorXd simplePoly1D(Eigen::VectorXd const& vars);
    
    /**
     * A 3D,  2nd order polynomial. Integrated over [-1,-1] with w(x)=1 gives
     * analytic integral: 8/9
     */
    Eigen::VectorXd simplePoly3D(Eigen::VectorXd const& vars);
    
    /**
     * From the Gerstner Gribel DASQ paper, Figure 6, left-most element.
     * Matlab symbolic answer: 10*sqrt(math::pi())*erf(1) = 14.9364826562485405
     */
    Eigen::VectorXd dasqTest2DIso1(Eigen::VectorXd const& vars);
    
    /**
     * From the Gerstner Gribel DASQ paper, Figure 6, middle element.
     * Matlab symbolic answer: Ei(1) - eulergamma + 2*exp(1) - 2 = 4.75446580837249437
     */
    Eigen::VectorXd dasqTest2DIso2(Eigen::VectorXd const& vars);
    
    /**
     * From the Gerstner Gribel DASQ paper, Figure 6, right-most element.
     * Matlab symbolic answer: (pi*erf(10^(1/2))^2)/40 = 0.0785385998857827117
     */
    Eigen::VectorXd dasqTest2DIso3(Eigen::VectorXd const& vars);
    
    /**
     * From the Gerstner Gribel DASQ paper, Figure 7, left-most element.
     * Matlab symbolic answer: (11*pi^(1/2)*erf(1))/2 = 8.21506546093669728
     */
    Eigen::VectorXd dasqTest2DAniso1(Eigen::VectorXd const& vars);
    
    /**
     * From the Gerstner Gribel DASQ paper, Figure 7, middle element.
     * Matlab symbolic answer: 10*Ei(1) - 10*eulergamma + 11*exp(1) - 11 = 32.0801216275935365
     */
    Eigen::VectorXd dasqTest2DAniso2(Eigen::VectorXd const& vars);
    
    /**
     * From the Gerstner Gribel DASQ paper, Figure 7, right-most element.
     * Matlab symbolic answer: (pi*2^(1/2)*erf(5^(1/2))*erf(10^(1/2)))/40 = 0.110897342159703106
     */
    Eigen::VectorXd dasqTest2DAniso3(Eigen::VectorXd const& vars);
    
  }
}


#endif /* QUADRATURETESTFUNCTIONS_H_ */
