//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * QuadratureFamily1D.h
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#ifndef QUADRATUREFAMILY1D_H_
#define QUADRATUREFAMILY1D_H_

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <Eigen/Core>

namespace muq{
  namespace polychaos{
    
    /**
     * An abstract class that defines a family of quadrature points in 1D. Subclasses actually
     * implement some family of nodes and weights, e.g. Gauss-Legendre integration.
     *
     * Subclasses must provide a way of deriving the nodes and weights, ComputeNodesAndWeights()
     * and a function that provides the precise polynomial order a particular rule is useful
     * for, GetPrecisePolyOrder(). This measure may be approximate if desired, but the idea
     * is that users can ensure that they're selecting a quadrature order sufficient for
     * the order of their particular polynomial, and use this function as a guide. This
     * metric should be monotonically increasing with order of the rules, but need not follow
     * any other pattern.
     *
     * This class provides caching of the rules and a common access interface, through the
     * GetWeights() and GetNodes() functions. This way, code using 1D quadrature rules
     * can easily swap between different types of rules. Coding particular quadrature rules is
     * simple because you merely need to implement the rule-specific method for deriving the
     * nodes and weights.
     *
     * This caching means that instances of these subclasses, in general, should be reused
     * as much as possible.
     *
     *  Expected implementations include Gaussian integration, Clenshaw Curtis, Gauss-Patterson, etc.
     */
    class QuadratureFamily1D {
    public:
      typedef boost::shared_ptr<QuadratureFamily1D> Ptr;
      
      QuadratureFamily1D();
      virtual ~QuadratureFamily1D();
      
      /**
       * Returns the weights for 1D quadrature of the input order. Computes the weights as needed.
       * @param order
       * @return A pointer to rowvec of length order, contains the weights for the quadrature rule.
       */
      boost::shared_ptr<Eigen::RowVectorXd> GetWeights(unsigned int const order);
      
      /**
       * Returns the nodes for 1D quadrature of the input order. Computes the weights as needed.
       * @param order
       * @return A pointer to rowvec of length order, contains the nodes for the quadrature rule.
       */
      boost::shared_ptr<Eigen::RowVectorXd> GetNodes(unsigned int const order);
      
      ///Get the order of the polynomial this order integration rule is good for.
      virtual unsigned int GetPrecisePolyOrder(unsigned int const order) const =0;
      
    private:
      ///Make class serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      /**
       * A private function, called if a Get method tries a cache lookup and misses.
       * Calls the virtual method ComputeNodesAndWeights, and stores the result in the cache.
       */
      void AddOrderToCache(unsigned int const order);
      
      ///Cache of nodes for various orders quadrature rules
      std::vector<boost::shared_ptr<Eigen::RowVectorXd> > nodes;
      
      ///Cache of weights for various order quadrature rules
      std::vector<boost::shared_ptr<Eigen::RowVectorXd> > weights;
      
      ///Implementations must provide a way of deriving the nth rule.
      /**
       * Compute the rule of the input order, and return the nodes and weights in the rowvecs.
       */
      virtual void ComputeNodesAndWeights(unsigned int const order,
					  boost::shared_ptr<Eigen::RowVectorXd>& nodes, boost::shared_ptr<Eigen::RowVectorXd>& weights) const =0;
					  
    };
    
  }
}
#endif /* QUADRATUREFAMILY1D_H_ */
