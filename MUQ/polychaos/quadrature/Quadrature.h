//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * Quadrature.h
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#ifndef QUADRATURE_H_
#define QUADRATURE_H_


#include <boost/shared_ptr.hpp>
#include <Eigen/Core>


namespace muq{
  namespace GeneralUtilities{
    class FunctionWrapper;
  }
}

namespace muq{
  namespace polychaos{
    
///forward declaration avoids inclusion
class VariableCollection;


/**
 * An abstract class for performing numerical quadrature of functions from
 * R^n -> R^m. Inputs and outputs to functions are represented as arma::colvec.
 *
 * There are two essential methods:
 * ComputeIntegral() - takes a function pointer and computes the numeric integral
 * GetNodesAndWeights() - return (by reference) the nodes and weights for the user
 *
 * The latter option is probably most useful for the computation of many integrals,
 * or where the user needs to control the evaluation of the integrand. The nodes
 * computation is pure virtual and needs to be provided by a subclass.
 *
 * The constructor takes a specification of the input variables to the function,
 * which also define the per-dimension 1D quadrature rule to use, which also determines
 * the domain and a weight function for the integral, if any.
 *
 * ComputeIntegral() will actually produce the integral, taking a FunctionWrapper,
 * which use functions from colvecs to colvecs.
 *
 * This class, and its subclasses, often rely on MultiIndexFamilies to help construct
 * the grid of quadrature points and iterate over them.
 */
class Quadrature {
public:
	/**
	 * Initialize the quadrature rule with the variables. Subclasses must also
	 * take in some specification of the order of the desired quadrature rule, if
	 * appropriate. Subclasses should also call this one. This class keeps a pointer
	 * to the input collection.
	 */
	Quadrature(boost::shared_ptr<VariableCollection> variables);
	virtual ~Quadrature();

	///Compute the nodes and weights of this quadrature rule
	/**
	 * A pure virtual function that computes and returns nodes and weights for this quadrature rule.
	 * @param nodes a matrix that will contain the nodes, where each col in the matrix
	 * is a node
	 * @param weights a column vector with the weights associated with each row of the nodes
	 * matrix
	 * @return
	 */
	virtual void GetNodesAndWeights(Eigen::MatrixXd& nodes, Eigen::VectorXd& weights) = 0;

	/**
	 * A function to compute a single integral. Calls GetNodesAndWeights and then evaluates f,
	 * a pointer to the function to integrate, at the nodes.
	 *
	 * @param f A function pointer to f, the integrand. The integrand takes a rowvec and
	 * returns a double.
	 * @return the integral, as evaluated by the selected quadrature rule.
	 */
	virtual Eigen::VectorXd ComputeIntegral(boost::shared_ptr< muq::GeneralUtilities::FunctionWrapper> fn);

	/**
	 * Pointer to the variable collection specification for this quadrature problem.
	 * which specifically provides references to the 1D quadrature rules for each dimension.
	 * Probably shouldn't be public, but it's easier.
	 *
	 * Does not clean up the variables, as they are a structure that is passed in!
	 */
	boost::shared_ptr<VariableCollection> variables;

};

  }
}

#endif /* QUADRATURE_H_ */
