//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussPattersonQuadrature1D.h
 *
 *  Created on: Jan 24, 2011
 *      Author: prconrad
 */

#ifndef CLENSHAWCURTISQUADRATURE1D_H_
#define CLENSHAWCURTISQUADRATURE1D_H_

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>

#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"

namespace muq{
  namespace polychaos{
    /**
     * Implements Clenshaw Curtis quadrature, based on Chebyshev interpolation. Rules are
     * precomputed by using http://www.sam.math.ethz.ch/~waldvoge/Papers/fejer.html
     * 
     * Stored in data/Chenshaw_Curtis. Indexed by hierarchical level, corresponding to points
     * n = 1,3,5,9,17,33,65,129, ..., 32769. More can be computed.
     **/
    class ClenshawCurtisQuadrature1D: public QuadratureFamily1D {
    public:
      ClenshawCurtisQuadrature1D();
      virtual ~ClenshawCurtisQuadrature1D();
      
      typedef boost::shared_ptr<ClenshawCurtisQuadrature1D> Ptr;
      
      ///Uses a lookup table to return the approximate orders
      /**
       * Taken from http://people.sc.fsu.edu/~jburkardt/cpp_src/patterson_rule/patterson_rule.html
       * Uses the actual accuracy, using the symmetry of the rule, which buys us +1 for all 
       * but the first rule.
       **/
      unsigned int GetPrecisePolyOrder(unsigned int const order) const;
      
    private:
      
      ///Make class serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      ///Return the pre-computed nodes and weights
      virtual void ComputeNodesAndWeights(unsigned int const  order,
					  boost::shared_ptr<Eigen::RowVectorXd>& nodes,
					  boost::shared_ptr<Eigen::RowVectorXd>& weights) const;
    };
    
  }
}

#endif /* CLENSHAWCURTISQUADRATURE1D_H_ */
