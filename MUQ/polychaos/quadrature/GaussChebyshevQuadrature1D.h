//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussChebyshevQuadrature1D.h
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */

#ifndef GAUSSCHEBYSHEVQUADRATURE1D_H_
#define GAUSSCHEBYSHEVQUADRATURE1D_H_

#include <boost/serialization/access.hpp>
#include <Eigen/Core>

#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"

namespace muq{
  namespace polychaos{
    
/**
 * Implements Gauss-Chebyshev quadrature, i.e. weighted with 1/sqrt(1-x)^2 over [-1,1].
 *
 * This class directly implements QuadratureFamily1D because there is an analytic form
 * for the nodes and weights.
 */
class GaussChebyshevQuadrature1D: public QuadratureFamily1D {
public:
	GaussChebyshevQuadrature1D();
	virtual ~GaussChebyshevQuadrature1D();


	///Implements the polynomial order. Gauss-Chebyshev accuracy is approximate.
	unsigned int GetPrecisePolyOrder(unsigned int const order) const;

private:
        ///Make class serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      
	///Implementations must provide a way of deriving the nth rule
	virtual void ComputeNodesAndWeights(unsigned int const order,
			boost::shared_ptr<Eigen::RowVectorXd>& nodes,
			boost::shared_ptr<Eigen::RowVectorXd>& weights) const;
};

  }
}
#endif /* GAUSSCHEBYSHEVQUADRATURE1D_H_ */
