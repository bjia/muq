//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussPattersonQuadrature1D.h
 *
 *  Created on: Jan 24, 2011
 *      Author: prconrad
 */

#ifndef GAUSSPATTERSONQUADRATURE1D_H_
#define GAUSSPATTERSONQUADRATURE1D_H_

#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>

#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"

namespace muq{
  namespace polychaos{
    
    /**
     * Implements Gauss-Patterson Quadrature, which is based on the 3-point Gauss-Legendre
     * rule, in the interval [-1,1] (w(x) = 1). Nodes are pre-computed and indexed by level.
     * Levels 1-8,  corresponding to 1, 3, 7, 15, 31, 63, 127, or 255 points are available.
     * 
     * There are two styles of indexing. Standard has the above eight indices. Non-standard 
     * mimics the indexing style of Gaussian quadrature, so 1-1pt, 2-3pt, 3-3pt, 4-7pt, 5-7pt, 6-7pt, and so on.
     * Non-standard indexing is not advised and is included for the HYCOM analysis.
     **/
    class GaussPattersonQuadrature1D: public QuadratureFamily1D {
    public:
      GaussPattersonQuadrature1D(bool standardIndexing = true);
      virtual ~GaussPattersonQuadrature1D();
      
      typedef boost::shared_ptr<GaussPattersonQuadrature1D> Ptr;
      
      ///Uses a lookup table to return the approximate orders
      /**
       * Taken from http://people.sc.fsu.edu/~jburkardt/cpp_src/patterson_rule/patterson_rule.html
       * Uses the actual accuracy, using the symmetry of the rule, which buys us +1 for all 
       * but the first rule.
       **/
      unsigned int GetPrecisePolyOrder(unsigned int const order) const;
      
    private:
      bool standardIndexing;
      
      ///Make class serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
      ///Return the pre-computed nodes and weights
      virtual void ComputeNodesAndWeights(unsigned int const  order,
					  boost::shared_ptr<Eigen::RowVectorXd>& nodes,
					  boost::shared_ptr<Eigen::RowVectorXd>& weights) const;
    };
    
  }
}
#endif /* GAUSSPATTERSONQUADRATURE1D_H_ */
