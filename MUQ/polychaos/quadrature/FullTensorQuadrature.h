//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * FullTensorQuadrature.h
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#ifndef FULLTENSORQUADRATURE_H_
#define FULLTENSORQUADRATURE_H_


#include <boost/shared_ptr.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/polychaos/quadrature/Quadrature.h"

namespace muq{
  namespace polychaos{
    
    class VariableCollection;
    
    /**
     * The simplest possible quadrature rule, a full tensor expansion.
     * Creates a the complete tensor product of the points of the given order
     * 1D quadrature rules for each dimension. The 1D quadrature order can
     * be isotropic or vary per dimension. Note that an isotropic order
     * does not mean isotropic number of points in each dimension, if different
     * 1D quadrature rules are used.
     *
     * Probably not the most useful class for actual analysis, because you have to know exactly
     * what order you want, but is an important building block for sparse
     * quadrature routines.
     */
    class FullTensorQuadrature: public Quadrature {
    public:
      typedef boost::shared_ptr<FullTensorQuadrature> Ptr;
      
      ///Isotropic order
      FullTensorQuadrature(boost::shared_ptr<VariableCollection> variables, unsigned int const order);
      
      ///Non-isotropic order
      FullTensorQuadrature(boost::shared_ptr<VariableCollection> variables, Eigen::RowVectorXu const& order);
      virtual ~FullTensorQuadrature();
      
      ///Implementation of the nodes and weights computation.
      void GetNodesAndWeights(Eigen::MatrixXd& nodes, Eigen::VectorXd& weights);
      
    private:
      ///Holds the anisotropic order of the quadrature
      Eigen::RowVectorXu order;
    };
    
  }
}

#endif /* FULLTENSORQUADRATURE_H_ */
