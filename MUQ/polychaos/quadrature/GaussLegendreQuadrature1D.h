//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussLegendreQuadrature1D.h
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#ifndef GAUSSLEGENDREQUADRATURE1D_H_
#define GAUSSLEGENDREQUADRATURE1D_H_


#include <boost/serialization/access.hpp>
#include <Eigen/Core>

#include "MUQ/polychaos/quadrature/GaussianQuadratureFamily1D.h"

namespace muq{
  namespace polychaos{
    
    
    /**
     * Impelements 1D gaussian quadrature for legendre rules, that is, uniform unity weight
     * over domain [-1,1] by extending the virtual class GaussianQuadratureFamily1D.
     */
    class GaussLegendreQuadrature1D: public GaussianQuadratureFamily1D {
    public:
      GaussLegendreQuadrature1D();
      virtual ~GaussLegendreQuadrature1D();
      
      ///Implements the function that provides the monic  coefficients.
      virtual Eigen::RowVectorXd GetMonicCoeff(unsigned int const j) const;
      
      ///Implements the function that provides the integral of the weight function over the domain.
      virtual double IntegralOfWeight() const;
      
    private:
      
      ///Make class serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
      
    };
  }
}
#endif /* GAUSSLEGENDREQUADRATURE1D_H_ */
