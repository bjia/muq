/*
 * GenzFunctions.h
 *
 *  Created on: Aug 10, 2011
 *      Author: prconrad
 */

#ifndef GENZFUNCTIONS_H_
#define GENZFUNCTIONS_H_

#include <boost/function.hpp>

/**
 * @brief Return a random instance of the Genz function requested.
 *
 * Uses boost::function pointers to return an instance that has used boost::bind to set
 * the w and c parameters. Thus, you get a colvec->colvec function, as needed by the package.
 * Works for arbitrary input dimension.
 * 
 * Applies normalization, as taken from: Algorithm 847: spinterp: Piecewise
 * Multilinear Hierarchical Sparse Grid Interpolation in MATLAB by ANDREAS KLIMKE and BARBARA WOHLMUTH
 * 
 * Note that the functions produced are shifted from interval [0,1] as their domain to [-1,1].
 * 
 * @param type Indicates which type of function you want.
 * @param dim The dimension of the input.
 * @return :function The constructed random Genz function.
 **/
boost::function<arma::colvec (arma::colvec)> GetRandomGenzFunction(unsigned int type, unsigned int dim);


/**
 * @brief Impelment the six Genz functions.
 *
 * @param x The input of the function.
 * @param w The first parameter.
 * @param c The second parameter.
 * @return :colvec
 **/
arma::colvec genzOsc(arma::colvec x, arma::colvec w, arma::colvec c);
arma::colvec genzProdPeak(arma::colvec x, arma::colvec w, arma::colvec c);
arma::colvec genzCornerPeak(arma::colvec x, arma::colvec w, arma::colvec c);
arma::colvec genzGaussian(arma::colvec x, arma::colvec w, arma::colvec c);
arma::colvec genzContinuous(arma::colvec x, arma::colvec w, arma::colvec c);
arma::colvec genzDiscontinuous(arma::colvec x, arma::colvec w, arma::colvec c);


/**
 * @brief An extra Genz style function, which has independent terms for the first three inputs, and the rest.
 * 
 * Constructed as the sum of genzOsc for the first three inputs and genzCornerPeak for the rest. I made up
 * the parameter normalizations.
 **/
arma::colvec genzMixed(arma::colvec x, arma::colvec w, arma::colvec c);
#endif /* GENZFUNCTIONS_H_ */
