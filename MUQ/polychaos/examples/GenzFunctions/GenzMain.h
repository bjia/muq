#ifndef GENZMAIN_H_
#define GENZMAIN_H_


#include <boost/function.hpp>
#include <boost/tuple/tuple.hpp>

#include "polychaos/pce/PolynomialChaosExpansion.h"

/**
 * @brief Test the Genz functions using several different approaches.
 * 
 * Note: This code, with 25 examples and 5 dimensions, takes about 10 hours to run on a fast desktop!
 * See the results/documentation to get the answer directly, or run it with just a few examples. Once
 * data is produced, code in polychaos/matlab/GenzFunctions can turn it into plots.
 * 
 * This code examines the performance of full tensor, isotropic (linear and exponential growth) and
 * adaptive (linear and exponential growth) sparse pseudospectral approximations on the Genz test functions.
 * We also construct a new Genz-like function that has very little dependence between dimnsions, it has
 * the form f = f(x_1, x_2, x_3) + g(x_4, x_5...), which is nice to show off adaptivity, as the Genz
 * functions are pretty isotropic.
 * 
 * Generally shows how to construct and run adaptive problems.
 **/

/**
 * @brief ...Estimate the error of the approximation.
 *
 * Estimate the approximation error by trying 2000 points and returning a tuple that approximates (L_2, L_inf)
 * that is, the L2 error and the L-infinity (max pointwise) error.
 * 
 * @param trueFn A pointer to a colvec->colvec function.
 * @param approxFn A pointer to a constructed PCE.
 * @param inputDim the size of the problem
 * @return A boost tuple with estimates (L_2, L_\infty)
 **/
boost::tuple<double, double> EstimateApproximationError(boost::function<arma::colvec (arma::colvec)> trueFn,
							PolynomialChaosExpansion::Ptr approxFn, unsigned int inputDim);


/**
 * @brief Run the Genz benchmark for all the types for a number of examples.
 *
 * Outputs results to polychaos/examples/GenzFunctions/ as .m files we can simply run to 
 * load the data into sensible data structures for plotting.
 * 
 * @param genzDim The number of input dimensions of the Genz functions.
 * @param numTrials The number of tries to attempt.
 **/
void RunAdaptiveGenzBenchmarking(unsigned int genzDim, unsigned int numTrials);
void RunNonAdaptiveGenzBenchmarking(unsigned int genzDim, unsigned int numTrials);
#endif //GENZMAIN_H_
