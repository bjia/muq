//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * LegendrePolynomials1DRecursive.cpp
 *
 *  Created on: May 31, 2011
 *      Author: prconrad
 */
#include "MUQ/polychaos/polynomials/LegendrePolynomials1DRecursive.h"

#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>

using namespace muq::polychaos;

LegendrePolynomials1DRecursive::LegendrePolynomials1DRecursive() {


}

LegendrePolynomials1DRecursive::~LegendrePolynomials1DRecursive() {

}

double LegendrePolynomials1DRecursive::alpha(double x, unsigned int k)
{
	return -(2.0*(double)k+1.0)*x/((double)k +1 );
}

double LegendrePolynomials1DRecursive::beta(double x, unsigned int k)
{
	return (double) k / ((double)k+1);
}

double LegendrePolynomials1DRecursive::phi0(double x)
{
	return 1.0;
}

double LegendrePolynomials1DRecursive::phi1(double x)
{
	return x;
}

double LegendrePolynomials1DRecursive::GetNormalization(unsigned int n)
{
	return 2.0/(2.0*(double)n+1);
//	return 1.0;
}
//
template<class Archive>
void LegendrePolynomials1DRecursive::serialize(Archive & ar, const unsigned int version)
{
	//nothing to serialize except for the parent and the type of the object
	 ar & boost::serialization::base_object<RecursivePolynomialFamily1D>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(muq::polychaos::LegendrePolynomials1DRecursive)

template void LegendrePolynomials1DRecursive::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void LegendrePolynomials1DRecursive::serialize(boost::archive::text_iarchive & ar, const unsigned int version);


