//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * RecursivePolynomialFamily1D.cpp
 *
 *  Created on: May 31, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/polynomials/RecursivePolynomialFamily1D.h"

#include <math.h>

#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>


using namespace muq::polychaos;

RecursivePolynomialFamily1D::RecursivePolynomialFamily1D() {

}

RecursivePolynomialFamily1D::~RecursivePolynomialFamily1D() {

}


double RecursivePolynomialFamily1D::evaluate(unsigned int order, double x)
{
	double bkp1 = 0; 
	double bkp2 = 0;
	double bk = 1.0-alpha(x,order)*bkp1-beta(x,order+1)*bkp2;


	for(int k=order-1; k>=0; k--)
	{
		//increment k
		bkp2 = bkp1;
		bkp1 = bk;

		//compute the new bk
		bk = -alpha(x,k)*bkp1 - beta(x,k+1)*bkp2;
	}

	double result = bk*phi0(x) + bkp1*(phi1(x)+alpha(x,0)*phi0(x));

	return result;
}

template<class Archive>
void RecursivePolynomialFamily1D::serialize(Archive & ar, const unsigned int version)
{
	//nothing to serialize except for the type of the object
}




BOOST_CLASS_EXPORT_IMPLEMENT(RecursivePolynomialFamily1D)

template void RecursivePolynomialFamily1D::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void RecursivePolynomialFamily1D::serialize(boost::archive::text_iarchive & ar, const unsigned int version);
