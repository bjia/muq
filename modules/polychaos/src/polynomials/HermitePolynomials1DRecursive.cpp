//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * HermitePolynomials1DRecursive.cpp
 *
 *  Created on: May 31, 2011
 *      Author: prconrad
 */
#include "MUQ/polychaos/polynomials/HermitePolynomials1DRecursive.h"


#include <boost/math/constants/constants.hpp>
#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>

using namespace muq::polychaos;

HermitePolynomials1DRecursive::HermitePolynomials1DRecursive() {
    
    
}

HermitePolynomials1DRecursive::~HermitePolynomials1DRecursive() {
    
}

double HermitePolynomials1DRecursive::alpha(double x, unsigned int k)
{
    return -2.0*x;
}

double HermitePolynomials1DRecursive::beta(double x, unsigned int k)
{
    return 2.0*k;
}

double HermitePolynomials1DRecursive::phi0(double x)
{
    return 1.0;
}

double HermitePolynomials1DRecursive::phi1(double x)
{
    return 2.0*x;
}

unsigned int factorial(unsigned int number) {
    unsigned int temp;
    
    if(number <= 1) return 1;
    temp = number * factorial(number - 1);
    return temp;
}

double HermitePolynomials1DRecursive::GetNormalization(unsigned int n)
{
    return sqrt(boost::math::constants::pi<double>())*pow(2.0,static_cast<double>(n))*
	static_cast<double>(factorial(n));
    //	return 1.0;
}


template<class Archive>
void HermitePolynomials1DRecursive::serialize(Archive & ar, const unsigned int version)
{
    //nothing to serialize except for the parent and the type of the object
    ar & boost::serialization::base_object<RecursivePolynomialFamily1D>(*this);
}

BOOST_CLASS_EXPORT(HermitePolynomials1DRecursive)




