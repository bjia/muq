//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "MUQ/polychaos/utilities/PolychaosXMLHelpers.h"
#include <MUQ/polychaos/utilities/Variable.h>

#include <assert.h>
#include <map>
#include <string>
#include <iostream>

#include <boost/property_tree/xml_parser.hpp>

#include <MUQ/GeneralUtilities/CachedFunctionWrapper.h>
#include <MUQ/polychaos/polynomials/HermitePolynomials1DRecursive.h>
#include <MUQ/polychaos/polynomials/LegendrePolynomials1DRecursive.h>
#include <MUQ/polychaos/quadrature/GaussPattersonQuadrature1D.h>
#include <MUQ/polychaos/quadrature/ExpGrowthQuadratureFamily1D.h>
#include <MUQ/polychaos/quadrature/GaussHermiteQuadrature1D.h>
#include <MUQ/polychaos/smolyak/SmolyakPCEFactory.h>
#include <MUQ/polychaos/utilities/VariableCollection.h>

using namespace std;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;
using namespace boost::property_tree;


boost::shared_ptr<VariableCollection> muq::polychaos::ConstructVariableCollection(boost::property_tree::ptree const& pt)
{
  //the map will sort the entries by key
  map<int, ptree> sortedVars;

  //make a sorted list of all the variables
  for(auto varPtree : pt)
  {       
    
    //store pointers to the ptree
      if(varPtree.first.compare("Variable") == 0)
      {
	sortedVars[varPtree.second.get<int>("index")] = (varPtree.second);
      }
  }
  //set up the quad and poly types
  GaussPattersonQuadrature1D::Ptr gpQuad(new GaussPattersonQuadrature1D());
  GaussHermiteQuadrature1D::Ptr hermiteQuad(new GaussHermiteQuadrature1D());
  ExpGrowthQuadratureFamily1D::Ptr expHermiteQuad(new ExpGrowthQuadratureFamily1D(hermiteQuad));
  
  LegendrePolynomials1DRecursive::Ptr legendrePoly(new LegendrePolynomials1DRecursive());
  HermitePolynomials1DRecursive::Ptr hermitePoly(new HermitePolynomials1DRecursive());
  
  
  VariableCollection::Ptr varCollection(new VariableCollection());
  
  //now loop over the ptrees and construct them - varPtree is a pair<int, ptree>
  for(auto varPtree : sortedVars)
  {
    //default to one block
    for(unsigned i=0; i<varPtree.second.get<unsigned>("count",1); ++i) 
    {
      if(varPtree.second.get<string>("type").compare("Gaussian") == 0)
      {
	varCollection->PushVariable(varPtree.second.get("name", "x"), hermitePoly, expHermiteQuad);
      }
      else if(varPtree.second.get<string>("type").compare("Legendre") == 0)
      {
	varCollection->PushVariable(varPtree.second.get("name", "x"), legendrePoly, gpQuad);
      }
      else
      {
	assert(false); //bad ptree
      }
    }

  }
  
  return varCollection;
}
    
boost::shared_ptr<CachedFunctionWrapper> muq::polychaos::ConstructFunctionWrapper(boost::property_tree::ptree const& pt)
{
 return CachedFunctionWrapper::WrapExecutableFunction(pt.get<string>("ExternalFunction"), 
						      pt.get<unsigned>("InputDim"), 
						      pt.get<unsigned>("OutputDim"));
}
  

boost::shared_ptr<SmolyakPCEFactory> muq::polychaos::ConstructSmolyakPCE(boost::property_tree::ptree const& pt)
{
  //set up the dependencies
  CachedFunctionWrapper::Ptr fn = ConstructFunctionWrapper(pt.get_child("Polychaos.Function"));
  VariableCollection::Ptr vars =  ConstructVariableCollection(pt.get_child("Polychaos.VariableCollection"));
  
  //create the factory
  SmolyakPCEFactory::Ptr pceFactory(new SmolyakPCEFactory(vars, fn));
  
  return pceFactory; 
}
    

PolynomialChaosExpansion::Ptr muq::polychaos::ComputePCE(boost::property_tree::ptree const& pt)
{
  //make the factory
  SmolyakPCEFactory::Ptr pceFactory = ConstructSmolyakPCE(pt);
  
  PolynomialChaosExpansion::Ptr pce = pceFactory->StartAdaptiveTimed(pt.get<int>("Polychaos.InitialSimplex", 1),
								     pt.get<double>("Polychaos.AdaptationTime", 0.0));
  
  //try to grab the path
  boost::optional<string> pceOutputPath = pt.get_optional<string>("Polychaos.PceOutputPath");
  if(pceOutputPath) 
  {
    //if there was one, print it
    pce->print(*pceOutputPath);
  }
  
  return pce;
}



