/*MIT UQ Library
Copyright (C) 2012 Patrick R. Conrad

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.*/
/*
 * Variable.cpp
 *
 *  Created on: Nov 12, 2010
 *      Author: prconrad
 */

#include "MUQ/polychaos/utilities/Variable.h"

#include <string>

#include <boost/serialization/export.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "MUQ/polychaos/polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"


using namespace muq::polychaos;

Variable::Variable()
{

}

Variable::Variable(std::string name, RecursivePolynomialFamily1D::Ptr polyFamily,
		QuadratureFamily1D::Ptr quadFamily)
{
	this->name = name;
	this->polyFamily = polyFamily;
	this->quadFamily = quadFamily;
}


Variable::~Variable() {

}

template<class Archive>
void Variable::serialize(Archive & ar, const unsigned int version)
{
	ar & name;
	ar & polyFamily;
	ar & quadFamily;
}

template void Variable::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void Variable::serialize(boost::archive::text_iarchive & ar, const unsigned int version);


//BOOST_CLASS_EXPORT(Variable)
