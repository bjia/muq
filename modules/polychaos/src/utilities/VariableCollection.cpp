/*MIT UQ Library
Copyright (C) 2012 Patrick R. Conrad

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.*/
/*
 * VariableCollection.cpp
 *
 *  Created on: Nov 12, 2010
 *      Author: prconrad
 */


#include "MUQ/polychaos/utilities/VariableCollection.h"

#include <vector>
#include <assert.h>

#include <boost/serialization/export.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>


#include "MUQ/polychaos/polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"
#include "MUQ/polychaos/utilities/Variable.h"

using namespace muq::polychaos;

VariableCollection::VariableCollection() {

}

VariableCollection::~VariableCollection() {

}

void VariableCollection::PushVariable(Variable::Ptr newVar)
{
	this->variables.push_back(newVar);
}

void VariableCollection::PushVariable(std::string name, RecursivePolynomialFamily1D::Ptr polyFamily,
		QuadratureFamily1D::Ptr quadFamily)
{
	PushVariable(Variable::Ptr(new Variable(name, polyFamily, quadFamily)));
}

unsigned int VariableCollection::length(){
	return this->variables.size();
}

Variable::Ptr VariableCollection::GetVariable(unsigned int i)
{
	//check for out of bounds
	assert(i<this->variables.size());

	//return a reference
	return this->variables[i];
}

template<class Archive>
void VariableCollection::serialize(Archive & ar, const unsigned int version){
	ar & variables;
}

//BOOST_EXPORT_CLASS_IMPLEMENT(VariableCollection)



template void VariableCollection::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void VariableCollection::serialize(boost::archive::text_iarchive & ar, const unsigned int version);

//
//BOOST_CLASS_EXPORT_IMPLEMENT(VariableCollection)

