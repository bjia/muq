//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * FullTensorQuadrature.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/FullTensorQuadrature.h"


#include <assert.h>
#include <stddef.h>
#include <vector>

#include <boost/scoped_ptr.hpp>

#include "MUQ/GeneralUtilities/multiIndex/FullTensorMultiIndexFamily.h"
#include "MUQ/polychaos/quadrature/Quadrature.h"
#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"
#include "MUQ/GeneralUtilities/LogConfig.h"

using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;

FullTensorQuadrature::FullTensorQuadrature(VariableCollection::Ptr variables, unsigned int const order):
Quadrature(variables)
{

	//initialize term-wise order limits isotropically
	this->order.setOnes(variables->length());
	this->order = order*this->order;
}

FullTensorQuadrature::FullTensorQuadrature(VariableCollection::Ptr variables, RowVectorXu const& order):
		Quadrature(variables), order(order)
{

}

FullTensorQuadrature::~FullTensorQuadrature() {

}


void FullTensorQuadrature::GetNodesAndWeights(MatrixXd& nodes, VectorXd& weights)
{
	BOOST_LOG_POLY("quad",debug) << "Get weights for " << order;
	unsigned int nVars = variables->length();

	//allocate a vector to hold the 1D quadrature points and weights
	//recall that each dim may be different lengths!
	std::vector<boost::shared_ptr<RowVectorXd> > quadNodes1D;
	std::vector<boost::shared_ptr<RowVectorXd> > quadWeights1D;
	//an irowvec that holds the number of quadrature points in each dimension
	RowVectorXu quadGridSize;
	quadGridSize.setZero(nVars);

	//Fetch all the 1D quadrature rules we need
	for(unsigned int i=0;i<nVars;i++) //iterate over the dimensions/vars
	{
		//check that there's something there
		assert(variables->GetVariable(i)->quadFamily != NULL);

		Variable::Ptr iVar = variables->GetVariable(i);

		//store the nodes and weights for this rule
		quadNodes1D.push_back(iVar->quadFamily->GetNodes(order(i)));
		quadWeights1D.push_back(iVar->quadFamily->GetWeights(order(i)));
		quadGridSize(i) = quadNodes1D[i]->cols(); //record how many points there are in this dim
	}

	//A multi-index family used to iterate over the grid points.
	FullTensorMultiIndexFamily::Ptr gridIndices = FullTensorMultiIndexFamily::Create(nVars, quadGridSize);

	unsigned int nPoints = gridIndices->GetNumberOfIndices(); //the total number of quadrature points

	//zero out the nodes and weights to the correct size
	nodes.setZero(nVars, nPoints);
	weights.setZero(nPoints);

	//Now collect all the points and weights
	for(unsigned int i=0; i<nPoints; i++) //loop over the points
	{
		//get the multiindex that specifies this grid point - tells you which 1D point in each dim to use
		RowVectorXu iPointIndex = gridIndices->IndexToMulti(i);

		weights(i)=1; //start at 1 so you can collect the product of 1D weights

		for(unsigned int j=0; j<nVars; j++) //loop over the dimensions
		{
			//tricky, note we want jth coordinate of the multiindex from i, and that
			//the ordering of i/j is reversed. Note the dereferencing of the pointers
			//after pulling them out of the vectors
			nodes(j,i) = (*(quadNodes1D[j]))(iPointIndex(j));
			weights(i) = weights(i)*(*(quadWeights1D[j]))(iPointIndex(j));
		}
	}
	
		BOOST_LOG_POLY("quad",debug) << "Done fetching weights";

}
