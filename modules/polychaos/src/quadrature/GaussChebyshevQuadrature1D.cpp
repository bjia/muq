//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussChebyshevQuadrature1D.cpp
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/GaussChebyshevQuadrature1D.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>


#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>

#include "MUQ/GeneralUtilities/EigenUtils.h"


using namespace Eigen;
using namespace muq::polychaos;

GaussChebyshevQuadrature1D::GaussChebyshevQuadrature1D() {


}

GaussChebyshevQuadrature1D::~GaussChebyshevQuadrature1D() {

}

void GaussChebyshevQuadrature1D::ComputeNodesAndWeights(unsigned int const order,
		boost::shared_ptr<RowVectorXd>& nodes,
		boost::shared_ptr<RowVectorXd>& weights) const
{
	//compute with rules from http://mathworld.wolfram.com/Chebyshev-GaussQuadrature.html

	assert(order >0); //bounds checking

	//all weights are equal value
	weights->setZero(order);
	double localWeight = boost::math::constants::pi<double>()/static_cast<double>(order);
	for(unsigned int i=0; i<order; ++i)
	{
		(*weights)(i) = localWeight;
	}

	RowVectorXd localNodes = RowVectorXd::Zero(order);
	for(unsigned int i=1; i<=order; i++)
	{
		//find the arguments of the cos
		localNodes(i-1) = (2.0*i-1)*boost::math::constants::pi<double>()/2.0/double(order);
	}
	//evaluate all the cos together
	localNodes = localNodes.array().cos(); 
	localNodes.reverseInPlace();	//flip so they come out sorted

	//copy the local nodes into the one to return
	nodes->setZero(order);
	for(unsigned int i=0; i<order; ++i)
	{
		(*nodes)(i) = localNodes(i);
	}
}

unsigned int GaussChebyshevQuadrature1D::GetPrecisePolyOrder(unsigned int const order) const
{
	assert(order >0);
	return 2*order-1; //standard gaussian rule
}



template<class Archive>
void GaussChebyshevQuadrature1D::serialize(Archive & ar, const unsigned int version){
	ar & boost::serialization::base_object<QuadratureFamily1D>(*this);
}


BOOST_CLASS_EXPORT(GaussChebyshevQuadrature1D)