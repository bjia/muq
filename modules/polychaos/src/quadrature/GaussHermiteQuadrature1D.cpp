//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussHermiteQuadrature1D.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/GaussHermiteQuadrature1D.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <boost/math/constants/constants.hpp>

using namespace Eigen;
using namespace muq::polychaos;

GaussHermiteQuadrature1D::GaussHermiteQuadrature1D() {


}

GaussHermiteQuadrature1D::~GaussHermiteQuadrature1D() {

}

///Implements the monic coefficients;
RowVectorXd GaussHermiteQuadrature1D::GetMonicCoeff(unsigned int const j) const
{

	RowVectorXd monicCoeff(2);
	monicCoeff << 0, j/2.0;
	return monicCoeff;
}


double GaussHermiteQuadrature1D::IntegralOfWeight() const
{
	return sqrt(boost::math::constants::pi<double>());
}


template<class Archive>
void GaussHermiteQuadrature1D::serialize(Archive & ar, const unsigned int version){
	ar & boost::serialization::base_object<QuadratureFamily1D>(*this);
}


BOOST_CLASS_EXPORT(muq::polychaos::GaussHermiteQuadrature1D)

template void GaussHermiteQuadrature1D::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void GaussHermiteQuadrature1D::serialize(boost::archive::text_iarchive & ar, const unsigned int version);
