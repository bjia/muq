//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * ExpGrowthQuadratureFamily1D.cpp
 *
 *  Created on: Mar 31, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/LinearGrowthQuadratureFamily1D.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"



using namespace Eigen;
using namespace muq::polychaos;

LinearGrowthQuadratureFamily1D::LinearGrowthQuadratureFamily1D()
{

}

LinearGrowthQuadratureFamily1D::LinearGrowthQuadratureFamily1D(QuadratureFamily1D::Ptr inputRule,
		unsigned int inputGrowthInterval):growthInterval(inputGrowthInterval), baseQuadrature1DRule(inputRule)
{
	//use an initialization list
}

LinearGrowthQuadratureFamily1D::~LinearGrowthQuadratureFamily1D() {
	// TODO Auto-generated destructor stub
}


unsigned int LinearGrowthQuadratureFamily1D::GetPrecisePolyOrder(unsigned int const order) const
{
	assert(order>0);
	//simply call the base with the new order
	return baseQuadrature1DRule->GetPrecisePolyOrder(1+(order-1)*growthInterval);
}


void LinearGrowthQuadratureFamily1D::ComputeNodesAndWeights(unsigned int const order,
		boost::shared_ptr<RowVectorXd>& nodes,
		boost::shared_ptr<RowVectorXd>& weights) const
{
	assert(order>0);
	//simply call the base with the new order
	nodes = baseQuadrature1DRule->GetNodes(1+(order-1)*growthInterval);
	weights = baseQuadrature1DRule->GetWeights(1+(order-1)*growthInterval);
}

template<class Archive>
void LinearGrowthQuadratureFamily1D::serialize(Archive & ar, const unsigned int version){
	ar & boost::serialization::base_object<QuadratureFamily1D>(*this);
	ar & baseQuadrature1DRule;
	ar & growthInterval;
}


BOOST_CLASS_EXPORT(LinearGrowthQuadratureFamily1D)
