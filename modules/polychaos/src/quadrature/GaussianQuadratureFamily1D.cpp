//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussianQuadratureFamily1D.cpp
 *
 *  Created on: Jan 11, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/GaussianQuadratureFamily1D.h"

#include <stddef.h>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/shared_ptr.hpp>
#include <Eigen/Eigenvalues>

using namespace Eigen;
using namespace muq::polychaos;

GaussianQuadratureFamily1D::GaussianQuadratureFamily1D() {
}

GaussianQuadratureFamily1D::~GaussianQuadratureFamily1D() {

}

void GaussianQuadratureFamily1D::ComputeNodesAndWeights(unsigned int const order,
		 boost::shared_ptr<RowVectorXd>& nodes,
		 boost::shared_ptr<RowVectorXd>& weights) const
{
	//The algorithm is taken from Numerical Recipes ed 3, p162
	MatrixXd z = MatrixXd::Zero(order,order);

	//Set up the matrix
	for(unsigned int i=0; i<order; i++){
		RowVectorXd monicCoeff = this->GetMonicCoeff(i);

		z(i,i) = monicCoeff(0);

		double sqrtbj = sqrt(monicCoeff(1));

		if(i!=0){
			z(i,i-1)= sqrtbj;
			z(i-1,i)= sqrtbj;
		}
	}

	//make a solver and solve the problem
	SelfAdjointEigenSolver<MatrixXd> eigenSolver;
	eigenSolver.compute(z);
	  
	//grab the results, which are sorted
	VectorXd eigval = eigenSolver.eigenvalues();
	MatrixXd eigvec = eigenSolver.eigenvectors();
	

	nodes->setZero(order);
	weights->setZero(order);

	//pick off weights and nodes
	for(unsigned int i=0; i<order; i++)
	{
		(*nodes)(i) = eigval(i);
		(*weights)(i) = eigvec(0,i)*eigvec(0,i)*this->IntegralOfWeight();
	}

}


unsigned int GaussianQuadratureFamily1D::GetPrecisePolyOrder(unsigned int const  order) const
{
	return 2*order-1; //standard gaussian rule
}

template<class Archive>
void GaussianQuadratureFamily1D::serialize(Archive & ar, const unsigned int version)
{
	//just serialize the type
	ar & boost::serialization::base_object<QuadratureFamily1D>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(GaussianQuadratureFamily1D)

template void GaussianQuadratureFamily1D::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void GaussianQuadratureFamily1D::serialize(boost::archive::text_iarchive & ar, const unsigned int version);

