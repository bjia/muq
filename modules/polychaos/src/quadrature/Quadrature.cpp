//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * Quadrature.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/Quadrature.h"

#include <assert.h>

#include <Eigen/Core>

#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/FunctionWrapper.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"

using namespace Eigen;
using namespace muq::polychaos;
using namespace muq::GeneralUtilities;

Quadrature::Quadrature(VariableCollection::Ptr variables):
	variables(variables){

}

Quadrature::~Quadrature() {

}

VectorXd Quadrature::ComputeIntegral(FunctionWrapper::Ptr fn)
{
	 BOOST_LOG_POLY("quad",normal) << "Computing an integral";
	MatrixXd nodes;
	VectorXd weights;

	//compute the nodes and weights
	this->GetNodesAndWeights(nodes, weights);

	//initialize someplace to put the evaluations
	MatrixXd weightedFnEvals = fn->Evaluate(nodes);

	MatrixXd integral = weightedFnEvals*weights;
	//check that the multiplication went well
	assert(integral.rows() == fn->outputDim && integral.cols() == 1);
	BOOST_LOG_POLY("quad",normal) << "Done computing integral.";
	return integral.col(0);
}

