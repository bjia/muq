//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussLegendreQuadrature1D.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/GaussLegendreQuadrature1D.h"

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
    
using namespace Eigen;
using namespace muq::polychaos;

GaussLegendreQuadrature1D::GaussLegendreQuadrature1D() {


}

GaussLegendreQuadrature1D::~GaussLegendreQuadrature1D() {

}

///Implements the monic coefficients;
RowVectorXd GaussLegendreQuadrature1D::GetMonicCoeff(unsigned int const j) const
{

	RowVectorXd monicCoeff(2);
	double jd = (double) j;

	double Bnsqrt = (jd/(2*jd+1))*sqrt((2*jd+1)/(2*jd-1));

	monicCoeff << 0, Bnsqrt*Bnsqrt;

	return monicCoeff;
}


double GaussLegendreQuadrature1D::IntegralOfWeight() const
{
	return 2;
}

template<class Archive>
	void GaussLegendreQuadrature1D::serialize(Archive & ar, const unsigned int version)
{
    ar & boost::serialization::base_object<GaussianQuadratureFamily1D>(*this);
}

BOOST_CLASS_EXPORT(GaussLegendreQuadrature1D)

