//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * PolynomialChaosExpansion.cpp
 *
 *  Created on: Jan 19, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/pce/PolynomialChaosExpansion.h"

#include <assert.h>
#include <fstream>
#include <iostream>
#include <functional>

#include <boost/unordered_map.hpp>


#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/functional/hash.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/tuple/tuple.hpp>

#include "MUQ/GeneralUtilities/multiIndex/TotalOrderMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/UnstructuredMultiIndexFamily.h"
#include "MUQ/polychaos/polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/polychaos/quadrature/FullTensorQuadrature.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
#include "MUQ/GeneralUtilities/SmartPtrNull.h"
#include "MUQ/GeneralUtilities/FunctionWrapper.h"

#include "MUQ/polychaos/utilities/VariableCollection.h"

using namespace std;
using namespace boost;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;

PolynomialChaosExpansion::PolynomialChaosExpansion()
{

}

PolynomialChaosExpansion::PolynomialChaosExpansion(VariableCollection::Ptr variables) {
	this->variables = variables;

}

PolynomialChaosExpansion::~PolynomialChaosExpansion() {

}

unsigned PolynomialChaosExpansion::InputDim() const
{
  return variables->length();
}
      
  
unsigned PolynomialChaosExpansion::OutputDim() const
{
  return coeffs.rows();
}
      
      
MatrixXd PolynomialChaosExpansion::Evaluate(const MatrixXd& vals) const
{
   
  //use this method to bind the eval vector and apply to each column vector
  return ApplyVectorFunction(std::bind(&PolynomialChaosExpansion::EvaluateVector, this, std::placeholders::_1), OutputDim(), vals);
}

VectorXd PolynomialChaosExpansion::EvaluateVector(const VectorXd& vals) const
{
	unsigned int nVars = this->variables->length();
	unsigned int nTerms = this->terms->GetNumberOfIndices();

	//the cache maps (dim, poly index, value) -> polynomial value
	boost::unordered_map<boost::tuple<unsigned int, unsigned int>, double, 
	    boost::hash<boost::tuple<unsigned int, unsigned int> >, evalPointEqStruct> evaluationCache;
	
	//make sure the number of inputs matches the number of vars in the expansion
	assert(vals.rows() == nVars);

	//a colvec for the evaluations of each of the terms of the expansion
	VectorXd polyTerms = VectorXd::Ones(nTerms);

	for(unsigned int i=0; i<nTerms; i++) //loop over terms
	{
		//fetch the multiIndex
		RowVectorXu iIndex = this->terms->IndexToMulti(i);
		
		for(unsigned int j=0; j<iIndex.cols(); ++j)
		{
		    //value is the coeff times the poly term at the point
		    		    
		    //form the tuple that says which eval we want
		    boost::tuple<unsigned int, unsigned int> evaluationNeeded(j, iIndex(j));
		    //check the cache and see if we have it already
		    boost::unordered_map<boost::tuple<unsigned int, unsigned int>, double>::iterator 
			cacheCheck = evaluationCache.find(evaluationNeeded);
		    //if not
		    if(cacheCheck == evaluationCache.end())
		    {
			//compute it directly
			double onePolyEval = variables->GetVariable(j)->polyFamily->evaluate(iIndex(j), vals(j));
			polyTerms(i) *= onePolyEval; 	//update the polyomial
			evaluationCache[evaluationNeeded] = onePolyEval; //and store it for later
		    }
		    else
		    {
			polyTerms(i) *= cacheCheck->second; //if we had it, just use the value from before
		    }
		}
	}

// 	cout << "poly terms " << polyTerms << endl;
	//return the sum of all the polynomial terms
	MatrixXd result = coeffs * polyTerms;
	assert(result.cols() == 1); //only one col of result

	return result.col(0); //return the column
}

double PolynomialChaosExpansion::EvaluateOneTerm(RowVectorXu const& multiIndex, VectorXd const& vals) const
{
	unsigned int nVars = this->variables->length();
	double result = 1.0; //var for the result

	for(unsigned int j=0; j<nVars; j++) // loop over dimensions to compute the whole term
	{
		//to compute the contribution of the j poly to this variable, fetch the poly family,
		//then the poly from the multiindex in the j spot, and evaluate it with the jth value.
//		cout << "selecting "<< multiIndex << " " << j << " " << vals << endl ;
		result *= this->variables->GetVariable(j)->polyFamily->evaluate(multiIndex(j), vals(j));
	}

	return result;
}

double PolynomialChaosExpansion::GetOneTermNormalization(RowVectorXu const& multiIndex) const
{
	double norm = 1.0; //start the normalization at 1

	unsigned int nVars = this->variables->length();
	//loop over the dimensions and multiply the normalizations for each component polynomial
	for(unsigned int j=0; j<nVars; j++)
	{
		norm*= this->variables->GetVariable(j)->polyFamily->GetNormalization(multiIndex(j));
	}
	return norm;
}

VectorXd PolynomialChaosExpansion::GetNormalizationVec() const
{
	VectorXd result = VectorXd::Zero(terms->GetNumberOfIndices());

	//compute each one
	for(unsigned int i=0; i<terms->GetNumberOfIndices(); i++)
	{
		result(i) = sqrt(GetOneTermNormalization(terms->IndexToMulti(i)));
	}
	return result;
}

ostream& muq::polychaos::operator<<(ostream& output, const PolynomialChaosExpansion& pce) {

	MatrixXu pceTerms = pce.terms->GetAllMultiIndices();
	
	output.precision(15);
	output.setf(ios::scientific);
	output.setf(ios::showpos);


	output << pceTerms.rows() << endl;
	output << pceTerms << endl;
	MatrixXd coeffsTrans = pce.coeffs.transpose();
	output << coeffsTrans << endl;

    return output;  // for multiple << operators.
}

ostream& muq::polychaos::operator<<(ostream& output, const PolynomialChaosExpansion::Ptr& pce) {

	output << *pce;

    return output;  // for multiple << operators.
}

void PolynomialChaosExpansion::print(string basename)
{
  ofstream myfile ((basename+"_results.dat").c_str());
  assert(myfile.good()); //file opening must work
  myfile << *this;
  myfile.close();
}

void PolynomialChaosExpansion::printNormalized(std::string basename)
{
  ofstream myfile ((basename+"_results.dat").c_str());
  assert(myfile.is_open());
  
  myfile.precision(15);
  myfile.setf(ios::scientific);
  myfile.setf(ios::showpos);
  
  
  MatrixXu pceTerms = terms->GetAllMultiIndices();
  myfile << pceTerms.cols() << endl;
  myfile << pceTerms;

  
  MatrixXd normCoeff = coeffs.transpose().array()*GetNormalizationVec().array();
  myfile << normCoeff;
  myfile.close();
  
}

bool PolynomialChaosExpansion::IsPolynomialInExpansion(RowVectorXu toFind)
{
    return terms->IsMultiInFamily(toFind);
}


PolynomialChaosExpansion::Ptr PolynomialChaosExpansion::SubtractExpansions(PolynomialChaosExpansion::Ptr const a, PolynomialChaosExpansion::Ptr const b)
{
    	PolynomialChaosExpansion::Ptr result(new PolynomialChaosExpansion(a->variables));
	   
	//check that the number of outputs are the same
	assert(a->coeffs.rows() == b->coeffs.rows());
	
	//create a multiIndex set for the terms of the result
	UnstructuredMultiIndexFamily::Ptr resultTerms = UnstructuredMultiIndexFamily::Create(result->variables->length());
	
	//add the terms from a and b into the result
	for(unsigned int i=0; i<a->terms->GetNumberOfIndices(); ++i)
	{
	    resultTerms->AddMultiIndex(a->terms->IndexToMulti(i));
	}
	
	for(unsigned int i=0; i<b->terms->GetNumberOfIndices(); ++i)
	{
	    resultTerms->AddMultiIndex(b->terms->IndexToMulti(i));
	}
	
	//set the terms
	result->terms = resultTerms;
	//initialize space for the coefficients
	result->coeffs = MatrixXd::Zero(a->coeffs.rows(), result->terms->GetNumberOfIndices());
	
	for(unsigned int i=0; i<result->terms->GetNumberOfIndices(); ++i)
	{
	    //fetch one term
	    RowVectorXu oneTerm = result->terms->IndexToMulti(i);

	    if(!a->IsPolynomialInExpansion(oneTerm)) //if it's not in a, must be in b
	    {
		result->coeffs.col(i) = -(b->coeffs.col(b->terms->MultiToIndex(oneTerm)));
	    }
	    else if(!b->IsPolynomialInExpansion(oneTerm)) //if it's not in b, must be in a
	    {
		result->coeffs.col(i) = a->coeffs.col(a->terms->MultiToIndex(oneTerm));
	    }
	    else //otherwise it's in both, so subtract
	    {
		result->coeffs.col(i) = a->coeffs.col(a->terms->MultiToIndex(oneTerm))-(b->coeffs.col(b->terms->MultiToIndex(oneTerm)));
	    }
	    
	}
	
	return result;
}

VectorXd PolynomialChaosExpansion::ComputeVariance() const
{
  
    VectorXd normalVec = GetNormalizationVec();

    //if there's only the one constant term, the PCE has variance zero
    if(normalVec.rows() <= 1)
    {
     return VectorXd::Zero(coeffs.rows()); 
    }
    
    VectorXd squareNorm = normalVec.tail(normalVec.rows()-1).array().square();

    //for each output, the variance is the dot product of the squared coeffs with the squared norms of the PCE terms.
    //Thus, grab all but the leftmost column.
    //Have to normalize by what the constant integrates to. 
    
    VectorXd dimMeasures(variables->length());
    for(unsigned int i=0; i<variables->length(); ++i)
    {
      dimMeasures(i) = variables->GetVariable(i)->polyFamily->GetNormalization(0);
      
    }
    
    //Since the variance is an expectation, we must normalize if the polynomials aren't quite set up
    //to integrate to one.
    return coeffs.rightCols(coeffs.cols()-1).array().square().matrix() * squareNorm / dimMeasures.prod();
    
}

VectorXd PolynomialChaosExpansion::ComputeMagnitude() const
{
      VectorXd normalVec = GetNormalizationVec();

      //take the matrix product between the squared coeffs and squared norms, then sqrt each term
      return (coeffs.array().square().matrix() * normalVec.array().square().matrix()).array().sqrt(); 
}

PolynomialChaosExpansion::Ptr PolynomialChaosExpansion::ComputeWeightedSum(std::vector<PolynomialChaosExpansion::Ptr> expansions, VectorXd const& weights, 
									   VariableCollection::Ptr variables, unsigned int const outputDim, 
									   UnstructuredMultiIndexFamily::Ptr const& polynomials,
									   std::map<unsigned int, std::set<unsigned int> > const& basisMultiIndexCache)
{
  PolynomialChaosExpansion::Ptr sumPCE(new PolynomialChaosExpansion(variables));
    
    //we already know exactly which terms in the pce we need, it was passed in
    sumPCE->terms = polynomials; 
    
    //allocate space for all the coeffs
    sumPCE->coeffs = MatrixXd::Zero(outputDim, sumPCE->terms->GetNumberOfIndices());

    //find the non-zero weights
    VectorXu nonZeroWeights = FindNonZeroInVector(weights);

    //loop over every polynomial in the result
    for(unsigned int i=0; i<sumPCE->terms->GetNumberOfIndices(); i++)
    {
	//allocate space for the values of the individual terms
	MatrixXd termCoeffs = MatrixXd ::Zero(outputDim, weights.rows());
	//save the multi-index we're looking for
	RowVectorXu multiToFind = sumPCE->terms->IndexToMulti(i);

	//Find the non-zero weighted polys that actually have term i
	//Start with this sorting because it's usually very small
	for(unsigned int j=0; j< nonZeroWeights.rows(); ++j)
	{
	    //if this one has the current polynomial
	    PolynomialChaosExpansion::Ptr jthEst = expansions.at(nonZeroWeights(j));

	    //do this by checking our cache whether this term is included, if the cache was provided
	    std::map<unsigned int, std::set<unsigned int> >::const_iterator it = basisMultiIndexCache.find(i);
	    if(it->second.find(nonZeroWeights(j)) != it->second.end())
	    { 
		//copy the coeffs into the results
		unsigned int index = jthEst->terms->MultiToIndex(multiToFind);
		termCoeffs.col(nonZeroWeights(j)) = jthEst->coeffs.col(index);
	    }
	    
	}
	//finish by summing the coeffs with the weights and store them in the result
	sumPCE->coeffs.col(i) = (termCoeffs * weights);
    }
    
    return sumPCE;
}



PolynomialChaosExpansion::Ptr PolynomialChaosExpansion::ComputeWeightedSum(std::vector<PolynomialChaosExpansion::Ptr> expansions, 
									   VectorXd const& weights, VariableCollection::Ptr variables,
									   unsigned int const outputDim)
{
    //First, we have to look through the expansions and figure out which terms we're going to end up with.
    //Then we can call the overloaded operator that actually knows how to add.
    
    //the list of all polynomial terms in the sum
    UnstructuredMultiIndexFamily::Ptr allPolynomials = UnstructuredMultiIndexFamily::Create(variables->length());
    
    //the cache of which expansions have which polynomials
    std::map<unsigned int, std::set<unsigned int> > basisMultiIndexCache;
    
    
      //find the non-zero weights, as those are the only ones we need to consider
    VectorXu nonZeroWeights = FindNonZeroInVector(weights);
    
    //now loop over the non-zero weighted expansions
    for(unsigned int j=0; j<nonZeroWeights.cols(); ++j)
    {
	//the j expansion
	PolynomialChaosExpansion::Ptr jthEst = expansions.at(nonZeroWeights(j));
	    
	//loop over all the polynomials in this expansion
	for(unsigned int i=0; i<jthEst->terms->GetNumberOfIndices(); ++i)
	{
	    //add the polynomial to the list of ones we need in the sum
	    RowVectorXu polyTerm = jthEst->terms->IndexToMulti(i);
	    allPolynomials->AddMultiIndex(polyTerm);
	    
	    //include this one in the cache of expansions that include the polynomial
	    basisMultiIndexCache[allPolynomials->MultiToIndex(polyTerm)].insert(nonZeroWeights(j));
	}
    }
    
    //the caches are now built, so call the private method
    return PolynomialChaosExpansion::ComputeWeightedSum(expansions, weights, variables, outputDim, allPolynomials, basisMultiIndexCache);
}

unsigned int PolynomialChaosExpansion::NumberOfPolynomials() const
{
  return terms->GetNumberOfIndices();
}

bool evalPointEqStruct::operator()(boost::tuple<unsigned int, unsigned int> const& a, boost::tuple<unsigned int, unsigned int> const& b) const
{
    return (a.get<0>() == b.get<0>()) && (a.get<1>() == b.get<1>()); 
}

namespace boost{ namespace tuples {
  std::size_t hash_value(const boost::tuples::tuple<unsigned int, unsigned int>& b)
{
    size_t seed = 0;
    //combine the hashes
    boost::hash_combine(seed, b.get<0>());
    boost::hash_combine(seed, b.get<1>());
    return seed;
}
}
}


PolynomialChaosExpansion::Ptr PolynomialChaosExpansion::LoadFromFile(std::string fileName)
{
    PolynomialChaosExpansion::Ptr pce(new PolynomialChaosExpansion());
  // create and open an archive for input
  std::ifstream ifs(fileName.c_str());
  assert(ifs.good());
  boost::archive::text_iarchive ia(ifs);

  // read class state from archive
  ia >> pce;

  return pce;
}
      
     
void PolynomialChaosExpansion::SaveToFile(PolynomialChaosExpansion::Ptr pce, std::string fileName)
{
    std::ofstream ofs(fileName.c_str());
  
  
  if(!ofs.good())
  {
    LOG(ERROR) << "PolynomialChaosExpansion::SaveToFile file name not valid. Falling back to \"pceSave.dat\"";
    std::ofstream ofs("pceSave.dat");		  
  }
  assert(ofs.good());
  
  
  // save data to archive
  {
    boost::archive::text_oarchive oa(ofs);
    // write class instance to archive
    oa << pce;
    // archive and stream closed when destructors are called
  }
}
  
VectorXd PolynomialChaosExpansion::ComputeSobolTotalSensitivityIndex(unsigned const targetDim) const
{
  //we're going to fill this index with either 1 or 0, depending on whether the term includes the target dimension
  ArrayXd contributesToIndex = VectorXd::Zero(NumberOfPolynomials());
  
    Array<unsigned, 1, Dynamic> deltaDim = RowVectorXu::Ones(variables->length());
  deltaDim(targetDim) = 0;
  
  //contributes as long as targetDim is involved
  for(unsigned int i=0; i<terms->GetNumberOfIndices(); ++i)
  {
    RowVectorXu oneIndex = terms->IndexToMulti(i);
    contributesToIndex(i) =  static_cast<double>((oneIndex(targetDim) != 0));
  }

  //grab the total normalizations
  VectorXd normalVec = GetNormalizationVec();

  //filter the normalizations so that the ones that don't involve targetDim are zero
  VectorXd filterdVec = normalVec.array()*contributesToIndex;

  
      VectorXd dimMeasures(variables->length());
    for(unsigned int i=0; i<variables->length(); ++i)
    {
      dimMeasures(i) = variables->GetVariable(i)->polyFamily->GetNormalization(0);
      
    }
    
  //we want the sum of normalized involved coeffs divided by the normalized sum of all of them.
  //Don't forget the constant normalization the variance requires
  return (coeffs.array().square().matrix() * filterdVec.array().square().matrix()).array() / dimMeasures.prod() / 
    ComputeVariance().array();
}

MatrixXd PolynomialChaosExpansion::ComputeAllSobolTotalSensitivityIndices() const
{
  MatrixXd result(coeffs.rows(), variables->length());
  
  for(unsigned int i=0; i<variables->length(); ++i)
  {
    result.col(i) = ComputeSobolTotalSensitivityIndex(i);
  }
  
  return result;
}
    
VectorXd PolynomialChaosExpansion::ComputeMainSensitivityIndex(unsigned const targetDim) const
{
  //we're going to fill this index with either 1 or 0, depending on whether the term includes the target dimension
  ArrayXd contributesToIndex = VectorXd::Zero(NumberOfPolynomials());
  
  //create a row vector with a one at only the place we're interested in
  Array<unsigned, 1, Dynamic> deltaDim = RowVectorXu::Ones(variables->length());
  deltaDim(targetDim) = 0;
  
  //each one contributes iff it is the only non-zero one
  for(unsigned int i=0; i<terms->GetNumberOfIndices(); ++i)
  {    
    RowVectorXu oneIndex = terms->IndexToMulti(i);
    contributesToIndex(i) = static_cast<double>((oneIndex(targetDim) != 0) && ((oneIndex.array() * deltaDim).matrix().sum() == 0));
    
//     cout << "one " << oneIndex << " A " << (oneIndex(targetDim) != 0) << " B " << ((oneIndex.array() * deltaDim).matrix().sum() == 0) << endl;
  }

  
  //grab the total normalizations
  VectorXd normalVec = GetNormalizationVec();

  //filter the normalizations so that the ones that don't involve targetDim are zero
  VectorXd filterdVec = normalVec.array()*contributesToIndex;
  
      VectorXd dimMeasures(variables->length());
    for(unsigned int i=0; i<variables->length(); ++i)
    {
      dimMeasures(i) = variables->GetVariable(i)->polyFamily->GetNormalization(0);
      
    }
    
    
    
  //we want the sum of normalized involved coeffs divided by the normalized sum of all of them.
  //Don't forget the constant normalization the variance requires
  return (coeffs.array().square().matrix() * filterdVec.array().square().matrix()).array() / dimMeasures.prod() / 
    ComputeVariance().array();
}
     
MatrixXd PolynomialChaosExpansion::ComputeAllMainSensitivityIndices() const
{
  
//   cout << "norm vec " << GetNormalizationVec() << endl;
//   cout << "coeffs " << coeffs.transpose() << endl;
  MatrixXd result(coeffs.rows(), variables->length());
  
  for(unsigned int i=0; i<variables->length(); ++i)
  {
    result.col(i) = ComputeMainSensitivityIndex(i);
  }
  
  return result;
}
      
      

template<class Archive>
void PolynomialChaosExpansion::serialize(Archive & ar, const unsigned int version)
{
	ar & terms;
  ar & coeffs;
	ar & variables;


}

BOOST_CLASS_EXPORT(muq::polychaos::PolynomialChaosExpansion)

template void PolynomialChaosExpansion::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void PolynomialChaosExpansion::serialize(boost::archive::text_iarchive & ar, const unsigned int version);

