//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SingleQuadraturePCEFactory.CPP
 *
 *  Created on: Jun 12, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/pce/SingleQuadraturePCEFactory.h"

#include <Eigen/Core>

#include "MUQ/GeneralUtilities/FunctionWrapper.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
#include "MUQ/GeneralUtilities/multiIndex/AdaptiveMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"
#include "MUQ/polychaos/quadrature/FullTensorQuadrature.h"
#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"
#include "MUQ/polychaos/utilities/Variable.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"

using namespace muq::GeneralUtilities;

using namespace Eigen;
using namespace muq::polychaos;

SingleQuadraturePCEFactory::SingleQuadraturePCEFactory(VariableCollection::Ptr variables):
variables(variables)
{
  
}

SingleQuadraturePCEFactory::~SingleQuadraturePCEFactory() {
  
}


PolynomialChaosExpansion::Ptr SingleQuadraturePCEFactory::ConstructPCE(FunctionWrapper::Ptr fn,
								       RowVectorXu const& order,
								       RowVectorXu const& offset,
								       MultiIndexFamily::Ptr tensorOrdersIncluded)
{
  PolynomialChaosExpansion::Ptr result(new PolynomialChaosExpansion(variables));
  
  FullTensorQuadrature fullQuad(variables, order+offset);
  
  //Compute the quadrature nodes and weights
  MatrixXd nodes;
  VectorXd weights;
  fullQuad.GetNodesAndWeights(nodes, weights);
  
  //allocate a nearly empty multi-index family for the pce terms
  AdaptiveMultiIndexFamily::Ptr pceTerms = AdaptiveMultiIndexFamily::Create(order.cols(),0,1);
  
  //if there wasn't an input group of pce terms to include, default to ones that are integrable
  if(!tensorOrdersIncluded)
  {
    RowVectorXu maxPCEorder;
    maxPCEorder.resizeLike(order);
    for(unsigned int i=0; i<order.cols(); i++)
    {
      //note that this uses integer division to round down, we want only ones that are
      //perfectly integrated
      maxPCEorder(i) = variables->GetVariable(i)->quadFamily->GetPrecisePolyOrder(order(i) + offset(i))/2;
    }
    
    //now create a multi-index family with the terms by forcibly adding the max term,
    //all predecessors will be added automatically
    pceTerms->ForciblyActivateMultiIndex(maxPCEorder);
  }
  else
  {
    //activate all the ones integrable under all the quadrature rules in the input set
    for(unsigned int i=0; i<tensorOrdersIncluded->GetNumberOfIndices(); ++i)
    {
      RowVectorXu oneTensorRule = tensorOrdersIncluded->IndexToMulti(i)+offset; //note offset to tensor orders
      RowVectorXu maxPCEorder;
      maxPCEorder.resizeLike<>(order);
      for(unsigned int i=0; i<order.cols(); i++)
      {
	//note that this uses integer division to round down, we want only ones that are
	//perfectly integrated
	maxPCEorder(i) = variables->GetVariable(i)->quadFamily->GetPrecisePolyOrder(oneTensorRule(i))/2;
      }
      
      //now create a multi-index family with the terms by forcibly adding the max term,
      //all predecessors will be added automatically
      pceTerms->ForciblyActivateMultiIndex(maxPCEorder);
    }
  }
  
  
  
  //space to store all the point evaluations for f*terms
  MatrixXd integrationResults = MatrixXd::Zero(fn->outputDim, weights.rows());
  
  //evaluate the function at the nodes
  MatrixXd nodeEvals = fn->Evaluate(nodes);
  
  //allocate space for results
  result->coeffs.resize(fn->outputDim, pceTerms->GetNumberOfIndices());
  
  //compute the coeffs for every basis term
  for(unsigned int i=0; i<pceTerms->GetNumberOfIndices(); ++i)
  {
    
    //for a given pce term, at each node
    for(unsigned int j=0; j<weights.rows(); j++)
    {
      
      
      
      RowVectorXu oneMultiIndex = pceTerms->IndexToMulti(i);
      //compute f*term
      integrationResults.col(j) = nodeEvals.col(j)*
      result->EvaluateOneTerm(oneMultiIndex,nodes.col(j))/
      result->GetOneTermNormalization(oneMultiIndex);
      
    }
    
    //Compute the weighted sum, giving <f, term>
    result->coeffs.col(i) = integrationResults*weights;
  }
  
  //copy the terms into the pce
  result->terms = pceTerms;
  
  return result;
}
