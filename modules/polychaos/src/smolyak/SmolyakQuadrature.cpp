//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakQuadrature.cpp
 *
 *  Created on: Jun 10, 2011
 *      Author: prconrad
 */


#include "MUQ/polychaos/smolyak/SmolyakQuadrature.h"

#include <string.h>
#include <iostream>
#include <fstream>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/function.hpp>

#include "MUQ/polychaos/quadrature/FullTensorQuadrature.h"
#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"
#include "MUQ/GeneralUtilities/FunctionWrapper.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
 

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;

SmolyakQuadrature::SmolyakQuadrature()
{

}

SmolyakQuadrature::SmolyakQuadrature(VariableCollection::Ptr inVariables, FunctionWrapper::Ptr inFnWrapper):
		SmolyakEstimate<VectorXd>(inVariables->length()),
		variables(inVariables), fnWrapper(inFnWrapper)

{
  assert(variables->length() == fnWrapper->inputDim); //check that the vars and fn match

}

SmolyakQuadrature::~SmolyakQuadrature() {

}

SmolyakQuadrature::Ptr SmolyakQuadrature::LoadProgress(string baseName, boost::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn)
{
	SmolyakQuadrature::Ptr progress(new SmolyakQuadrature());
		// create and open an archive for input
		std::string fullName = baseName;
		std::ifstream ifs(fullName.c_str());
		boost::archive::text_iarchive ia(ifs);
		// read class state from archive
		ia >> progress;

		progress->fnWrapper->SetVectorFunction(fn);
		return progress;
}

void SmolyakQuadrature::SaveProgress(SmolyakQuadrature::Ptr quad, string baseName)
{

		std::string fullString = baseName;
		
		std::ofstream ofs(fullString.c_str());
		
		if(!ofs.good())
		{
		 LOG(ERROR) << "SmolyakQuadrature::SaveProgress file name not valid. Falling back to \"quadratureProgress.dat\"";
		std::ofstream ofs("quadratureProgress.dat");		  
		}
		assert(ofs.good());
		
		// save data to archive
		{
			boost::archive::text_oarchive oa(ofs);
			// write class instance to archive
			oa << quad;
			// archive and stream closed when destructors are called
		}
	}



VectorXd SmolyakQuadrature::ComputeOneEstimate(RowVectorXu const& multiIndex)
{

	//Else, one estimate is the full tensor quadrature of the given order
	FullTensorQuadrature quad(variables, multiIndex+RowVectorXu::Ones(multiIndex.cols()));
	return quad.ComputeIntegral(fnWrapper);
}

VectorXd SmolyakQuadrature::WeightedSumOfEstimates(VectorXd const& weights) const
{
	BOOST_LOG_POLY("smolyak",debug) << "Computing a weighted sum";

	MatrixXd allEst = MatrixXd::Zero(fnWrapper->outputDim, termsIncluded->GetNumberOfIndices());

	for(unsigned int i=0; i<allEst.cols(); i++)
	{
		allEst.col(i) = termEstimates.at(i);
	}

	return allEst * weights;
}

double SmolyakQuadrature::ComputeMagnitude(VectorXd estimate)
{
	//return the largest magnitude component
	return estimate.array().abs().matrix().maxCoeff();
}

MatrixXu SmolyakQuadrature::GetEffectiveIncludedTerms()
{
	MatrixXu result = MatrixXu::Zero(termsIncluded->GetNumberOfIndices(), termsIncluded->GetMultiIndexLength());

	for(unsigned int i=0; i< termsIncluded->GetNumberOfIndices(); ++i)
	{
		RowVectorXu multiIndex = termsIncluded->IndexToMulti(i);
		for(unsigned int j=0; j<termsIncluded->GetMultiIndexLength(); ++j)
		{
			result(i,j) = variables->GetVariable(j)->quadFamily->GetPrecisePolyOrder(multiIndex(j)+1);
		}
	}

	return result;
}

unsigned int SmolyakQuadrature::ComputeWorkDone() const
{
    return fnWrapper->GetNumOfEvals();
}

template<class Archive>
void SmolyakQuadrature::serialize(Archive & ar, const unsigned int version)
{
	ar & boost::serialization::base_object<SmolyakEstimate<VectorXd> >(*this);
	ar & variables;
	ar & fnWrapper;
}

BOOST_CLASS_EXPORT(SmolyakQuadrature)
