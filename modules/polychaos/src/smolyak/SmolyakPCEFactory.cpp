//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakPCEFactory.cpp
 *
 *  Created on: Jun 12, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/smolyak/SmolyakPCEFactory.h"

#include <string.h>
#include <iostream>
#include <fstream>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/function.hpp>

#include "MUQ/GeneralUtilities/multiIndex/UnstructuredMultiIndexFamily.h"
#include "MUQ/polychaos/pce/PolynomialChaosExpansion.h"
#include "MUQ/polychaos/pce/SingleQuadraturePCEFactory.h"
#include "MUQ/polychaos/quadrature/QuadratureFamily1D.h"
#include "MUQ/polychaos/smolyak/SmolyakEstimate.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
#include "MUQ/GeneralUtilities/FunctionWrapper.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"


using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;

SmolyakPCEFactory::SmolyakPCEFactory()
{
  
}

SmolyakPCEFactory::SmolyakPCEFactory(VariableCollection::Ptr inVariables, FunctionWrapper::Ptr inFn, unsigned int simplexLimit):
SmolyakEstimate<PolynomialChaosExpansion::Ptr>(inVariables->length(), simplexLimit), variables(inVariables), fn(inFn)
{
  
  assert(variables->length() == fn->inputDim); //check that the vars and fn match

  //initialize the unstructured set indicating which terms are present
  polynomialBasisTermsEstimated = UnstructuredMultiIndexFamily::Create(inVariables->length(), 0);
  
}

SmolyakPCEFactory::SmolyakPCEFactory(VariableCollection::Ptr inVariables, FunctionWrapper::Ptr inFn, MultiIndexFamily::Ptr limitingSet):
SmolyakEstimate<PolynomialChaosExpansion::Ptr>(inVariables->length(), limitingSet), variables(inVariables), fn(inFn)
{
  assert(variables->length() == fn->inputDim); //check that the vars and fn match
  //initialize the unstructured set indicating which terms are present
  polynomialBasisTermsEstimated = UnstructuredMultiIndexFamily::Create(inVariables->length(), 0);
  
}

SmolyakPCEFactory::~SmolyakPCEFactory() {
  
}


PolynomialChaosExpansion::Ptr SmolyakPCEFactory::ComputeOneEstimate(RowVectorXu const& multiIndex)
{
  SingleQuadraturePCEFactory pceFactory(variables);
  
  //bump up the order by one because 0th order quadrature is meaningless
  PolynomialChaosExpansion::Ptr result = pceFactory.ConstructPCE(fn, multiIndex, RowVectorXu::Ones(multiIndex.cols()));
  
  //stash away the terms that this one estimated
  for(unsigned int i=0; i<result->terms->GetNumberOfIndices(); ++i)
  {
    MultiIndexWrapper polyTermIncluded = result->terms->IndexToMulti(i);
    unsigned int polyTermIncludedIndex = polynomialBasisTermsEstimated->AddMultiIndex(polyTermIncluded);
    
    //indicate that this multiIndex of Smolyak contributes to the polyTermIncluded
    basisMultiIndexCache[polyTermIncludedIndex].insert(termsIncluded->MultiToIndex(multiIndex));
  }
  
  
  
  //return the estimate
  return result;
}

PolynomialChaosExpansion::Ptr SmolyakPCEFactory::WeightedSumOfEstimates(Eigen::VectorXd const& weights) const
{
  //just call the class method, passing on the caches you have available, being careful to copy the 
  //family of the indices in the polynomial
  return PolynomialChaosExpansion::ComputeWeightedSum(termEstimates, weights, variables, fn->outputDim, 
						      UnstructuredMultiIndexFamily::CloneExisting(polynomialBasisTermsEstimated), 
						      basisMultiIndexCache);
}


double SmolyakPCEFactory::ComputeMagnitude(PolynomialChaosExpansion::Ptr estimate)
{
  return estimate->ComputeMagnitude().norm();
}

MatrixXu SmolyakPCEFactory::GetEffectiveIncludedTerms()
{
  MatrixXu result = MatrixXu::Zero(termsIncluded->GetNumberOfIndices(), termsIncluded->GetMultiIndexLength());
  
  for(unsigned int i=0; i< termsIncluded->GetNumberOfIndices(); ++i)
  {
    RowVectorXu multiIndex = termsIncluded->IndexToMulti(i);
    for(unsigned int j=0; j<termsIncluded->GetMultiIndexLength(); ++j)
    {
      result(i,j) = variables->GetVariable(j)->quadFamily->GetPrecisePolyOrder(multiIndex(j)+1);
    }
  }
  
  return result;
}

boost::shared_ptr<muq::GeneralUtilities::FunctionWrapper> SmolyakPCEFactory::GetFunction()
{
  return fn;
}


PolynomialChaosExpansion::Ptr SmolyakPCEFactory::ComputeFixedPolynomials(MatrixXu polys)
{
  AdaptiveMultiIndexFamily::Ptr quadSet = AdaptiveMultiIndexFamily::Create(polys.cols(),0,1);
  
  for(unsigned int i=0; i<polys.rows(); ++i)
  {
    RowVectorXu newQuad = RowVectorXu::Zero(polys.cols());
    for(unsigned int j=0; j<polys.cols(); ++j)
    {
      unsigned int necessaryOrder = 0;
      while(polys(i,j)*2 > variables->GetVariable(j)->quadFamily->GetPrecisePolyOrder(necessaryOrder+1))
      {
	++necessaryOrder;
      }
      newQuad(j) = necessaryOrder;
    }
    
    quadSet->ForciblyActivateMultiIndex(newQuad);
  }
  
  //compute the estimate with these terms
  return StartFixedTerms(quadSet);
}

SmolyakPCEFactory::Ptr SmolyakPCEFactory::LoadProgress(string baseName, boost::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn)
{
  
  SmolyakPCEFactory::Ptr progress(new SmolyakPCEFactory());
  // create and open an archive for input
  std::string fullName = baseName;
  std::ifstream ifs(fullName.c_str());
  assert(ifs.good());
  boost::archive::text_iarchive ia(ifs);

  // read class state from archive
  ia >> progress;
  
  progress->fn->SetVectorFunction(fn);

  return progress;
}

unsigned int SmolyakPCEFactory::ComputeWorkDone() const
{
  return fn->GetNumOfEvals();
}

void SmolyakPCEFactory::SaveProgress(SmolyakPCEFactory::Ptr pceFactory, string baseName)
{
  
  std::ofstream ofs(baseName.c_str());
  
  
  if(!ofs.good())
  {
    LOG(ERROR) << "SmolyakPCEFactory::SaveProgress file name not valid. Falling back to \"pceProgress.dat\"";
    std::ofstream ofs("pceProgress.dat");		  
  }
  assert(ofs.good());
  
  
  // save data to archive
  {
    boost::archive::text_oarchive oa(ofs);
    // write class instance to archive
    oa << pceFactory;
    // archive and stream closed when destructors are called
  }
}

template<class Archive>
void SmolyakPCEFactory::serialize(Archive & ar, const unsigned int version)
{
  ar & boost::serialization::base_object<SmolyakEstimate<PolynomialChaosExpansion::Ptr> >(*this);
  ar & variables;
  ar & polynomialBasisTermsEstimated;
  ar & basisMultiIndexCache;
  ar & fn;
}

BOOST_CLASS_EXPORT(SmolyakPCEFactory)
