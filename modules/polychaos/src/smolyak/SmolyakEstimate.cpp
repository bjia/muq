//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakEstimate.cpp
 *
 *  Created on: Jun 22, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/smolyak/SmolyakEstimate.h"

#include <assert.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <time.h>

#include <boost/serialization/vector.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/export.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "MUQ/GeneralUtilities/multiIndex/AdaptiveMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/FullTensorMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/GeneralLimitedMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/SimplexLimitedMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/TotalOrderMultiIndexFamily.h"
#include "MUQ/polychaos/pce/PolynomialChaosExpansion.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/LogConfig.h"

using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;


template<typename T>
SmolyakEstimate<T>::SmolyakEstimate(unsigned int numDimensions, unsigned int simplexLimit)
{
    //always initialize with exactly the (0,...0) element
    
    if(simplexLimit == 0)
    {
	SmolyakEstimate<T>::termsIncluded = AdaptiveMultiIndexFamily::Create(numDimensions, 0, 1);
    }
    else
    {
	SmolyakEstimate<T>::termsIncluded = SimplexLimitedMultiIndexFamily::Create(numDimensions, simplexLimit, 0, 1);
    }
    
    //prepare the differential neighbor cache
    //differentialNeighborCache = FullTensorMultiIndexFamily::Create(numDimensions,2);
    
    //reset the term weights
    termWeights.resize(0);
    
    //clear the vectors
    termEstimates.clear();
    differentialMagnitudes.clear();
}

template<typename T>
SmolyakEstimate<T>::SmolyakEstimate(unsigned int numDimensions, MultiIndexFamily::Ptr limitingSet)
{
    SmolyakEstimate<T>::termsIncluded = GeneralLimitedMultiIndexFamily::Create(numDimensions, limitingSet, 0, 1);
        //reset the term weights
    termWeights.resize(0);
    
    //clear the vectors
    termEstimates.clear();
    differentialMagnitudes.clear();
}

template<typename T>
SmolyakEstimate<T>::~SmolyakEstimate()
{
    
}

template<typename T>
T SmolyakEstimate<T>::StartAdaptiveTimed(MultiIndexFamily::Ptr initialTerms, double secondsLimit)
{
    time_t start,end;
    std::time (&start); //record the start time
    
    //compute all the initial terms
    ComputeInitialTerms(initialTerms);
    
    //time so far
    std::time (&end);
    
    return AdaptForTime(secondsLimit, std::difftime(end, start));
    
}

template<typename T>
T SmolyakEstimate<T>::AdaptForTime(double secondsLimit, double timeElapsed)
{
    
    using namespace boost::posix_time;
    
    ptime start =  microsec_clock::local_time();
//    time_t start,end;
  //  time (&start); //record the start time
    
    //time so far
    //time (&end);
    ptime now =  microsec_clock::local_time();
    time_duration diff = now-start;
    while( diff.total_microseconds()*1e-6 + timeElapsed < secondsLimit)
    {
	VectorXu newTermIndices;
	if(!Refine(newTermIndices))
	{
	    break;
	}
	AddBatchOfTerms(newTermIndices);
	RecordAdaptProgress();
	//time so far
	now =  microsec_clock::local_time();
	diff = now-start;
    }
    
    finalEstimate = PreciseWeightedSumOfEstimates(termWeights);
    //finally, return the global result
    
 
    
    return finalEstimate;
}


template<typename T>
T SmolyakEstimate<T>::StartAdaptiveToTolerance(MultiIndexFamily::Ptr initialTerms, double errorLimit)
{
    //compute all the initial terms
    ComputeInitialTerms(initialTerms);
    
    return AdaptToTolerance(errorLimit);
    
    
}

template<typename T>
T SmolyakEstimate<T>::AdaptToTolerance(double errorLimit)
{
    BOOST_LOG_POLY("smolyak",debug) << "Current indicator: " <<
    ComputeGlobalErrorIndicator() << " tol: " << errorLimit;
    while(errorLimit < ComputeGlobalErrorIndicator())
    {
	Eigen::VectorXu newTermIndices;
	if(!Refine(newTermIndices))
	{
	    break;
	}
	AddBatchOfTerms(newTermIndices);
	RecordAdaptProgress();
	BOOST_LOG_POLY("smolyak",debug) << "Current indicator: " <<
	ComputeGlobalErrorIndicator() << " tol: " << errorLimit;
    }
    
    BOOST_LOG_POLY("smolyak",debug) << "Used terms: " << termsIncluded->GetAllMultiIndices();
    BOOST_LOG_POLY("smolyak",debug) << "Used weights: " << termWeights;
    //	for(unsigned int i=0; i<termEstimates.size(); i++)
    //	{
	//		BOOST_LOG_POLY("smolyak",debug) << "Estimate " << i << ": " << termEstimates[i];
    //	}
    
    finalEstimate = PreciseWeightedSumOfEstimates(termWeights);
    //finally, return the global result
    return finalEstimate;
}

template<typename T>
T SmolyakEstimate<T>::StartFixedTerms(MultiIndexFamily::Ptr fixedTerms)
{
    BOOST_LOG_POLY("smolyak",debug) << "Computing fixed with terms: " << fixedTerms->GetAllMultiIndices();
    
    //compute all the initial terms
    ComputeInitialTerms(fixedTerms);
    
    BOOST_LOG_POLY("smolyak",debug) << "Done computing terms.";
    
    finalEstimate = PreciseWeightedSumOfEstimates(termWeights);
    RecordAdaptProgress();
    //finally, return the global result
    return finalEstimate;
}

template<typename T>
T SmolyakEstimate<T>::AddFixedTerms(MultiIndexFamily::Ptr fixedTerms)
{
    BOOST_LOG_POLY("smolyak",debug) << "Adding fixed terms.";
    
    AddBatchOfTerms(fixedTerms);
    finalEstimate = PreciseWeightedSumOfEstimates(termWeights);
    RecordAdaptProgress();
    //finally, return the global result
    return finalEstimate;
}


template<typename T>
void SmolyakEstimate<T>::OutputVerbose(std::string baseName)
{
    std::string termsName = "results/" + baseName + "_terms.dat";
    MatrixXu allTerms = GetEffectiveIncludedTerms();
    
    std::ofstream allTermsFile (termsName.c_str());
    assert(allTermsFile.good());
    allTermsFile << allTerms;
    allTermsFile.close();
    
    //save the terms in the pce and the coefficients
    std::string resultName = "results/" + baseName + "_result.dat";
    
    std::ofstream myfile (resultName.c_str());
    assert(myfile.good());
    myfile << finalEstimate;
    myfile.close();
}

template<typename T>
bool SmolyakEstimate<T>::Refine(VectorXu& newTermIndices)
{
    double largestError = -1.0;
    unsigned int indexToExpand = 0;
    bool termFound = false;
    
    //search for the expandable term with the largest local error indicator
    for(unsigned int i=0; i<termsIncluded->GetNumberOfIndices(); i++)
    {
	double localError = ComputeLocalErrorIndicator(i);

	if(IsTermEligibleForExpansion(i) && (localError > largestError))
	{
	    largestError = localError;
	    indexToExpand = i;
	    termFound = true;
	}
    }
    
    if(!termFound)
    {
	BOOST_LOG_POLY("smolyak",debug)  << "No more terms to add. Halt!";
	return false;
    }
    
    BOOST_LOG_POLY("smolyak",debug)  << "Refining: " << indexToExpand << " with error: " << largestError << " which is: " << 
	termsIncluded->IndexToMulti(indexToExpand);
    
    //Expand the largest one, and return all the indices added to the termsIncluded set
    //in the process.
    newTermIndices = termsIncluded->ExpandIndex(indexToExpand).transpose();
    BOOST_LOG_POLY("smolyak",debug)  << "Refine selected: " << newTermIndices << "which are: ";
    for(unsigned int i=0; i<newTermIndices.rows(); ++i)
    {
	 BOOST_LOG_POLY("smolyak",debug)  <<termsIncluded->IndexToMulti(newTermIndices(i));
    }
    return true;
}

/*template<typename T>
MatrixXu SmolyakEstimate<T>::GetTopNewOrdersToAdd(unsigned int const numberToExpand)
{
    //construct just as the regular one
    AdaptiveMultiIndexFamily::Ptr duplicateTermsIncluded = AdaptiveMultiIndexFamily::Create(termsIncluded->GetMultiIndexLength(),0,1);
    
    //take all the ones in the set and forcibly expand them
    for(unsigned int i=0; i<termsIncluded->GetNumberOfIndices(); ++i)
    {
	duplicateTermsIncluded->ForciblyActivateMultiIndex(termsIncluded->IndexToMulti(i));
    }
    
     
     VectorXd localErrorIndicators = VectorXd::Zero(duplicateTermsIncluded->GetNumberOfIndices());
     
     //collect the local error indicators if the terms are expandable without considering any limits
     for(unsigned int i=0; i<duplicateTermsIncluded->GetNumberOfIndices(); ++i)
     {
	if(duplicateTermsIncluded->IsIndexExpandable(i))
	{
	    //compute the local error indicator for the termsIncluded equivalent of the duplicate one
	    localErrorIndicators(i) = ComputeLocalErrorIndicator(termsIncluded->MultiToIndex(duplicateTermsIncluded->IndexToMulti(i)));
	}
     }
     
     //sort in descending order
     VectorXu indices = arma::sort_index(localErrorIndicators, 1);
     
     MatrixXu results;
     
     //expand the top ones
     for(unsigned int i=0; i<numberToExpand; ++i)
     {
	RowVectorXu newTermsIncluded = duplicateTermsIncluded->ExpandIndex(indices(i));
	BOOST_LOG_POLY("smolyak",normal) << "Top local error indicator:" << localErrorIndicators(indices(i)) << std::endl;
	for(unsigned int j=0; j<newTermsIncluded.cols(); ++j)
	{	
	  //add a row
	  results.conservativeResize(results.rows()+1, results.cols());
	  //and insert
	  results.row(results.rows()-1) = duplicateTermsIncluded->IndexToMulti(newTermsIncluded(j));
	}
     }
     
   return results;
}*/

template<typename T>
void SmolyakEstimate<T>::AddBatchOfTerms(VectorXu const& newTermIndices)
{
    BOOST_LOG_POLY("smolyak",debug) << "Expanding storage for new batch." << newTermIndices;
    ExpandStorage();
    BOOST_LOG_POLY("smolyak",debug) << "Done.";
    
    BOOST_LOG_POLY("smolyak",debug) << "Computing terms.";
    //Compute all the estimates first, because some of them may be needed
    //for the differentials computed next
    for(unsigned int i=0; i<newTermIndices.rows(); i++)
    {
	termEstimates.at(newTermIndices(i)) = ComputeOneEstimate(termsIncluded->IndexToMulti(newTermIndices(i)));
    }
    BOOST_LOG_POLY("smolyak",debug) << "Done.";
    
    BOOST_LOG_POLY("smolyak",debug) << "Updating differentials and weights.";
    //now compute the weights and differentials
    for(unsigned int i=0; i<newTermIndices.rows(); i++)
    {
	//compute the differential coefficients
	VectorXd differentialCoeff;
	ComputeDifferentialCoeff(newTermIndices(i), differentialCoeff);

	//update the weights
	termWeights = termWeights + differentialCoeff;

	differentialMagnitudes.at(newTermIndices(i)) = ComputeMagnitude(WeightedSumOfEstimates(differentialCoeff));
    }
    BOOST_LOG_POLY("smolyak",debug) << "Done updating differentials and weights.";
    
    //as a final step, compute the current estimate, using the less precise weighted sum function
    finalEstimate = WeightedSumOfEstimates(termWeights);
}

template<typename T>
void SmolyakEstimate<T>::AddBatchOfTerms(MultiIndexFamily::Ptr const multiIndicesToAdd)
{
    RowVectorXu termsAdded;
      BOOST_LOG_POLY("smolyak",debug) << "Forcibly adding terms: "
				       << multiIndicesToAdd->GetAllMultiIndices();
    //first, forcibly add all the terms to figure out which ones we're adding
    for(unsigned int i=0; i< multiIndicesToAdd->GetNumberOfIndices(); i++)
    {
      RowVectorXu toAdd = termsIncluded->ForciblyActivateMultiIndex(multiIndicesToAdd->IndexToMulti(i));
      //expand and copy
      termsAdded.conservativeResize(termsAdded.cols() + toAdd.cols());
      termsAdded.tail(toAdd.cols()) = toAdd;
    }
    
  BOOST_LOG_POLY("smolyak",debug) << "Hence, adding indices:" << termsAdded;
				       
    //call the regular method
    AddBatchOfTerms(termsAdded.transpose());
}

template<typename T>
void SmolyakEstimate<T>::ComputeInitialTerms(MultiIndexFamily::Ptr const initialTerms)
{
    //first, add all the input initial terms to termsIncluded.
    for(unsigned int i=0; i< initialTerms->GetNumberOfIndices(); i++)
    {
	termsIncluded->ForciblyActivateMultiIndex(initialTerms->IndexToMulti(i));
    }
    
    BOOST_LOG_POLY("smolyak",debug) << "Actually going to start with terms: "
				       << termsIncluded->GetAllMultiIndices();
				       
    //now add all of them as a batch
    AddBatchOfTerms(termsIncluded->GetAllIndices());
    RecordAdaptProgress();
}


template<typename T>
void SmolyakEstimate<T>::ExpandStorage()
{
    //check that there aren't 0 elements total
    assert(termsIncluded->GetNumberOfIndices() > 0);
    
    //find out how many terms are included
    unsigned int newLength = termsIncluded->GetNumberOfIndices();
    
    //resize the vectors
    termEstimates.resize(newLength);
    differentialMagnitudes.resize(newLength);
    
    //resize the colvec
    if(termWeights.rows() == 0)
    {
	termWeights.setZero(newLength);
    }
    else if(termWeights.rows() < newLength);
    {
      unsigned int oldSize = termWeights.rows();
      termWeights.conservativeResize(newLength);
      termWeights.tail(newLength-oldSize) = VectorXd::Zero(newLength-oldSize);
    }
}


template<typename T>
void SmolyakEstimate<T>::ComputeDifferentialCoeff(unsigned int const index, VectorXd& coeff) const
{
    //allocate a vector we can multiply against the tensorRuleValues
    coeff = VectorXd::Zero(termsIncluded->GetNumberOfIndices());
    
    //the base multiIndex
    RowVectorXu baseMultiIndex = termsIncluded->IndexToMulti(index);
    
    
    for(unsigned int i=0; i<termsIncluded->GetNumberOfIndices(); ++i)
    {
	   RowVectorXi baseInt = baseMultiIndex.cast<int>(); 
	   RowVectorXi iInt = termsIncluded->IndexToMulti(i).cast<int>();
    
	RowVectorXi neighborDifferential = baseInt - iInt;
	
	//if this isn't a direct backwards neighbor, skip it
	int diffMax = neighborDifferential.maxCoeff();
	int diffMin = neighborDifferential.minCoeff();
	if(diffMax > 1 || diffMin < 0)
	{
	    continue;
	}
	
	//else, compute the difference
	 //compute how many -1 terms the neighbor got from subtracting the neighbor difference
	    //note that this could potentially overflow, converting from unsigned to signed
	    int parity = neighborDifferential.array().sum();
	    //take this neighbor and add (-1)^parity, computed with ternary operator
	    coeff(i) = coeff(i) + (parity % 2 == 0 ? 1 : -1);
    }
        
}



template<typename T>
T SmolyakEstimate<T>::StartFixedTerms(unsigned int initialSimplexLevels)
{
    assert(initialSimplexLevels > 0); //0 doesn't work because of the -1 below.
    return StartFixedTerms(TotalOrderMultiIndexFamily::Create(termsIncluded->GetMultiIndexLength(),
								initialSimplexLevels-1));
}

template<typename T>
T SmolyakEstimate<T>::AddFixedTerms(unsigned int initialSimplexLevels)
{
    assert(initialSimplexLevels > 0); //0 doesn't work because of the -1 below.
    return AddFixedTerms(TotalOrderMultiIndexFamily::Create(termsIncluded->GetMultiIndexLength(),
								initialSimplexLevels-1));
}

template<typename T>
T SmolyakEstimate<T>::StartAdaptiveTimed(unsigned int initialSimplexLevels, double secondsLimit)
{
    //just call through with the simplex
    return StartAdaptiveTimed(TotalOrderMultiIndexFamily::Create(termsIncluded->GetMultiIndexLength(),
								 initialSimplexLevels-1), secondsLimit);
}

template<typename T>
T SmolyakEstimate<T>::StartAdaptiveToTolerance(unsigned int initialSimplexLevels, double errorLimit)
{
    //just call through with the simplex
    return StartAdaptiveToTolerance(TotalOrderMultiIndexFamily::Create(termsIncluded->GetMultiIndexLength(),
								       initialSimplexLevels-1), errorLimit);
}

template<typename T>
T SmolyakEstimate<T>::PreciseWeightedSumOfEstimates(VectorXd const& weights) const
{
    return WeightedSumOfEstimates(weights);
}


template<typename T>
double SmolyakEstimate<T>::ComputeGlobalErrorIndicator()
{
    BOOST_LOG_POLY("smolyak",debug) << "Computing global error indicator.";
    double estimate = 0.0;
    for(unsigned int i=0; i<termsIncluded->GetNumberOfIndices(); i++)
    {
	if(IsTermEligibleForExpansion(i))
	{
	    estimate += ComputeLocalErrorIndicator(i);
	}
    }
    
    BOOST_LOG_POLY("smolyak",debug) << "Done computing global error indicator.";
    return estimate;
}


template<typename T>
double SmolyakEstimate<T>::GetGlobalErrorIndicator()
{
    return ComputeGlobalErrorIndicator();
}



	
	
template<typename T>
double SmolyakEstimate<T>::ComputeLocalErrorIndicator(unsigned int const termIndex)
{
    return differentialMagnitudes.at(termIndex);
}

template<typename T>
bool SmolyakEstimate<T>::IsTermEligibleForExpansion(unsigned int const termIndex)
{
    return (termsIncluded->IsIndexExpandable(termIndex));
}


template<typename T>
SmolyakEstimate<T>::SmolyakEstimate()
{
    
}

template<typename T>
MatrixXu SmolyakEstimate<T>::GetEffectiveIncludedTerms()
{
    return termsIncluded->GetAllMultiIndices();
}

template<typename T>
void SmolyakEstimate<T>::RecordAdaptProgress()
{
    finalEstimate = PreciseWeightedSumOfEstimates(termWeights);
    
    estimatesTrace.push_back(finalEstimate);
    workDoneTrace.push_back(ComputeWorkDone());
}

template<typename T>
std::vector<T>* SmolyakEstimate<T>::GetEstimateTrace()
{
    return &estimatesTrace;
}

template<typename T>
std::vector<unsigned int>* SmolyakEstimate<T>::GetWorkTrace()
{
    return &workDoneTrace;
}

template<typename T> template<class Archive>
void SmolyakEstimate<T>::serialize(Archive & ar, const unsigned int version)
{
    ar & termsIncluded;
    ar & termEstimates;
    ar & differentialMagnitudes;
    ar & termWeights;
    //ar & differentialNeighborCache;
    ar & finalEstimate;
    ar & estimatesTrace;
    ar & workDoneTrace;
}


///force it to build the templates
template class SmolyakEstimate<Eigen::VectorXd>;
template class SmolyakEstimate<PolynomialChaosExpansion::Ptr>;


BOOST_CLASS_EXPORT(SmolyakEstimate<Eigen::VectorXd>)


template void SmolyakEstimate<VectorXd>::serialize(boost::archive::text_oarchive & ar,
						       const unsigned int version);

template void SmolyakEstimate<VectorXd>::serialize(boost::archive::text_iarchive & ar,
						       const unsigned int version);
//
BOOST_CLASS_EXPORT(SmolyakEstimate<PolynomialChaosExpansion::Ptr>)

template void SmolyakEstimate<PolynomialChaosExpansion::Ptr>::serialize(boost::archive::text_oarchive & ar,
									const unsigned int version);
template void SmolyakEstimate<PolynomialChaosExpansion::Ptr>::serialize(boost::archive::text_iarchive & ar,
									const unsigned int version);
