%Plot the results of the nonadaptive Genz Testing
% plot cubics fit to the scatter plots
close all
clear

%%

try
GenzResultsAdaptive1
GenzResultsAdaptive2
GenzResultsAdaptive3
GenzResultsAdaptive4
GenzResultsAdaptive5
GenzResultsAdaptive6
catch
    
end

%% Set up plot vars
close all

plotColors = [228, 26, 28; 55, 126, 184; 77, 175, 74; 152, 78, 163; 255, 127, 0; ]/255;
plotMarkerStyleLine = '.-';
plotMarkerStyleScatter = '.';
plotMarkerSize = 5;
plotLineWidth = 1;
greyColor = [0.502 0.502 0.502];
font = 'Helvetica';
fontSize = 12;




%% Plot scatter

figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [5.5 8]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 5.5 8]);








for type=1:6
   subfig1 = subplot(3,2,type);
 
  loglog(GenzResults{type}.pattersonAdaptPoints(1,:), GenzResults{type}.pattersonAdaptErrorL2(1,:),...
       '*','Color', plotColors(1,:),'MarkerSize',plotMarkerSize)
   
    hold on
  
     loglog(GenzResults{type}.pattersonIso2Points(1,:), GenzResults{type}.pattersonIso2ErrorL2(1,:),...
       'o','Color', plotColors(2,:),'MarkerSize',plotMarkerSize)
   
   %hack to get the legend to show correctly
  loglog(GenzResults{type}.pattersonAdaptPoints(2:end,:), GenzResults{type}.pattersonAdaptErrorL2(2:end,:),...
       '*','Color', plotColors(1,:),'MarkerSize',plotMarkerSize)
     loglog(GenzResults{type}.pattersonIso2Points(2:end,:), GenzResults{type}.pattersonIso2ErrorL2(2:end,:),...
       'o','Color', plotColors(2,:),'MarkerSize',plotMarkerSize)
   



   set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font);
   legend off
   
%     set(subfig1,'ZColor',greyColor,'YScale','log',...
%     'YMinorTick','on',...
%     'YColor',greyColor,...
%     'XScale','log',...
%     'XMinorTick','on',...
%     'XColor',greyColor,...
%     'FontName',font, ...
%     'FontSize', fontSize, 'XTick', [10 1e3 1e5 1e7]);
 xlim([1, 3e7])


switch type
    case 1
        title('Oscillatory','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)

   

    case 2
        title('Product Peak' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
       
        %set(gca,'YTick',[1e-5,1e-3,1e-1])
        
    case 3
        title('Corner Peak','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
        
        set(gca,'YTick',[1e-5, 1e-4, 1e-3,1e-2,1e-1])
    case 4
        title('Gaussian' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
        
 
    case 5
        title('Continuous (C^1)' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
         ylabel('Approximate L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)
          xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
        
    case 6
        title('Discontinuous (C^0)' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
       % ylim([2, 100])
       leg = legend('Adapt G-P', 'Non-Adapt G-P');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, ...
    'YColor', greyColor,'FontSize', 8,'Location', 'SouthWest');%'Position',[.78 .05 .2 .2])
       
end


   
end



print -painters -dpdf -r600 GenzAdaptiveScatter.pdf


