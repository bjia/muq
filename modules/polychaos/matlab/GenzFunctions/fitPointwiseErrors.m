function [x,y] = fitPointwiseErrors(evaluations, errors, type)
if (nargin == 2)
    type = 1;
end

% evaluations = log(reshape(evaluations,[],1))/log(10);
% errors = log(abs(reshape(errors,[],1)))/log(10);
% 
% 
% ft_ = fittype('poly3');
% cf = fit(evaluations,errors,ft_);
% 
% x = sort(evaluations);
% y = feval(cf,x);
if(type==1)
x = evaluations(1,:)';
y = mean(errors,1)';
else
    x=evaluations;
    y=errors;
end