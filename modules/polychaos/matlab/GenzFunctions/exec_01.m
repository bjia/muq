%Plot the results of the Genz Testing
close all
clear
%%

try
GenzResults1
GenzResults2
GenzResults3
GenzResults4
GenzResults5
GenzResults6
GenzResults7
catch
    
end

%% Set up plot vars

plotColors = [142,31,33;
        36,31,142;
        127,74,151;
        47,134,20;
        208,126,23]/255;
plotMarkerStyle = '.-';
plotLineWidth = .1;
greyColor = [0.502 0.502 0.502];
font = 'Helvetica';

%% Make plots of L_inf error
figure1 = figure('Color', [1,1,1]);
for type=1:7
   subfig1 = subplot(2,4,type);
 
  

   %figure
   loglog(GenzResults{type}.FullTensorPoints',...,
       GenzResults{type}.FullTensorErrorLinf', plotMarkerStyle, 'Color', plotColors(1,:) ...
       , 'LineWidth', plotLineWidth)
  
    hold on
    loglog(GenzResults{type}.LinIsoPoints',...,
       GenzResults{type}.LinIsoErrorLinf',  plotMarkerStyle, 'Color', plotColors(2,:) ...
       , 'LineWidth', plotLineWidth)
   loglog(GenzResults{type}.ExpIsoPoints',...,
       GenzResults{type}.ExpIsoErrorLinf',  plotMarkerStyle, 'Color', plotColors(3,:) ...
       , 'LineWidth', plotLineWidth)
   loglog(GenzResults{type}.LinAdaptPoints',...,
       GenzResults{type}.LinAdaptErrorLinf',  plotMarkerStyle, 'Color', plotColors(4,:) ...
       , 'LineWidth', plotLineWidth)
   loglog(GenzResults{type}.ExpAdaptPoints',...,
       GenzResults{type}.ExpAdaptErrorLinf',  plotMarkerStyle, 'Color', plotColors(5,:) ...
       , 'LineWidth', plotLineWidth)
   
    set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName',font);

    
   title(['Genz function ' num2str(type)],'FontName',font,...
    'Color',greyColor)
   xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor)
   ylabel('Approximate L_\infty Error','FontName',font,...
    'Color',greyColor)
   
end

subfig1=subplot(2,4,8);
plot(1,1,'.-','Color', plotColors(1,:))
hold on
plot(1,1,'.-','Color', plotColors(2,:))
plot(1,1,'.-','Color', plotColors(3,:))
plot(1,1,'.-','Color', plotColors(4,:))
plot(1,1,'.-','Color', plotColors(5,:))

set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName','AvantGarde');

leg = legend('FullTensor', 'LinIso', 'ExpIso', 'LinAdapt', 'ExpAdapt');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, 'YColor', greyColor)
%% Make plots of L_2 error
figure1 = figure('Color', [1,1,1]);
for type=1:7
   subfig1 = subplot(2,4,type);
 
  

   %figure
   loglog(GenzResults{type}.FullTensorPoints',...,
       GenzResults{type}.FullTensorErrorL2', plotMarkerStyle, 'Color', plotColors(1,:) ...
       , 'LineWidth', plotLineWidth)
  
    hold on
    loglog(GenzResults{type}.LinIsoPoints',...,
       GenzResults{type}.LinIsoErrorL2',  plotMarkerStyle, 'Color', plotColors(2,:) ...
       , 'LineWidth', plotLineWidth)
   loglog(GenzResults{type}.ExpIsoPoints',...,
       GenzResults{type}.ExpIsoErrorL2',  plotMarkerStyle, 'Color', plotColors(3,:) ...
       , 'LineWidth', plotLineWidth)
   loglog(GenzResults{type}.LinAdaptPoints',...,
       GenzResults{type}.LinAdaptErrorL2',  plotMarkerStyle, 'Color', plotColors(4,:) ...
       , 'LineWidth', plotLineWidth)
   loglog(GenzResults{type}.ExpAdaptPoints',...,
       GenzResults{type}.ExpAdaptErrorL2',  plotMarkerStyle, 'Color', plotColors(5,:) ...
       , 'LineWidth', plotLineWidth)
   
    set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName',font);

    
   title(['Genz function ' num2str(type)],'FontName',font,...
    'Color',greyColor)
   xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor)
   ylabel('Approximate L_2 Error','FontName',font,...
    'Color',greyColor)
   
end

subfig1=subplot(2,4,8);
plot(1,1,'.-','Color', plotColors(1,:))
hold on
plot(1,1,'.-','Color', plotColors(2,:))
plot(1,1,'.-','Color', plotColors(3,:))
plot(1,1,'.-','Color', plotColors(4,:))
plot(1,1,'.-','Color', plotColors(5,:))

set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName','AvantGarde');

leg = legend('FullTensor', 'LinIso', 'ExpIso', 'LinAdapt', 'ExpAdapt');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, 'YColor', greyColor)




%% Plot error indicator Linf

figure1 = figure('Color', [1,1,1]);
for type=1:7
   subfig1 = subplot(2,4,type);
   loglog(GenzResults{type}.LinAdaptIndicator',...,
       GenzResults{type}.LinAdaptErrorLinf', plotMarkerStyle, 'Color', plotColors(4,:) ...
       , 'LineWidth', plotLineWidth)
   hold on
   loglog(GenzResults{type}.ExpAdaptIndicator',...,
       GenzResults{type}.ExpAdaptErrorLinf', plotMarkerStyle, 'Color', plotColors(5,:) ...
       , 'LineWidth', plotLineWidth)
    set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName',font);

    
   title(['Genz function ' num2str(type)],'FontName',font,...
    'Color',greyColor)
   xlabel('Error Indicator','FontName',font,...
    'Color',greyColor)
   ylabel('Approximate L_\infty Error','FontName',font,...
    'Color',greyColor)
end

subfig1=subplot(2,4,8);


plot(1,1,'.-','Color', plotColors(4,:))
hold on
plot(1,1,'.-','Color', plotColors(5,:))

set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName','AvantGarde');

leg = legend('LinAdapt', 'ExpAdapt');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, 'YColor', greyColor)



%% Plot error indicator L2

figure1 = figure('Color', [1,1,1]);
for type=1:7
   subfig1 = subplot(2,4,type);
   loglog(GenzResults{type}.LinAdaptIndicator',...,
       GenzResults{type}.LinAdaptErrorL2', plotMarkerStyle, 'Color', plotColors(4,:) ...
       , 'LineWidth', plotLineWidth)
   hold on
   loglog(GenzResults{type}.ExpAdaptIndicator',...,
       GenzResults{type}.ExpAdaptErrorL2', plotMarkerStyle, 'Color', plotColors(5,:) ...
       , 'LineWidth', plotLineWidth)
    set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName',font);

    
   title(['Genz function ' num2str(type)],'FontName',font,...
    'Color',greyColor)
   xlabel('Error Indicator','FontName',font,...
    'Color',greyColor)
   ylabel('Approximate L_2 Error','FontName',font,...
    'Color',greyColor)
end


subfig1=subplot(2,4,8);


plot(1,1,'.-','Color', plotColors(4,:))
hold on
plot(1,1,'.-','Color', plotColors(5,:))

set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName','AvantGarde');

leg = legend('LinAdapt', 'ExpAdapt');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, 'YColor', greyColor)
