%Plot the results of the nonadaptive Genz Testing
% plot cubics fit to the scatter plots
close all
clear

%%

try
GenzResults1
GenzResults2
GenzResults3
GenzResults4
GenzResults5
GenzResults6
GenzResults7
catch
    
end

%% Set up plot vars
close all

plotColors = [228, 26, 28; 55, 126, 184; 77, 175, 74; 152, 78, 163; 255, 127, 0; ]/255;
plotMarkerStyleLine = '.-';
plotMarkerStyleScatter = '.';
plotMarkerSize = 5;
plotLineWidth = 1;
greyColor = [0.502 0.502 0.502];
font = 'Helvetica';
fontSize = 12;




%% Plot average lines

figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [5.5 8]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 5.5 8]);








for type=1:6
   subfig1 = subplot(3,2,type);
 
  [x,y]=fitPointwiseErrors(GenzResults{type}.FullTensorPoints, GenzResults{type}.FullTensorErrorL2);
   loglog(x,y,'-*','Color', plotColors(1,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
   
    hold on
     [x,y]=fitPointwiseErrors(GenzResults{type}.LinIsoPoints, GenzResults{type}.LinIsoErrorL2);
   loglog(x,y,'-o','Color', plotColors(2,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)




      [x,y]=fitPointwiseErrors(GenzResults{type}.ExpIsoPoints, GenzResults{type}.ExpIsoErrorL2);
   loglog(x,y,'-+','Color', plotColors(3,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)


       [x,y]=fitPointwiseErrors(GenzResults{type}.pattersonIsoPoints, GenzResults{type}.pattersonIsoErrorL2);
   loglog(x,y,'-s','Color', plotColors(4,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)


       [x,y]=fitPointwiseErrors(GenzResults{type}.ccIsoPoints, GenzResults{type}.ccIsoErrorL2);
   loglog(x,y,'-d','Color', plotColors(5,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)

   set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font);
   legend off
   
%     set(subfig1,'ZColor',greyColor,'YScale','log',...
%     'YMinorTick','on',...
%     'YColor',greyColor,...
%     'XScale','log',...
%     'XMinorTick','on',...
%     'XColor',greyColor,...
%     'FontName',font, ...
%     'FontSize', fontSize, 'XTick', [10 1e3 1e5 1e7]);
 xlim([1, 3e7])


switch type
    case 1
        title('Oscillatory','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)

   

    case 2
        title('Product Peak' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
        ylim([1e-5,.2])
        set(gca,'YTick',[1e-5,1e-3,1e-1])
        
    case 3
        title('Corner Peak','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
        ylim([1e-3,.4])
        set(gca,'YTick',[1e-3,1e-2,1e-1])
    case 4
        title('Gaussian' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
        
 
    case 5
        title('Continuous (C^1)' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
         ylabel('Approximate L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)
          xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
        leg = legend('Full Tensor', 'Lin-G', 'Exp-G','G-P','C-C');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, ...
    'YColor', greyColor,'FontSize', 8,'Location', 'SouthWest');%'Position',[.78 .05 .2 .2])
    case 6
        title('Discontinuous (C^0)' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
        ylim([1, 100])
       
end


   
end



print -painters -dpdf -r600 GenzImprovements.pdf

%% Plot full scatter lines

figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [7 8]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 7 8]);

subfig1=subplot(3,3,7);
plot(1,1,'.-','Color', plotColors(1,:))
hold on
axis off
plot(1,1,'.-','Color', plotColors(2,:))
plot(1,1,'.-','Color', plotColors(3,:))
plot(1,1,'.-','Color', plotColors(4,:))
plot(1,1,'.-','Color', plotColors(5,:))



leg = legend('FullTensor', 'LinIso', 'ExpIso','Patterson','CC');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, 'YColor', greyColor,'FontSize', fontSize)


for type=1:6
   subfig1 = subplot(3,3,type);
 
  [x,y]=fitPointwiseErrors(GenzResults{type}.FullTensorPoints, GenzResults{type}.FullTensorErrorL2,2);
   loglog(x,y,plotMarkerStyleScatter,'Color', plotColors(1,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
   
    hold on
     [x,y]=fitPointwiseErrors(GenzResults{type}.LinIsoPoints, GenzResults{type}.LinIsoErrorL2,2);
   loglog(x,y,plotMarkerStyleScatter,'Color', plotColors(2,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)




      [x,y]=fitPointwiseErrors(GenzResults{type}.ExpIsoPoints, GenzResults{type}.ExpIsoErrorL2,2);
   loglog(x,y,plotMarkerStyleScatter,'Color', plotColors(3,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)


       [x,y]=fitPointwiseErrors(GenzResults{type}.pattersonIsoPoints, GenzResults{type}.pattersonIsoErrorL2,2);
   loglog(x,y,plotMarkerStyleScatter,'Color', plotColors(4,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)


       [x,y]=fitPointwiseErrors(GenzResults{type}.ccIsoPoints, GenzResults{type}.ccIsoErrorL2,2);
   loglog(x,y,plotMarkerStyleScatter,'Color', plotColors(5,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)

   legend off
   
%     set(subfig1,'ZColor',greyColor,'YScale','log',...
%     'YMinorTick','on',...
%     'YColor',greyColor,...
%     'XScale','log',...
%     'XMinorTick','on',...
%     'XColor',greyColor,...
%     'FontName',font, ...
%     'FontSize', fontSize, 'XTick', [10 1e3 1e5 1e7]);
% xlim([10, 3e7])


switch type
    case 1
        title('Oscillatory','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
 
    ylabel('Approximate L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)

    case 2
        title('Product Peak' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 3
        title('Corner Peak','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 4
        title('Gaussian' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
        
          xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
    case 5
        title('Continuous' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 6
        title('Discontinuous' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
end


   
end



%print -painters -dpdf -r600 GenzScatter.pdf

