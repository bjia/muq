%Plot the results of the Genz Testing
close all

%%

try
GenzResults1
GenzResults2
GenzResults3
GenzResults4
GenzResults5
GenzResults6
GenzResults7
catch
    
end

%% Set up plot vars
close all
plotColors = [142,31,33;
        36,31,142;
        127,74,151;
        47,134,20;
        208,126,23]/255;
plotMarkerStyle = '.';
plotMarkerSize = 13;
plotLineWidth = 1;
greyColor = [0.502 0.502 0.502];
font = 'Helvetica';
fontSize = 14;

%% Plot relative decrease in error indicator

figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [6 4]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 6 4]);


for type=1:6
   subfig1 = subplot(2,4,type+1*(type>3));
 
  
   

   %figure

   
   tensorImprovements = (GenzResults{type}.FullTensorErrorL2(:,1)*ones(1,size(GenzResults{type}.FullTensorErrorL2,2)))./GenzResults{type}.FullTensorErrorL2;
   loglog(GenzResults{type}.FullTensorPoints(1,:), mean(tensorImprovements), plotMarkerStyle, 'Color', plotColors(1,:) ...
       , 'LineWidth', plotLineWidth)
  
  
    hold on
    
    tensorImprovements = (GenzResults{type}.FullTensorErrorL2(:,1)*ones(1,size(GenzResults{type}.LinIsoErrorL2,2)))./GenzResults{type}.LinIsoErrorL2;
    loglog(GenzResults{type}.LinIsoPoints(1,:), mean(tensorImprovements), plotMarkerStyle, 'Color', plotColors(2,:) ...
        , 'LineWidth', plotLineWidth)
    
    tensorImprovements = (GenzResults{type}.FullTensorErrorL2(:,1)*ones(1,size(GenzResults{type}.ExpIsoErrorL2,2)))./GenzResults{type}.ExpIsoErrorL2;
    loglog(GenzResults{type}.ExpIsoPoints(1,:), mean(tensorImprovements), plotMarkerStyle, 'Color', plotColors(3,:) ...
        , 'LineWidth', plotLineWidth)
    
    tensorImprovements = (GenzResults{type}.FullTensorErrorL2(:,1)*ones(1,size(GenzResults{type}.LinAdaptErrorL2,2)))./GenzResults{type}.LinAdaptErrorL2;
    loglog(GenzResults{type}.LinAdaptPoints(1,:), mean(tensorImprovements), plotMarkerStyle, 'Color', plotColors(4,:) ...
        , 'LineWidth', plotLineWidth)
    
    tensorImprovements = (GenzResults{type}.FullTensorErrorL1(:,1)*ones(1,size(GenzResults{type}.ExpAdaptErrorL2,2)))./GenzResults{type}.ExpAdaptErrorL2;
    loglog(GenzResults{type}.ExpAdaptPoints(1,:), mean(tensorImprovements), plotMarkerStyle, 'Color', plotColors(5,:) ...
        , 'LineWidth', plotLineWidth)
   
    set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName',font, ...
    'FontSize', fontSize);
xlim([10, 3e7])

switch type
    case 1
        title('Oscillatory','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
   
   

    case 2
        title('Product Peak' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 3
        title('Corner Peak','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 4
        title('Gaussian' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
         ylabel('Approximate Improvement in L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)
          xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
    case 5
        title('Continuous' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 6
        title('Discontinuous' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
end


   
end



leg = legend('FullTensor', 'LinIso', 'ExpIso', 'LinAdapt', 'ExpAdapt','Location','NorthEastOutside');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, 'YColor', greyColor,'FontSize', fontSize)

print -painters -dpdf -r600 GenzImprovements.pdf


%% Plot scatter

figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [7 8]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 7 8]);


for type=1:6
   subfig1 = subplot(3,3,type);
 
  
   loglog(GenzResults{type}.FullTensorPoints',...,
       GenzResults{type}.FullTensorErrorLinf', plotMarkerStyle, 'Color', plotColors(1,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
  
    hold on
    loglog(GenzResults{type}.LinIsoPoints',...,
       GenzResults{type}.LinIsoErrorLinf',  plotMarkerStyle, 'Color', plotColors(2,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
   loglog(GenzResults{type}.ExpIsoPoints',...,
       GenzResults{type}.ExpIsoErrorLinf',  plotMarkerStyle, 'Color', plotColors(3,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
   loglog(GenzResults{type}.LinAdaptPoints',...,
       GenzResults{type}.LinAdaptErrorLinf',  plotMarkerStyle, 'Color', plotColors(4,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
   loglog(GenzResults{type}.ExpAdaptPoints',...,
       GenzResults{type}.ExpAdaptErrorLinf',  plotMarkerStyle, 'Color', plotColors(5,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
   
    set(subfig1,'ZColor',greyColor,'YScale','log',...
    'YMinorTick','on',...
    'YColor',greyColor,...
    'XScale','log',...
    'XMinorTick','on',...
    'XColor',greyColor,...
    'FontName',font, ...
    'FontSize', fontSize, 'XTick', [10 1e3 1e5 1e7]);
xlim([10, 3e7])


switch type
    case 1
        title('Oscillatory','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
   , 'MarkerSize', plotMarkerSize
    ylabel('Approximate L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)

    case 2
        title('Product Peak' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 3
        title('Corner Peak','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 4
        title('Gaussian' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
        
          xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
    case 5
        title('Continuous' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 6
        title('Discontinuous' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
end


   
end


subfig1=subplot(3,3,7);
plot(1,1,'.-','Color', plotColors(1,:))
hold on
axis off
plot(1,1,'.-','Color', plotColors(2,:))
plot(1,1,'.-','Color', plotColors(3,:))
plot(1,1,'.-','Color', plotColors(4,:))
plot(1,1,'.-','Color', plotColors(5,:))


leg = legend('FullTensor', 'LinIso', 'ExpIso', 'LinAdapt', 'ExpAdapt');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, 'YColor', greyColor,'FontSize', fontSize)

print -painters -dpdf -r600 GenzScatter.pdf

%% Plot one scatter

figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [7 6]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 7 6]);


type=4;

  
plot1 = loglog(1e2,1e-10,'.-','Color', plotColors(1,:), 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize);
hold on

loglog(1e2,1e-10,'.-','Color', plotColors(2,:), 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
loglog(1e2,1e-10,'.-','Color', plotColors(3,:), 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
loglog(1e2,1e-10,'.-','Color', plotColors(4,:), 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
loglog(1e2,1e-10,'.-','Color', plotColors(5,:), 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)

 
   loglog(GenzResults{type}.FullTensorPoints',...,
       GenzResults{type}.FullTensorErrorLinf', plotMarkerStyle, 'Color', plotColors(1,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
  
    hold on
    loglog(GenzResults{type}.LinIsoPoints',...,
       GenzResults{type}.LinIsoErrorLinf',  plotMarkerStyle, 'Color', plotColors(2,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
   loglog(GenzResults{type}.ExpIsoPoints',...,
       GenzResults{type}.ExpIsoErrorLinf',  plotMarkerStyle, 'Color', plotColors(3,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
   loglog(GenzResults{type}.LinAdaptPoints',...,
       GenzResults{type}.LinAdaptErrorLinf',  plotMarkerStyle, 'Color', plotColors(4,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
   loglog(GenzResults{type}.ExpAdaptPoints',...,
       GenzResults{type}.ExpAdaptErrorLinf',  plotMarkerStyle, 'Color', plotColors(5,:) ...
       , 'LineWidth', plotLineWidth, 'MarkerSize', plotMarkerSize)
   
    set(gca,...
    'YColor',greyColor,...
    ...
    'XColor',greyColor,...
    'FontName',font, ...
    'FontSize', fontSize, 'XTick', [10 1e3 1e5 1e7]);


switch type
    case 1
        title('Oscillatory','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
   
    ylabel('Approximate L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)

    case 2
        title('Product Peak' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 3
        title('Corner Peak','FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 4
        title('Gaussian Genz Function' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
          ylabel('Approximate L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)
          xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
xlim([10,2e7]);
ylim([1e-11,8e-4]);
    case 5
        title('Continuous' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 6
        title('Discontinuous' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
    case 7
      title('Decoupled' ,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)
end


   



% subfig1=subplot(3,3,7);



leg = legend('FullTensor', 'LinIso', 'ExpIso', 'LinAdapt', 'ExpAdapt', 'Location', 'Southwest');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, 'YColor', greyColor,'FontSize', fontSize)

print -painters -dpdf -r600 GenzScatterGauss.pdf