%% plot the performance of the three methods on the combustion problem
%load data
load simplexL2.dat
load adaptiveGPL2.dat
load adaptiveGaussianL2.dat
load adaptiveCCL2.dat
load adaptiveGaussianLinearL2.dat


%% Set up plot vars
close all

plotColors = [228, 26, 28; 55, 126, 184; 77, 175, 74; 152, 78, 163; 255, 127, 0; ]/255;
plotMarkerStyleLine = '.-';
plotMarkerStyleScatter = '.';
plotMarkerSize = 4;
plotLineWidth = 1;
greyColor = [0.502 0.502 0.502];
font = 'Helvetica';
fontSize = 12;


%% actually plot the convergence
figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [6 5]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 6 5]);
    
    loglog(simplexL2(1:end-1,1),simplexL2(1:end-1,2), '-*', 'Color', plotColors(1,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize);
    
    hold on;
        semilogy(adaptiveGaussianLinearL2(:,1),adaptiveGaussianLinearL2(:,2),  '-o','Color', plotColors(2,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize);
    loglog(adaptiveGaussianL2(:,1),adaptiveGaussianL2(:,2),  '-+','Color', plotColors(3,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize);

    loglog(adaptiveGPL2(:,1),adaptiveGPL2(:,2),  '-s','Color', plotColors(4,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize);
    loglog(adaptiveCCL2(:,1),adaptiveCCL2(:,2),  '-d','Color', plotColors(5,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize);
leg = legend('Non-Adaptive', 'Lin. Gauss', 'Exp. Gauss','Gauss-Patterson','Clenshaw-Curtis');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, ...
    'YColor', greyColor,'FontSize', fontSize)    
set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, 'FontSize', fontSize);

 ylabel('Approximate L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)
          xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)

print -painters -dpdf -r600 CombustionConvergence.pdf
