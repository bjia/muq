%plots the field of coefficients for the combustion example
clear
close all
% from http://colorbrewer2.org/
blueColormap = [255, 247, 251; 236, 231, 242; 208, 209, 230; 166, 189, 219; 116, 169, 207; 54, 144, 192; 5, 112, 176; 4, 90, 141; 2, 56, 88]/255;
divergingColorMap = [103, 0, 31; 178, 24, 43; 214, 96, 77; 244, 165, 130; 253, 219, 199; 225, 225, 225; 209, 229, 240; 146, 197, 222; 67, 147, 195; 33, 102, 172; 5, 48, 97]/255;
blueColormapDetailed = interp1([1:9]',blueColormap, [2:.05:9]');
divergingColorMapDetailed = interp1([1:11]',divergingColorMap, [1:.05:11]');
greyColor = [0.502 0.502 0.502];
font = 'Helvetica';


%%

[myindex2, mycoeff2] = readPCE('adaptivePCEGP_results.dat',14,1);


    
    
    figure1 = figure('Color',[1 1 1]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [5 3]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 5 3]);


axes1 = axes('Parent',figure1,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font);
hold(axes1,'all');
    
    
    coeffData2(:,1) = floor(log(abs(mycoeff2))/log(10)); %log of coeffs
    coeffData2(:,2) = sum(myindex2,2);%max(myindex2,[],2); % total orders
    
    
    [X2,Y2] = meshgrid(0:30,-15:3);
    
    coeffLocations2 = zeros(size(X2));
    
    for i=1:size(X2,1)
        for j=1:size(X2,2)
            coeffLocations2(i,j) = sum(ismember(coeffData2, [Y2(i,j),X2(i,j)],'rows'));
        end
    end
    
    h=pcolor(X2,Y2,sign(coeffLocations2).*log(coeffLocations2)/log(10));
    set(h,'LineStyle','none')
    colorbar('ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font)
caxis([0,4])    
colormap(blueColormapDetailed)
xlabel('Total Order of Basis','FontName',font,...
    'Color',greyColor)
   ylabel('Log Coefficient Values','FontName',font,...
    'Color',greyColor)
ylim([-15,3])


    print -painters -dpdf -r600 SimpleCombustionCoeffAdaptive.pdf
    %%
    
   [myindex, mycoeff] = readPCE('simplex_results.dat',14,1);
     
   
    
    figure1 = figure('Color',[1 1 1]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [5 3]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 5 3]);

axes1 = axes('Parent',figure1,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font);
hold(axes1,'all');
    
    coeffData(:,1) = floor(log(abs(mycoeff))/log(10)); %log of coeffs
    coeffData(:,2) = sum(myindex,2); %max(myindex,[],2);% total orders
    
    
    [X,Y] = meshgrid(0:30,-15:3);
    
    coeffLocations = zeros(size(X));
    
    for i=1:size(X,1)
        for j=1:size(X,2)
            coeffLocations(i,j) = sum(ismember(coeffData, [Y(i,j),X(i,j)],'rows'));
        end
    end
    
   h=pcolor(X,Y,sign(coeffLocations).*log(coeffLocations)/log(10));
    set(h,'LineStyle','none')
    cbar=colorbar('ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font);
ylabel(cbar, 'log_{10} (# of coefficients)','FontName',font,...
    'Color',greyColor)
caxis([0,4])    
colormap(blueColormapDetailed)
xlabel('Total Order of Basis','FontName',font,...
    'Color',greyColor)
   ylabel('Log Coefficient Values','FontName',font,...
    'Color',greyColor)
ylim([-15,3])
    print -painters -dpdf -r600 SimpleCombustionCoeffNonAdaptive.pdf
    %% diff
    
    
      
    figure1 = figure('Color',[1 1 1]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [5 3]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 5 3]);

axes1 = axes('Parent',figure1,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font);
hold(axes1,'all');

    diffCoeffsRaw = coeffLocations-coeffLocations2;
    
    diffCoeffs = sign(diffCoeffsRaw).*log(abs(diffCoeffsRaw))/log(10);
    
   badNan = isnan(diffCoeffs) & (diffCoeffsRaw == 0) & (( coeffLocations~=0) | ( coeffLocations2~=0));
   diffCoeffs(badNan) = 1e-5;
  
    
    
    h=pcolor(X,Y,diffCoeffs);
    set(h,'LineStyle','none')
    cbar = colorbar('ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font);
ylabel(cbar, 'log_{10} (\Delta # of coefficients)','FontName',font,...
    'Color',greyColor)
 caxis([-4,4])   
  colormap(divergingColorMapDetailed)
xlabel('Total Order of Basis','FontName',font,...
    'Color',greyColor)
   ylabel('Log Coefficient Values','FontName',font,...
    'Color',greyColor)
ylim([-15,3])

print -painters -dpdf -r600 SimpleCombustionCoeffDiff.pdf
    