
close all;

plotSinglePCEErrors('Constantine/constantineSpamPaper1f', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper1f',2,1);
plotSinglePCEErrors('Constantine/constantineSpamPaper1e', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper1e',2,1);

plotSinglePCEErrors('Constantine/constantineSpamPaper2f', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper2f',2,1);
plotSinglePCEErrors('Constantine/constantineSpamPaper2e', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper2e',2,1);

plotSinglePCEErrors('Constantine/constantineSpamPaper3f', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper3f',2,1);
plotSinglePCEErrors('Constantine/constantineSpamPaper3e', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper3e',2,1);

plotSinglePCEErrors('Constantine/constantineSpamPaper4f', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper4f',2,1);
plotSinglePCEErrors('Constantine/constantineSpamPaper4e', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper4e',2,1);

plotSinglePCEErrors('Constantine/constantineSpamPaper5f', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper5f',2,1);
plotSinglePCEErrors('Constantine/constantineSpamPaper5e', 2,1)
plotConvergenceResultsFixed('Constantine/constantineSpamPaper5e',2,1);

plotConvergenceResultsFixed('Constantine/constantinePaperSPAM1f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM1e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM2f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM2e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM3f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM3e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM4f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM4e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM5f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperSPAM5e',2,1);

plotConvergenceResultsFixed('Constantine/constantinePaperTrad1f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad1e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad2f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad2e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad3f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad3e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad4f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad4e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad5f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTrad5e',2,1);


plotConvergenceResultsFixed('Constantine/constantinePaperTensor1_15',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor1_129',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor2_15',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor2_129',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor3_15',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor3_129',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor4_15',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor4_129',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor5_15',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperTensor5_129',2,1);

plotConvergenceResultsFixed('Constantine/constantinePaperAdapt1f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt1e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt2f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt2e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt3f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt3e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt4f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt4e',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt5f',2,1);
plotConvergenceResultsFixed('Constantine/constantinePaperAdapt5e',2,1);

plotConvergenceResultsFixed('Constantine/singlePoly',2,1);
plotConvergenceResultsFixed('Constantine/singlePolySpamf_8_8',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTradf_8_8',2,1);
plotConvergenceResultsFixed('Constantine/singlePolySpamf_16_0',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTradf_16_0',2,1);
plotConvergenceResultsFixed('Constantine/singlePolySpamf_14_0',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTradf_14_0',2,1);
plotConvergenceResultsFixed('Constantine/singlePolySpamf_6_8',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTradf_6_8',2,1);
plotConvergenceResultsFixed('Constantine/singlePolySpamf_3_4',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTradf_3_4',2,1);
plotConvergenceResultsFixed('Constantine/singlePolySpamf_20_20',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTradf_20_20',2,1);
plotConvergenceResultsFixed('Constantine/singlePolySpamf_6_8_skip',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTradf_6_8_skip',2,1);
plotConvergenceResultsFixed('Constantine/singlePolySpamf_5_3_skip_asym',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTradf_5_3_skip_asym',2,1);

plotConvergenceResultsFixed('Constantine/singlePolySpame',2,1);
plotConvergenceResultsFixed('Constantine/singlePolyTrade_1_3_skip_asym',2,1);
clim(-14, 0)