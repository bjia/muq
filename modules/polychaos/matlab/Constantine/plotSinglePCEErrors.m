function plotSinglePCEErrors(baseName, inputDim, outputDim)

if(inputDim==2)
 figure
   
    %errors
testData = load(['results/' baseName '_points.dat']);
   testPoints = testData(:, 1:inputDim);
 
    x = reshape(testPoints(:,1), sqrt(size(testPoints,1)),sqrt(size(testPoints,1)));
    y = reshape(testPoints(:,2), sqrt(size(testPoints,1)),sqrt(size(testPoints,1)));
    
   realValues = testData(:, inputDim+1:inputDim+outputDim);
   pceValues = testData(:, inputDim+outputDim + 1:end);
   for j=1:size(realValues,1)
   errors(j) = log(norm(realValues(j,:) - pceValues(j,:)))/log(10);
   end
   
   surf(x,y,reshape(errors, sqrt(size(testPoints,1)),sqrt(size(testPoints,1))), 'FaceAlpha', .5);
   zlabel('Log Error')
   hold on
   
   
   
   title(['Function Errors for ' baseName])
   colorbar
   
   %Actual function
   figure
   x = reshape(testPoints(:,1), sqrt(size(testPoints,1)),sqrt(size(testPoints,1)));
   y = reshape(testPoints(:,2), sqrt(size(testPoints,1)),sqrt(size(testPoints,1)));
   
   for j=1:outputDim
       zReal = reshape(realValues(:,j), sqrt(size(testPoints,1)),sqrt(size(testPoints,1)));
       surf(x,y,zReal);
       hold on
   end
   
   legend('Real');
   
   title(['First Output Function Values for ' baseName])
   
end
