
%These will generate the four comparison charts for the paper, although
%plotPCECoeff needs some tweaking before they will work correctly,
%that is, to set the point and the output file.
plotConvergenceResultsFixed('Constantine/dqInternal',2,1);
plotConvergenceResultsFixed('Constantine/dqExternal',2,1);
plotConvergenceResultsFixed('Constantine/smolExternal',2,1);
plotConvergenceResultsFixed('Constantine/smolInternal',2,1);