function plotConvergenceResultsFixed(baseName, inputDim, outputDim)


%% plot pce terms

  
    
    quadrature = load([ baseName '_terms.dat']);
   
    
    fpce = fopen([ baseName  '_result.dat']);
    numPceTerms = textscan(fpce, '%d',1);
    
    formatD = '';
    formatF = '';
    for j=1:inputDim
        formatD = [formatD ' %d'];
    end
    for j=1:outputDim
        formatF = [formatF ' %f'];
    end
    
    pceTerms = textscan(fpce,formatD, numPceTerms{1}, 'CollectOutput', 1);
    pceCoeffs = textscan(fpce, formatF, numPceTerms{1}, 'CollectOutput', 1);
    fclose(fpce);
    
    plotPCECoeff(inputDim, pceTerms{1}, pceCoeffs{1}, quadrature, baseName);
    


