function plotPCECoeff(inputDim, pceTerms, pceCoeffs, quadrature, name, tolerance, useLog)
if(nargin < 7)
    useLog = 1;
end
if(nargin < 6)
    tolerance = 0;
end

if(inputDim == 1)
    pceTerms(:,2) = ones(length(pceTerms),1);
    quadrature(:,2) = 3*ones(length(quadrature),1);
end
if(useLog)
    pointWeight = log(sum(pceCoeffs, 2))/log(10);
else
    pointWeight = abs(sum(pceCoeffs, 2));
end

%%
greyColor = [0.502 0.502 0.502];
font = 'Helvetica';

% from http://colorbrewer2.org/
blueColormap = [255, 247, 251; 236, 231, 242; 208, 209, 230; 166, 189, 219; 116, 169, 207; 54, 144, 192; 5, 112, 176; 4, 90, 141; 2, 56, 88]/255;

blueColormapDetailed = interp1([1:9]',blueColormap, [3:.05:9]');

%%



pceTerms = double(pceTerms);
X(1,:) = pceTerms(:,1)'-.5;
X(2,:) = pceTerms(:,1)'-.5;
X(3,:) = pceTerms(:,1)'+.5;
X(4,:) = pceTerms(:,1)'+.5;

Y(1,:) = pceTerms(:,2)'-.5;
Y(2,:) = pceTerms(:,2)'+.5;
Y(3,:) = pceTerms(:,2)'+.5;
Y(4,:) = pceTerms(:,2)'-.5;

figure1 = figure('Color',[1 1 1]);
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [3 2.5]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 3 2.5]);

axes1 = axes('Parent',figure1,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, ...
'FontSize', 8);
hold(axes1,'all');

patch(X,Y,pointWeight','LineStyle','none');
xlim([-.5, max(max(X))])
ylim([-.5, max(max(Y))])
axis square
caxis([-15, 1])

%scatter(pceTerms(:,1), pceTerms(:,2), ...
%        2000*ones(size(pointWeight,1),1),pointWeight,'.');
colormap(blueColormapDetailed)

    cbar = colorbar('ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font);
xlabel(cbar, 'log_{10} |f_i|','FontName',font,...
    'Color',greyColor,'FontSize', 10)

   ylabel(gca, 'Order of y polynomial','FontName',font,...
    'Color',greyColor,'FontSize', 10)
xlabel(gca, 'Order of x polynomial','FontName',font,...
    'Color',greyColor, 'FontSize', 10)
hold on
    scatter(0,4,200, 'ok', 'LineWidth', 2);
    print -painters -dpdf -r600 InternalSmol.pdf
     
%     title(['PCE Coeff for ' name ' at tol=' num2str(tolerance)]);
     
     %uncomment to plot the order the terms were added
     %plot(pceTerms(:,1), pceTerms(:,2)); ./bootstrap.sh --prefix=path/to/installation/prefix