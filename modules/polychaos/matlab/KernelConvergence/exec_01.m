%close all;

clear

%%

plotColors = [228, 26, 28; 55, 126, 184; 77, 175, 74; 152, 78, 163; 255, 127, 0; 166,86,40]/255;
plotMarkerStyleLine = '.-';
plotMarkerStyleScatter = '.';
plotMarkerSize = 10;
plotLineWidth = 2;
greyColor = [0.502 0.502 0.502];
font = 'Helvetica';
fontSize = 12;

%% 1/r for a few sizes

figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [6 5]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 6 5]);



load Laplace3_uniform_adaptive_dim3_1_results.dat
load Laplace3_uniform_adaptive_dim3_10_results.dat
load Laplace3_uniform_adaptive_dim3_100_results.dat
load Laplace3_uniform_adaptive_dim3_1000_results.dat

load Laplace3_uniform_adaptive_dim3_1_work.dat
load Laplace3_uniform_adaptive_dim3_10_work.dat
load Laplace3_uniform_adaptive_dim3_100_work.dat
load Laplace3_uniform_adaptive_dim3_1000_work.dat

load Laplace3_uniform_tensor_dim3_1_results.dat
load Laplace3_uniform_tensor_dim3_10_results.dat
load Laplace3_uniform_tensor_dim3_100_results.dat
load Laplace3_uniform_tensor_dim3_1000_results.dat

load Laplace3_uniform_tensor_dim3_1_work.dat
load Laplace3_uniform_tensor_dim3_10_work.dat
load Laplace3_uniform_tensor_dim3_100_work.dat
load Laplace3_uniform_tensor_dim3_1000_work.dat





loglog(reshape(Laplace3_uniform_tensor_dim3_1_work,1,[]),...
    reshape(Laplace3_uniform_tensor_dim3_1_results,1,[]) , 'o','Color', plotColors(1,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
hold on
loglog(reshape(Laplace3_uniform_tensor_dim3_100_work,1,[]),...
    reshape(Laplace3_uniform_tensor_dim3_100_results,1,[]) , '^','Color', plotColors(2,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
   

          
    loglog(reshape(Laplace3_uniform_tensor_dim3_1000_work,1,[]),...
    reshape(Laplace3_uniform_tensor_dim3_1000_results,1,[]) , '*','Color', plotColors(5,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
      
    loglog(reshape(Laplace3_uniform_adaptive_dim3_1_work,1,[]),...
    reshape(Laplace3_uniform_adaptive_dim3_1_results,1,[]) , 'd','Color', plotColors(3,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
    
    loglog(reshape(Laplace3_uniform_adaptive_dim3_100_work,1,[]),...
    reshape(Laplace3_uniform_adaptive_dim3_100_results,1,[]) , 's','Color', plotColors(4,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
   

        loglog(reshape(Laplace3_uniform_adaptive_dim3_1000_work,1,[]),...
    reshape(Laplace3_uniform_adaptive_dim3_1000_results,1,[]) , 'x','Color', plotColors(6,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)


    leg = legend('Full tensor, 1', 'Full tensor, 100', 'Full tensor, 1000', 'Adaptive, 1','Adaptive, 100','Adaptive, 1000');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, ...
    'YColor', greyColor,'FontSize', fontSize, 'Location', 'SouthWest')    
set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, 'FontSize', fontSize);

 ylabel('Approximate Pointwise Relative Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)
 xlabel('Number of Equivalent Charges','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)

print -painters -dpdf -r600 LaplaceSizes.pdf

%% plot a bunch of kernels

load Laplace2_tensor_dim2_100_results.dat
load Laplace2_adaptive_dim2_100_results.dat
load Laplace3_tensor_dim3_100_results.dat
load Laplace3_adaptive_dim3_100_results.dat
load LaplaceM3_tensor_dim3_100_results.dat
load LaplaceM3_adaptive_dim3_100_results.dat
load Gauss_tensor_dim3_100_results.dat
load Gauss_adaptive_dim3_100_results.dat
load Plate_tensor_dim4_100_results.dat
load Plate_adaptive_dim4_100_results.dat

load Laplace2_tensor_dim2_100_work.dat
load Laplace2_adaptive_dim2_100_work.dat
load Laplace3_tensor_dim3_100_work.dat
load Laplace3_adaptive_dim3_100_work.dat
load LaplaceM3_tensor_dim3_100_work.dat
load LaplaceM3_adaptive_dim3_100_work.dat
load Gauss_tensor_dim3_100_work.dat
load Gauss_adaptive_dim3_100_work.dat
load Plate_tensor_dim4_100_work.dat
load Plate_adaptive_dim4_100_work.dat

figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [12 8]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 12 8]);

subplot(2,3,1)

loglog(reshape(Laplace2_tensor_dim2_100_work,1,[]),...
    reshape(Laplace2_tensor_dim2_100_results,1,[]) , 'o','Color', plotColors(1,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
hold on
loglog(reshape(Laplace2_adaptive_dim2_100_work,1,[]),...
    reshape(Laplace2_adaptive_dim2_100_results,1,[]) , '^','Color', plotColors(2,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)

    leg = legend('Full tensor', 'Adaptive');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, ...
    'YColor', greyColor,'FontSize', fontSize)    
set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, 'FontSize', fontSize);

 ylabel('Approximate Pointwise Relative Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)
 xlabel('Number of Equivalent Charges','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)

 title('Laplace 2D Kernel','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
xlim([1 3000])
ylim([1e-16, 1000])
%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,3,2)

loglog(reshape(Laplace3_tensor_dim3_100_work,1,[]),...
    reshape(Laplace3_tensor_dim3_100_results,1,[]) , 'o','Color', plotColors(1,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
hold on
loglog(reshape(Laplace3_adaptive_dim3_100_work,1,[]),...
    reshape(Laplace3_adaptive_dim3_100_results,1,[]) , '^','Color', plotColors(2,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)

 
 title('3D Laplace Kernel','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
xlim([1 3000])
ylim([5e-8, 200])
set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, 'FontSize', fontSize);
%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,3,3)

loglog(reshape(LaplaceM3_tensor_dim3_100_work,1,[]),...
    reshape(LaplaceM3_tensor_dim3_100_results,1,[]) , 'o','Color', plotColors(1,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
hold on
loglog(reshape(LaplaceM3_adaptive_dim3_100_work,1,[]),...
    reshape(LaplaceM3_adaptive_dim3_100_results,1,[]) , '^','Color', plotColors(2,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)

 
 title('Modified Laplace 3D Kernel','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
xlim([1 3000])
ylim([5e-8, 200])
set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, 'FontSize', fontSize);
%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,3,4)

loglog(reshape(Gauss_tensor_dim3_100_work,1,[]),...
    reshape(Gauss_tensor_dim3_100_results,1,[]) , 'o','Color', plotColors(1,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
hold on
loglog(reshape(Gauss_adaptive_dim3_100_work,1,[]),...
    reshape(Gauss_adaptive_dim3_100_results,1,[]) , '^','Color', plotColors(2,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)

 
 title('Gaussian 3D Kernel','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
xlim([1 3000])
ylim([5e-13, 1000])
set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, 'FontSize', fontSize);
%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,3,5)

loglog(reshape(Plate_tensor_dim4_100_work,1,[]),...
    reshape(Plate_tensor_dim4_100_results,1,[]) , 'o','Color', plotColors(1,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
hold on
loglog(reshape(Plate_adaptive_dim4_100_work,1,[]),...
    reshape(Plate_adaptive_dim4_100_results,1,[]) , '^','Color', plotColors(2,:),...
        'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)

 
 title('Thin Plate Spline 4D Kernel','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, 'FontSize', fontSize);

xlim([1 3000])
ylim([5e-8, 100])

print -painters -dpdf -r600 VariousKernels.pdf
%%
