%%
load point_approx_1.dat
load point_approx_2.dat
load point_approx_3.dat
load point_real_1.dat
load point_real_2.dat
load point_real_3.dat
load point_summary_1.dat
load point_summary_2.dat
load point_summary_3.dat

[x,y] = meshgrid(linspace(3,5,100), linspace(-1,1,100));

figure;
surf(x',y',point_real_3, 'LineStyle', 'none', 'FaceAlpha', 1);
hold on
surf(x',y',point_summary_3, 'LineStyle', 'none', 'FaceAlpha', .5);
surf(x',y',point_approx_3,'LineStyle', 'none');

figure
surf(x',y',point_real_3-point_approx_3, 'LineStyle', 'none');