function [terms, coefficients] = readPCE(filename,inputDim,outputDim)
%Read a PCE results file in results/basename_result.dat.
 
    
    fpce = fopen(filename);
    numPceTerms = textscan(fpce, '%d',1);
    
    formatD = '';
    formatF = '';
    for j=1:inputDim
        formatD = [formatD ' %d'];
    end
    for j=1:outputDim
        formatF = [formatF ' %f'];
    end
    
    terms = textscan(fpce,formatD, numPceTerms{1}, 'CollectOutput', 1);
    coefficients = textscan(fpce, formatF, numPceTerms{1}, 'CollectOutput', 1);
    fclose(fpce);
    
    terms=terms{1,1};
    coefficients=coefficients{1,1};