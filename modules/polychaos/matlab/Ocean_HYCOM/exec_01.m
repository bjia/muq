%% Designed to look at the impact and results of adaptivity

close all

%% 60 
load sst_int_adaptiveTrace_60.dat
load sst_int_increasingSimplex_60.dat

plotHYCOMComparison(sst_int_increasingSimplex_60, sst_int_adaptiveTrace_60, ...
    'Integrated SST at T=60', 'sst_int_adaptive_60.pdf')


 
 %% 132
load sst_int_adaptiveTrace_132.dat
load sst_int_increasingSimplex_132.dat

plotHYCOMComparison(sst_int_increasingSimplex_132, sst_int_adaptiveTrace_132, ...
    'Integrated SST at T=132', 'sst_int_adaptive_132.pdf')
%% 135
load sst_int_adaptiveTrace_135.dat
load sst_int_increasingSimplex_135.dat

plotHYCOMComparison(sst_int_increasingSimplex_135, sst_int_adaptiveTrace_135, ...
    'Integrated SST at T=135', 'sst_int_adaptive_135.pdf')
 
 %% 60 qtot
load qtot_adaptiveTrace_60.dat
load qtot_increasingSimplex_60.dat

plotHYCOMComparison(qtot_increasingSimplex_60, qtot_adaptiveTrace_60, ...
    'Total Heat at T=60', 'qtot_adaptive_60.pdf')

 %% 60 qtot
load qtot_adaptiveTrace_132.dat
load qtot_increasingSimplex_132.dat

plotHYCOMComparison(qtot_increasingSimplex_132, qtot_adaptiveTrace_132, ...
    'Total Heat at T=132', 'qtot_adaptive_132.pdf')


 
  %% 60  sst_int_precond
load sst_int_precond_adaptiveTrace_60.dat
load sst_int_precond_increasingSimplex_60.dat

plotHYCOMComparison(sst_int_precond_increasingSimplex_60, sst_int_precond_adaptiveTrace_60, ...
     'Preconditioned Integrated SST at T=60', 'sst_int__precond_adaptive_60.pdf')


   %% 135  sst_int_precond
load sst_int_precond_adaptiveTrace_135.dat
load sst_int_precond_increasingSimplex_135.dat

plotHYCOMComparison(sst_int_precond_increasingSimplex_135, sst_int_precond_adaptiveTrace_135, ...
     'Preconditioned Integrated SST at T=135', 'sst_int__precond_adaptive_135.pdf')
