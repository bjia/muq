function plotHYCOMComparison(simplex, adaptive, titleString, filename)


figure1 = figure('Color', [1,1,1]);

set(figure1, 'PaperUnits', 'inches');
set(figure1, 'PaperSize', [7 5]);
set(figure1, 'PaperPositionMode', 'manual');
set(figure1, 'PaperPosition', [0 0 7 5]);

plotColors = [228, 26, 28; 55, 126, 184; 77, 175, 74; 152, 78, 163; 255, 127, 0; ]/255;
plotMarkerStyleLine = '.-';
plotMarkerStyleScatter = '.';
plotMarkerSize = 15;
plotLineWidth = 2;
greyColor = [0.502 0.502 0.502]*.7;
font = 'Helvetica';
fontSize = 16;



   simplex(end+1,:) = adaptive(end,:);
   
    semilogy(simplex(:,1), simplex(:,2),'.-', ...
        'Color', plotColors(1,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
    hold on
        semilogy(adaptive(:,1), adaptive(:,2),'.-', ...
        'Color', plotColors(2,:),'LineWidth', plotLineWidth,'MarkerSize',plotMarkerSize)
   
set(gca,'ZColor',greyColor,...
    'YColor',greyColor,...
    'XColor',greyColor,...
    'FontName',font, ...
    'FontSize', fontSize);
   legend off
    title(titleString,'FontName',font,...
            'Color',greyColor,'FontSize', fontSize)

       % ylim([1e-3, .2])
       xlim([0 520])
        
        
         ylabel('Estimated L_2 Error','FontName',font,...
     'Color',greyColor,'FontSize', fontSize)
          xlabel('Number of Evaluations','FontName',font,...
    'Color',greyColor,'FontSize', fontSize)
        leg = legend('Non-Adaptive', 'Adaptive');
set(leg,'FontName',font,'TextColor',greyColor,'XColor', greyColor, ...
    'YColor', greyColor,'FontSize', 8,'Location', 'North', 'FontSize', fontSize, 'Box', 'off'); 

print(figure1, '-painters', '-dpdf', '-r600' ,filename);