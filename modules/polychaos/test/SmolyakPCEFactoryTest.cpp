//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakPCEFactoryTest.cpp
 *
 *  Created on: Jun 12, 2011
 *      Author: prconrad
 */

#include <stddef.h>
#include <iostream>

#include <boost/math/constants/constants.hpp>

#include "gtest/gtest.h"

#include "MUQ/polychaos/pce/PolynomialChaosExpansionTest.h"
#include "MUQ/polychaos/polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/polychaos/quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/polychaos/quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/polychaos/smolyak/SmolyakPCEFactory.h"
#include "MUQ/polychaos/smolyak/SmolyakQuadrature.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/CachedFunctionWrapper.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
#include "MUQ/polychaos/utilities/Variable.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"


using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;

TEST(SmolyakPCE, adaptiveRegressionConstantine1)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

	LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

	VariableCollection::Ptr varCollection(new VariableCollection());

	//Try a mixed integration type
	varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);


	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(pceTestConstantine1,1,2);


	SmolyakPCEFactory pceFactory(varCollection, fn);

	PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(1, 1e-4);

	VectorXd point1(1);
	VectorXd point2(1);
	VectorXd point3(1);
	VectorXd point4(1);
	//two arbitrary points to test
	point1 << .9 ;//, .22;
	point2 << -.3 ;//, .5;
	point3 << .58 ;//, .7;
	point4 << -.192747;// , -.4;

	//check the values against stored ones that came from this code - regression
	VectorXd result = pceTestConstantine1(point1);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point1), 1e-3));
	result = pceTestConstantine1(point2);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point2), 1e-3));
	result = pceTestConstantine1(point3);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point3), 1e-3));
	result = pceTestConstantine1(point4);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point4), 1e-3));

}


TEST(SmolyakPCE, adaptiveRegressionConstantine2)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
	LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

	VariableCollection::Ptr varCollection(new VariableCollection());

	//Try a mixed integration type
	varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);
	varCollection->PushVariable("y", legendrePoly1D, legendreQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(pceTestConstantine2,2,2);
	SmolyakPCEFactory pceFactory(varCollection, fn);

	PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(1, .01);

	VectorXd point1(2);
	VectorXd point2(2);
	VectorXd point3(2);
	VectorXd point4(2);
	//two arbitrary points to test
	point1 << .9 , .22;
	point2 << -.3 , .5;
	point3 << .58 , .7;
	point4 << -.192747 , -.4;

	//check the values against stored ones that came from this code - regression
	VectorXd result = pceTestConstantine2(point1);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point1), 4e-2));
	result = pceTestConstantine2(point2);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point2), 8e-2));
	result = pceTestConstantine2(point3);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point3), 5e-2));
	result = pceTestConstantine2(point4);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point4), 2e-1));

}

TEST(SmolyakPCE, adaptiveRegressionConstantine3)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
	LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

	VariableCollection::Ptr varCollection(new VariableCollection());

	//Try a mixed integration type
	varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);
	varCollection->PushVariable("y", legendrePoly1D, legendreQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(pceTestConstantine3,2,2);
	SmolyakPCEFactory pceFactory(varCollection, fn);

	PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(1, 1e-3);

	VectorXd point1(2);
	VectorXd point2(2);
	VectorXd point3(2);
	VectorXd point4(2);
	//two arbitrary points to test
	point1 << .9 , .22;
	point2 << -.3 , .5;
	point3 << .58 , .7;
	point4 << -.192747 , -.4;

	//check the values against stored ones that came from this code - regression
	VectorXd result = pceTestConstantine3(point1);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point1), 1e-3));
	result = pceTestConstantine3(point2);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point2), 1e-3));
	result = pceTestConstantine3(point3);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point3), 1e-3));
	result = pceTestConstantine3(point4);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point4), 1e-3));

}

TEST(SmolyakPCE, adaptiveRegressionConstantine3StartStop)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
	LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

	VariableCollection::Ptr varCollection(new VariableCollection());

	//Try a mixed integration type
	varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);
	varCollection->PushVariable("y", legendrePoly1D, legendreQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(pceTestConstantine3,2,2);
	SmolyakPCEFactory::Ptr pceFactory(new SmolyakPCEFactory(varCollection, fn));

	PolynomialChaosExpansion::Ptr pce = pceFactory->StartAdaptiveToTolerance(1, 1e-2);

	BOOST_LOG_POLY("smolyak",debug) << "Starting to save ";

	SmolyakPCEFactory::SaveProgress(pceFactory, "results/tests/adaptiveRegressionConstantine3StartStop");

	BOOST_LOG_POLY("smolyak",debug) << "Progress saved. ";

	SmolyakPCEFactory::Ptr pceFactory2 = SmolyakPCEFactory::LoadProgress("results/tests/adaptiveRegressionConstantine3StartStop",
			&pceTestConstantine3);

	pce = pceFactory2->AdaptToTolerance(1e-4);
	
	PolynomialChaosExpansion::SaveToFile(pce, "results/tests/pce.dat");
	
	PolynomialChaosExpansion::Ptr pceCopy = PolynomialChaosExpansion::LoadFromFile("results/tests/pce.dat");

	VectorXd point1(2);
	VectorXd point2(2);
	VectorXd point3(2);
	VectorXd point4(2);
	//two arbitrary points to test
	point1 << .9 , .22;
	point2 << -.3 , .5;
	point3 << .58 , .7;
	point4 << -.192747 , -.4;

	//check the values against stored ones that came from this code - regression
	VectorXd result = pceTestConstantine3(point1);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point1), 1e-3));
	result = pceTestConstantine3(point2);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point2), 1e-3));
	result = pceTestConstantine3(point3);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point3), 1e-3));
	result = pceTestConstantine3(point4);
	EXPECT_TRUE(MatrixApproxEqual( result, pce->Evaluate(point4), 1e-3));
	
	EXPECT_TRUE(MatrixApproxEqual(pce->Evaluate(point4), pceCopy->Evaluate(point4), 1e-14));

}

TEST(SmolyakPCE, addSpecificPolynomials)
{
    
    GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
    LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());
    
    VariableCollection::Ptr varCollection(new VariableCollection());
    
    //Try a mixed integration type
    varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);
    varCollection->PushVariable("y", legendrePoly1D, legendreQuad1D);
    
    CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(pceTestConstantine3,2,2);
    SmolyakPCEFactory::Ptr pceFactory(new SmolyakPCEFactory(varCollection, fn));
    
    MatrixXu polys(2,2);
    polys << 8 , 1 , 0, 4;
    
    PolynomialChaosExpansion::Ptr pce = pceFactory->ComputeFixedPolynomials(polys);
    
    RowVectorXu poly1(2), poly2(2), poly3(2);
    poly1 << 8 , 1;
    poly2 << 0 , 4;
    poly3 << 9, 9;
    EXPECT_TRUE(pce->IsPolynomialInExpansion(poly1));
    EXPECT_TRUE(pce->IsPolynomialInExpansion(poly2));
    EXPECT_FALSE(pce->IsPolynomialInExpansion(poly3));
}


VectorXd IshigamiPolySensitivityIndexFn(VectorXd const& input)
{
    //This is a test function from Polynomial chaos expansion for sensitivity analysis, 
  //Crestaux, Le Maitre, Martinez 2009.
  
  VectorXd firstStep = input.array()*boost::math::constants::pi<double>();
  
  double a=1.0, b=2.0;
    
  VectorXd result(2);
  double term2 = sin(firstStep(1));
  result(0) = sin(firstStep(0)) + a*term2*term2 + b*sin(firstStep(0))*firstStep(2)*firstStep(2)*firstStep(2)*firstStep(2);
 
  double c=3.0, d=4.0;
  double term3 = sin(firstStep(1));
  result(1) = sin(firstStep(0)) + c*term3*term3 + d*sin(firstStep(0))*firstStep(2)*firstStep(2)*firstStep(2)*firstStep(2);
 
  return result; 
}

///Test the variance and sensitivity estimates against known results
TEST(SmolyakPCE, testSensitivityEstimates)
{
      //This is a test function from Polynomial chaos expansion for sensitivity analysis, 
  //Crestaux, Le Maitre, Martinez 2009.
  
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
	LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

	VariableCollection::Ptr varCollection(new VariableCollection());

	
	unsigned inputDim = 3;

	
	for(unsigned i=0; i<inputDim; ++i)
	{
	  varCollection->PushVariable("x", legendrePoly1D, pattersonQuad1D);
	}

	FunctionWrapper::Ptr fn = FunctionWrapper::WrapVectorFunction(IshigamiPolySensitivityIndexFn,inputDim,2);


	SmolyakPCEFactory pceFactory(varCollection, fn);

	PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(3, 1e-4);

	
	double pi = boost::math::constants::pi<double>();
	double a=1.0, b=2.0;
	  double c=3.0, d=4.0;
	
	VectorXd variance(2);
	variance(0) = a*a/8.0 + b/5.0*pow(pi,4) + b*b*pow(pi,8)/18.0+.5;
	variance(1) = c*c/8.0 + d/5.0*pow(pi,4) + d*d*pow(pi,8)/18.0+.5;

	MatrixXd mainEffects(2,3);
	mainEffects <<  b/5.0*pow(pi,4)+b*b*pow(pi,8)/50.0+.5, a*a/8, 0,
	  d/5.0*pow(pi,4)+d*d*pow(pi,8)/50.0+.5, c*c/8, 0;
	 mainEffects.row(0) /= variance(0); //note that the paper doesn't normalize by the variance
	mainEffects.row(1) /= variance(1);

	MatrixXd totalEffects(2,3);
	totalEffects <<  b/5.0*pow(pi,4)+b*b*pow(pi,8)/18.0+.5, 
	  a*a/8, 
	  b*b*pow(pi,8)/18.0-b*b*pow(pi,8)/50.0,
	   d/5.0*pow(pi,4)+d*d*pow(pi,8)/18.0+.5, 
	  c*c/8, 
	  d*d*pow(pi,8)/18.0-d*d*pow(pi,8)/50.0;
      totalEffects.row(0) /=variance(0);
	totalEffects.row(1) /=variance(1);
	  
	  EXPECT_TRUE(MatrixApproxEqual( variance, pce->ComputeVariance(), 1e-5));
	  EXPECT_TRUE(MatrixApproxEqual( mainEffects,  pce->ComputeAllMainSensitivityIndices(), 1e-5));
	  EXPECT_TRUE(MatrixApproxEqual( totalEffects, pce->ComputeAllSobolTotalSensitivityIndices(), 1e-5));
	  
	
}
