//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * PolynomialChaosExpansionTest.cpp
 *
 *  Created on: Jan 19, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/pce/PolynomialChaosExpansionTest.h"

#include <iostream>
#include <fstream>

#include <Eigen/Dense>
#include <Eigen/LU>
#include "gtest/gtest.h"

#include "MUQ/polychaos/pce/PolynomialChaosExpansion.h"
#include "MUQ/polychaos/quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/polychaos/quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/polychaos/quadrature/GaussChebyshevQuadrature1D.h"
#include "MUQ/polychaos/polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/CachedFunctionWrapper.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;


VectorXd pceTestConstantine1(VectorXd const& val)
{
	double epsilon = 0.4;
	Matrix2d opMat;
	opMat << 1+epsilon , val(0),
		val(0), 1;

	Vector2d rhs;
	rhs << 2, 1;
	return opMat.colPivHouseholderQr().solve(rhs);
}

VectorXd pceTestConstantine2(VectorXd const& val)
{
	double epsilon1 = 0.2;
	double epsilon2 = 2;
	Matrix2d opMat;
	opMat << 1+epsilon1, val(0),
			  val(0) , 1;

	Matrix2d opMat2;
		opMat2 << 1+epsilon2, val(1),  
				 val(1), 1;
	Vector2d rhs;
	rhs << 1 , 1;
	Matrix2d fullMat = opMat*opMat2;
	return fullMat.colPivHouseholderQr().solve(rhs);
}

VectorXd pceTestConstantine3(VectorXd const& val)
{
	double epsilon = 0.01;
	Matrix2d opMat;
	opMat << 2+val(0), epsilon,
			  epsilon, 2+val(1);

	Vector2d rhs;
	rhs << 1 , 1;
	return opMat.colPivHouseholderQr().solve(rhs);
}

VectorXd pceTestConstantine4(VectorXd const& val)
{
	Matrix2d opMat;
	opMat << 4 , val(1) + val(0) * val(1) ,
			  val(0) + val(0) * val(1), 1;

	Vector2d rhs;
	rhs << 1 , 1;
	return opMat.colPivHouseholderQr().solve(rhs);
}



