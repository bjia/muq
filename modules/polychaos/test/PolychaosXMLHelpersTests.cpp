//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <Eigen/Core>
#include <gtest/gtest.h>

#include <MUQ/GeneralUtilities/EigenUtils.h>
#include <MUQ/polychaos/pce/PolynomialChaosExpansion.h>
#include <MUQ/polychaos/utilities/PolychaosXMLHelpers.h>

using namespace std;
using namespace boost::property_tree;
using namespace muq::polychaos; 


TEST(polychaos, XMLHelpers)
{
  ptree pt;
  read_xml("data/tests/ExamplePolychaosConfig.xml", pt);
  
  PolynomialChaosExpansion::Ptr pce = ComputePCE(pt);
  
  Eigen::MatrixXd testInputs(3,4);
  testInputs << .1, .2, .4, .5, .6, .7, .8, -.1, -.2, -.3, -.4, -.5;

  EXPECT_TRUE(muq::GeneralUtilities::MatrixApproxEqual(testInputs, pce->Evaluate(testInputs), 1e-13));
  
}