//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * HermitePolynomials1DTest.cpp
 *
 *  Created on: Nov 18, 2010
 *      Author: prconrad
 */

#include "MUQ/polychaos/polynomials/HermitePolynomials1DRecursive.h"


#include "gtest/gtest.h"

using namespace muq::polychaos;

TEST(polychaosHermitePolyTest, spotChecks){
    HermitePolynomials1DRecursive HermitePolyR;
    
    //A sample of points, the lhs is from mathematica. Note that these
    //have very poor absolute accuracy because the numbers are huge, but the relative
    //precision is to the full double.
    
    EXPECT_NEAR(1.0 , HermitePolyR.evaluate(0,.4), 1e-14);
    EXPECT_NEAR(.6 , HermitePolyR.evaluate(1,.3), 1e-14);
    EXPECT_NEAR(-.56 , HermitePolyR.evaluate(2,.6), 1e-14);
    EXPECT_NEAR(33.6235290625 , HermitePolyR.evaluate(5,0.325), 1e-14);
    EXPECT_NEAR(6219.5581337600015 , HermitePolyR.evaluate(8,1.6), 2e-12);
    EXPECT_NEAR(6.075804453410837e11, HermitePolyR.evaluate(20,-.845), 3e-4);
    EXPECT_NEAR(-5.8505463205709636e38 , HermitePolyR.evaluate(50,.1264), 1e24);
    EXPECT_NEAR(5.4520440325350614e216, HermitePolyR.evaluate(200,-.3598), 2.5e201);
}
