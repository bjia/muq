//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * SmolyakQuadratureTest.cpp
 *
 *  Created on: Jun 10, 2011
 *      Author: prconrad
 */

#include <iostream>
#include <stddef.h>


#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include "gtest/gtest.h"

#include "MUQ/polychaos/quadrature/ExpGrowthQuadratureFamily1D.h"
#include "MUQ/polychaos/quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/polychaos/quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/polychaos/quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/polychaos/quadrature/QuadratureTestFunctions.h"
#include "MUQ/polychaos/smolyak/SmolyakQuadrature.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/CachedFunctionWrapper.h"
#include "MUQ/GeneralUtilities/FunctionWrapper.h"
#include "MUQ/GeneralUtilities/SmartPtrNull.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"
#include "MUQ/polychaos/utilities/Variable.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;

TEST(SmolyakIsoSparseQuadrature, dasqTest2DIso1)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), legendreQuad1D);
	varCollection->PushVariable("y", GetNull(), legendreQuad1D);

	FunctionWrapper::Ptr fn= FunctionWrapper::WrapVectorFunction(dasqTest2DIso1,2,1);
	SmolyakQuadrature isoQuad(varCollection,fn);
	VectorXd integral = isoQuad.StartFixedTerms(12);
	VectorXd correctIntegral(1);
	correctIntegral << 14.9364826562485405;
	EXPECT_TRUE(MatrixApproxEqual(correctIntegral,integral, 1e-12));
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DIso2)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), legendreQuad1D);
	varCollection->PushVariable("y", GetNull(), legendreQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DIso2,2,1);
	SmolyakQuadrature isoQuad(varCollection,fn);
	VectorXd integral = isoQuad.StartFixedTerms(14);
	VectorXd correctIntegral(1);
	correctIntegral << 4.75446580837249437;
	EXPECT_TRUE(MatrixApproxEqual(correctIntegral,integral, 1e-12));
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DIso3)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), legendreQuad1D);
	varCollection->PushVariable("y", GetNull(), legendreQuad1D);


	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DIso3,2,1);
	SmolyakQuadrature isoQuad(varCollection,fn);
	VectorXd integral = isoQuad.StartFixedTerms(20);
	VectorXd correctIntegral(1);
	correctIntegral << 0.0785385998857827117;
	EXPECT_TRUE(MatrixApproxEqual(correctIntegral,integral, 1e-12));
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DAniso1)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), legendreQuad1D);
	varCollection->PushVariable("y", GetNull(), legendreQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DAniso1,2,1);
	SmolyakQuadrature isoQuad(varCollection,fn);
	VectorXd integral = isoQuad.StartFixedTerms(14);
	VectorXd correctIntegral(1);
	correctIntegral <<  8.21506546093669728;
	EXPECT_TRUE(MatrixApproxEqual( correctIntegral,integral, 1e-12));
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DAniso2)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), legendreQuad1D);
	varCollection->PushVariable("y", GetNull(), legendreQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DAniso2,2,1);
	SmolyakQuadrature isoQuad(varCollection,fn);
	VectorXd integral = isoQuad.StartFixedTerms(14);
	VectorXd correctIntegral(1);
	correctIntegral <<  32.0801216275935365;
	EXPECT_TRUE(MatrixApproxEqual( correctIntegral,integral, 1e-12));
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DAniso3)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), legendreQuad1D);
	varCollection->PushVariable("y", GetNull(), legendreQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DAniso3,2,1);
	SmolyakQuadrature isoQuad(varCollection,fn);
	VectorXd integral = isoQuad.StartFixedTerms(15);
	VectorXd correctIntegral(1);
	correctIntegral << 0.110897342159703106;
	EXPECT_TRUE(MatrixApproxEqual( correctIntegral,integral, 1e-12));
}


TEST(SmolyakAdaptiveSparseQuadrature, simplePoly1D)
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());

	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var

//	EXPECT_TRUE(false) << "test message.";

	varCollection->PushVariable("x1", GetNull(), pattersonQuad1D);


	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(simplePoly1D,1,1);
	SmolyakQuadrature quad(varCollection,fn);
	VectorXd integral = quad.StartAdaptiveToTolerance(3,1e-5);

	VectorXd correctIntegral(1);
	correctIntegral << 7.0+1.0/3.0;

	EXPECT_TRUE(MatrixApproxEqual(  correctIntegral, integral, 1e-5)); //analytic answer: 7+1/3
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DIso1)
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), pattersonQuad1D);
	varCollection->PushVariable("y", GetNull(), pattersonQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DIso1,2,1);
	SmolyakQuadrature quad(varCollection,fn);
	VectorXd integral = quad.StartAdaptiveToTolerance(3,1e-10);
	VectorXd correctIntegral(1);
	correctIntegral << 14.9364826562485405;
	EXPECT_TRUE(MatrixApproxEqual(correctIntegral, integral, 1e-10));
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DIso2)
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), pattersonQuad1D);
	varCollection->PushVariable("y", GetNull(), pattersonQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DIso2,2,1);
	SmolyakQuadrature quad(varCollection,fn);
	VectorXd integral = quad.StartAdaptiveToTolerance(3,1e-10);
	VectorXd correctIntegral(1);
	correctIntegral << 4.75446580837249437;

	EXPECT_TRUE(MatrixApproxEqual(correctIntegral, integral, 1e-10));
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DIso3)
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), pattersonQuad1D);
	varCollection->PushVariable("y", GetNull(), pattersonQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DIso3,2,1);
	SmolyakQuadrature quad(varCollection,fn);
	VectorXd integral = quad.StartAdaptiveToTolerance(3,1e-15);
	VectorXd correctIntegral(1);
	correctIntegral << 0.0785385998857827117;
	EXPECT_TRUE(MatrixApproxEqual(correctIntegral, integral, 1e-10));
}

TEST(SmolyakAdaptiveSparseQuadrature,dasqTest2DAniso1 )
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), pattersonQuad1D);
	varCollection->PushVariable("y", GetNull(), pattersonQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DAniso1,2,1);
	SmolyakQuadrature quad(varCollection,fn);
	VectorXd integral = quad.StartAdaptiveToTolerance(3,1e-14);
	VectorXd correctIntegral(1);
	correctIntegral <<  8.21506546093669728;
	EXPECT_TRUE(MatrixApproxEqual(correctIntegral, integral, 1e-14));
}

TEST(SmolyakAdaptiveSparseQuadrature,dasqTest2DAniso2 )
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), pattersonQuad1D);
	varCollection->PushVariable("y", GetNull(), pattersonQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DAniso2,2,1);
	SmolyakQuadrature quad(varCollection,fn);
	VectorXd integral = quad.StartAdaptiveToTolerance(4,1e-13);
	VectorXd correctIntegral(1);
	correctIntegral << 32.0801216275935365;

	EXPECT_TRUE(MatrixApproxEqual(correctIntegral, integral, 1e-10));
}

TEST(SmolyakAdaptiveSparseQuadrature,dasqTest2DAniso3 )
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), pattersonQuad1D);
	varCollection->PushVariable("y", GetNull(), pattersonQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DAniso3,2,1);
	SmolyakQuadrature quad(varCollection,fn);
	VectorXd integral = quad.StartAdaptiveToTolerance(3,1e-10);
	VectorXd correctIntegral(1);
	correctIntegral << 0.110897342159703106;
	EXPECT_TRUE(MatrixApproxEqual(correctIntegral, integral, 1e-15));
}

TEST(SmolyakAdaptiveSparseQuadrature,dasqTest2DAniso3ExpGrowth )
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
	ExpGrowthQuadratureFamily1D::Ptr expLegendreQuad1D(new ExpGrowthQuadratureFamily1D(legendreQuad1D));
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), expLegendreQuad1D);
	varCollection->PushVariable("y", GetNull(), expLegendreQuad1D);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(dasqTest2DAniso3,2,1);
	SmolyakQuadrature quad(varCollection,fn);
	VectorXd integral = quad.StartAdaptiveToTolerance(3,1e-10);
	VectorXd correctIntegral(1);
	correctIntegral << 0.110897342159703106;
	
	EXPECT_TRUE(MatrixApproxEqual(correctIntegral, integral, 1e-12));
}

///Based on dasqTest2DAniso3
TEST(SmolyakAdaptiveSparseQuadrature,SaveAndLoad)
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable("x", GetNull(), pattersonQuad1D);
	varCollection->PushVariable("y", GetNull(), pattersonQuad1D);

	FunctionWrapper::Ptr fn= FunctionWrapper::WrapVectorFunction(dasqTest2DAniso3,2,1);
	SmolyakQuadrature::Ptr outQuad(new SmolyakQuadrature(varCollection,fn));

	outQuad->StartAdaptiveToTolerance(3,1e-10);

	SmolyakQuadrature::SaveProgress(outQuad, "results/tests/SmolyakAdaptiveSparseQuadrature_SaveAndLoad");

	SmolyakQuadrature::Ptr inQuad = SmolyakQuadrature::LoadProgress(
			"results/tests/SmolyakAdaptiveSparseQuadrature_SaveAndLoad", &dasqTest2DAniso3);

	VectorXd integral = inQuad->AdaptToTolerance(1e-15);
	VectorXd correctIntegral(1);
	correctIntegral << 0.110897342159703106;
	EXPECT_TRUE(MatrixApproxEqual( correctIntegral, integral, 1e-15));
}
