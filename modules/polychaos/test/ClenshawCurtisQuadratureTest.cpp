//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * ClenshawCurtisQuadrature1D.cpp
 *
 *  Created on: Jan 24, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/ClenshawCurtisQuadrature1D.h"

#include <Eigen/Core>
#include "gtest/gtest.h"

#include "MUQ/GeneralUtilities/EigenUtils.h"


using namespace muq::GeneralUtilities;
    
using namespace Eigen;
using namespace muq::polychaos;

TEST(polychaosQuadrature1DTest, ClenshawCurtis){
  
  ClenshawCurtisQuadrature1D ccQuad;
  
  unsigned int size=4;
  
  boost::shared_ptr<RowVectorXd> nodes(ccQuad.GetNodes(size));
  boost::shared_ptr<RowVectorXd> weights(ccQuad.GetWeights(size));
  
  EXPECT_EQ(9, nodes->cols());
  EXPECT_EQ(9, weights->cols());
  EXPECT_EQ(9, ccQuad.GetPrecisePolyOrder(size));
}