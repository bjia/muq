//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * FullTensorQuadratureTest.cpp
 *
 *  Created on: Jan 14, 2011
 *      Author: prconrad
 */

#include <iostream>
#include <stddef.h>

#include "glog/logging.h"
#include "gtest/gtest.h"

#include "MUQ/polychaos/polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/polychaos/quadrature/FullTensorQuadrature.h"
#include "MUQ/polychaos/quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/polychaos/quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/polychaos/quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/polychaos/quadrature/QuadratureTestFunctions.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/CachedFunctionWrapper.h"
#include "MUQ/GeneralUtilities/SmartPtrNull.h"
#include "MUQ/polychaos/utilities/Variable.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"


using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;

///Regression test for a 6-dim mixed var type and mixed order full tensor quadrature grid
TEST(polychaosFullTensorQuadrature, regressionTest)
{
	GaussHermiteQuadrature1D::Ptr gaussQuad1D(new GaussHermiteQuadrature1D());
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

	VariableCollection::Ptr varCollection(new VariableCollection());
	//say we have 3 spatial vars and 3 random vars
	varCollection->PushVariable(Variable::Ptr(new Variable("x1", GetNull(), legendreQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("x2", GetNull(), legendreQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("x3", GetNull(), legendreQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("xi1", GetNull(), gaussQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("xi2", GetNull(), gaussQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("xi3", GetNull(), gaussQuad1D)));


	MatrixXd quadNodes;
	VectorXd quadWeights;

	RowVectorXu order(6);
	order << 6 , 5 , 3 , 9 , 4 , 5;
	FullTensorQuadrature quad(varCollection, order);

	quad.GetNodesAndWeights(quadNodes, quadWeights);

	//prints the data used below
//	cout << quadNodes->row(265);
//	cout << quadNodes->row(1641);
//	cout << quadNodes->row(11995);
//	cout << quadWeights->row(265);
//	cout << quadWeights->row(1641);
//	cout << quadWeights->row(11995);
//	cout << endl << sum(*quadWeights);
	VectorXd storedNodes265(6);
	storedNodes265 << -9.3247e-01 , -9.0618e-01  ,-8.6736e-18 , -4.0244e-17  ,-5.2465e-01,  -2.0202e+00;
	VectorXd storedNodes1641(6);
	storedNodes1641 <<   -0.9325 ,  0.5385 , -0.7746 , -2.2666 , -1.6507 , -0.9586;
	VectorXd storedNodes11995(6);
	storedNodes11995 <<  6.6121e-01  , 2.1924e-16  ,-7.7460e-01  , 7.2355e-01 ,  1.6507e+00 , -2.0202e+00;

	//now test
	EXPECT_TRUE(MatrixApproxEqual(storedNodes265, quadNodes.col(265), 2e-4));
	EXPECT_TRUE(MatrixApproxEqual(storedNodes1641, quadNodes.col(1641), 2e-4));
	EXPECT_TRUE(MatrixApproxEqual(storedNodes11995, quadNodes.col(11995), 2e-4));

	EXPECT_NEAR(4.173675e-04, quadWeights(265), 2e-4);
	EXPECT_NEAR(7.91456e-08, quadWeights(1641), 2e-4);
	EXPECT_NEAR(7.43761e-05, quadWeights(11995), 2e-4);
	EXPECT_NEAR(44.5466, quadWeights.array().sum(), 2e-4);

}

TEST(polychaosFullTensorQuadrature, legendreSimplePoly1D)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable(Variable::Ptr(new Variable("x1",
			RecursivePolynomialFamily1D::Ptr(), legendreQuad1D)));

	RowVectorXu order(1);
	order << 2;
	FullTensorQuadrature quad(varCollection, order);

	auto stupidfn = [](Eigen::MatrixXd const& input){LOG(INFO) << "stupidSays" << input; return input;};
	
	MatrixXd test(2,2);
	test << 1,2,3,4;
	stupidfn(test);

	CachedFunctionWrapper::Ptr fn= CachedFunctionWrapper::WrapVectorFunction(simplePoly1D,1,1);
	
	MatrixXd correctAnswer(1,1);
	correctAnswer << 7+1.0/3.0;
	auto computedAnswer = quad.ComputeIntegral(fn);

	EXPECT_TRUE(MatrixApproxEqual(correctAnswer,computedAnswer, 1e-10)); //analytic answer: 7+1/3
}



TEST(polychaosFullTensorQuadrature, legendreSimplePoly3D)
{
	GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable(Variable::Ptr(new Variable("x", GetNull(), legendreQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("y", GetNull(), legendreQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("z", GetNull(), legendreQuad1D)));

	RowVectorXu order(3);
	order << 2 ,2 , 2;
	FullTensorQuadrature quad(varCollection, order);

	CachedFunctionWrapper::Ptr fn = CachedFunctionWrapper::WrapVectorFunction(simplePoly3D,3,1);
	VectorXd integral = quad.ComputeIntegral(fn);
	VectorXd correctAnswer(1);
	correctAnswer << 8.0/9.0;

	EXPECT_TRUE(MatrixApproxEqual(correctAnswer, integral, 1e-12)); //analytic answer: 8/9
}

TEST(polychaosFullTensorQuadrature, pattersonSimplePoly1D)
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());

	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable(Variable::Ptr(new Variable("x1", GetNull(), pattersonQuad1D)));

	RowVectorXu order(1);
	order << 2;
	FullTensorQuadrature quad(varCollection, order);

	CachedFunctionWrapper::Ptr fn = CachedFunctionWrapper::WrapVectorFunction(simplePoly1D,1,1);

	VectorXd correctAnswer(1);
		correctAnswer << 7+1.0/3.0;
		EXPECT_TRUE(MatrixApproxEqual(correctAnswer, quad.ComputeIntegral(fn), 1e-10)); //analytic answer: 7+1/3
}



TEST(polychaosFullTensorQuadrature, pattersonSimplePoly3D)
{
	GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());

	VariableCollection::Ptr varCollection(new VariableCollection());
	//just one 1D var
	varCollection->PushVariable(Variable::Ptr(new Variable("x", GetNull(), pattersonQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("y", GetNull(), pattersonQuad1D)));
	varCollection->PushVariable(Variable::Ptr(new Variable("z", GetNull(), pattersonQuad1D)));

	RowVectorXu order(3);
	order << 2 , 2 , 2;
	FullTensorQuadrature quad(varCollection, order);
	CachedFunctionWrapper::Ptr fn = CachedFunctionWrapper::WrapVectorFunction(simplePoly3D,3,1);
	VectorXd integral = quad.ComputeIntegral(fn);

	VectorXd correctAnswer(1);
	correctAnswer << 8.0/9.0;

	EXPECT_TRUE(MatrixApproxEqual(correctAnswer, integral, 1e-12)); //analytic answer: 8/9
}

/* Development test - prints for visual inspection
TEST(FullTensorQuadrature, spotCheck)
{

	GaussHermiteQuadrature1D gaussQuad1D;

	cout << "nodes 1D 2:" << gaussQuad1D.GetNodes(2);
	cout << "weights 1D 2:" << gaussQuad1D.GetWeights(2);
	cout << "nodes 1D 4:" << gaussQuad1D.GetNodes(4);
	cout << "weights 1D 4:" << gaussQuad1D.GetWeights(4);

	VariableCollection varCollection1D;
	varCollection1D.PushVariable(new Variable("x1", NULL, &gaussQuad1D));


	mat quadNodes1D;
	colvec quadWeights1D;

	FullTensorQuadrature quad1D(&varCollection1D, 4);

	quad1D.GetNodesAndWeights(&quadNodes1D, &quadWeights1D);

	cout << "tensor 1D nodes" << quadNodes1D;
	cout << "tensor 1d weights" << quadWeights1D;

	VariableCollection varCollection2D;
	varCollection2D.PushVariable(new Variable("x1", NULL, &gaussQuad1D));
	varCollection2D.PushVariable(new Variable("x2", NULL, &gaussQuad1D));

	mat quadNodes2D;
	colvec quadWeights2D;

	urowvec order2D;
	order2D << 2 << 4;
	FullTensorQuadrature quad2D(&varCollection2D, order2D);

	quad2D.GetNodesAndWeights(&quadNodes2D, &quadWeights2D);

	cout << "tensor 2d nodes" << quadNodes2D;
	cout << "tensor 2d weights" << quadWeights2D;

	VariableCollection varCollection3D;
	varCollection3D.PushVariable(new Variable("x1", NULL, &gaussQuad1D));
	varCollection3D.PushVariable(new Variable("x2", NULL, &gaussQuad1D));
	varCollection3D.PushVariable(new Variable("x3", NULL, &gaussQuad1D));

	mat quadNodes3D;
	colvec quadWeights3D;

	urowvec order3D;
	order3D << 2 << 4 << 3;
	FullTensorQuadrature quad3D(&varCollection3D, order3D);

	quad3D.GetNodesAndWeights(&quadNodes3D, &quadWeights3D);

	cout << "tensor 3d nodes" << quadNodes3D;
	cout << "tensor 3d weights" << quadWeights3D;


}*/
