//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * QuadratureTestFunctions.cpp
 *
 *  Created on: Jan 24, 2011
 *      Author: prconrad
 */

#include "MUQ/polychaos/quadrature/QuadratureTestFunctions.h"

#include <iostream>

using namespace Eigen;

Eigen::VectorXd muq::polychaos::simplePoly1D(Eigen::VectorXd const& vars)
{
	VectorXd result(1);

	result << 2*vars(0)*vars(0)+3;
	return result;
}

Eigen::VectorXd muq::polychaos::simplePoly3D(Eigen::VectorXd const& vars)
{
	//x*x*y + yyxx -xyz
	Eigen::VectorXd result(1);
	result << vars(0)*vars(0)*vars(1)+ vars(0)*vars(0)*vars(1)*vars(1)-vars(0)*vars(1)*vars(2);
	return result;
}

Eigen::VectorXd muq::polychaos::dasqTest2DIso1(Eigen::VectorXd const& vars)
{
	//shift domain to [0,1]
	VectorXd temp = (vars.array()+1.0)/2.0;
	double val = 10.0*exp(-temp(0)*temp(0)) + 10.0*exp(-temp(1)*temp(1));
	val /= 4; //correct for domain shift
	VectorXd result(1);
	result << val;
	return result;
}

Eigen::VectorXd muq::polychaos::dasqTest2DIso2(Eigen::VectorXd const& vars)
{
	//shift domain to [0,1]
	VectorXd temp = (vars.array()+1.0)/2.0;
	double val = exp(temp(0)) + exp(temp(1)) + exp(temp(0)*temp(1));
	val /= 4; //correct for domain shift
	VectorXd result(1);
	result << val;
	return result;
}

Eigen::VectorXd muq::polychaos::dasqTest2DIso3(Eigen::VectorXd const& vars)
{
	//shift domain to [0,1]
	VectorXd temp = (vars.array()+1.0)/2.0;
	double val = exp(-10.0*temp(0)*temp(0) - 10.0*temp(1)*temp(1));
	val /= 4; //correct for domain shift
	VectorXd result(1);
	result << val;
	return result;
}

Eigen::VectorXd muq::polychaos::dasqTest2DAniso1(Eigen::VectorXd const& vars)
{
	//shift domain to [0,1]
	VectorXd temp = (vars.array()+1.0)/2.0;
	double val = exp(-temp(0)*temp(0)) + 10.0*exp(-temp(1)*temp(1));
	val /= 4; //correct for domain shift
	VectorXd result(1);
	result << val;
	return result;
}

Eigen::VectorXd muq::polychaos::dasqTest2DAniso2(Eigen::VectorXd const& vars)
{
	//shift domain to [0,1]
	VectorXd temp = (vars.array()+1.0)/2.0;
	double val = exp(temp(0)) + 10.0*exp(temp(1)) + 10.0*exp(temp(0)*temp(1));
	val /= 4; //correct for domain shift
	VectorXd result(1);
	result << val;
	return result;
}

Eigen::VectorXd muq::polychaos::dasqTest2DAniso3(Eigen::VectorXd const& vars)
{
	//shift domain to [0,1]
	VectorXd temp = (vars.array()+1.0)/2.0;
	double val = exp(-10.0*temp(0)*temp(0) - 5.0*temp(1)*temp(1));
	val /= 4; //correct for domain shift
	VectorXd result(1);
	result << val;
	return result;
}
