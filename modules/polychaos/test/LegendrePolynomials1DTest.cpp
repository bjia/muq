//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * HermitePolynomials1DTest.cpp
 *
 *  Created on: Nov 18, 2010
 *      Author: prconrad
 */

#include "MUQ/polychaos/polynomials/LegendrePolynomials1DRecursive.h"

#include "gtest/gtest.h"

using namespace muq::polychaos;

TEST(polychaosLegendrePolyTest, spotChecks){
	LegendrePolynomials1DRecursive legendrePolyR;

	//A sample of points tested against mathematica
	EXPECT_NEAR(0.3375579333496094 , legendrePolyR.evaluate(5,0.325), 1e-14);
	EXPECT_NEAR(-0.05346106275520913, legendrePolyR.evaluate(20,-.845), 1e-14);
	EXPECT_NEAR(-0.1119514835092105 , legendrePolyR.evaluate(50,.1264), 1e-14);
	EXPECT_NEAR(-.001892916076323403 , legendrePolyR.evaluate(200,-.3598), 1e-14);
	EXPECT_NEAR(0.01954143166718206 , legendrePolyR.evaluate(1000,.4587), 1e-14);
}
