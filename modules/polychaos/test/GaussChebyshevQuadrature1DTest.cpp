//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * GaussChebyshevQuadrature1DTest.cpp
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */


#include <boost/math/constants/constants.hpp>
#include <boost/shared_ptr.hpp>
#include "gtest/gtest.h"

#include "MUQ/polychaos/quadrature/GaussChebyshevQuadrature1D.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"

using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;

TEST(Quadrature1DTest, ChebyshevTest){
	//store of right answers - analytic
	RowVectorXd nodesStored3(3);
	nodesStored3 << -0.5*sqrt(3.0) , 0 , 0.5*sqrt(3.0);
	RowVectorXd weightsStored3 = RowVectorXd::Ones(3)*boost::math::constants::pi<double>()/3.0;

	RowVectorXd nodesStored5(5);
	nodesStored5 << -0.5*sqrt(0.5*(5+sqrt(5))), -0.5*sqrt(0.5*(5-sqrt(5))), 1e-20,
			 0.5*sqrt(0.5*(5-sqrt(5))) , 0.5*sqrt(0.5*(5+sqrt(5)));
	RowVectorXd weightsStored5 = RowVectorXd::Ones(5)*boost::math::constants::pi<double>()/5.0;

	//compute and test
	GaussChebyshevQuadrature1D ChebyshevQuad;

	int size = 3;
	boost::shared_ptr<RowVectorXd> nodes = ChebyshevQuad.GetNodes(size);
	boost::shared_ptr<RowVectorXd> weights = ChebyshevQuad.GetWeights(size);

	EXPECT_TRUE(MatrixApproxEqual(*nodes, nodesStored3,1e-15));
	EXPECT_TRUE(MatrixApproxEqual(*weights, weightsStored3,1e-15));

	size = 5;
	nodes = ChebyshevQuad.GetNodes(size);
	weights = ChebyshevQuad.GetWeights(size);

	EXPECT_TRUE(MatrixApproxEqual( *nodes, nodesStored5, 1e-15));
	EXPECT_TRUE(MatrixApproxEqual(*weights, weightsStored5, 1e-15));
}
