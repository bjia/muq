//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <iostream>

#include <Eigen/Core>

#include "MUQ/polychaos/pce/PolynomialChaosExpansion.h"
#include "MUQ/polychaos/polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/polychaos/quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/polychaos/smolyak/SmolyakPCEFactory.h"
#include "MUQ/GeneralUtilities/FunctionWrapper.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;


VectorXd easyFunction(VectorXd input)
{
    VectorXd output(1);
    output << sin(input(0)) + pow(input(1), 3);
    return output;
}

int main(int argc, char **argv) {
    //logging boilerplate
    InitializeLogging(argc, argv);
    
    //we need a VariableCollection to specify the tensor space
    VariableCollection::Ptr vars(new VariableCollection());
    
    //use Gauss Patterson quadrature
    GaussPattersonQuadrature1D::Ptr quad1D(new GaussPattersonQuadrature1D());
    //and Legendre polynomials
    LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());
    
    //two input variables, both are the same
    vars->PushVariable("x", legendrePoly1D, quad1D);
    vars->PushVariable("y", legendrePoly1D, quad1D);
    
    //create a wrapper around the function, it has two inputs and one output
    FunctionWrapper::Ptr fn = FunctionWrapper::WrapVectorFunction(easyFunction,2,1);
    
    //create the pce factory
    SmolyakPCEFactory::Ptr factory(new SmolyakPCEFactory(vars, fn));
    
    //Run it adaptively for a little time, after initializing with 
    //a three level simplex
    PolynomialChaosExpansion::Ptr pce1 = factory->StartAdaptiveTimed(3,.01);
    
    //save our progress so far
    SmolyakPCEFactory::SaveProgress(factory, "results/tests/simpleFactorySave");
    
    //load it again, note we have to provide the function in the code
    SmolyakPCEFactory::Ptr factory2 = SmolyakPCEFactory::LoadProgress("results/tests/simpleFactorySave",&easyFunction);
    
    //continue for a little more time
    PolynomialChaosExpansion::Ptr pce2 = factory2->AdaptForTime(.01,0);
    
    Vector2d testPoint;
    testPoint << .3, .6;
    cout << "Test point: " << testPoint << endl;
    cout << "Actual: " << fn->Evaluate(testPoint)<< endl;
    cout << "Approximate: " << pce2->Evaluate(testPoint)<< endl;
    cout << "Difference: " << fn->Evaluate(testPoint)-pce2->Evaluate(testPoint)<< endl;
    return 1;
}
