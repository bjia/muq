//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <iostream>

#include <boost/mpi.hpp>

#include <Eigen/Core>

#include "MUQ/polychaos/pce/PolynomialChaosExpansion.h"
#include "MUQ/polychaos/polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/polychaos/quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/polychaos/smolyak/SmolyakPCEFactory.h"
#include "MUQ/GeneralUtilities/ParallelFunctionWrapper.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
#include "MUQ/polychaos/utilities/VariableCollection.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
using namespace muq::polychaos;


VectorXd easyFunction(VectorXd input)
{
    VectorXd output(1);
    output << sin(input(0)) + pow(input(1), 3);
      
    //make it take some time!
    sleep(.1);
    
    return output;
}

//can be run with mpirun -c 6 -hostfile ~/myhostfile build/modules/polychaos/examples/ParallelSimplePolychaos
//use env to set to std error as necessary. myhostfile = localhost slots=3


int main(int argc, char **argv) {
    //logging boilerplate
    InitializeLogging(argc, argv);
    boost::mpi::environment env(argc, argv);
    boost::mpi::communicator world;
        
    //create a wrapper on all processes
    boost::shared_ptr<ParallelFunctionWrapper> fn = ParallelFunctionWrapper::WrapVectorFunction(easyFunction,2,1);
    
    //get the workers started
     fn->InitializeWorkers();
    
    if(world.rank() == 0)
    {
    //we need a VariableCollection to specify the tensor space
    VariableCollection::Ptr vars(new VariableCollection());
    
    //use Gauss Patterson quadrature
    GaussPattersonQuadrature1D::Ptr quad1D(new GaussPattersonQuadrature1D());
    //and Legendre polynomials
    LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());
    
    //two input variables, both are the same
    vars->PushVariable("x", legendrePoly1D, quad1D);
    vars->PushVariable("y", legendrePoly1D, quad1D);
    


    //create the pce factory
    SmolyakPCEFactory::Ptr factory(new SmolyakPCEFactory(vars, fn));
    
    //Run it adaptively for a little time, after initializing with 
    //a three level simplex
    PolynomialChaosExpansion::Ptr pce1 = factory->StartAdaptiveTimed(3,.5);
    

    cout << "Done!" << endl;
    VectorXd testPoint(2);
    testPoint << 0, .1;
    LOG(INFO) << "Test point: " << testPoint << endl;
    LOG(INFO) << "Actual: " << fn->Evaluate(testPoint)<< endl;
    LOG(INFO) << "Approximate: " << pce1->Evaluate(testPoint)<< " with " << pce1->NumberOfPolynomials() << " terms." << endl;
    LOG(INFO) << "Difference: " << fn->Evaluate(testPoint)-pce1->Evaluate(testPoint)<< endl;
       cout<< "Test point: " << testPoint << endl;
   cout << "Actual: " << fn->Evaluate(testPoint)<< endl;
     cout << "Approximate: " << pce1->Evaluate(testPoint)<< " with " << pce1->NumberOfPolynomials() << " terms." << endl;
     cout << "Difference: " << fn->Evaluate(testPoint)-pce1->Evaluate(testPoint)<< endl;

       fn->CloseWorkers();

    }



    
    return 1;
}
