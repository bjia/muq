%% Create a PCE
close all
%clear all the C++ objects
clear muqPolychaos
%create a two dimension input problem, where the first is uniform and the 
%second is gaussian
vars = {'Uniform', 'Gaussian'};
%the function we'll use is matlabBindingsExampleFn
fnName = 'matlabBindingsExampleFn';
%two input dims, one output
inputDim = 2;
outputDim = 1;

%be careful, if this gets too large (a second for me) you'll use
%all the Gauss-Patterson nodes and matlab will crash. 
%The error message will show up on a linux command line.
timeToRun = 1e-3;

%create the pce problem, telling it about the variables
%and what function to approximate
pceFactoryHandle = muqPolychaos('CreatePCEFactory', vars, fnName, outputDim);

%Tell it to adaptively sample the function and construct a PCE, 
%returning a handle to us.
pceHandle = muqPolychaos('BeginAdaptForTime', pceFactoryHandle, timeToRun);

%Evaluate the PCE we've gotten so far
testPoint = [.1;.3];
result = muqPolychaos('EvaluatePCE', pceHandle, testPoint);

%If we're not satisfied, we can tell it to keep sampling, resuming
%where it left off. Note that a new handle is returned, and we overwrite
%the old handle, but the old handle is still valid and may be used.
pceHandle = muqPolychaos('ContinueAdaptForTime', pceFactoryHandle, timeToRun);

%Now evaluate the improved one.
result = muqPolychaos('EvaluatePCE', pceHandle, testPoint);

%% Graph the true and approximate functions

[X,Y] = meshgrid(-1:.05:1, -1:.05:1);

zApprox = zeros(size(X));
zTrue = zeros(size(X));

for i=1:size(X,1)
    for j=1:size(X,2)
        zApprox(i,j) = muqPolychaos('EvaluatePCE', pceHandle, [X(i,j) ;Y(i,j)]);
        zTrue(i,j) = matlabBindingsExampleFn([X(i,j) ;Y(i,j)]);
    end
end

figure();
surf(X,Y,zTrue);
hold on
surf(X,Y,zApprox, 'FaceAlpha', .8);
figure()
surf(X,Y,log(abs(zTrue-zApprox))/log(10), 'FaceAlpha', .8);

%% Save and load abilities

%save the best PCE we have
pceDataFilename = 'pceData';
muqPolychaos('SavePCE', pceDataFilename, pceHandle); 

smolyakDataFilename = 'smolyakData';
%save the factory
muqPolychaos('SaveSmolyakFactory', smolyakDataFilename, pceFactoryHandle);

%Throw away all the memory, so the C++ objects are gone, which simulates
%closing matlab.
clear muqPolychaos

%Load the pce back in
pceHandle2 = muqPolychaos('LoadPCE', pceDataFilename); 

%See that the loaded result is exactly identical
pceSaveError = result - muqPolychaos('EvaluatePCE', pceHandle2, testPoint)


%Now load the factory back in
pceFactoryHandle = muqPolychaos('LoadSmolyakFactory', smolyakDataFilename,...
    fnName, inputDim, outputDim);

%Resume adapting where we were
pceHandle = muqPolychaos('ContinueAdaptForTime', pceFactoryHandle, timeToRun);