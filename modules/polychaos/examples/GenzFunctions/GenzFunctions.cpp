/*
 * GenzFunctions.cpp
 *
 *  Created on: Aug 10, 2011
 *      Author: prconrad
 */

#include "polychaos/examples/GenzFunctions/GenzFunctions.h"

#include <assert.h>
#include <iostream>

#include <boost/bind.hpp>

using namespace arma;



colvec genzOsc(colvec x, colvec w, colvec c)
{
	//shift from [-1,1] to [0,1]
	x = x/2.0+0.5;

	colvec result = zeros(1);
	result(0) = cos(2.0*math::pi()*w(0) + dot(c,x));
	return result;
}

colvec genzProdPeak(colvec x, colvec w, colvec c)
{
	//shift from [-1,1] to [0,1]
	x = x/2.0+0.5;

	colvec result = zeros(1);
	result(0) = 1.0/as_scalar(prod(pow(c, -2) + pow(x-w,2)));
	return result;
}

colvec genzCornerPeak(colvec x, colvec w, colvec c)
{
	//shift from [-1,1] to [0,1]
	x = x/2.0+0.5;

	colvec result = zeros(1);
	result(0) = pow(1.0+dot(c,x), -static_cast<double>(x.n_rows)-1.0);
	return result;
}

colvec genzGaussian(colvec x, colvec w, colvec c)
{
	//shift from [-1,1] to [0,1]
	x = x/2.0+0.5;

	colvec result = zeros(1);
	result(0) = exp(-dot(pow(c,2),pow(x-w,2.0)));
	return result;
}

colvec genzContinuous(colvec x, colvec w, colvec c)
{
	//shift from [-1,1] to [0,1]
	x = x/2.0+0.5;

	colvec result = zeros(1);
	result(0) = exp(-dot(c,abs(x-w)));
	return result;
}

colvec genzDiscontinuous(colvec x, colvec w, colvec c)
{
	//shift from [-1,1] to [0,1]
	x = x/2.0+0.5;

	colvec result = zeros(1);

	if(x(0) <= w(0) && x(1) <= w(1))
	{
		return result;
	}

	result(0) = exp(dot(c,x));
	return result;
}

colvec genzMixed(colvec x, colvec w, colvec c)
{
	//always make this a length >3 vector!
	return genzOsc(x.subvec(0,2), w.subvec(0,2), c.subvec(0,2)) +
			genzCornerPeak(x.subvec(3, x.n_rows-1), w.subvec(3, x.n_rows-1), c.subvec(3, x.n_rows-1));
}


boost::function<colvec (colvec)> GetRandomGenzFunction(unsigned int type, unsigned int dim)
{
	colvec w = randu<colvec>(dim);
	colvec c = randu<colvec>(dim);

	double c_norm;
	switch(type)
	{
		case 1: c_norm = 1.5; break;
		case 2: c_norm = dim; break;
		case 3: c_norm = 1.85; break;
		case 4: c_norm = 7.03; break;
		case 5: c_norm = 20.4; break;
		case 6: c_norm = 4.3; break;
		case 7: c_norm = 1.7; break;
		default:
			assert(false);
	};

	//rescale c for the right norm
	c = c*c_norm/sum(c);

	//rescale w
	w = w/sum(w);

	boost::function<colvec (colvec)> result;

	switch(type)
		{
			case 1: result = boost::bind(genzOsc, _1, w,c); break;
			case 2: result = boost::bind(genzProdPeak, _1, w,c); break;
			case 3: result = boost::bind(genzCornerPeak, _1, w,c); break;
			case 4: result = boost::bind(genzGaussian, _1, w,c); break;
			case 5: result = boost::bind(genzContinuous, _1, w,c); break;
			case 6: result = boost::bind(genzDiscontinuous, _1, w,c); break;
			case 7: result = boost::bind(genzMixed, _1, w, c); break;

			default:
				assert(false);
		};

	return result;
}



