/*
 * GenzMain.cpp
 *
 *  Created on: Aug 15, 2011
 *      Author: prconrad
 */
#include "polychaos/examples/GenzFunctions/GenzMain.h"

#include <fstream>
#include <iostream>


#include <armadillo>
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tuple/tuple.hpp>

#include "polychaos/examples/GenzFunctions/GenzFunctions.h"
#include "polychaos/multiIndex/TotalOrderMultiIndexFamily.h"
#include "polychaos/pce/PolynomialChaosExpansion.h"
#include "polychaos/pce/SingleQuadraturePCEFactory.h"
#include "polychaos/polynomials/LegendrePolynomials1DRecursive.h"
#include "polychaos/quadrature/ExpGrowthQuadratureFamily1D.h"
#include "polychaos/quadrature/GaussLegendreQuadrature1D.h"
#include "polychaos/quadrature/ClenshawCurtisQuadrature1D.h"
#include "polychaos/quadrature/GaussPattersonQuadrature1D.h"
#include "polychaos/smolyak/SmolyakPCEFactory.h"
#include "polychaos/utilities/FunctionWrapper.h"
#include "polychaos/utilities/LogConfig.h"
#include "polychaos/utilities/VariableCollection.h"

using namespace arma;
using namespace std;


int main(int argc, char **argv) {
    //logging boilerplate
    InitializeLogging(argc, argv);
    
    
    //Run the benchmarking
    //RunNonAdaptiveGenzBenchmarking(5,20);
    RunAdaptiveGenzBenchmarking(5,20);
    
    return 1;
}


boost::tuple<double, double> EstimateApproximationError(boost::function<colvec (colvec)> trueFn,
							PolynomialChaosExpansion::Ptr approxFn, unsigned int inputDim)
{
    //the number of samples
    unsigned int samples = 1000;
    //allocate space for all the errors, and for one at a time
    colvec termErrors = zeros<colvec>(samples);
    colvec oneError = zeros<colvec>(1);
    
    //loop over the samples
    for(unsigned int i=0; i<samples; ++i)
    {
	//get a random point using the armadillo random fn
	colvec randPoint = randu<colvec>(inputDim);
	randPoint = 2.0*randPoint-1.0; //shift from [0,1] to [-1,1]
	
	//compute the difference between the true and approximation.
	//Note that we can call the boost::function just like a regular function.
	oneError = trueFn(randPoint)-approxFn->Evaluate(randPoint);
	
	//compute the norm of this one term 
	termErrors(i) = norm(oneError, 2);
	
    }
    
    //return a tuple with the scaled norm (L_2) and the max (L_\infty)
    return boost::tuple<double, double>(norm(termErrors,2)*
    sqrt(pow(2.0,static_cast<double>(inputDim))/static_cast<double>(samples)),
					max(termErrors));
}


void RunNonAdaptiveGenzBenchmarking(unsigned int genzDim, unsigned int numTrials)
{
    //we need a var collection for linear and exponential growth
    VariableCollection::Ptr linVarCollection(new VariableCollection());
    VariableCollection::Ptr expVarCollection(new VariableCollection());
    VariableCollection::Ptr ccVarCollection(new VariableCollection());
    VariableCollection::Ptr pattersonVarCollection(new VariableCollection());
    
    //We need Legendre quadrature
    GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
    //Make a wrapper around the legendre quadrature that grows exponentially
    ExpGrowthQuadratureFamily1D::Ptr expLegendreQuad1D(new ExpGrowthQuadratureFamily1D(legendreQuad1D));
    //Make a family of Legendre polynomials
    LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());
    //make a clenshaw curtis rule
    ClenshawCurtisQuadrature1D::Ptr ccQuad1D(new ClenshawCurtisQuadrature1D());
    GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
    
    //constuct the two variable collections
    for(unsigned int i=0; i<genzDim; ++i)
    {
	linVarCollection->PushVariable("x"+ boost::lexical_cast<string>(i),
				       legendrePoly1D, legendreQuad1D);
	
	expVarCollection->PushVariable("x"+ boost::lexical_cast<string>(i),
				       legendrePoly1D, expLegendreQuad1D);
	
	ccVarCollection->PushVariable("x"+ boost::lexical_cast<string>(i),
				      legendrePoly1D, ccQuad1D);
	
	pattersonVarCollection->PushVariable("x"+ boost::lexical_cast<string>(i),
					     legendrePoly1D, pattersonQuad1D);
    }
    
    //These variables tune how much work to do. As packaged, they perform 
    //about 10^7 evaluations for each type.
    urowvec fullTensorOrders;
    fullTensorOrders  << 1 << 2 << 3 << 4 << 5;
    urowvec linTotalIsoOrders;
    linTotalIsoOrders << 1 <<  2 << 4 << 6 << 8 << 9;
    urowvec expTotalIsoOrders;
    expTotalIsoOrders <<  1 <<  2 << 4 << 6 << 7;
    urowvec pattersonTotalIsoOrders;
    pattersonTotalIsoOrders  << 1 <<  2 << 4 <<5<< 6;
    //     urowvec timeToAdapt;
    //     timeToAdapt << 0 << 1 << 2 << 2 << 2 << 2 << 5 << 5 << 10 << 10; //adaptation time in seconds, initial rounds are fairly fast.
    //     unsigned int initialAdapt = 2;
    
    
    //Primary benchmarking loop, try all 7 types
    for(unsigned int genzType =1; genzType <= 3; ++genzType)
    {
	//put the results here
	string outFileName = "results/GenzResults/GenzResults" + boost::lexical_cast<string>(genzType) + ".m";
	ofstream outFile(outFileName.c_str());
	
	//loop over the trials
	for(unsigned int i=1; i<=numTrials; ++i)
	{
	    //get a random function of this type and make a wrapper for it
	    boost::function<colvec (colvec)> randGenzFn = GetRandomGenzFunction(genzType, genzDim);
	    //Note that the eval counter for this is not affected by testing with randGenzFn!
	    FunctionWrapper::Ptr fn(new FunctionWrapper(randGenzFn,genzDim,1));
	    
	    //Linear full tensors of sizes j from fullTensorOrders
	    for(unsigned int j=0; j<fullTensorOrders.n_cols; ++j)
	    {
		//reset the number of evaluations used
		fn->ResetEvalsCount();
		
		//construct a single factory
		SingleQuadraturePCEFactory linSingleFactory(linVarCollection);
		//tell it to make the approximation of the given size
		PolynomialChaosExpansion::Ptr linTensorPCE = linSingleFactory.ConstructPCE(fn,
											   ones<urowvec>(linVarCollection->length())*fullTensorOrders(j),
											   zeros<urowvec>(linVarCollection->length()));
		
		
		//output the number of points used
		outFile << "GenzResults{" << genzType << "}.FullTensorPoints(" << i << "," << j+1 <<
		")=" << static_cast<int>(fn->GetNumOfEvals()) << ";"<< endl;
		
		//estimate the error and output it
		boost::tuple<double, double> estError =
		EstimateApproximationError(randGenzFn,linTensorPCE,genzDim);
		
		outFile << "GenzResults{" << genzType << "}.FullTensorErrorLinf(" << i << "," << j+1 <<
		")=" << estError.get<0>() << ";"<< endl;
		outFile << "GenzResults{" << genzType << "}.FullTensorErrorL2(" << i << "," << j+1 <<
		")=" << estError.get<1>() << ";"<< endl;
	    }
	    
	    fn->ResetEvalsCount();
	    //Isotropic linear growth, for sizes in linTotalIsoOrders
	    SmolyakPCEFactory linSingleFactory(linVarCollection, fn);
	    for(unsigned int j=0; j<linTotalIsoOrders.n_cols; ++j)
	    {
		PolynomialChaosExpansion::Ptr linIsoPCE;
		if(j==0)
		{
		    linIsoPCE = linSingleFactory.StartFixedTerms(linTotalIsoOrders(j));
		    
		}
		else
		{
		    linIsoPCE = linSingleFactory.AddFixedTerms(linTotalIsoOrders(j));
		}
		
		//output the number of points used
		outFile << "GenzResults{" << genzType << "}.LinIsoPoints(" << i << "," << j+1 <<
		")=" <<fn->GetNumOfEvals() << ";"<< endl;
		//estimate the error and output it
		boost::tuple<double, double> estError =
		EstimateApproximationError(randGenzFn,linIsoPCE,genzDim);
		
		outFile << "GenzResults{" << genzType << "}.LinIsoErrorLinf(" << i << "," << j+1 <<
		")=" << estError.get<0>() << ";"<< endl;
		outFile << "GenzResults{" << genzType << "}.LinIsoErrorL2(" << i << "," << j+1 <<
		")=" << estError.get<1>() << ";"<< endl;
	    }
	    
	    
	    
	    //Isotropic exp growth, as the last group, except with exponential growth quadrature
	    fn->ResetEvalsCount();
	    SmolyakPCEFactory expSingleFactory(expVarCollection, fn);
	    for(unsigned int j=0; j<expTotalIsoOrders.n_cols; ++j)
	    {
		PolynomialChaosExpansion::Ptr expIsoPCE;
		if(j==0)
		{
		    expIsoPCE = expSingleFactory.StartFixedTerms(expTotalIsoOrders(j));
		}
		else
		{
		    expIsoPCE = expSingleFactory.AddFixedTerms(expTotalIsoOrders(j));
		}
		
		//output the number of points used
		outFile << "GenzResults{" << genzType << "}.ExpIsoPoints(" << i << "," << j+1 <<
		")=" <<fn->GetNumOfEvals() << ";"<< endl;
		
		//estimate the error and output it
		boost::tuple<double, double> estError =
		EstimateApproximationError(randGenzFn,expIsoPCE,genzDim);
		
		outFile << "GenzResults{" << genzType << "}.ExpIsoErrorLinf(" << i << "," << j+1 <<
		")=" << estError.get<0>() << ";"<< endl;
		outFile << "GenzResults{" << genzType << "}.ExpIsoErrorL2(" << i << "," << j+1 <<
		")=" << estError.get<1>() << ";"<< endl;
	    }
	    
	    
	    
	    
	    
	    //As the last group, except with clenshaw curtis
	    fn->ResetEvalsCount();
	    SmolyakPCEFactory ccSingleFactory(ccVarCollection, fn);
	    for(unsigned int j=0; j<expTotalIsoOrders.n_cols; ++j)
	    {
		PolynomialChaosExpansion::Ptr ccIsoPCE;
		if(j==0)
		{
		    ccIsoPCE = ccSingleFactory.StartFixedTerms(expTotalIsoOrders(j));
		}
		else
		{
		    ccIsoPCE = ccSingleFactory.AddFixedTerms(expTotalIsoOrders(j));
		}
		
		//output the number of points used
		outFile << "GenzResults{" << genzType << "}.ccIsoPoints(" << i << "," << j+1 <<
		")=" <<fn->GetNumOfEvals() << ";"<< endl;
		
		//estimate the error and output it
		boost::tuple<double, double> estError =
		EstimateApproximationError(randGenzFn,ccIsoPCE,genzDim);
		
		outFile << "GenzResults{" << genzType << "}.ccIsoErrorLinf(" << i << "," << j+1 <<
		")=" << estError.get<0>() << ";"<< endl;
		outFile << "GenzResults{" << genzType << "}.ccIsoErrorL2(" << i << "," << j+1 <<
		")=" << estError.get<1>() << ";"<< endl;
	    }
	    
	    
	    
	    
	    //As the last group, except with gauss patterson
	    fn->ResetEvalsCount();
	    SmolyakPCEFactory pattersonSingleFactory(pattersonVarCollection, fn);
	    for(unsigned int j=0; j<pattersonTotalIsoOrders.n_cols; ++j)
	    {
		PolynomialChaosExpansion::Ptr pattersonIsoPCE;
		if(j==0)
		{
		    pattersonIsoPCE = pattersonSingleFactory.StartFixedTerms(pattersonTotalIsoOrders(j));
		}
		else
		{
		    pattersonIsoPCE = pattersonSingleFactory.AddFixedTerms(pattersonTotalIsoOrders(j));
		}
		
		//output the number of points used
		outFile << "GenzResults{" << genzType << "}.pattersonIsoPoints(" << i << "," << j+1 <<
		")=" <<fn->GetNumOfEvals() << ";"<< endl;
		
		//estimate the error and output it
		boost::tuple<double, double> estError =
		EstimateApproximationError(randGenzFn,pattersonIsoPCE,genzDim);
		
		outFile << "GenzResults{" << genzType << "}.pattersonIsoErrorLinf(" << i << "," << j+1 <<
		")=" << estError.get<0>() << ";"<< endl;
		outFile << "GenzResults{" << genzType << "}.pattersonIsoErrorL2(" << i << "," << j+1 <<
		")=" << estError.get<1>() << ";"<< endl;
	    }
	    
	    
	    //As above, but for CC adaptation.
	    // 	    fn->ResetEvalsCount();
	    // 	    SmolyakPCEFactory ccAdapt(ccVarCollection, fn);
	    // 	    //do very little adaptation
	    // 	    ccAdapt.StartAdaptiveTimed(initialAdapt, 0);
	    // 	    for(unsigned int j=0; j<timeToAdapt.n_cols; ++j)
	    // 	    {
		// 		PolynomialChaosExpansion::Ptr ccAdaptPCE = ccAdapt.AdaptForTime(timeToAdapt(j),0);
		// 		
		// 		outFile << "GenzResults{" << genzType << "}.ccAdaptPoints(" << i << "," << j+1 <<
		// 		")=" << static_cast<int>(fn->GetNumOfEvals()) << ";"<< endl;
		// 		
		// 		boost::tuple<double, double> estError =
		// 		EstimateApproximationError(randGenzFn,ccAdaptPCE,genzDim);
		// 		
		// 		outFile << "GenzResults{" << genzType << "}.ccAdaptErrorLinf(" << i << "," << j+1 <<
		// 		")=" << estError.get<0>() << ";"<< endl;
		// 		outFile << "GenzResults{" << genzType << "}.ccAdaptErrorL2(" << i << "," << j+1 <<
		// 		")=" << estError.get<1>() << ";"<< endl;
		// 		
		// 		outFile << "GenzResults{" << genzType << "}.ccAdaptIndicator(" << i << "," << j+1 <<
		// 		")=" << ccAdapt.GetGlobalErrorIndicator() << ";"<< endl;
		// 	    }
		// 		
		// 		
		// 			    //As above, but for exponential adaptation.
		// 	    fn->ResetEvalsCount();
		// 	    SmolyakPCEFactory pattersonAdapt(pattersonVarCollection, fn);
		// 	    //do very little adaptation
		// 	    pattersonAdapt.StartAdaptiveTimed(initialAdapt, 0);
		// 	    for(unsigned int j=0; j<timeToAdapt.n_cols; ++j)
		// 	    {
		    // 		PolynomialChaosExpansion::Ptr pattersonAdaptPCE = pattersonAdapt.AdaptForTime(timeToAdapt(j),0);
		    // 		
		    // 		outFile << "GenzResults{" << genzType << "}.pattersonAdaptPoints(" << i << "," << j+1 <<
		    // 		")=" << static_cast<int>(fn->GetNumOfEvals()) << ";"<< endl;
		    // 		
		    // 		boost::tuple<double, double> estError =
		    // 		EstimateApproximationError(randGenzFn,pattersonAdaptPCE,genzDim);
		    // 		
		    // 		outFile << "GenzResults{" << genzType << "}.pattersonAdaptErrorLinf(" << i << "," << j+1 <<
		    // 		")=" << estError.get<0>() << ";"<< endl;
		    // 		outFile << "GenzResults{" << genzType << "}.pattersonAdaptErrorL2(" << i << "," << j+1 <<
		    // 		")=" << estError.get<1>() << ";"<< endl;
		    // 		
		    // 		outFile << "GenzResults{" << genzType << "}.pattersonAdaptIndicator(" << i << "," << j+1 <<
		    // 		")=" << pattersonAdapt.GetGlobalErrorIndicator() << ";"<< endl;
		    // 	    }
		    
		    
	}
	
	
	
	//close the file for this type
	outFile.close();
	
	
    }
}


void RunAdaptiveGenzBenchmarking(unsigned int genzDim, unsigned int numTrials)
{
    //we need a var collection for linear and exponential growth
    
    VariableCollection::Ptr pattersonVarCollection(new VariableCollection());
    
    LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());
    GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
    
    //constuct the two variable collections
    for(unsigned int i=0; i<genzDim; ++i)
    {
	pattersonVarCollection->PushVariable("x"+ boost::lexical_cast<string>(i),
					     legendrePoly1D, pattersonQuad1D);
    }
    
    urowvec pattersonTotalIsoOrders;
    pattersonTotalIsoOrders  << 1 <<  2 << 4 <<5<< 6;
    rowvec timeToAdapt;
    timeToAdapt << 0 << 1e-6 << 1e-5 <<1e-4 << 1e-3 << 1e-2 << 1e-1 << 1 << 5;
    unsigned int initialAdapt = 1;
    
    
    //Primary benchmarking loop, try all 7 types
    for(unsigned int genzType =1; genzType <= 6; ++genzType)
    {
	//put the results here
	string outFileName = "results/GenzResults/GenzResultsAdaptive" + boost::lexical_cast<string>(genzType) + ".m";
	ofstream outFile(outFileName.c_str());
	
	//loop over the trials
	for(unsigned int i=1; i<=numTrials; ++i)
	{
	    //get a random function of this type and make a wrapper for it
	    boost::function<colvec (colvec)> randGenzFn = GetRandomGenzFunction(genzType, genzDim);
	    //Note that the eval counter for this is not affected by testing with randGenzFn!
	    FunctionWrapper::Ptr fn(new FunctionWrapper(randGenzFn,genzDim,1));
	    
			    
			    //As above, but for exponential adaptation.
			    fn->ResetEvalsCount();
			    SmolyakPCEFactory gpAdapt(pattersonVarCollection, fn);
			    //do no adaptation
			    gpAdapt.StartAdaptiveTimed(initialAdapt, 0);
			    for(unsigned int j=0; j<timeToAdapt.n_cols; ++j)
			    {
		    		PolynomialChaosExpansion::Ptr gpAdaptAdaptPCE = gpAdapt.AdaptForTime(timeToAdapt(j),0);
		    		
		    		outFile << "GenzResults{" << genzType << "}.pattersonAdaptPoints(" << i << "," << j+1 <<
		    		")=" << static_cast<int>(fn->GetNumOfEvals()) << ";"<< endl;
		    		
		    		boost::tuple<double, double> estError =
		    		EstimateApproximationError(randGenzFn,gpAdaptAdaptPCE,genzDim);
		    		
		    		outFile << "GenzResults{" << genzType << "}.pattersonAdaptErrorLinf(" << i << "," << j+1 <<
		    		")=" << estError.get<0>() << ";"<< endl;
		    		outFile << "GenzResults{" << genzType << "}.pattersonAdaptErrorL2(" << i << "," << j+1 <<
		    		")=" << estError.get<1>() << ";"<< endl;
		    		
		    		outFile << "GenzResults{" << genzType << "}.pattersonAdaptIndicator(" << i << "," << j+1 <<
		    		")=" << gpAdapt.GetGlobalErrorIndicator() << ";"<< endl;
		    	    }
		    
		    //As the last group, except with gauss patterson
		    fn->ResetEvalsCount();
		    SmolyakPCEFactory pattersonSingleFactory(pattersonVarCollection, fn);
		    for(unsigned int j=0; j<pattersonTotalIsoOrders.n_cols; ++j)
		    {
			PolynomialChaosExpansion::Ptr pattersonIsoPCE;
			if(j==0)
			{
			    pattersonIsoPCE = pattersonSingleFactory.StartFixedTerms(pattersonTotalIsoOrders(j));
			}
			else
			{
			    pattersonIsoPCE = pattersonSingleFactory.AddFixedTerms(pattersonTotalIsoOrders(j));
			}
			
			//output the number of points used
			outFile << "GenzResults{" << genzType << "}.pattersonIso2Points(" << i << "," << j+1 <<
			")=" <<fn->GetNumOfEvals() << ";"<< endl;
			
			//estimate the error and output it
			boost::tuple<double, double> estError =
			EstimateApproximationError(randGenzFn,pattersonIsoPCE,genzDim);
			
			outFile << "GenzResults{" << genzType << "}.pattersonIso2ErrorLinf(" << i << "," << j+1 <<
			")=" << estError.get<0>() << ";"<< endl;
			outFile << "GenzResults{" << genzType << "}.pattersonIso2ErrorL2(" << i << "," << j+1 <<
			")=" << estError.get<1>() << ";"<< endl;
		    }
		    
		    
			    
	}
	
	
	
	//close the file for this type
	outFile.close();
	
	
    }
}