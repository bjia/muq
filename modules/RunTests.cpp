//
//  RunTests.cpp
//  
//
//  Created by Matthew Parno on 1/17/12.
//  Copyright (c) 2012 MIT. All rights reserved.
//

#include <iostream>

// include the google testing header
#include "gtest/gtest.h"

#include "MUQ/GeneralUtilities/LogConfig.h"

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	muq::GeneralUtilities::InitializeLogging(argc, argv);
	
	return RUN_ALL_TESTS();
}


