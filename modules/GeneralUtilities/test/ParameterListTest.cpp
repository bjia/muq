//
//  DimensionCheckTest.cpp
//  
//
//  Created by Matthew Parno on 1/13/12.
//  Copyright (c) 2012 MIT. All rights reserved.
//

#include <iostream>
#include "MUQ/GeneralUtilities/ParameterList.h"




#include "gtest/gtest.h"

using namespace std;
using namespace muq::GeneralUtilities;



TEST(GeneralUtilities, ParameterListWriteAndRead){
    // create an empty parameter list
    ParameterList TestList;
    
    // add a few parameters
    TestList.Set("AllDoubles.Double1",11.2);
    TestList.Set("AllDoubles.Double2",20.2);
    TestList.Set("StringTest","blahblah");
    TestList.Set("IntTest", 12);
    
    TestList.WriteToFile("results/tests/testsTestFile.xml");
    
    ParameterList Test2( "results/tests/testsTestFile.xml");
    
    
    EXPECT_EQ(11.2,Test2.Get("AllDoubles.Double1",0.0));
    EXPECT_EQ(20.2,Test2.Get("AllDoubles.Double2",0.0));
    EXPECT_STREQ("blahblah",Test2.Get("StringTest","nope").c_str());
    EXPECT_EQ(12,Test2.Get("IntTest",0));
    
}


TEST(GeneralUtilities,ParameterListReadExampleParamFile){
    // create an empty parameter list
    ParameterList TestList("data/tests/ExampleParamFile.xml");
    
    // test reading a few parameters
    EXPECT_EQ(2, TestList.Get("Dim",2));
    EXPECT_EQ(2, TestList.Get("Verbose",2));
    
    EXPECT_EQ(500000, TestList.Get("MCMC.Steps",2));
    EXPECT_EQ(2000, TestList.Get("MCMC.BurnIn",2));
    
    EXPECT_EQ(.15, TestList.Get("MCMC.MH.PropSize",2.0));
    
    EXPECT_EQ(1.2, TestList.Get("MCMC.AM.AdaptScale",2.0));
    
    EXPECT_STREQ("Power", TestList.Get("MCMC.DR.ScaleType","none").c_str());
    
}



