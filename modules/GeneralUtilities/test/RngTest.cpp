//
//  RngTest.cpp
//  
//
//  Created by Matthew Parno on 1/30/12.
//  Copyright (c) 2012 MIT. All rights reserved.
//

#include "gtest/gtest.h"

#include "MUQ/GeneralUtilities/RandGen.h"


using namespace muq::GeneralUtilities;

TEST(GeneralUtiltiesRandGen, UniformReal){
    
    // create an instance of the boost RNG class
    RandomGenerator RNG;
    
    // generate a bunch of uniformly distributed samples and keep track of the mean
    int N = 100000;
    double mean = 0;
    for(int i=0; i<N; ++i)
        mean += RNG.GetUniform();
        
    mean /= N;
    
    EXPECT_NEAR(mean, 0.5, 1e-2);
    
}

TEST(GeneralUtiltiesRandGen, Gaussian){
    
    // create an instance of the boost RNG class
    RandomGenerator RNG;
    
    // generate a bunch of uniformly distributed samples and keep track of the mean
    int N = 100000;
    double mean = 0;
    for(int i=0; i<N; ++i)
        mean += RNG.GetNormal();
    
    mean /= N;
    
    EXPECT_NEAR(mean, 0.0, 1e-2);
    
}

TEST(GeneralUtiltiesRandGen, UniformRealInt){
    
    // create an instance of the boost RNG class
    RandomGenerator RNG;
    
    // generate a bunch of uniformly distributed samples and keep track of the mean
    int N = 100000;
    double mean = 0;
    for(int i=0; i<N; ++i)
        mean += RNG.GetUniformInt(1,15);
    
    mean /= N;
    
    EXPECT_NEAR(mean, 8, 1e-1);
    
}

TEST(GeneralUtiltiesRandGen, SeedResetTest){
    
    // create an instance of the boost RNG class
    RandomGenerator RNG;
    
    // set the seed
    RNG.SetSeed(3001);
    
    // generate 3 normal samples
    double before[3];
    double after[3];
    before[0] = RNG.GetNormal();
    before[1] = RNG.GetNormal();
    before[2] = RNG.GetNormal();
    
    // reset the seed
    RNG.SetSeed(3001);
    after[0] = RNG.GetNormal();
    after[1] = RNG.GetNormal();
    after[2] = RNG.GetNormal();
    
    // compare the before and after
    EXPECT_EQ(before[0],after[0]);
    EXPECT_EQ(before[1],after[1]);
    EXPECT_EQ(before[2],after[2]);
    
}

