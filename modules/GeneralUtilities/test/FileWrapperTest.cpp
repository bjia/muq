//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "gtest/gtest.h"

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>

#include "MUQ/GeneralUtilities/CachedFunctionWrapper.h"
#include "MUQ/GeneralUtilities/DataFileWrapper.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"


using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

//This test exercises all of the data file interfaces, along with the cached inherited functions
TEST(GeneralUtilitiesDeathTest, FunctionWrapperDataFile)
{
  ::testing::FLAGS_gtest_death_test_style = "threadsafe"; //avoids a warning about not being threadsafe

  //load some data
  DataFileWrapper::Ptr testFn = DataFileWrapper::WrapFile("data/tests/DataFileWrapperExample.dat", 3,2);
  
  MatrixXd allPoints = testFn->GetCachePoints();

  EXPECT_EQ(3, allPoints.rows());
  EXPECT_EQ(3, allPoints.cols());

  Vector3d testInput;
  testInput << 1,2,3;
  Vector2d testTrueResult;
  testTrueResult << 4,5;
  VectorXd testResult = testFn->Evaluate(testInput);
  EXPECT_TRUE(MatrixApproxEqual(testTrueResult, testResult, 1e-14));

  //not there, so it fails
  Vector3d testBadInput;
  testBadInput << 1,2,1;
  EXPECT_DEATH(testFn->Evaluate(testBadInput),"");
}

TEST(GeneralUtilities, ExternalFunction)
{
  CachedFunctionWrapper::Ptr fn = CachedFunctionWrapper::WrapExecutableFunction("data/tests/testShellFn", 3, 3);
  
  MatrixXd testPoints(3,4);
  testPoints << 1,2,3,4,5,6,7,8,9,10,11,12;
  
    EXPECT_TRUE(MatrixApproxEqual(testPoints, fn->Evaluate(testPoints), 1e-14));
    
}