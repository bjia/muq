//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <iostream>

#include "gtest/gtest.h"

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexWrapper.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/FullTensorMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/TotalOrderMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/AdaptiveMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/ExpLimitedMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/UnstructuredMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/LogConfig.h"

using namespace std;

using namespace Eigen;
using namespace muq::GeneralUtilities;

///Test the first, last, and a couple of middle multi-indices from the full tensor family.
TEST(GeneralUtilitiesMultiIndexFamily, FullTensor)
{
	FullTensorMultiIndexFamily::Ptr indexFamily = FullTensorMultiIndexFamily::Create(5,6);

	RowVectorXu index0 = indexFamily->IndexToMulti(0);
	RowVectorXu index28 = indexFamily->IndexToMulti(28);
	RowVectorXu index837 = indexFamily->IndexToMulti(837);
	RowVectorXu indexLast = indexFamily->IndexToMulti(7775);

	RowVectorXu index0Correct(5);
	index0Correct << 0 , 0 , 0 , 0 , 0;
	RowVectorXu index28Correct(5);
	index28Correct << 0 , 0 , 0 ,4 , 4;
	RowVectorXu index837Correct(5);
	index837Correct << 0 , 3 , 5 , 1 , 3;
	RowVectorXu indexLastCorrect(5);
	indexLastCorrect << 5 , 5 , 5, 5 , 5;

	EXPECT_TRUE(MatrixEqual( index0, index0Correct));
	EXPECT_TRUE(MatrixEqual( index28, index28Correct));
	EXPECT_TRUE(MatrixEqual( index837, index837Correct));
	EXPECT_TRUE(MatrixEqual( indexLast, indexLastCorrect));

	EXPECT_EQ(837, indexFamily->MultiToIndex(index837Correct));
}

///Test the first, last, and a couple of middle multi-indices from the total order family.
TEST(GeneralUtilitiesMultiIndexFamily, TotalOrder)
{

	//Test the number of a larger family
	TotalOrderMultiIndexFamily::Ptr indexFamily = TotalOrderMultiIndexFamily::Create(7,6);

	EXPECT_EQ(1716, indexFamily->GetNumberOfIndices());

// 	cout << "num: " << indexFamily->GetNumberOfIndices() << endl;
// 	for(int i =0; i<indexFamily->GetNumberOfIndices(); i++)
// 		cout << indexFamily->IndexToMulti(i);

	RowVectorXu index0 = indexFamily->IndexToMulti(0);
	RowVectorXu index49 = indexFamily->IndexToMulti(49);
	RowVectorXu index1647 = indexFamily->IndexToMulti(1647);
	RowVectorXu indexLast = indexFamily->IndexToMulti(1715);

	RowVectorXu index0Correct(7);
	index0Correct << 0 , 0 , 0 , 0 , 0, 0, 0;
	RowVectorXu index49Correct(7);
	index49Correct << 0 , 0 , 0 , 0 , 2, 0, 0;
	
	RowVectorXu index1647Correct(7);
	index1647Correct << 3 , 0 , 2 , 0 , 0, 0, 1;
	
	RowVectorXu indexLastCorrect(7);
	indexLastCorrect << 6 , 0 , 0 , 0 , 0, 0, 0;
	

	EXPECT_TRUE(MatrixEqual( index0, index0Correct));
	EXPECT_TRUE(MatrixEqual( index49, index49Correct));
	EXPECT_TRUE(MatrixEqual( index1647, index1647Correct));
	EXPECT_TRUE(MatrixEqual( indexLast, indexLastCorrect));

	EXPECT_EQ(1647, indexFamily->MultiToIndex(index1647));

}

///Test the first, last, and a couple of middle multi-indices from the total order family.
TEST(GeneralUtilitiesMultiIndexFamily, TotalOrderWithMin)
{

	//Test the number of a larger family
	TotalOrderMultiIndexFamily::Ptr indexFamily = TotalOrderMultiIndexFamily::Create(7,8,4);

	EXPECT_EQ(6315, indexFamily->GetNumberOfIndices());

//	cout << "num: " << indexFamily->GetNumberOfIndices() << endl;
//	for(int i =0; i<indexFamily->GetNumberOfIndices(); i++)
//		cout << indexFamily->IndexToMulti(i);

	RowVectorXu index0 = indexFamily->IndexToMulti(0);
	RowVectorXu index53 = indexFamily->IndexToMulti(53);
	RowVectorXu index5461 = indexFamily->IndexToMulti(5461);
	RowVectorXu indexLast = indexFamily->IndexToMulti(6314);


	RowVectorXu index0Correct(7);
	index0Correct << 0 , 0 , 0 , 0 , 0, 0, 4;
	
	RowVectorXu index53Correct(7);
	index53Correct <<0 , 0 , 0 , 0 ,1, 3, 3;
	
	RowVectorXu index5461Correct(7);
	index5461Correct << 2 , 3 , 0 ,1 , 0, 0, 1;
	
	RowVectorXu indexLastCorrect(7);
	indexLastCorrect << 8 , 0 , 0 , 0 , 0, 0, 0;
	

	EXPECT_TRUE(MatrixEqual( index0, index0Correct));
	EXPECT_TRUE(MatrixEqual( index53, index53Correct));
	EXPECT_TRUE(MatrixEqual( index5461, index5461Correct));
	EXPECT_TRUE(MatrixEqual( indexLast, indexLastCorrect));

	EXPECT_EQ(5461, indexFamily->MultiToIndex(index5461Correct));
}

TEST(GeneralUtilitiesMultiIndexFamily, TotalOrderWithMinBase)
{

	//Test the number of a larger family
	TotalOrderMultiIndexFamily::Ptr indexFamily = TotalOrderMultiIndexFamily::Create(8,7,4,3);

	EXPECT_EQ(6431, indexFamily->GetNumberOfIndices());

//	cout << "num: " << indexFamily->GetNumberOfIndices() << endl;
//	for(int i =0; i<indexFamily->GetNumberOfIndices(); i++)
//		cout << indexFamily->IndexToMulti(i);

	RowVectorXu index0 = indexFamily->IndexToMulti(0);
	RowVectorXu index53 = indexFamily->IndexToMulti(53);
	RowVectorXu index5461 = indexFamily->IndexToMulti(5461);
	RowVectorXu indexLast = indexFamily->IndexToMulti(6430);


	RowVectorXu index0Correct(8);
	index0Correct << 3 , 3 , 3 , 3 , 3, 3, 3, 7;
	
	RowVectorXu index53Correct(8);
	index53Correct << 3 , 3 , 3 , 3 , 3, 4, 6, 6;
	
	RowVectorXu index5461Correct(8);
	index5461Correct <<  5 , 3 , 4 , 3 , 6, 3, 3, 3;
	
	RowVectorXu indexLastCorrect(8);
	indexLastCorrect << 10 , 3 , 3 , 3 , 3, 3, 3, 3;
	

	EXPECT_TRUE(MatrixEqual( index0, index0Correct));
	EXPECT_TRUE(MatrixEqual( index53, index53Correct));
	EXPECT_TRUE(MatrixEqual( index5461, index5461Correct));
	EXPECT_TRUE(MatrixEqual( indexLast, indexLastCorrect));

	EXPECT_EQ(5461, indexFamily->MultiToIndex(index5461Correct));
}



///Test the first, last, and a couple of middle multi-indices from the total order family, non-uniform.
TEST(GeneralUtilitiesMultiIndexFamily, NonUniformTensor)
{

	//Test the number of a larger family
	RowVectorXu order(5);
	order << 2 , 3 , 5 , 4 , 5;
	FullTensorMultiIndexFamily::Ptr indexFamily = FullTensorMultiIndexFamily::Create(5, order);

	EXPECT_EQ(600, indexFamily->GetNumberOfIndices());


// 	cout << "num: " << indexFamily->GetNumberOfIndices() << endl;
// 	for(int i =0; i<indexFamily->GetNumberOfIndices(); i++)
// 		cout << indexFamily->IndexToMulti(i);


	RowVectorXu index0 = indexFamily->IndexToMulti(0);
	RowVectorXu index146 = indexFamily->IndexToMulti(146);
	RowVectorXu index316 = indexFamily->IndexToMulti(316);
	RowVectorXu indexLast = indexFamily->IndexToMulti(599);

	RowVectorXu index0Correct(5);
	index0Correct << 0,0,0,0,0;
	RowVectorXu index146Correct(5);
	index146Correct <<0,1,2,1,1;
	
	RowVectorXu index316Correct(5);
	index316Correct << 1,0,0,3,1;
	
	RowVectorXu indexLastCorrect(5);
	indexLastCorrect << 1,2,4,3,4;
	

	EXPECT_TRUE(MatrixEqual( index0, index0Correct));
	EXPECT_TRUE(MatrixEqual( index146, index146Correct));
	EXPECT_TRUE(MatrixEqual( index316, index316Correct));
	EXPECT_TRUE(MatrixEqual( indexLast, indexLastCorrect));

	EXPECT_EQ(316, indexFamily->MultiToIndex(index316Correct));
}


///A simple test of a small adaptive multiIndex set
TEST(GeneralUtilitiesMultiIndexFamily, AdaptiveMultiIndex)
{
	AdaptiveMultiIndexFamily::Ptr adaptiveFamily = AdaptiveMultiIndexFamily::Create(2,1);

	//add a few of the admissible ones to the active set
	RowVectorXu a(2);
	a << 1 , 2;

	adaptiveFamily->MakeMultiIndexActive(a);
	RowVectorXu b(2);
	b << 1, 3;

	adaptiveFamily->MakeMultiIndexActive(b);
	RowVectorXu c(2);
	c << 2 , 1;

	adaptiveFamily->MakeMultiIndexActive(c);

	//check one that isn't supposed to work yet
	RowVectorXu d(2);
	d << 2 , 3;
	EXPECT_FALSE(adaptiveFamily->IsMultiIndexAdmissible(d));

	//now test that precisely the correct ones are admissible and active
	MatrixXu active(4,2);
	active <<  1     ,   1 ,
			1   ,     2,
			1   ,    3,
			2   ,     1;
	MatrixXu admissible(3,2);
	admissible << 1      ,   4,
			 2        , 2,
			 3     ,    1;

	EXPECT_TRUE(MatrixEqual( active, adaptiveFamily->GetAllMultiIndices()));
	EXPECT_TRUE(MatrixEqual( admissible, adaptiveFamily->GetAdmissibleMultiIndices()));
}

///A simple test of a small adaptive multiIndex set, testing out the initialization with a simplex.
TEST(GeneralUtilitiesMultiIndexFamily, AdaptiveMultiIndexSimplex)
{
	AdaptiveMultiIndexFamily::Ptr adaptiveFamily = AdaptiveMultiIndexFamily::Create(3,0,3);

	//now test that precisely the correct ones are admissible and active
	MatrixXu active(10,3);
	active <<  0    ,    0 ,       0 ,
	        0   ,     0   ,     1 ,
	        0   ,     0  ,     2 ,
	        0   ,     1   ,    0 ,
	        0   ,     1 ,    1 ,
	        0   ,     2  ,   0 ,
	        1   ,     0 ,    0 ,
	        1  ,     0  ,   1 ,
	        1   ,    1  ,      0 ,
	        2  ,    0   ,    0;


	MatrixXu admissible(10,3);
	admissible <<  0 ,      0      ,  3,
			        0  ,     1   ,    2 ,
			        0    ,  2   ,    1,
			        0       , 3  ,     0,
			        1 ,    0  ,    2,
			        1   ,     1  ,     1,
			        1 ,    2    ,   0,
			        2 ,    0  ,   1,
			        2   ,    1   ,     0,
			        3  ,    0   ,    0;

	EXPECT_TRUE(MatrixEqual( active, adaptiveFamily->GetAllMultiIndices()));
	EXPECT_TRUE(MatrixEqual( admissible, adaptiveFamily->GetAdmissibleMultiIndices()));
}

///A simple test of a small adaptive multiIndex set, testing out the initialization with a simplex.
TEST(GeneralUtilitiesMultiIndexFamily, AdaptiveMultiIndexForcibleAdding)
{
	AdaptiveMultiIndexFamily::Ptr adaptiveFamily = AdaptiveMultiIndexFamily::Create(2,0,1);

	RowVectorXu inadmissibleTerm(2);
	inadmissibleTerm << 2 , 2;

	RowVectorXu added = adaptiveFamily->ForciblyActivateMultiIndex(inadmissibleTerm);



	//now expand that same term, which is definitely not expandable
	added = adaptiveFamily->ForciblyExpandIndex(adaptiveFamily->MultiToIndex(inadmissibleTerm));


		MatrixXu result(15,2);
		result << 0   ,     0 ,
				   1    ,   0 ,
				      2 ,      0 ,
				   0   ,  1 ,
				  1   ,   1 ,
				  2   ,    1 ,
				    0    ,   2 ,
				   1    ,    2 ,
				    2  ,     2 ,
				    0   ,   3 ,
				    1    ,    3 ,
				     2  ,    3 ,
				     3   ,    0 ,
				   3    ,    1 ,
				   3  ,    2;

 EXPECT_TRUE(MatrixEqual(result, adaptiveFamily->GetAllMultiIndices()));

}

TEST(GeneralUtilitiesMultiIndexFamily, ExpMultiIndexFamily)
{
	ExpLimitedMultiIndexFamily::Ptr indexFamily = ExpLimitedMultiIndexFamily::Create(4,6,0);


	EXPECT_EQ(1520, indexFamily->GetNumberOfIndices());
	
//  		cout << "num: " << indexFamily->GetNumberOfIndices() << endl;
//  		for(unsigned int i =0; i<indexFamily->GetNumberOfIndices(); i++)
//  			cout << indexFamily->IndexToMulti(i) << endl;
	

		RowVectorXu index0 = indexFamily->IndexToMulti(0);
		RowVectorXu index356 = indexFamily->IndexToMulti(356);
		RowVectorXu index974 = indexFamily->IndexToMulti(974);
		RowVectorXu indexLast = indexFamily->IndexToMulti(1519);

		RowVectorXu index0Correct(4);
		index0Correct <<   0 , 0 , 0 , 0;
		RowVectorXu index356Correct(4);
		index356Correct <<  0 ,  1 , 20 ,  0;
		RowVectorXu index974Correct(4);
		index974Correct << 2 ,  0 ,  0 , 14;
		RowVectorXu indexLastCorrect(4);
		indexLastCorrect <<  63 ,  0 ,  0 ,  0;

		EXPECT_TRUE(MatrixEqual( index0, index0Correct));
		EXPECT_TRUE(MatrixEqual( index356, index356Correct));
		EXPECT_TRUE(MatrixEqual( index974, index974Correct));
		EXPECT_TRUE(MatrixEqual( indexLast, indexLastCorrect));

		EXPECT_EQ(974, indexFamily->MultiToIndex(index974Correct));
}

TEST(GeneralUtilitiesMultiIndexFamily, UnstructuredMultiIndexFamily)
{
    UnstructuredMultiIndexFamily::Ptr indexFamily = UnstructuredMultiIndexFamily::Create(4,2);
    
    RowVectorXu index1(4);
    RowVectorXu index2(4);
    RowVectorXu index3(4);
    index1 << 1, 2, 3, 4;
    index2 << 2, 2, 3, 4;
    index3 << 2, 2, 2, 2;
    
    indexFamily->AddMultiIndex(index1);
    
    EXPECT_TRUE(MatrixEqual(indexFamily->IndexToMulti(0), index3));
    EXPECT_FALSE(indexFamily->IsMultiInFamily(index2));
    EXPECT_EQ(1, indexFamily->MultiToIndex(index1));
}
