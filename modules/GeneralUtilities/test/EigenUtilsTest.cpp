//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "gtest/gtest.h"

#include <iostream>
#include <fstream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "MUQ/GeneralUtilities/EigenUtils.h"

using namespace std;
using namespace Eigen;

TEST(GeneralUtilities, EigenSerializeDouble)
{
  MatrixXd randomMat = MatrixXd::Random(5,10);
  
  string fullString = "results/tests/EigenSerializeTest.dat";
  ofstream ofs(fullString.c_str());
	// save data to archive
	{
		boost::archive::text_oarchive oa(ofs);
		// write class instance to archive
		oa << randomMat;
		// archive and stream closed when destructors are called
	}
	
	
	MatrixXd recoveredMat;
	ifstream ifs(fullString.c_str());
	// load data to archive
	{
		boost::archive::text_iarchive oa(ifs);
		// write class instance to archive
		oa >> recoveredMat;
		// archive and stream closed when destructors are called
	}
	EXPECT_TRUE(muq::GeneralUtilities::MatrixApproxEqual(randomMat, recoveredMat, 1e-14));
}

TEST(GeneralUtilities, EigenSerializeUnsigned)
{
  MatrixXu testMat(3,2);
  testMat << 1,2,3,4,5,6;
  
  string fullString = "results/tests/EigenSerializeUTest.dat";
  ofstream ofs(fullString.c_str());
	// save data to archive
	{
		boost::archive::text_oarchive oa(ofs);
		// write class instance to archive
		oa << testMat;
		// archive and stream closed when destructors are called
	}
	
	
	MatrixXu recoveredMat;
	ifstream ifs(fullString.c_str());
	// load data to archive
	{
		boost::archive::text_iarchive oa(ifs);
		// write class instance to archive
		oa >> recoveredMat;
		// archive and stream closed when destructors are called
	}
	EXPECT_TRUE(muq::GeneralUtilities::MatrixEqual(testMat, recoveredMat));
}