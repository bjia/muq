/*MIT UQ Library
 * Copyright (C) 2012 Patrick R. Conrad
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.*/
/*
 * 
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/PointWrapper.h"

#include <iostream>

#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>


#include "MUQ/GeneralUtilities/EigenUtils.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

PointWrapper::PointWrapper() {
  
  
}

PointWrapper::PointWrapper(VectorXd const& point)
{
  //copy it locally
  this->point = point;
}

PointWrapper::~PointWrapper() {
  
}

const double PointWrapper::tolerance = 1e-10;

bool PointWrapper::operator==(const PointWrapper& b) const
{
  return MatrixApproxEqual(this->point, b.point, this->tolerance);
}

bool PointWrapper::operator<(const PointWrapper& b) const
{
  assert(this->point.rows() == b.point.rows());
  
  //make sure they aren't equal first, sort of inefficient to handle this way, but simple
  if(*this == b)
  {
    return false;
  }
  //then check the coordinates in order for something that's <
    for(unsigned int i=0; i<this->point.rows(); i++) //check each in order
    {
      if(this->point(i) < b.point(i)-tolerance) //if definitely less, return true
      {
	return true;

      }
      else if(this->point(i) > b.point(i)+tolerance) //if definitely greater, return true
      {
	return false;
      }

	//keep looking only if they're "equal"

  }

  return false;
}

template<class Archive>
void PointWrapper::serialize(Archive & ar, const unsigned int version)
{
	ar & point;
}


template void PointWrapper::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void PointWrapper::serialize(boost::archive::text_iarchive & ar, const unsigned int version);


template<class Archive>
void PointComp::serialize(Archive & ar, const unsigned int version)
{
}

//BOOST_CLASS_EXPORT(muq::GeneralUtilities::PointComp)

template void PointComp::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void PointComp::serialize(boost::archive::text_iarchive & ar, const unsigned int version);

