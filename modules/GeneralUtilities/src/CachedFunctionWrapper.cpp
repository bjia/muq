/*MIT UQ Library
Copyright (C) 2012 Patrick R. Conrad

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.*/
/*
 * CachedFunctionWrapper.cpp
 *
 *  Created on: Feb 1, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/CachedFunctionWrapper.h"

#include <assert.h>
#include <fstream>
#include <iostream>
#include <map>
#include <set>

#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include <Eigen/Core>

#include "MUQ/GeneralUtilities/FunctionWrapper.h"
#include "MUQ/GeneralUtilities/PointWrapper.h"
#include "MUQ/GeneralUtilities/LogConfig.h"
#include "MUQ/GeneralUtilities/EigenUtils.h"

using namespace std;
using namespace boost;
using namespace Eigen;
using namespace muq::GeneralUtilities;

CachedFunctionWrapper::CachedFunctionWrapper()
{
}


CachedFunctionWrapper::CachedFunctionWrapper(boost::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> fn,
        unsigned int inputDim,
        unsigned int outputDim):
        FunctionWrapper(fn, inputDim, outputDim)
{


}

CachedFunctionWrapper::CachedFunctionWrapper(unsigned int inputDim,
        unsigned int outputDim):
        FunctionWrapper(inputDim, outputDim)
{


}

CachedFunctionWrapper::Ptr CachedFunctionWrapper::WrapVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> vectorFn, unsigned const inputDim, unsigned const outputDim)
{
  using namespace std::placeholders;
  
  //use ApplyVectorFunction to create a batch version of this function, leave the rest to the other construct
  return WrapBatchVectorFunction(bind(ApplyVectorFunction, vectorFn, outputDim, _1), inputDim, outputDim);
}

CachedFunctionWrapper::Ptr CachedFunctionWrapper::WrapBatchVectorFunction(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchVectorFn, unsigned const inputDim, unsigned const outputDim)
{
  return CachedFunctionWrapper::Ptr(new CachedFunctionWrapper(batchVectorFn, inputDim, outputDim));
}

CachedFunctionWrapper::Ptr CachedFunctionWrapper::WrapExecutableFunction(std::string const& executable, unsigned const inputDim, unsigned const outputDim)
{
    using namespace std::placeholders;
  
  //bind the executable string into ApplyExecutableFunction to make a batch function
  return WrapBatchVectorFunction(bind(ApplyExecutableFunction, executable, outputDim, _1), inputDim, outputDim);
}


Eigen::MatrixXd CachedFunctionWrapper::Evaluate(Eigen::MatrixXd const& x) 
{
  assert(inputDim == x.rows()); //check input dim
  
 //First, collect elements not in the cache
 set<unsigned> newInputs;
 
 //find them
 for(unsigned int i=0; i<x.cols(); ++i)
 {
   if(!IsEntryInCache(x.col(i)))
   {
     newInputs.insert(i);
   }
 }
 
 
 //now put them into a matrix
 MatrixXd allNewPoints(inputDim, newInputs.size());
 unsigned j=0;
 for(auto i : newInputs)
 {
   allNewPoints.col(j) = x.col(i);
   ++j;
 }

  //if there were points not in the cache
  if(allNewPoints.cols() != 0)
  {
    //go evaluate these new points and put them into the cache
    ExtendCache(allNewPoints);
  }

  //Now collect all of them from the cache
  MatrixXd result(outputDim, x.cols());
  for(unsigned i=0; i<x.cols(); ++i)
  {
    result.col(i) = FetchCachedResult(x.col(i));
  }

  return result;
}

void CachedFunctionWrapper::ExtendCache(Eigen::MatrixXd const& newPoints)
{
  //compute the results with the parent methd
  MatrixXd results = FunctionWrapper::Evaluate(newPoints);
  
  //put them in the cache
  for(unsigned i=0; i<newPoints.cols(); ++i)
  {
    AddEntryToCache(newPoints.col(i), results.col(i));
  }
}


bool CachedFunctionWrapper::IsEntryInCache(VectorXd const& pointToTest) const
{
   PointWrapper point(pointToTest);

    //check the cache for the point
    map<PointWrapper,VectorXd>::const_iterator it = this->fnEvalCache.find(point);
    
    return (it != this->fnEvalCache.end());
}

void CachedFunctionWrapper::AddEntryToCache(VectorXd const& point, VectorXd const& result)
{
      PointWrapper pointWrapper(point);
       this->fnEvalCache[pointWrapper] = result;
}

Eigen::VectorXd CachedFunctionWrapper::FetchCachedResult(Eigen::VectorXd const& point) const
{
 PointWrapper pointWrapper(point);
 
    //check the cache for the point
    map<PointWrapper,VectorXd>::const_iterator it = this->fnEvalCache.find(point);
 
    //check it's there
    assert (it != this->fnEvalCache.end());
    
    //return it
    return it->second;
}


unsigned int CachedFunctionWrapper::GetNumOfEvals() const
{
    return fnEvalCache.size();
}

void CachedFunctionWrapper::ClearCache()
{
    fnEvalCache.clear();
    ResetEvalsCount();
}

void CachedFunctionWrapper::SaveCache(string filename) const
{
    ofstream os;
    os.open(filename.c_str());
    
    if(!os.good())
    {
      LOG(ERROR) << "Bad filename. Falling back to \"DataCache.dat\"";
     os.open("DataCache.dat");
    }
    assert(os.good()); //file must be good to write to.
    
    std::map<PointWrapper, VectorXd, PointComp>::const_iterator it;
    os.precision(16); //use full precision
    os.setf(ios::fixed);

    for (it = fnEvalCache.begin(); it != fnEvalCache.end(); it++)
    {
      VectorXd aPoint = it->first.point;
      
      //output the point
      for(unsigned int i=0; i<aPoint.rows(); ++i)
      {
	os << aPoint(i) << " ";
      }
      
      //output the result
      for(unsigned int i=0; i<it->second.rows(); ++i)
      {
	os << it->second(i) << " ";
      }
      
      os << endl;
    }

    os.close();
}

void CachedFunctionWrapper::LoadCache(std::string filename)
{
   //load the data
    MatrixXd inputData;
    
    ifstream inFile(filename.c_str());
    assert(inFile.good()); //check that the file is good
    
    string oneLine;
    //read all the lines
    bool firstLine = true;
    unsigned int perLine =0;
    char_separator<char> sep(", \t"); //set up the separator
    while(getline(inFile, oneLine))
    {
      unsigned numThisLine =0;
      
      //look at the data once to count the number
      tokenizer< char_separator<char> > tokens(oneLine, sep);
      for(tokenizer<char_separator<char> >::iterator beg=tokens.begin(); beg!=tokens.end();++beg) {
       ++numThisLine;
      }
      
      if(firstLine)
      {
	assert(numThisLine); //if zero, there's no data
	perLine = numThisLine;
	assert(perLine == inputDim + outputDim); //check that it matches what we were told
	
      }
      else
      {
	//if none, we've reached the end of the data
	if(!numThisLine)
	{
	  break;
	}
	else
	{
	  assert(numThisLine == perLine); //check that there's the right number on this line
	}
      }
      
      //now we may actually read this line
      RowVectorXd aRow(perLine);
      unsigned int i=0;
      //loop over the tokens, casting them to doubles
       BOOST_FOREACH (const string& t, tokens) {

        aRow(i) = lexical_cast<double>(t);
	++i;
      }

      //add a row and put the new data in it
      inputData.conservativeResize(inputData.rows()+1, perLine);
      inputData.row(inputData.rows()-1) = aRow;

      firstLine = false;
    }
    
    //loop over all the data
    for(unsigned int i=0; i<inputData.rows(); i++)
    {
	//put the input point in a PointWrapper, transposing the rowvec into colvec
	PointWrapper point(inputData.row(i).head(inputDim).transpose());
    
	//Store the output
	this->fnEvalCache[point] = inputData.row(i).tail(outputDim).transpose();	
    }
}

MatrixXd CachedFunctionWrapper::GetCachePoints() const
{
  
  MatrixXd result;
  result.setZero(inputDim, fnEvalCache.size());
     
      std::map<PointWrapper, Eigen::VectorXd, PointComp>::const_iterator it;

      //iterate over all the values and grab the points
      unsigned int i=0;
  for (it = fnEvalCache.begin(); it != fnEvalCache.end(); it++, ++i)
  {
    result.col(i) = it->first.point;
  }
    
    return result;
}

template<class Archive>
void CachedFunctionWrapper::serialize(Archive & ar, const unsigned int version)
{
  ar & boost::serialization::base_object<FunctionWrapper>(*this);
  ar & fnEvalCache;
}

BOOST_CLASS_EXPORT_IMPLEMENT(muq::GeneralUtilities::CachedFunctionWrapper)


template void CachedFunctionWrapper::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void CachedFunctionWrapper::serialize(boost::archive::text_iarchive & ar, const unsigned int version);
