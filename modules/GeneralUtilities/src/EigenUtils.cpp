/*MIT UQ Library
Copyright (C) 2012 Patrick R. Conrad

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.*/
/*
 * AmardilloUtils.cpp
 *
 *  Created on: Jan 19, 2011
 *      Author: prconrad
 */


#include "MUQ/GeneralUtilities/EigenUtils.h"

#include <functional>
#include <iostream>

#include <boost/serialization/export.hpp>
#include <boost/filesystem.hpp>
#include <Eigen/Core>

#include <glog/logging.h>

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

Eigen::VectorXu muq::GeneralUtilities::FindNonZeroInVector(Eigen::VectorXd const& input)
{
 unsigned int nonZeroCount =0;
 
 for(unsigned int i=0; i < input.rows(); ++i)
 {
   if(input(i) != 0)
   {
     ++nonZeroCount;
   }
 }
 
 VectorXu result(nonZeroCount);

 unsigned int j=0;
  for(unsigned int i=0; i< input.rows(); ++i)
 {
   if(input(i) != 0)
   {
     result(j) = i;
     ++j;
   }
 }
  
  return result;
}


Eigen::MatrixXd muq::GeneralUtilities::ApplyVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn, 
							    unsigned const outputDim, Eigen::MatrixXd const& inputs)
{
  MatrixXd result(outputDim, inputs.cols()); //set up the return matrix
  for(unsigned i=0;i<inputs.cols(); ++i)
  {
    result.col(i) = fn(inputs.col(i)); //computing and equating checks the output dimensionality
  }

  return result;
}


Eigen::MatrixXd muq::GeneralUtilities::ApplyExecutableFunction(std::string const& executable, unsigned const outputDim, Eigen::MatrixXd const& inputs)
{
  //construct the paths
  auto pointFile = boost::filesystem::unique_path("/tmp/polychaosPoints-%%%%-%%%%-%%%%-%%%%");
  auto resultFile = boost::filesystem::unique_path("/tmp/polychaosResults-%%%%-%%%%-%%%%-%%%%");
  
  
  ofstream ofs(pointFile.c_str());
  assert(ofs.good());
  for(unsigned i=0; i<inputs.rows(); ++i)
  {
    for(unsigned j=0; j<inputs.cols(); ++j)
    {
      ofs <<  setprecision(15) << inputs(i,j) << " ";
    }
    ofs << endl;
  }
  
  
  string command = executable + " " + pointFile.native() + " " + resultFile.native();
  LOG(INFO) << "Running external command: " << command;
  assert(!system(command.c_str()));
  
  ifstream ifs(resultFile.c_str());
  assert(ifs.good());
  string aNumber;
  
  MatrixXd result(outputDim, inputs.cols());
  
  for(unsigned int i=0; i<outputDim; ++i)
  {
    for(unsigned j=0; j<inputs.cols(); ++j)
    {
      ifs >> result(i,j);
    }
  }

   
   //clean up!
   boost::filesystem::remove(pointFile);
   boost::filesystem::remove(resultFile);
    
  return result;
}

namespace boost {
namespace serialization {


  // Copy for MatrixXd  *************************************
template<class Archive>
void save(Archive & ar, Eigen::MatrixXd const& mat, const unsigned int version)
{
  unsigned int rows = mat.rows();
  unsigned int cols = mat.cols();
  ar & rows;
  ar & cols;
  for(unsigned int i=0; i<mat.rows(); ++i)
  {
    for(unsigned int j=0; j<mat.cols(); ++j)
    {
      ar & mat(i,j);
    }
  }
}

template<class Archive>
void load(Archive & ar, Eigen::MatrixXd& mat, const unsigned int version)
{
  unsigned int numRows=0, numCols=0;
  ar & numRows;
  ar & numCols;
    
  mat.resize(numRows, numCols); // resize the derived object
  for(unsigned int i=0; i<mat.rows(); ++i)
  {
    for(unsigned int j=0; j<mat.cols(); ++j)
    {
      ar & mat(i,j);
    }
  }
}

template<class Archive>
 void serialize(
		Archive & ar,
		Eigen::MatrixXd& mat,
		const unsigned int file_version
){
	split_free(ar, mat, file_version);
}

// Copy for MatrixXd  *************************************
template<class Archive>
void save(Archive & ar, Eigen::MatrixXu const& mat, const unsigned int version)
{
  unsigned int rows = mat.rows();
  unsigned int cols = mat.cols();
  ar & rows;
  ar & cols;
  for(unsigned int i=0; i<mat.rows(); ++i)
  {
    for(unsigned int j=0; j<mat.cols(); ++j)
    {
      ar & mat(i,j);
    }
  }
}

template<class Archive>
void load(Archive & ar, Eigen::MatrixXu& mat, const unsigned int version)
{
  unsigned int numRows=0, numCols=0;
  ar & numRows;
  ar & numCols;
    
  mat.resize(numRows, numCols); // resize the derived object
  for(unsigned int i=0; i<mat.rows(); ++i)
  {
    for(unsigned int j=0; j<mat.cols(); ++j)
    {
      ar & mat(i,j);
    }
  }
}

template<class Archive>
 void serialize(
		Archive & ar,
		Eigen::MatrixXu& mat,
		const unsigned int file_version
){
	split_free(ar, mat, file_version);
}

// Copy for RowVectorXu  *************************************
template<class Archive>
void save(Archive & ar, Eigen::RowVectorXu const& vec, const unsigned int version)
{

  unsigned int cols = vec.cols();
  ar & cols;
  for(unsigned int i=0; i<vec.cols(); ++i)
  {
    ar & vec(i);
  }
}

template<class Archive>
void load(Archive & ar, Eigen::RowVectorXu& vec, const unsigned int version)
{
  unsigned int numCols=0;
  ar & numCols;
    
  vec.resize(numCols); // resize the derived object
  for(unsigned int i=0; i<vec.cols(); ++i)
  {
      ar & vec(i);
  }
}

template<class Archive>
 void serialize(
		Archive & ar,
		Eigen::RowVectorXu& vec,
		const unsigned int file_version
){
	split_free(ar, vec, file_version);
}

// Copy for VectorXd  *************************************


} // namespace serialization
} // namespace boost


BOOST_CLASS_EXPORT_IMPLEMENT(Eigen::MatrixXd)
BOOST_CLASS_EXPORT_IMPLEMENT(Eigen::MatrixXu)
BOOST_CLASS_EXPORT_IMPLEMENT(Eigen::RowVectorXu)
BOOST_CLASS_EXPORT_IMPLEMENT(Eigen::VectorXd)

template void boost::serialization::serialize(boost::archive::text_oarchive & ar,Eigen::MatrixXd& mat,  const unsigned int version);
template void boost::serialization::serialize(boost::archive::text_iarchive & ar,Eigen::MatrixXd& mat,  const unsigned int version);

template void boost::serialization::serialize(boost::archive::text_oarchive & ar,Eigen::MatrixXu& mat,  const unsigned int version);
template void boost::serialization::serialize(boost::archive::text_iarchive & ar,Eigen::MatrixXu& mat,  const unsigned int version);

template void boost::serialization::serialize(boost::archive::text_oarchive & ar, Eigen::RowVectorXu & vec, const unsigned int version);
template void boost::serialization::serialize(boost::archive::text_iarchive & ar, Eigen::RowVectorXu & vec, const unsigned int version);

template void boost::serialization::serialize(boost::archive::text_oarchive & ar,Eigen::VectorXd & vec,  const unsigned int version);
template void boost::serialization::serialize(boost::archive::text_iarchive & ar,Eigen::VectorXd & vec,  const unsigned int version);
