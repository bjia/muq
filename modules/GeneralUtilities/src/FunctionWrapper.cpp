/*MIT UQ Library
Copyright (C) 2012 Patrick R. Conrad

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.*/
/*
 * FunctionWrapper.cpp
 *
 *  Created on: Jan 31, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/FunctionWrapper.h"

#include <assert.h>
#include <iostream>
#include <fstream>
#include <functional>

#include <Eigen/Core>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/export.hpp>

#include <glog/logging.h>

#include "MUQ/GeneralUtilities/EigenUtils.h"

using namespace Eigen;
using namespace std;
using namespace muq::GeneralUtilities;

FunctionWrapper::Ptr FunctionWrapper::WrapVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> vectorFn, unsigned const inputDim, unsigned const outputDim)
{
  using namespace std::placeholders;
  
  //use ApplyVectorFunction to create a batch version of this function, leave the rest to the other construct
  return WrapBatchVectorFunction(bind(ApplyVectorFunction, vectorFn, outputDim, _1), inputDim, outputDim);
  //return WrapBatchVectorFunction([](Eigen::MatrixXd const& input){LOG(INFO) << "a" << input; return input;}, inputDim, outputDim);
}

FunctionWrapper::Ptr FunctionWrapper::WrapBatchVectorFunction(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchVectorFn, unsigned const inputDim, unsigned const outputDim)
{
  return FunctionWrapper::Ptr(new FunctionWrapper(batchVectorFn, inputDim, outputDim));
}

FunctionWrapper::Ptr FunctionWrapper::WrapExecutableFunction(std::string const& executable, unsigned const inputDim, unsigned const outputDim)
{
    using namespace std::placeholders;
  
  //bind the executable string into ApplyExecutableFunction to make a batch function
  return WrapBatchVectorFunction(bind(ApplyExecutableFunction, executable, outputDim, _1), inputDim, outputDim);
}


FunctionWrapper::FunctionWrapper()
{
  
}

FunctionWrapper::FunctionWrapper(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchFn, 
		      unsigned int inputDim, unsigned int outputDim):
		inputDim(inputDim), outputDim(outputDim), batchFn(batchFn), numEvals(0)
{

  
}


FunctionWrapper::FunctionWrapper(unsigned int inputDim, unsigned int outputDim):
  inputDim(inputDim), outputDim(outputDim), numEvals(0)
{
  
}

FunctionWrapper::~FunctionWrapper() {

}

void FunctionWrapper::SetVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn)
{
  using namespace std::placeholders;
  

  //bind the function
  batchFn = bind(ApplyVectorFunction, fn, outputDim, _1);
  
}
 
void FunctionWrapper::SetBatchVectorFunction(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> fn)
{
  batchFn = fn;
}

void FunctionWrapper::SetExecutableFunction(std::string const& executable)
{
}
      
      
      
Eigen::MatrixXd FunctionWrapper::Evaluate(Eigen::MatrixXd const& x)
{

  assert(batchFn); //assert that the function has been initialized
  assert(x.rows() == inputDim); //check the dim of input vectors

  //count the new evaluations
  numEvals += x.cols();

  //compute the batch of values
  Eigen::MatrixXd result = batchFn(x);

    assert(result.rows() == outputDim); //check the output dim

    return result; //return the result
}




unsigned int FunctionWrapper::GetNumOfEvals() const
{
	return numEvals;
}

void FunctionWrapper::ResetEvalsCount()
{
	numEvals = 0;
}


template<class Archive>
void FunctionWrapper::serialize(Archive & ar, const unsigned int version)
{
  ar & inputDim;
  ar & outputDim;
  ar & numEvals;
}

BOOST_CLASS_EXPORT_IMPLEMENT(muq::GeneralUtilities::FunctionWrapper)

template void FunctionWrapper::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void FunctionWrapper::serialize(boost::archive::text_iarchive & ar, const unsigned int version);
