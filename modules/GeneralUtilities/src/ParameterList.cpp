//MIT UQ Library
//Copyright (C) 2012 Matt Parno
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "MUQ/GeneralUtilities/ParameterList.h"

#include <iostream>
#include <fstream>

#include <boost/property_tree/xml_parser.hpp>


using namespace muq::GeneralUtilities;

using boost::property_tree::ptree;

/// Construct a list from and xml file
ParameterList::ParameterList(const std::string &XmlFile){
    
    // call the set from file function to read the file
    SetFromFile(XmlFile);
}


/// Construct list from another list
ParameterList::ParameterList(const ParameterList &input){
    
    // copy the property tree from the other list
    pt = input.pt;
    
}

/// set parameter list from xml input
void ParameterList::SetFromFile(const std::string &XmlFile){
    
    // check to make sure the file exists
    std::ifstream ifile(XmlFile.c_str());
    if(ifile.good()) {
        ifile.close();
    
        // read the file
        read_xml(XmlFile, pt);
    }else{
        std::cerr << "\nWARNING: Not able to open  " << XmlFile << "  please make sure the file exists.  This may cause the program to crash.\n\n";
    }
    
}

/// write parameter list to an xml file
void ParameterList::WriteToFile(const std::string &XmlFile){
    
    write_xml(XmlFile,pt);
    
}


// specializations of the set template
double ParameterList::Get(std::string name, double DefVal){
    
    return pt.get(name,DefVal);
}
int ParameterList::Get(std::string name, int DefVal){
    
    return pt.get(name,DefVal);
}
std::string ParameterList::Get(std::string name, std::string DefVal){
    
    return pt.get(name,DefVal);
}


// specializations of the set template
void ParameterList::Set(std::string name, const double &value){
    pt.put(name,value);
}
void ParameterList::Set(std::string name, const int &value){
    pt.put(name,value);
}
void ParameterList::Set(std::string name, const std::string &value){
    pt.put(name,value);
}


