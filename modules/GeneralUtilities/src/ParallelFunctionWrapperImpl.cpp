//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <vector>

#include <boost/function.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/status.hpp>
#include <boost/mpi/request.hpp>
#include <boost/mpi/nonblocking.hpp>
#include <boost/serialization/utility.hpp>

#include <glog/logging.h>

#include <MUQ/GeneralUtilities/EigenUtils.h>
#include <MUQ/GeneralUtilities/ParallelFunctionWrapper.h>

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;
namespace mpi = boost::mpi;

namespace muq{
  namespace GeneralUtilities{
struct ParallelTags
{
 static const int requestWork = 0; 
 static const int issueWork = 1; 
 static const int returnResults = 2; 
 static const int doneWithBatch = 3;
 static const int quitNow = 4;
};


    
void CloseWorkersImpl()
{
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);
  
  for(int i=1; i<worldComm->size();++i)
  {
    LOG(INFO) <<worldComm->rank() << "Telling worker " << i << " to quit.";
    VectorXd sizeZeroVec;
    worldComm->send(i, ParallelTags::quitNow, 0); //send the quit message
    worldComm->send(i, ParallelTags::issueWork, sizeZeroVec); //then get it to loop
    LOG(INFO) <<worldComm->rank() << "Quit received.";
  } 
}


void InitializeWorkersImpl(boost::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> fn)
{
    unique_ptr<mpi::communicator> worldComm(new mpi::communicator);
  LOG(INFO) << "Initializing workers." << worldComm->rank();
  //don't want the base one to become a worker
  if(worldComm->rank() == 0)
  {
    LOG(INFO) << worldComm->rank() <<"Primary process exiting.";
    return;    
  }
  
  LOG(INFO) << worldComm->rank() <<"Trapping workers.";
  
      
      boost::optional<mpi::status> probeReturn;
      
  //loop until you get a quit message
  while(!(probeReturn = worldComm->iprobe(0, ParallelTags::quitNow)))
  {
    //ask for new work
     LOG(INFO) << worldComm->rank() <<"Asking for work.";
    worldComm->send(0, ParallelTags::requestWork, 0);
    
    LOG(INFO) <<worldComm->rank() << "Done.";
    VectorXd input; //create someplace for the requested vector to go
    
   
    LOG(INFO) << worldComm->rank() <<"Waiting";
    worldComm->recv(0,ParallelTags::issueWork, input);

     
    if(input.rows()>0) //check that actual work was assigned
    {
      LOG(INFO) <<worldComm->rank() << "Computing for " << input;
      
      //Since the request was to do work, let's begin
      
      //compute the vector result
      VectorXd resultVec = fn(input);
    
      
      //create the pair with both
      std::pair<Eigen::VectorXd, Eigen::VectorXd> resultPair{input,resultVec};
    
      LOG(INFO) <<worldComm->rank() << "Done.";
      //return the result, blocking until it is sent so the data doesn't vanish
      worldComm->send(0, ParallelTags::returnResults, resultPair);  
      LOG(INFO) <<worldComm->rank() << "Results sent.";
    }
    else
    {
        LOG(INFO) <<worldComm->rank() << "No size, skipping.";
    }
  }
  LOG(INFO) <<worldComm->rank()  << "Finished being a worker.";
}


void ExtendCacheImpl(MatrixXd const& x, ParallelFunctionWrapper &wrapper)
{
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);
  LOG(INFO) << "Beginning parallel extend cache.";
  
  //make sure the workers didn't slip in, may mean InitializeWorkers wasn't called.
  assert(worldComm->rank() == 0); 
  //make sure there's more than one process
  assert(worldComm->size() > 1); 
  
  //we're going to explicitly keep track of how much work we've given out
  vector<int> dispatchedWork(worldComm->size()-1);

  
  typedef vector<mpi::request> RequestVector;
  //create a vector for all the remote processes
  RequestVector readyForWorkRequests(worldComm->size()-1);

  int trashBuffer=0;
   
  VectorXd sizeZeroVec;
  
  //create a request for workers that want work to do
  for(int i=0; i<worldComm->size()-1;++i)
  {
    readyForWorkRequests.at(i) = worldComm->irecv(i+1, ParallelTags::requestWork, trashBuffer);
    dispatchedWork.at(i);
  }
  
  LOG(INFO) <<worldComm->rank() << "Work request polls created.";
  
  //we have to resolve each of the column evaluations
  for(unsigned i=0; i<x.cols(); ++i)
  {
   
    LOG(INFO) <<worldComm->rank() << "Finding a worker.";
 
    //We need to find a worker to send it to.
    //Wait for one of the workers to be done
   std::pair<mpi::status, RequestVector::iterator> firstResponse = 
    mpi::wait_any(readyForWorkRequests.begin(), readyForWorkRequests.end());
    
    //Make a copy into a colvec - helps resolve through all the templates
    VectorXd workToIssue = x.col(i);
    
    LOG(INFO) <<worldComm->rank() << "Found worker " << firstResponse.first.source();
    
    ++(dispatchedWork.at(firstResponse.first.source()-1));
    //reply with this vector of work to do, don't need to wait for
    worldComm->isend(firstResponse.first.source(), ParallelTags::issueWork, workToIssue);
  
    //Also set up another request for when this worker is ready for more work
    readyForWorkRequests.at(firstResponse.first.source()-1) =  
      worldComm->irecv(firstResponse.first.source(), ParallelTags::requestWork, trashBuffer);
          LOG(INFO) <<worldComm->rank() << "Work sent " << firstResponse.first.source();
  }
  
  
  //cancel all the waiting requests
  for(auto it : readyForWorkRequests)
  {
    it.cancel();
  }
  
  
  //tell all the workers we're done with this batch
  for(int i=0; i<worldComm->size()-1;++i)
  {
   worldComm->send(i+1, ParallelTags::issueWork, sizeZeroVec);
  }
  
    LOG(INFO) << worldComm->rank() <<"All reset.";
    
  //then empty all the worker results messages using probe/recv, adding to the cache
  for(int i=1; i<worldComm->size();++i)
  {
    LOG(INFO) << worldComm->rank() <<"Retrieving work from " << i << ", there's " << dispatchedWork.at(i-1) <<  "units.";
    
    //while there's another set of results to return

    while(((dispatchedWork.at(i-1))-- )>0) //while there's another, decrement and recieve it
    {
      LOG(INFO) <<worldComm->rank() << "Starting recv";
      std::pair<Eigen::VectorXd, Eigen::VectorXd> resultPair;
      //receive the message with the results
      worldComm->recv(i,ParallelTags::returnResults, resultPair);
      
      LOG(INFO) <<worldComm->rank() << "Got " << resultPair.first << " evals to " << resultPair.second;
      //add the computed entry to the cache
      wrapper.AddEntryToCache(resultPair.first, resultPair.second);
    }
  }
  
    LOG(INFO) <<worldComm->rank() << "All processed.";
  
  /////////////////////////////////check that they all get cancelled
  
}

  }
}