//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * FullTensorMultiIndexFamily.cpp
 *
 *  Created on: Jan 12, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/multiIndex/FullTensorMultiIndexFamily.h"

#include <assert.h>

#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"

using namespace Eigen;
using namespace muq::GeneralUtilities;

FullTensorMultiIndexFamily::FullTensorMultiIndexFamily()
{

}

FullTensorMultiIndexFamily::FullTensorMultiIndexFamily(unsigned int const length):
		MultiIndexFamily(length)
{

}

FullTensorMultiIndexFamily::Ptr FullTensorMultiIndexFamily::Create(unsigned int const length, unsigned int const order)
{
	Ptr family(new FullTensorMultiIndexFamily(length));
	//first, set the order to the right length, then multiply to get the right termwise limit
	family->order.setOnes(length);
	family->order = order *family->order;
	family->init();
	return family;
}

FullTensorMultiIndexFamily::Ptr FullTensorMultiIndexFamily::Create(unsigned int const length, RowVectorXu const& order)
{
	//check the length of the order is right
	assert(length == order.cols());

	Ptr family(new FullTensorMultiIndexFamily(length));
	//make a new copy of the termwise order limits
	family->order = RowVectorXu(order);

	family->init();
	return family;
}

FullTensorMultiIndexFamily::~FullTensorMultiIndexFamily() {

}

RowVectorXu FullTensorMultiIndexFamily::DeriveNextMultiIndex(RowVectorXu const& multiIndex)
{
	RowVectorXu newMultiIndex = multiIndex;

	//We need to find an index we can increment without violating the term-wise order limit.

	//Start by trying the first index
	unsigned int indexToChange = this->GetMultiIndexLength()-1;
	bool changedYet = false;

	//Keep searching until we find an index to change
	do{
		//If we can increment this one without violating the order
		if(newMultiIndex(indexToChange)+1 < order(indexToChange))
		{
			//do so, and note that we found one
			newMultiIndex(indexToChange)++;
			changedYet = true;
		}
		else
		{
			//Otherwise, reset it to zero, and move on to the next one.
			newMultiIndex(indexToChange) = 0;


			//If we would go off the end of the index without finding one, then we've found the last
			//one already.
			if(indexToChange == 0)
			{
				newMultiIndex.resize(0);
				break;
			}

			--indexToChange;
		}

	}while(!changedYet);

	return newMultiIndex;
}

template<class Archive>
void FullTensorMultiIndexFamily::serialize(Archive & ar, const unsigned int version)
{
	ar & boost::serialization::base_object<MultiIndexFamily>(*this);
	ar & order;
}

template void FullTensorMultiIndexFamily::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void FullTensorMultiIndexFamily::serialize(boost::archive::text_iarchive & ar, const unsigned int version);


BOOST_CLASS_EXPORT(muq::GeneralUtilities::FullTensorMultiIndexFamily)
