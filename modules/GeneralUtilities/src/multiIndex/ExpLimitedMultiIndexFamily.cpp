//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * ExpLimitedMultiIndexFamily.cpp
 *
 *  Created on: May 30, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/multiIndex/ExpLimitedMultiIndexFamily.h"

#include <iostream>


#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <Eigen/Core>

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

ExpLimitedMultiIndexFamily::ExpLimitedMultiIndexFamily()
{
}


ExpLimitedMultiIndexFamily::ExpLimitedMultiIndexFamily(unsigned int const length):
MultiIndexFamily(length) {
  
  
}

ExpLimitedMultiIndexFamily::~ExpLimitedMultiIndexFamily() {
  
}

ExpLimitedMultiIndexFamily::Ptr ExpLimitedMultiIndexFamily::Create(unsigned int const length,
								   unsigned int const maxOrder, unsigned int const baseValue)
{
  assert(length>0); //check that there's non-zero length
  Ptr family(new ExpLimitedMultiIndexFamily(length));
  family->maxOrder = maxOrder;
  family->baseValue = baseValue;
  family->init();
  return family;
}

RowVectorXu ExpLimitedMultiIndexFamily::GetFirstIndex(){
  //Start with a list of zeros the right length
  RowVectorXu firstIndex = baseValue*RowVectorXu::Ones(this->lengthOfMultiIndex);
  
  return firstIndex;
  
}


RowVectorXu ExpLimitedMultiIndexFamily::DeriveNextMultiIndex(RowVectorXu const& multiIndex)
{
  RowVectorXu newMultiIndex = multiIndex.array()+1-baseValue;
  
      unsigned int currentExp = 0;
  double oneExp = 0;
  RowVectorXd logOfNewIndex ;
  
  do{
    //compute the exponent this one works on
    logOfNewIndex = log(newMultiIndex.array().cast<double>());
    currentExp = 0;
    for(unsigned int i=0; i< newMultiIndex.cols(); ++i)
    {
      oneExp = logOfNewIndex(i)/log(2.0);
      currentExp += static_cast<unsigned int>(ceil(oneExp)); 
    }
    
    //If this one hasn't hit the limit yet, just increment first term
    if(currentExp <= this->maxOrder)
    {
      newMultiIndex(this->GetMultiIndexLength()-1)++;
    }
    else
    {
      //Otherwise, find the first non-zero element to reset to zero and increment the next one
      
      //an exception - if there's only one term in the multi-index, the following overflow doesn't work!
      //thus, test for it, and skip entirely on the first need for an overflow
      if(this->GetMultiIndexLength()==1)
      {
	newMultiIndex.resize(0);
	return newMultiIndex;
      }
      
      //Start by trying the first index
      unsigned int indexToChange = this->GetMultiIndexLength()-1;
      bool changedYet = false;
      
      //Keep searching until we find an index to change
      do{
	//If it's not one
	if(newMultiIndex(indexToChange) > 1)
	{
	  //do set it back to one, and note that we found one
	  newMultiIndex(indexToChange)=1;
	  newMultiIndex(indexToChange-1)++;
	  changedYet = true;
	}
	else
	{
	  //else, keep looking
	  --indexToChange;
	  
	  //If we go off the end of the index without finding one, then we've found the last
	  //one already.
	  if(indexToChange < 1)
	  {
	    newMultiIndex.resize(0);
	    return newMultiIndex;
	  }
	}
	
      }while(!changedYet);
    }
    
    //check the exponent again before deciding to continue
    logOfNewIndex = log(newMultiIndex.array().cast<double>());
    currentExp = 0;
    for(unsigned int i=0; i< newMultiIndex.cols(); ++i)
    {
      oneExp = logOfNewIndex(i)/log(2.0);
      currentExp += static_cast<unsigned int>(ceil(oneExp)); 
    }
    
  } while( !(currentExp <= this->maxOrder) );
  
  
  
  return newMultiIndex.array()-1+baseValue;
}

template<class Archive>
void ExpLimitedMultiIndexFamily::serialize(Archive & ar, const unsigned int version)
{
    ar & boost::serialization::base_object<MultiIndexFamily>(*this);
    ar & maxOrder;
    ar & baseValue;
}

BOOST_CLASS_EXPORT(muq::GeneralUtilities::ExpLimitedMultiIndexFamily)

template void ExpLimitedMultiIndexFamily::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void ExpLimitedMultiIndexFamily::serialize(boost::archive::text_iarchive & ar, const unsigned int version);