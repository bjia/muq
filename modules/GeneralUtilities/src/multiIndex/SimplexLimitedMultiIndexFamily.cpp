//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#include "MUQ/GeneralUtilities/multiIndex/SimplexLimitedMultiIndexFamily.h"


#include <assert.h>
#include <iostream>

#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexWrapper.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/TotalOrderMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/LogConfig.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

SimplexLimitedMultiIndexFamily::Ptr SimplexLimitedMultiIndexFamily::Create(unsigned int const length, unsigned int const simplexLimit, 
									   unsigned int const baseElement,
									   unsigned int const initSimplexLevels)
{
	SimplexLimitedMultiIndexFamily::Ptr family(new SimplexLimitedMultiIndexFamily(simplexLimit, length, baseElement));

	assert(initSimplexLevels>0);
	//use a total order family to create the simplex.
	TotalOrderMultiIndexFamily::Ptr simplex = TotalOrderMultiIndexFamily::Create(length, initSimplexLevels-1);

	//Next, copy all the elements of the simplex into this family.
	for(unsigned int i=0; i<simplex->GetNumberOfIndices(); i++)
	{
		//add the base element to convert our simplex rooted at zero into a simplex rooted at the baseElement
		family->InsertMultiIndex(simplex->IndexToMulti(i).array()+baseElement);
	}

	//Now make sure the correct forward neighbors are marked as admissible. We can just call the function
	//on all of them.
	for(unsigned int i=0; i<family->GetNumberOfIndices(); i++)
	{
		//add the base element to convert our simplex rooted at zero into a simplex rooted at the baseElement
		family->MakeFwdNeighborsAdmissible(family->IndexToMulti(i));
	}

	return family;
}


SimplexLimitedMultiIndexFamily::SimplexLimitedMultiIndexFamily()
{

}

SimplexLimitedMultiIndexFamily::SimplexLimitedMultiIndexFamily( unsigned int const simplexLimit, unsigned int const length, 
								unsigned int const baseElement): AdaptiveMultiIndexFamily(length, baseElement)
{
	this->simplexLimit = simplexLimit;
}

SimplexLimitedMultiIndexFamily::~SimplexLimitedMultiIndexFamily()
{
}

bool SimplexLimitedMultiIndexFamily::IsMultiIndexAdmissible(const RowVectorXu& multiIndex) const
{
    return (multiIndex.array().sum() <= simplexLimit) && AdaptiveMultiIndexFamily::IsMultiIndexAdmissible(multiIndex);
    
}


template<class Archive>
void SimplexLimitedMultiIndexFamily::serialize(Archive & ar, const unsigned int version)
{
	ar & boost::serialization::base_object<MultiIndexFamily>(*this);
	ar & simplexLimit;
}

BOOST_CLASS_EXPORT(muq::GeneralUtilities::SimplexLimitedMultiIndexFamily)
template void SimplexLimitedMultiIndexFamily::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void SimplexLimitedMultiIndexFamily::serialize(boost::archive::text_iarchive & ar, const unsigned int version);
