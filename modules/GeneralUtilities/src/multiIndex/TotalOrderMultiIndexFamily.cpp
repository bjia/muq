//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * TotalOrderMultiIndexFamily
 *
 *  Created on: Jan 12, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/multiIndex/TotalOrderMultiIndexFamily.h"



#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"

using namespace Eigen;
using namespace muq::GeneralUtilities;

TotalOrderMultiIndexFamily::TotalOrderMultiIndexFamily()
{

}

TotalOrderMultiIndexFamily::TotalOrderMultiIndexFamily(unsigned int const length):
		MultiIndexFamily(length)
{

}

TotalOrderMultiIndexFamily::Ptr TotalOrderMultiIndexFamily::Create(unsigned int const length,
		unsigned int const maxOrder, unsigned int const minOrder, unsigned int const baseValue)
{
	assert(length>0); //check that there's non-zero length
	Ptr family(new TotalOrderMultiIndexFamily(length));
	family->maxOrder = maxOrder;
	family->minOrder = minOrder;
	family->baseValue = baseValue;
	family->init();
	return family;
}

TotalOrderMultiIndexFamily::~TotalOrderMultiIndexFamily() {

}

RowVectorXu TotalOrderMultiIndexFamily::GetFirstIndex(){
	//Start with a list of zeros the right length
	RowVectorXu firstIndex = baseValue*RowVectorXu::Ones(this->lengthOfMultiIndex);
	//set the low order term, the last one, to the minimum order.
	firstIndex(this->lengthOfMultiIndex-1) = baseValue+this->minOrder;
	return firstIndex;

}

RowVectorXu TotalOrderMultiIndexFamily::DeriveNextMultiIndex(RowVectorXu const& multiIndex)
{
	RowVectorXu newMultiIndex = multiIndex;

	//Keep looking until you find items of at least the min order
	do{

		//If this one hasn't hit the limit yet, just increment first term
		if(newMultiIndex.array().sum()+1-baseValue*GetMultiIndexLength() <= this->maxOrder)
		{
			newMultiIndex(this->GetMultiIndexLength()-1)++;
		}
		else
		{
			//Otherwise, find the first non-zero element to reset to zero and increment the next one

			//an exception - if there's only one term in the multi-index, the following overflow doesn't work!
			//thus, test for it, and skip entirely on the first need for an overflow
			if(this->GetMultiIndexLength()==1)
			{
				newMultiIndex.resize(0);
				return newMultiIndex;
			}

			//Start by trying the first index
			unsigned int indexToChange = this->GetMultiIndexLength()-1;
			bool changedYet = false;

			//Keep searching until we find an index to change
			do{
				//If it's not zero
				if(newMultiIndex(indexToChange) > baseValue)
				{
					//do so, and note that we found one
					newMultiIndex(indexToChange)=baseValue;
					newMultiIndex(indexToChange-1)++;
					changedYet = true;
				}
				else
				{
					//else, keep looking
					--indexToChange;

					//If we go off the end of the index without finding one, then we've found the last
					//one already.
					if(indexToChange < 1)
					{
						newMultiIndex.resize(0);
						return newMultiIndex;
					}
				}

			}while(!changedYet);
		}

	}while(newMultiIndex.array().sum() < this->minOrder);

	return newMultiIndex;
}

template<class Archive>
void TotalOrderMultiIndexFamily::serialize(Archive & ar, const unsigned int version)
{
	ar & boost::serialization::base_object<MultiIndexFamily>(*this);
	ar & maxOrder;
	ar & minOrder;
	ar & baseValue;
}

BOOST_CLASS_EXPORT(muq::GeneralUtilities::TotalOrderMultiIndexFamily)
