//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * MultiIndexWrapper.cpp
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexWrapper.h"


#include <iostream>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/functional/hash.hpp>
#include <boost/serialization/export.hpp>

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

bool MultiIndexWrapperEq::operator()(const MultiIndexWrapper& lhs, const MultiIndexWrapper& rhs) const
{
    if(lhs.cols() != rhs.cols()) //check the length first
	{
	    return false;
	}
	
	for(unsigned int i=0; i<lhs.cols(); i++) //check each element
	{
	    if(lhs(i) != rhs(i))
	    {
		return false;
	    }
	}
	
	return true; //no problems found, so return true!
	
}

namespace Eigen
{
    std::size_t hash_value(const RowVectorXu& b)
    {
      std::size_t seed = 0;
      
      for(unsigned int i=0; i<b.cols(); ++i)
      {
        boost::hash_combine(seed, b(i));
      }
	
	return seed;
    }
}

bool MultiIndexWrapperLess::operator()(const MultiIndexWrapper& lhs, const MultiIndexWrapper& rhs) const
{
		assert(lhs.cols() == rhs.cols());
		for(unsigned int i=0; i<lhs.cols(); i++) //check each in order
		{
			if(lhs(i) < rhs(i)) //if less, return true
			{
				return true;

			}
			else if(lhs(i) > rhs(i)) //if greater, return true
			{
				return false;
			}
			//keep looking only if they're equal

		}
		return false;
}

