//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * AdaptiveMultiIndexFamily.cpp
 *
 *  Created on: Jan 20, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/multiIndex/AdaptiveMultiIndexFamily.h"

#include <assert.h>

#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <Eigen/Core>

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexWrapper.h"
#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"
#include "MUQ/GeneralUtilities/multiIndex/TotalOrderMultiIndexFamily.h"
#include "MUQ/GeneralUtilities/LogConfig.h"


using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

AdaptiveMultiIndexFamily::Ptr AdaptiveMultiIndexFamily::Create(unsigned int const length,
							       unsigned int const baseElement, unsigned int const initSimplexLevels)
{
  Ptr family(new AdaptiveMultiIndexFamily(length, baseElement));
  
  assert(initSimplexLevels>0);
  //use a total order family to create the simplex.
  TotalOrderMultiIndexFamily::Ptr simplex = TotalOrderMultiIndexFamily::Create(length, initSimplexLevels-1);
  
  //Next, copy all the elements of the simplex into this family.
  for(unsigned int i=0; i<simplex->GetNumberOfIndices(); i++)
  {
    //add the base element to convert our simplex rooted at zero into a simplex rooted at the baseElement
    family->InsertMultiIndex((simplex->IndexToMulti(i).array()+baseElement).matrix());
  }
  
  //Now make sure the correct forward neighbors are marked as admissible. We can just call the function
  //on all of them.
  for(unsigned int i=0; i<family->GetNumberOfIndices(); i++)
  {
    //add the base element to convert our simplex rooted at zero into a simplex rooted at the baseElement
    family->MakeFwdNeighborsAdmissible(family->IndexToMulti(i));
  }
  
  return family;
}


AdaptiveMultiIndexFamily::AdaptiveMultiIndexFamily()
{
  
}

AdaptiveMultiIndexFamily::AdaptiveMultiIndexFamily(unsigned int const length, unsigned int const baseElement):
MultiIndexFamily(length)
{
  this->baseElement = baseElement;
}

AdaptiveMultiIndexFamily::~AdaptiveMultiIndexFamily()
{
}

///Fill in the cache of multi-indicies.
void AdaptiveMultiIndexFamily::init()
{
  
  RowVectorXu multiIndex = this->GetFirstIndex(); //get the first multiindex
  InsertMultiIndex(multiIndex);
  
  //Make any neighbors of this first one
  this->MakeFwdNeighborsAdmissible(multiIndex);
}

bool AdaptiveMultiIndexFamily::IsMultiIndexAdmissible(const RowVectorXu& multiIndex) const
{
  assert(this->GetMultiIndexLength() == multiIndex.cols()); //check input
  /*
   * First, check the active and admissible sets. Since the active set is non-decreasing
   * determining it is admissible once suffices forever.
   */
  //recall that you check a set by testing if find() returns the end() iterator
  if(this->IsMultiInFamily(multiIndex) ||
    this->admissibleSet.find(MultiIndexWrapper(multiIndex)) != this->admissibleSet.end())
  {
    return true;
  }
  
  /*
   * Else, derive whether this one is admissible.
   */
  
  //a multiIndex is admissible if all it's backward neighbors are in the set
  for(unsigned int i=0; i< this->neighborCache.rows(); i++)
  {
    //If any of the coordinates of this backward neighbor are negative, then
    //it doesn't matter to the test, because it's meaningless, so skip it.
    //Since everything is unsigned, we can't trust the subtraction first,
    //so check the addition first
    if((multiIndex.array() <baseElement+this->neighborCache.row(i).array()).any())
    {
      continue;
    }
    
    //compute the ith neighbor by subtracting, which we now know is safe
    RowVectorXu iNeighbor = multiIndex-this->neighborCache.row(i);
    
    if(!this->IsMultiInFamily(iNeighbor)) //if it's not in the family, ie in the active set
    {
      return false; //return false
    }
  }
  
  //if they all pass
  return true;
}

RowVectorXu AdaptiveMultiIndexFamily::GetActiveBackwardNeighborIndices(unsigned int const index) const
{
  RowVectorXu result; //space for the indices of the backward neighbors
  RowVectorXu multiIndex = IndexToMulti(index); //get the multi index
  
  for(unsigned int i=0; i< this->neighborCache.rows(); i++)
  {
    //If any of the coordinates of this backward neighbor are negative, then
    //it doesn't matter to the test, because it's meaningless, so skip it.
    //Since everything is unsigned, we can't trust the subtraction first,
    //so check the addition first
    if((multiIndex.array() <baseElement+this->neighborCache.row(i).array()).any())
    {
      continue;
    }
    
    //compute the ith neighbor by subtracting, which we now know is safe
    RowVectorXu iNeighbor = multiIndex-this->neighborCache.row(i);
    
    if(this->IsMultiInFamily(iNeighbor)) //if it's not in the family, ie in the active set
		{
		  //make space, add one row
		  if(result.cols() == 0)
		  {
		    result.setZero(1);
		  }
		  else
		  {
		    //expand it by one and set the new one to zero
		    result.conservativeResize(result.cols()+1);
		    result(result.cols()-1) = 0;
		  }
		  
		  //find the index of the neighbor and store it in the new spot
		    result(result.cols()-1) = MultiToIndex(iNeighbor);
		}
  }
  
  return result;
}

void AdaptiveMultiIndexFamily::MakeMultiIndexActive(const RowVectorXu& multiIndex)
{
  assert(this->GetMultiIndexLength() == multiIndex.cols()); //check input
  
  if(this->IsMultiInFamily(multiIndex)) //if it's already in the set, just ignore and return
	{
	  return;
	}
	
	//make sure you only add admissible indices
	assert(this->IsMultiIndexAdmissible(multiIndex));
  
  //otherwise, put it in the set
	InsertMultiIndex(multiIndex);
	
	//remove this multiIndex from the admissible set, as it is now active
	this->admissibleSet.erase(MultiIndexWrapper(multiIndex));
	
	//try making any neighbors active
	this->MakeFwdNeighborsAdmissible(multiIndex);
}

void AdaptiveMultiIndexFamily::MakeFwdNeighborsAdmissible(const RowVectorXu& multiIndex)
{
  assert(this->GetMultiIndexLength() == multiIndex.cols()); //check input
  
  BOOST_LOG_POLY("multiIndex",debug) << "Making fwd neighbors admissible: " << multiIndex;
  //loop over the neighbors
  for(unsigned int i=0; i< this->neighborCache.rows(); i++)
  {
    //compute the ith forward neighbor by adding
    RowVectorXu iNeighbor = multiIndex+this->neighborCache.row(i);
    
    //		BOOST_LOG_POLY("multiIndex",debug) << "Considering neighbor: " << iNeighbor;
    
    //The admissibleSet is supposed to contain admissible but not active multiIndices.
    //Therefore, if this neighbor is not in the family, but is admissible, add it to the set.
    if(!IsMultiInFamily(iNeighbor) && IsMultiIndexAdmissible(iNeighbor))
    {
      //			BOOST_LOG_POLY("multiIndex",debug) << "Adding.";
      this->admissibleSet.insert(iNeighbor);
    }
  }
}

MatrixXu AdaptiveMultiIndexFamily::GetAdmissibleMultiIndices() const
{
  MatrixXu admissibleIndices = MatrixXu::Zero(this->admissibleSet.size(), this->GetMultiIndexLength());
  std::set<MultiIndexWrapper, MultiIndexWrapperLess>::iterator it;
  
  unsigned int i=0;
  for ( it=admissibleSet.begin() ; it != admissibleSet.end(); ++it )
  {
    admissibleIndices.row(i) = *it;
    ++i;
  }
  return admissibleIndices;
}

unsigned int AdaptiveMultiIndexFamily::DeriveNumberOfIndices()
{
  BOOST_LOG_POLY("multiIndex",error) << "Trying to derive the number of indices of adaptive indicies.";
  assert(false); //don't end up here!
  return 0;
}


RowVectorXu AdaptiveMultiIndexFamily::DeriveNextMultiIndex(const RowVectorXu& multiIndex)
{
  BOOST_LOG_POLY("multiIndex",error) << "Trying to derive the next adaptive index.";
  assert(false); //don't end up here!
  return RowVectorXu::Zero(1);
}

RowVectorXu AdaptiveMultiIndexFamily::GetFirstIndex()
{
  //just return a list of ones
  return RowVectorXu::Ones(this->lengthOfMultiIndex)*baseElement;
}



bool AdaptiveMultiIndexFamily::IsIndexExpandable(unsigned int const index) const
{
  //check that the index is valid
  assert(IsIndexInFamily(index));
  
  //get the multiIndex from the index
  RowVectorXu multiIndex = this->IndexToMulti(index);
  
  //loop over the forward neighbors
  for(unsigned int i=0; i< this->neighborCache.rows(); i++)
  {
    //compute the ith forward neighbor by adding
    RowVectorXu iNeighbor = multiIndex+this->neighborCache.row(i);
    
    //if any forward neighbor is not active and is admissible, return true
    if(!this->IsMultiInFamily(iNeighbor) && this->IsMultiIndexAdmissible(iNeighbor))
    {
      return true;
    }
  }
  return false; //else, return false
}

RowVectorXu AdaptiveMultiIndexFamily::ExpandIndex(unsigned int const index)
{
  unsigned int numberMadeActive = 0; //so far, none have been made active
  RowVectorXu activatedIndices;
  //get the multiIndex from the index
  RowVectorXu multiIndex = this->IndexToMulti(index);
  
  //loop over the forward neighbors
  for(unsigned int i=0; i< this->neighborCache.rows(); i++)
  {
    //compute the ith forward neighbor by adding
    RowVectorXu iNeighbor = multiIndex+this->neighborCache.row(i);
    
    //if this neighbor is not active already, and is admissible
    if(!this->IsMultiInFamily(iNeighbor) && this->IsMultiIndexAdmissible(iNeighbor))
    {
      this->MakeMultiIndexActive(iNeighbor); //then make it active
      numberMadeActive++; //found one more
      
      //so, expand the record by one
      if(activatedIndices.cols() == 0)
      {
	activatedIndices.setZero(1);
      }
      else
      {
	//expand it by one and set the new one to zero
	activatedIndices.conservativeResize(activatedIndices.cols()+1);
	activatedIndices(activatedIndices.cols()-1) = 0;
      }
      //and save it
      activatedIndices(numberMadeActive-1) = this->MultiToIndex(iNeighbor);
    }
  }
  
  assert(activatedIndices.rows() != 0); //something should happen
  return activatedIndices;
}


RowVectorXu AdaptiveMultiIndexFamily::ForciblyExpandIndex(unsigned int const index)
{
  
  RowVectorXu indicesAdded;
  //indicesAdded.set_size(1,0);
  
  RowVectorXu multiIndex = IndexToMulti(index);
  
  //just loop over the forward neighbors and forcibly activate them,
  //collecting the ones that get turned on
  for(unsigned int i=0; i< this->neighborCache.rows(); i++)
  {
    //compute the ith forward neighbor by adding
    RowVectorXu iNeighbor = multiIndex+this->neighborCache.row(i);
    
    //it is a reasonable multi-index, so activate it.
    RowVectorXu neighborAddedIndices = ForciblyActivateMultiIndex(iNeighbor);
    
    //concatenate these onto the list of ones we've added
    
    //if we didn't find any, continue
    if(neighborAddedIndices.cols() == 0)
      continue;
    
    if(indicesAdded.cols() == 0)
    {
      indicesAdded = neighborAddedIndices;
    }
    else
    {
      //resize and insert into the last cols
      indicesAdded.conservativeResize(indicesAdded.cols() + neighborAddedIndices.cols());
      indicesAdded.tail(neighborAddedIndices.cols())= neighborAddedIndices;
    }
    
  }
  
  return indicesAdded;
}

RowVectorXu AdaptiveMultiIndexFamily::ForciblyActivateMultiIndex(RowVectorXu const& multiIndex)
{
  RowVectorXu indicesAdded;
  
  if(IsMultiInFamily(multiIndex))
    return indicesAdded;
  
  if(!IsMultiIndexAdmissible(multiIndex))
  {
    //if it's not admissible yet, forcibly add all the backward neighbors
    for(unsigned int i=0; i< this->neighborCache.cols(); i++)
    {
      RowVectorXu neighborDelta = this->neighborCache.row(i);
      
      //If any of the coordinates of this backward neighbor are negative, then
      //it isn't valid, because it's meaningless, so skip it.
      if((multiIndex.array() <baseElement+neighborDelta.array()).any())
      {
	continue;
      }
      
      
      //compute the ith neighbor by subtracting
      RowVectorXu iNeighbor = multiIndex-neighborDelta;
      
      //it is a reasonable multi-index, so activate it.
      RowVectorXu neighborAddedIndices = ForciblyActivateMultiIndex(iNeighbor);
      
      if(neighborAddedIndices.cols() == 0)
	continue;
      
      //concatenate these onto the list of ones we've added
	if(indicesAdded.cols() == 0)
	{
	  indicesAdded = neighborAddedIndices;
	}
	else
	{
	  //resize and insert into the last cols
	  indicesAdded.conservativeResize(indicesAdded.cols() + neighborAddedIndices.cols());
	  indicesAdded.tail(neighborAddedIndices.cols())= neighborAddedIndices;
	}
    }
  }
  
  //now we can add this one
  MakeMultiIndexActive(multiIndex);
  
  RowVectorXu inputIndex(1);
  inputIndex(0) =  MultiToIndex(multiIndex);
  
  
  if(indicesAdded.cols() == 0)
  {
    indicesAdded = inputIndex;
  }
  else
  {
    //resize and insert into the last cols
    indicesAdded.conservativeResize(indicesAdded.cols() + inputIndex.cols());
    indicesAdded.tail(inputIndex.cols())= inputIndex;
  }
  
  return indicesAdded;
}



VectorXd AdaptiveMultiIndexFamily::SelectExpandable() const
{
  VectorXd result = VectorXd::Zero(GetNumberOfIndices());
  for(unsigned int i=0; i<result.rows(); i++)
  {
    if(IsIndexExpandable(i)) //if this index is not buried
    {
      result(i) = 1.0;
    }
  }
  return result;
}

template<class Archive>
void AdaptiveMultiIndexFamily::serialize(Archive & ar, const unsigned int version)
{
  ar & boost::serialization::base_object<MultiIndexFamily>(*this);
  ar & admissibleSet;
  ar & baseElement;
}

BOOST_CLASS_EXPORT(muq::GeneralUtilities::AdaptiveMultiIndexFamily)
template void AdaptiveMultiIndexFamily::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void AdaptiveMultiIndexFamily::serialize(boost::archive::text_iarchive & ar, const unsigned int version);

