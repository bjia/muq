//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "MUQ/GeneralUtilities/multiIndex/UnstructuredMultiIndexFamily.h"

#include <assert.h>

#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "MUQ/GeneralUtilities/LogConfig.h"

using namespace Eigen;
using namespace muq::GeneralUtilities;

UnstructuredMultiIndexFamily::UnstructuredMultiIndexFamily()
{

}

UnstructuredMultiIndexFamily::~UnstructuredMultiIndexFamily()
{

}

UnstructuredMultiIndexFamily::Ptr UnstructuredMultiIndexFamily::Create(unsigned int const length,
							       unsigned int const baseElement)
{
    //only initialize with the first element
    UnstructuredMultiIndexFamily::Ptr family(new UnstructuredMultiIndexFamily(length, baseElement));
        
    family->init();
    return family;
}

UnstructuredMultiIndexFamily::UnstructuredMultiIndexFamily(unsigned int const length, unsigned int const baseElement):
MultiIndexFamily(length)
{
    this->baseElement = baseElement;
}


void UnstructuredMultiIndexFamily::init()
{
    
    RowVectorXu multiIndex = GetFirstIndex(); //get the first multiindex
    InsertMultiIndex(multiIndex);
}

RowVectorXu UnstructuredMultiIndexFamily::GetFirstIndex()
{
    //just return a list of ones
    return RowVectorXu::Ones(this->lengthOfMultiIndex)*baseElement;
}

unsigned int UnstructuredMultiIndexFamily::DeriveNumberOfIndices()
{
    BOOST_LOG_POLY("multiIndex",error) << "Trying to derive the number of indices of adaptive indicies.";
    assert(false); //don't end up here!
    return 0;
}


RowVectorXu UnstructuredMultiIndexFamily::DeriveNextMultiIndex(const RowVectorXu& multiIndex)
{
    BOOST_LOG_POLY("multiIndex",error) << "Trying to derive the next adaptive index.";
    assert(false); //don't end up here!
    return RowVectorXu::Zero(1);
}


unsigned int UnstructuredMultiIndexFamily::AddMultiIndex(RowVectorXu const& multiIndex)
{
    return InsertMultiIndex(multiIndex);
}

UnstructuredMultiIndexFamily::Ptr UnstructuredMultiIndexFamily::CloneExisting(MultiIndexFamily::Ptr existing)
{
    RowVectorXu firstElement = existing->GetFirstIndex();
    //check that they're all the same or cloning won't work
    for(unsigned int i=1; i<firstElement.cols(); ++i)
    {
	assert(firstElement(0) == firstElement(i));
    }
    
    //create the clone, copy the first element
    UnstructuredMultiIndexFamily::Ptr clone = UnstructuredMultiIndexFamily::Create(existing->GetMultiIndexLength(), firstElement(0));
    
    //clone all the rest
    for(unsigned int i=0; i<existing->GetNumberOfIndices(); ++i)
    {
	clone->AddMultiIndex(existing->IndexToMulti(i));
    }
    
    return clone;
}

template<class Archive>
void UnstructuredMultiIndexFamily::serialize(Archive & ar, const unsigned int version)
{
    ar & boost::serialization::base_object<MultiIndexFamily>(*this);
    ar & baseElement;
}

BOOST_CLASS_EXPORT_IMPLEMENT (muq::GeneralUtilities::UnstructuredMultiIndexFamily)

template void UnstructuredMultiIndexFamily::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void UnstructuredMultiIndexFamily::serialize(boost::archive::text_iarchive & ar, const unsigned int version);