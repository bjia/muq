//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
/*
 * MultiIndexFamily.cpp
 *
 *  Created on: Jan 12, 2011
 *      Author: prconrad
 */

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexFamily.h"

#include <assert.h>
#include <map>


#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/functional/hash.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/export.hpp>

#include "MUQ/GeneralUtilities/multiIndex/MultiIndexWrapper.h"
#include "MUQ/GeneralUtilities/LogConfig.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

MultiIndexFamily::MultiIndexFamily()
{

}

MultiIndexFamily::MultiIndexFamily(unsigned int const length){
	assert(length>0); //check for non-zero length
	this->lengthOfMultiIndex = length;


	//set up the neighbor cache
	this->neighborCache = MatrixXu::Zero(length, length);
	for(unsigned int i=0; i<length; i++)
	{
		VectorXu oneNeighbor = VectorXu::Zero(length);
		oneNeighbor(length-i-1) = 1;
		neighborCache.row(i) = oneNeighbor;

	}
}

MultiIndexFamily::~MultiIndexFamily() {

}

unsigned int MultiIndexFamily::GetNumberOfIndices() const
{

	return multiIndexCache.size();
}

void MultiIndexFamily::init()
{
//	BOOST_LOG_POLY("multiIndex",fntrace) <<"Begin init";

	unsigned int numberOfIndices = this->DeriveNumberOfIndices();

	VectorXu multiIndex = this->GetFirstIndex(); //get the first multiindex
	InsertMultiIndex(multiIndex); //and put it in the set

	for(unsigned int i=1; i<numberOfIndices; i++) //then loop to compute the rest
	{
		multiIndex = this->DeriveNextMultiIndex(multiIndex); //compute the next one
		InsertMultiIndex(multiIndex); //and insert it into the set
	}

}

unsigned int MultiIndexFamily::GetMultiIndexLength() const
{
	return this->lengthOfMultiIndex;
}

RowVectorXu MultiIndexFamily::IndexToMulti(unsigned int const i) const
{
	//Search the multiIndexCache by indices to find i, which produces an iterator
	//of a type we pull from the bimap
	MultiIndexBimap::map_by<index>::const_iterator is
	        = multiIndexCache.by<index>().find(i);

	//assert that we didn't get the end iterator, which implies it isn't here
	assert(is != multiIndexCache.by<index>().end());

	//Since we did find it, get the MultiIndexWrapper out of the iterator,
	//and return its multiIndex
	return is->get<multiIndex>();
}

unsigned int MultiIndexFamily::MultiToIndex(RowVectorXu const& multiIndexToFind) const
{
	//Search the multiIndexCache by multiIndex to find the multiIndex, which produces an iterator
	//of a type we pull from the bimap. Note that we have to wrap the multiIndex in a wrapper to find it.
	MultiIndexBimap::map_by<multiIndex>::const_iterator is
	        = multiIndexCache.by<multiIndex>().find(multiIndexToFind);

	//assert that we didn't get the end iterator, which implies it isn't here
	assert(is != multiIndexCache.by<multiIndex>().end());

	//since we did find it, get the index out of the iterator
	return is->get<index>();
}

bool MultiIndexFamily::IsMultiInFamily(RowVectorXu const& multiIndexToFind) const
{
	//Search the multiIndexCache by multiIndex to find the multiIndex, which produces an iterator
	//of a type we pull from the bimap. Note that we have to wrap the multiIndex in a wrapper to find it.
	MultiIndexBimap::map_by<multiIndex>::const_iterator is
	        = multiIndexCache.by<multiIndex>().find(MultiIndexWrapper(multiIndexToFind));

	//return whether the multiIndex is here, by testing if the iterator points to the end
	return (is != multiIndexCache.by<multiIndex>().end());
}

bool MultiIndexFamily::IsIndexInFamily(unsigned int const indexToFind) const
{
	//Search the multiIndexCache by multiIndex to find the multiIndex, which produces an iterator
	//of a type we pull from the bimap. Note that we have to wrap the multiIndex in a wrapper to find it.
	MultiIndexBimap::map_by<index>::const_iterator is
	        = multiIndexCache.by<index>().find(indexToFind);

	//return whether the multiIndex is here, by testing if the iterator points to the end
	return (is != multiIndexCache.by<index>().end());
}

unsigned int MultiIndexFamily::DeriveNumberOfIndices()
{
	//start with the zero element
	RowVectorXu multiIndex = this->GetFirstIndex();
	unsigned int count = 0;

	//compute new ones until you run out
	while(true)
	{
		++count;
		RowVectorXu newIndex = this->DeriveNextMultiIndex(multiIndex);

		//if there's no rows
		if(!newIndex.cols())
			break;

		//otherwise, continue
		multiIndex = newIndex;
	}

	return count;
}

RowVectorXu MultiIndexFamily::GetFirstIndex(){
	//just return a list of zeros
	return RowVectorXu::Zero(this->lengthOfMultiIndex);
}

unsigned int MultiIndexFamily::InsertMultiIndex(RowVectorXu const& newMultiIndex)
{
	pair<MultiIndexBimap::iterator,bool> result = multiIndexCache.insert(IndexMultiPair(multiIndexCache.size(),  newMultiIndex));

	return result.first->left;
}

MatrixXu MultiIndexFamily::GetAllMultiIndices() const
{
	//allocate space
	MatrixXu result = MatrixXu(GetNumberOfIndices(), GetMultiIndexLength());

	for( MultiIndexBimap::const_iterator
			i = multiIndexCache.begin(),
			i_end = multiIndexCache.end();

			i != i_end ; ++i )
	{
		result.row(i->get<index>()) = i->get<multiIndex>();
	}
	return result;
}

VectorXu MultiIndexFamily::GetAllIndices() const
{
//allocate space
VectorXu result = VectorXu::Zero(GetNumberOfIndices());

for( MultiIndexBimap::const_iterator
		i = multiIndexCache.begin(),
		i_end = multiIndexCache.end();

		i != i_end ; ++i )
{
	result(i->get<index>()) = i->get<index>();
}
return result;
}

bool MultiIndexFamily::IsIndexBuried(unsigned int index) const
{
	//check that the index is valid
	assert(IsIndexInFamily(index));

	//get the multiIndex from the index
	RowVectorXu multiIndex = this->IndexToMulti(index);

	//loop over the forward neighbors
	for(unsigned int i=0; i< neighborCache.rows(); i++)
	{
		//compute the ith forward neighbor by adding
		RowVectorXu iNeighbor = multiIndex+this->neighborCache.row(i);

		if(!this->IsMultiInFamily(iNeighbor)) //if any are not in the family
		{
			return false; //not buried
		}
	}
	return true; //else is buried
}

VectorXd MultiIndexFamily::SelectNonBuriedIndices() const
{
	VectorXd result = VectorXd::Zero(GetNumberOfIndices());
	for(unsigned int i=0; i<result.rows(); i++)
		{
			if(!IsIndexBuried(i)) //if this index is not buried
			{
				result(i) = 1.0;
			}
		}
	return result;
}

template<class Archive>
void MultiIndexFamily::serialize(Archive & ar, const unsigned int version)
{
	ar & lengthOfMultiIndex;
	ar & neighborCache;
	ar & multiIndexCache;
}

// BOOST_CLASS_EXPORT(MultiIndexFamily)

template void MultiIndexFamily::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void MultiIndexFamily::serialize(boost::archive::text_iarchive & ar, const unsigned int version);


