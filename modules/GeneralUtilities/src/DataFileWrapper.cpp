/*MIT UQ Library
Copyright (C) 2012 Patrick R. Conrad

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.*/
#include "MUQ/GeneralUtilities/DataFileWrapper.h"

#include <assert.h>
#include <iostream>
#include <fstream>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <Eigen/Dense>

#include "MUQ/GeneralUtilities/PointWrapper.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;

DataFileWrapper::DataFileWrapper()
{
}

DataFileWrapper::DataFileWrapper(std::string filename, unsigned int inputDim,
        unsigned int outputDim) : CachedFunctionWrapper(inputDim, outputDim)
        
{
  //just load the cache
   this->LoadCache(filename);
}

boost::shared_ptr<DataFileWrapper> DataFileWrapper::WrapFile(std::string filename, unsigned int inputDim, unsigned int outputDim)
{
  //just call the constructor
  return boost::shared_ptr<DataFileWrapper>(new DataFileWrapper(filename, inputDim, outputDim));
}

DataFileWrapper::~DataFileWrapper() {

}

Eigen::MatrixXd DataFileWrapper::Evaluate(Eigen::MatrixXd const& x)
{
  //add the cols to the used cache
  for(unsigned i=0; i<x.cols(); ++i)
  {
     pointsUsedCache.insert(PointWrapper(x.col(i)));
  }
  
  //call the parent method
  return CachedFunctionWrapper::Evaluate(x);
}


void DataFileWrapper::ExtendCache(Eigen::MatrixXd const& newPoints)
{
 assert(false); 
}


unsigned int DataFileWrapper::GetNumOfEvals() const
{
    return pointsUsedCache.size();
}

void DataFileWrapper::ClearCache()
{
    pointsUsedCache.clear();
    ResetEvalsCount();
}


template<class Archive>
void DataFileWrapper::serialize(Archive & ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CachedFunctionWrapper>(*this);
}


template void DataFileWrapper::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void DataFileWrapper::serialize(boost::archive::text_iarchive & ar, const unsigned int version);