//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "MUQ/GeneralUtilities/ParallelFunctionWrapper.h"


#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "MUQ/GeneralUtilities/EigenUtils.h"
#include "MUQ/GeneralUtilities/ParallelFunctionWrapperImpl.h"

using namespace std;
using namespace Eigen;
using namespace muq::GeneralUtilities;


ParallelFunctionWrapper::Ptr ParallelFunctionWrapper::WrapVectorFunction(std::function<Eigen::VectorXd (Eigen::VectorXd const&)> vectorFn, unsigned const inputDim, unsigned const outputDim)
{
  using namespace std::placeholders;
  
  //use ApplyVectorFunction to create a batch version of this function, leave the rest to the other construct
  return WrapBatchVectorFunction(bind(ApplyVectorFunction, vectorFn, outputDim, _1), inputDim, outputDim);
}

ParallelFunctionWrapper::Ptr ParallelFunctionWrapper::WrapBatchVectorFunction(std::function<Eigen::MatrixXd (Eigen::MatrixXd const&)> batchVectorFn, unsigned const inputDim, unsigned const outputDim)
{
  return ParallelFunctionWrapper::Ptr(new ParallelFunctionWrapper(batchVectorFn, inputDim, outputDim));
}

ParallelFunctionWrapper::Ptr ParallelFunctionWrapper::WrapExecutableFunction(std::string const& executable, unsigned const inputDim, unsigned const outputDim)
{
    using namespace std::placeholders;
  
  //bind the executable string into ApplyExecutableFunction to make a batch function
  return WrapBatchVectorFunction(bind(ApplyExecutableFunction, executable, outputDim, _1), inputDim, outputDim);
}

ParallelFunctionWrapper::ParallelFunctionWrapper(boost::function<MatrixXd (MatrixXd const&)> fn, 
			unsigned int inputDim, unsigned int outputDim):
			CachedFunctionWrapper(fn, inputDim, outputDim)
{
 
}



      
void ParallelFunctionWrapper::InitializeWorkers()
{
  InitializeWorkersImpl(batchFn);
}
  
      
void ParallelFunctionWrapper::CloseWorkers()
{
 CloseWorkersImpl();
}
      
void ParallelFunctionWrapper::ExtendCache(Eigen::MatrixXd const& newPoints)
{
  ExtendCacheImpl(newPoints, *this);
}

      
template<class Archive>
void ParallelFunctionWrapper::serialize(Archive & ar, const unsigned int version)
{
  //just do the base object
  ar & boost::serialization::base_object<CachedFunctionWrapper>(*this);
  //the communicator is correctly reconstructed by the default constructor, so we may ignore it here
}

BOOST_CLASS_EXPORT(muq::GeneralUtilities::ParallelFunctionWrapper)

template void muq::GeneralUtilities::ParallelFunctionWrapper::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
template void muq::GeneralUtilities::ParallelFunctionWrapper::serialize(boost::archive::text_iarchive & ar, const unsigned int version);
